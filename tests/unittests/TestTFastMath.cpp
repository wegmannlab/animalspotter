//
// Created by beat on 13.06.18.
//

#include "../TestCase.h"
#include "../../core/TFastMath.h"

TEST_CASE( "2: Test fast log calculation", "[multi-file:3]" ) {
    TFastMath fMath(1000);

    for(int i = 0; i < 999; i++){
        double val = i * 0.001;
        REQUIRE( fMath.Log(val) == Approx(std::log(val)));
    }

}