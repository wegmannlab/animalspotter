//
// Created by beat on 12.06.18.
//


#include "../TestCase.h"
#include "../../core/Sigma.h"
#include "../../core/TAlgorithm.h"
#include "../../core/TMemoryHelper.h"

TEST_CASE( "1: Test matrix determinant (for symetric matrices only)", "[multi-file:2]" ) {
    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 1;
    matrix[1][0] = 2;
    matrix[2][0] = 3;

    matrix[0][1] = 2;
    matrix[1][1] = 5;
    matrix[2][1] = 3;

    matrix[0][2] = 3;
    matrix[1][2] = 3;
    matrix[2][2] = 7;

    REQUIRE(determinantSymetric(matrix) == -11);

    matrix[0][0] = 1;
    matrix[1][0] = 2;
    matrix[2][0] = 3;

    matrix[0][1] = 2;
    matrix[1][1] = 5;
    matrix[2][1] = 4;

    matrix[0][2] = 3;
    matrix[1][2] = 4;
    matrix[2][2] = 7;

    REQUIRE(determinantSymetric(matrix) == -6);
}

TEST_CASE( "2: Test matrix inversion", "[multi-file:2]"){
    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 1;
    matrix[1][0] = 2;
    matrix[2][0] = 3;

    matrix[0][1] = 2;
    matrix[1][1] = 5;
    matrix[2][1] = 4;

    matrix[0][2] = 3;
    matrix[1][2] = 4;
    matrix[2][2] = 7;

    double **inv = inverse(matrix);

    REQUIRE(inv[0][0] == Approx(-3.1666667));
    REQUIRE(inv[1][0] == Approx(0.3333333));
    REQUIRE(inv[2][0] == Approx(1.1666667));

    REQUIRE(inv[0][1] == Approx(0.3333333));
    REQUIRE(inv[1][1] == Approx(0.3333333));
    REQUIRE(inv[2][1] == Approx(-0.3333333));

    REQUIRE(inv[0][2] == Approx(1.1666667));
    REQUIRE(inv[1][2] == Approx(-0.3333333));
    REQUIRE(inv[2][2] == Approx(-0.1666667));
}

TEST_CASE( "3: Test real matrix", "[multi-file:2]"){

    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 0.00398244;
    matrix[1][0] = 0.00494968;
    matrix[2][0] = 0.00254704;

    matrix[0][1] = 0.00494968;
    matrix[1][1] = 0.0110882;
    matrix[2][1] = 0.00458836;

    matrix[0][2] = 0.00254704;
    matrix[1][2] = 0.00458836;
    matrix[2][2] = 0.00479751;

    Sigma sigma(CreateLUT(), matrix);

    double l = sigma.GetL();

    REQUIRE( 24.346083 == Approx(sigma.GetL(0, 0) * l));
    REQUIRE( 12.21706 == Approx(sigma.GetL(1, 1) * l));
    REQUIRE( 14.4375 == Approx(sigma.GetL(2, 2) * l));

    REQUIRE( -9.134242 == Approx(sigma.GetL(0, 1) * l));
    REQUIRE( -4.189519 == Approx(sigma.GetL(0, 2) * l));
    REQUIRE( -11.68445 == Approx(sigma.GetL(1, 2) * l));

    unsigned char color[3] = {150, 150, 150};
    double mean[3] = {0.4, 0.89, 0.33};

    double density = sigma.LogDensity(color, mean);
    std::cout << density << std::endl;

    REQUIRE( density == Approx(-43.39062));
}


TEST_CASE( "4: Test singular matrix", "[multi-file:2]"){

    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 9.10534e-06;
    matrix[1][0] = 9.10534e-06;
    matrix[2][0] = 9.10534e-06;

    matrix[0][1] = 9.10534e-06;
    matrix[1][1] = 9.10534e-06;
    matrix[2][1] = 9.10534e-06;

    matrix[0][2] = 9.10534e-06;
    matrix[1][2] = 9.10534e-06;
    matrix[2][2] = 9.10534e-06;

    Sigma sigma(CreateLUT(), matrix);

    REQUIRE( sigma.GetL() == sigma.GetL());
}

TEST_CASE( "5: Test almost singular matrix", "[multi-file:2]"){

    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 1.92234e-05;
    matrix[1][0] = 1.53787e-05;
    matrix[2][0] = 1.66603e-05;

    matrix[0][1] = 2.09321e-05;
    matrix[1][1] = 2.30681e-05;
    matrix[2][1] = 1.53787e-05;

    matrix[0][2] = 2.30681e-05;
    matrix[1][2] = 2.69127e-05;
    matrix[2][2] = 1.92234e-05;

    Sigma sigma(CreateLUT(), matrix);

    REQUIRE( sigma.GetL() == sigma.GetL());
}


TEST_CASE( "6: Test matrix sigma extraction", "[multi-file:2]" ) {
    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 0.00398244;
    matrix[1][0] = 0.00494968;
    matrix[2][0] = 0.00254704;

    matrix[0][1] = 0.00494968;
    matrix[1][1] = 0.0110882;
    matrix[2][1] = 0.00458836;

    matrix[0][2] = 0.00254704;
    matrix[1][2] = 0.00458836;
    matrix[2][2] = 0.00479751;

    Sigma sigma(CreateLUT(), matrix);

    double **orig = sigma.GetSigma();

    for(int x = 0; x < 3; x++){
        for(int y = 0; y < 3; y++){
            Approx(orig[x][y] == matrix[x][y]);
        }
    }
}


TEST_CASE( "7: Test setting l manually", "[multi-file:2]" ) {
    double ** matrix;

    mem::InitializeArray2d(&matrix, 3, 3);
    matrix[0][0] = 0.00398244;
    matrix[1][0] = 0.00494968;
    matrix[2][0] = 0.00254704;

    matrix[0][1] = 0.00494968;
    matrix[1][1] = 0.0110882;
    matrix[2][1] = 0.00458836;

    matrix[0][2] = 0.00254704;
    matrix[1][2] = 0.00458836;
    matrix[2][2] = 0.00479751;

    Sigma sigma(CreateLUT(), matrix);

    REQUIRE(sigma.GetLChanges() == 0);
    REQUIRE(sigma.GetLChanges(0,0) == 0);
    REQUIRE(sigma.GetLChanges(1,1) == 0);
    REQUIRE(sigma.GetLChanges(2,2) == 0);
    REQUIRE(sigma.GetLChanges(0,1) == 0);
    REQUIRE(sigma.GetLChanges(0,2) == 0);
    REQUIRE(sigma.GetLChanges(1,2) == 0);

    sigma.SetL(0,0, 2);
    Approx(2 == sigma.GetL(0,0));
    REQUIRE(sigma.GetLChanges(0,0) == 1);
}
