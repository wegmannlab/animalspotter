//
// Created by asraniel on 14.06.18.
//

#include "../TestCase.h"
#include "../../core/TAlgorithm.h"

TEST_CASE( "1: Test basic initialization", "[multi-file:3]" ) {

    double standardDeviations[6];
    standardDeviations[0] = 0.1;
    standardDeviations[1] = 0.1;
    standardDeviations[2] = 0.1;
    standardDeviations[3] = 0.1;
    standardDeviations[4] = 0.1;
    standardDeviations[5] = 0.1;

    TAlgorithm* algo = new TAlgorithm(1.0, 0, 0,0,
                                      standardDeviations, 1234, 0.1, 1);

    unsigned char data[2 * 2 * 3] = {255, 255, 255,
                           255, 255, 255,
                           255, 255, 255,
                           255, 255, 255};

    std::vector<TImage *> images;

    images.push_back(new TImage("1", data, 3, 4, 3, 4));

    images.push_back(new TImage("2", data, 3, 4, 3, 4));

    images.push_back(new TImage("3", data, 3, 4, 3, 4));

    algo->SetImages(images);

    REQUIRE( algo->GetImageCount() == 3);

    algo->InitializeMatChain(NULL);
}