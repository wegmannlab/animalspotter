//
// Created by beat on 18.09.18.
//



#include "../TestCase.h"

#include "../../core/TBoundingBox.h"

TEST_CASE( "1: Test overlaping bounding box test", "[multi-file:5]") {
    TBoundingBox boxA(10, 10, 10, 10);
    TBoundingBox boxB(10, 10, 10, 10);

    REQUIRE(boxA.isOverlapping(boxB));

    TBoundingBox boxC(47, 230, 233 - 47, 370 - 230);
    TBoundingBox boxD(55, 382, 241 - 55, 522 - 382);

    REQUIRE(!boxC.isOverlapping(boxD));
}