# README #

### What is this repository for? ###

### How do I get set up? ###
For the initial checkout use the following command:

```
git clone --recurse-submodules git@bitbucket.org:WegmannLab/animalspotter.git
```

To update the code use the following command:
```
git pull --recurse-submodules
```

If you already use a branch that had a submodule added after you checked it out use:
```
git submodule update --init --recursive
```
To get the latest submodule.

### How do i compile it? ###
Simply compile the project using cmake, like in the script below.

```
mkdir build
cd build
cmake ..
make
```

### How do i run it? ###
Animalspotter can be run directly using the animalspotter executable produced by the compilator.
Alternatively one of the launcher scripts can be used in the scripts folder. 

### Development ###

The code integrates the gperftools profiler.
To activate the profiler, simply enable the corresponding section in the cmakelists.txt file.
During execution the code generates a profile.prof file.
This can be analysed further using the google-pprof tools, with the easiest analysing being the creation of a PDF.
```
google-pprof --pdf ./animalspotter profile.prof > profiling.pdf
```

### Dependencies ###
Animalspotter has four external dependecies, jpeg_decoder, date and easyexif.

Jpeg_decoder, easyexif and date have been included directly in this repository in the libs folder.

