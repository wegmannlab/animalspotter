library(ggplot2)

folder<-"/media/beat/NO_NAME/current/"

inputFiles = files<-list.files(path = folder, pattern = "^(logImage)", recursive = TRUE, full.names = TRUE)

for(input in inputFiles){
  if(!endsWith(input, ".pdf")){
    pdf(paste(input, ".pdf", sep="") ,width = 10, height = 10);
    
    data<-read.table(input, header=TRUE, sep = "\t")
    
    layout(matrix(c(1,2,3,4,5,5,6,6,6,7,7,7), 4,3, byrow = TRUE))
    par(mar=c(5.5,6,4,1.5),mgp=c(3.5,1.5,0))
    
    matplot(data[, c("r.dist", "g.dist", "b.dist")], main="Dist.", type = c("l"), lty=1,pch=1,col = c("red", "green", "blue")) #plot
    matplot(data[, c("r.mean.FG", "g.mean.FG", "b.mean.FG")], main="FG mean", lty=1, type = c("l"),pch=1,col = c("red", "green", "blue")) #plot
    matplot(data[, c("FG")], main="FG pixels", type = c("l"), pch=1, lty=1,col = c("black")) #plot
    
    matplot(data[, c("l.Acceptance", "mean.Acceptance", "dist.Acceptance")], lty=1, main="Acceptance (l,mean,dist)", type = c("l"),pch=1, ylim=c(0,1),col = c("red", "green", "blue")) #plot
    matplot(data[, c("l.FG")], main="l FG", type = c("l"),pch=1) #plot
    matplot(data[, c("l11.FG", "l22.FG", "l33.FG", "l21.FG", "l31.FG", "l32.FG")], main="L FG", type = c("l"),pch=1, lty=1, col = c("red", "green", "blue", "yellow", "darkmagenta", "black")) #plot
    matplot(data[, c("s11.FG", "s22.FG", "s33.FG", "s21.FG", "s31.FG", "s32.FG")], main="Sigma FG", type = c("l"), ylim=c(-.1,0.1), pch=1, lty=1, col = c("red", "green", "blue", "yellow", "darkmagenta", "black")) #plot
    
    dev.off()
  }
  
}

inputFiles = files<-list.files(path = folder, pattern = "^(logPixel)", recursive = TRUE, full.names = TRUE)

for(input in inputFiles){
  if(!endsWith(input, ".pdf")){
    pdf(paste(input, ".pdf", sep="") ,width = 10, height = 10);
    
    data<-read.table(input, header=TRUE, sep = "\t")
    
    layout(matrix(c(1,2,3, 4,4,4, 5,5,5, 6,6, 6), 4,3, byrow = TRUE))
    par(mar=c(5.5,6,4,1.5),mgp=c(3.5,1.5,0))
    
    matplot(data[, c("r.mean.BG", "g.mean.BG", "b.mean.BG")], main="BG mean", lty=1, type = c("l"),pch=1,col = c("red", "green", "blue")) #plot
    matplot(data[, c("FG")], main="BG pixels", type = c("l"), lty=1, pch=1,col = c("black")) #plot
    matplot(data[, c("l.Acceptance", "mean.Acceptance")], main="Acceptance (l,mean)", lty=1, type = c("l"),pch=1, ylim=c(0,1),col = c("red", "green", "blue")) #plot
    
    matplot(data[, c("l.BG")], main="l BG", type = c("l"),pch=1) #plot
    matplot(data[, c("l11.BG", "l22.BG", "l33.BG", "l21.BG", "l31.BG", "l32.BG")], main="L BG", type = c("l"),pch=1, lty=1, col = c("red", "green", "blue", "yellow", "darkmagenta", "black")) #plot
    matplot(data[, c("s11.BG", "s22.BG", "s33.BG", "s21.BG", "s31.BG", "s32.BG")], main="Sigma BG", ylim=c(-0.1,0.1), type = c("l"),pch=1, lty=1,col = c("red", "green", "blue", "yellow", "darkmagenta", "black")) #plot
    
    dev.off()
  }
}