
#include <regex>

#include "TAnimalSpotter.h"
#include "core/Sigma.h"
#include "core/TImageLoader.h"
#include "core/TImageSplitter.h"

#ifdef GPERFTOOLS_PROF

#include <gperftools/profiler.h>

#endif

#ifdef __GNUC__
    # if __GNUC__ < 5
        #define OLD_GCC
    #endif
#endif

#ifndef OLD_GCC
std::vector<std::string> Split(const std::string &subject) {
    static const std::regex re{":"};

    std::vector<std::string> container{
            std::sregex_token_iterator(subject.begin(), subject.end(), re, -1),
            std::sregex_token_iterator()};

    return container;
}
#endif

int main(int argc, char **argv) {

    // Get the current time as a string
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%Y_%m_%d_%I_%M_%S", timeinfo);
    std::string dateTime(buffer);

    cli::Parser parser(argc, argv);

    parser.set_required<std::string>(
            "i", "input",
            "A path to a directory containing the input images in a JPEG format.");
#ifndef OLD_GCC
    parser.set_optional<std::string>(
            "n", "input", "",
            "Optional list of image names to be analysed in that folder. Image names "
            "are separated by ;");
#endif

    parser.set_optional<std::string>(
            "o", "output", "",
            "A path to a directory to which the output RAW images will be written. "
            "Same as input directory if not set.");
    parser.set_optional<std::string>(
            "l", "log", "",
            "A path to a directory to which the output data will be written. Same as "
            "output (or input) directory if not set.");
    parser.set_optional<std::string>(
            "t", "tag", dateTime,
            "A tag to be prepended to the output RAW image files. If no value is "
            "provided, a date-time string will be written.");
    parser.set_optional<unsigned int>(
            "r", "imagereduction", 1,
            "Reduce the size of the original image by this factor.");

    parser.set_optional<double>("he", "heuristic", 0.15,
                                "Percentage of each image that should start as "
                                "foreground based on a heuristic");

    parser.set_optional<unsigned int>("c", "chainlength", 500,
                                      "The length of the MCMC chain.");
    parser.set_optional<unsigned int>("b", "burnin", 50,
                                      "The size of each burnin of the MCMC chain.");
    parser.set_optional<unsigned int>("b_nr", "burnin_nr", 5, "The nr of burnins that should be performed.");
    parser.set_optional<double>(
            "a", "alpha", 1.15, "Ising alpha used for neighbor pixel correlation within img");
    parser.set_optional<double>("beta", "beta", 1.15, "Ising beta used for neighbor pixel correlation among subsequent imgs with time diff <5s");
    // pixel correlation based on similarity of pixels");
    parser.set_optional<double>("p", "prior", 0.05, "Prior");

    parser.set_optional<double>(
            "u", "sdbgmeans", 0.01,
            "The standard deviation for the background means.");
    parser.set_optional<double>(
            "im", "sdidmeans", 0.01,
            "The standard deviation for the image distortions");
    parser.set_optional<double>(
            "x", "sdfgmeans", 0.05,
            "The standard deviation for the foreground means.");

    parser.set_optional<double>(
            "w", "sdbgsigma", 0.2,
            "The standard deviation for the background sigma.");
    parser.set_optional<double>(
            "y", "sdfgsigma", 0.3, "The standard deviation for the foreground sigma");

    parser.set_optional<int>("td", "maxbindiff", 10 * 60,
                             "Maximum difference in seconds between two pictures "
                             "to be in the same bin");
    parser.set_optional<int>(
            "mb", "minBinSize", 3,
            "Minimum amount of images to be put together in a bin to be analysed");

    parser.set_optional<bool>(
            "dsm", "discardSmallBins", false,
            "If set to true, discard bins smaller than minBinSize");

    parser.set_optional<double>(
            "gn", "minGrayscaleNight", 0.0,
            "Minimum percentage of pixels in grayscale for image to be considered a "
            "night picture, use 0 to disable this option");

    parser.set_optional<unsigned int>(
            "s", "randomseed", 123456789,
            "The seed of the random uniform number generator.");
    parser.set_optional<unsigned int>("th", "numthreads", 2,
                                      "Set the number of parallel CPU threads.");
    parser.set_optional<bool>("v", "verbose", false,
                              "Set to verbose to have more information");

    parser.set_optional<bool>("di", "debugimage", false,
                              "Set to verbose to have more information");
    parser.set_optional<bool>("bb", "bounding-boxes", false,
                              "Calculate bounding boxes");
    parser.set_optional<int>("dp", "debugpixel", -1,
                             "Set to verbose to have more information");
    parser.set_optional<std::string>("bc", "color", "255,69,0",
                                     "Set the color of the output box");
    parser.set_optional<double>(
            "mpt", "min-box-prob", 0.51,
            "Minimum probability for a pixel to be considered foreground in the "
            "bounding box algorithm");

    parser.set_optional<float>(
            "pfd", "pxl-frac-dist", 1,
            "Smallest x (a fraction) pxl-wise distance that should be considered for merging bins ");

    parser.set_optional<int>(
            "hmp", "how-many-pxl-used", 5,
            "How maniest pxl should be used to calculate a rough distance to get candidates for merging");

    parser.set_optional<bool>("dn", "daynightonly", false,"If set to true, only split images into night and day images, don't really run the AS");
    // TODO: By Max: Make parser cmd to write optionally the log files out.
    // parser.set_optional<bool>("p", "pixellog", false, "The Pixel values will be
    // printed out.");

    parser.run_and_exit_if_error();

    std::string input = parser.get<std::string>("i");
#ifndef OLD_GCC
    std::string names = parser.get<std::string>("n");
#endif

    std::string output =
            parser.get<std::string>("o") == "" ? input : parser.get<std::string>("o");
    std::string logg = parser.get<std::string>("l") == ""
                       ? output
                       : parser.get<std::string>("l");
    std::string tag = parser.get<std::string>("t");

    unsigned int length = parser.get<unsigned int>("c");
    unsigned int burnin = parser.get<unsigned int>("b");
    unsigned int burnin_nr=parser.get<unsigned int>("b_nr");
    unsigned int reduction = parser.get<unsigned int>("r");
    double isingAlpha = parser.get<double>("a");
    double isingBeta = parser.get<double>("beta");
    double prior = parser.get<double>("p");
    double heuristic = parser.get<double>("he");
    int maxTimeDifference = parser.get<int>("td");
    int minBinSize = parser.get<int>("mb");
    double minGrayscaleForNight = parser.get<double>("gn");
    bool verbose = parser.get<bool>("v");
    bool discardSmallBins = parser.get<bool>("dsm");

    bool debugImages = parser.get<bool>("di");
    bool calculateBoundingBoxes = parser.get<bool>("bb");
    int debugPixel = parser.get<int>("dp");
    float pxlFractionDist=parser.get<float>("pfd");
    int howManiestPxl=parser.get<int>("hmp");
    bool splitDayNight = parser.get<bool>("dn");

    double standardDeviations[6];
    standardDeviations[0] = parser.get<double>("u");
    standardDeviations[1] = parser.get<double>("im");
    standardDeviations[2] = parser.get<double>("w");
    standardDeviations[3] = parser.get<double>("x");
    standardDeviations[4] = parser.get<double>("y");

    double min_foreground_prob = parser.get<double>("mpt");

    unsigned char boxColors[3]; //an unsigned char can store values 0 to 255

    std::vector<int> colorVector;
    std::stringstream ss(parser.get<std::string>("bc")); //the color of the bounding box, default=255,69,0 (in rgb, see above)

    int i;

    while (ss >> i) {
        colorVector.push_back(i); // create a new element at the end of the colorVector holding i

        if (ss.peek() == ',') { // if the next char in ss is a comma
            ss.ignore(); // ignore the char
        }
    }

    if (colorVector.size() == 3) {
        for (int i = 0; i < 3; i++) {
            boxColors[i] = colorVector[i];
        }
    }

    // TODO: change Random number. just two inputs
    unsigned int seed = parser.get<unsigned int>("s");

    unsigned int numthreads = parser.get<unsigned int>("th");

    if (verbose) {
        std::cout << "Using OpenMP with " << numthreads << " threads" << std::endl;
    }

    char outputPath[1024]; //array with 1024 positions reserved for it
    strcpy(outputPath, output.c_str()); //copies the src string called output (right) to the dest str (left), replacing the dest str
    //dona problem
    //daniel? Why do I have to change this? It was already read as a string above?

    char logPath[1024];
    strcpy(logPath, logg.c_str());

    char prependTag[1024];
    strcpy(prependTag, tag.c_str());

#ifdef GPERFTOOLS_PROF
    ProfilerStart("profile.prof");
#endif

    TAlgorithm *algo = new TAlgorithm(1.0, length, burnin, burnin_nr, standardDeviations,
                                      seed, prior, numthreads);
    //dona problem: why are some parameters given to the constructor and directly used to set the objects var while
    //others are set below through these setters?
    //I think we would have an entry for every parameter, as there are defaults everywhere, no?

    algo->SetOutputBoundingBoxes(calculateBoundingBoxes); //calculateBoundingBoxes is a bool

    algo->SetVerbose(verbose);

    algo->SetLogPath(logPath);

    algo->SetHeuristic(heuristic);//

    algo->SetNeighborAlpha(isingAlpha);//

    algo->SetNeighborBeta(isingBeta);//

    algo->SetOutputPath(outputPath);//

    algo->SetDebugImages(debugImages);//

    algo->SetDebugPixel(debugPixel);//

    algo->SetOutputBoxColor(boxColors);

    algo->SetMinOutputBoxProbability(min_foreground_prob); //

    std::vector<std::string> imageNames; //create a vector storing strings called imageNames
#ifndef OLD_GCC
    if (!names.empty()) { //if we gave an image list as input
        imageNames = Split(names);
    }
#endif

    auto start = std::chrono::system_clock::now(); //store actual time

    std::vector<TImage *> images =
            TImageLoader::LoadImages(output, input, imageNames, reduction);

    if (images.size() == 0) {
        std::cout << "No Images loaded" << std::endl;
    }

    if (verbose) {
        auto end = std::chrono::system_clock::now();
        auto elapsed =
                std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Loaded " << images.size() << " images in " << elapsed.count() <<"ms"
                  << std::endl;
    }

    start = std::chrono::system_clock::now();

    TImageSplitter splitter(maxTimeDifference, minBinSize, minGrayscaleForNight, splitDayNight, pxlFractionDist, howManiestPxl);
    splitter.SetVerbose(verbose);
    splitter.SetDiscardSmallBins(discardSmallBins);
    splitter.SetOutputPath(outputPath);

    std::vector<std::vector<TImage *>> imageBins = splitter.SplitImages(images);
    if (!splitDayNight){
        if (verbose) {
            auto end = std::chrono::system_clock::now();
            auto elapsed =
                    std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

            std::cout << "Split images in " << elapsed.count() << " ms into "
                      << imageBins.size() << " bins" << std::endl;
        }

        auto startBins = std::chrono::system_clock::now();
        for (int binID = 0; binID < imageBins.size(); binID++) {
            auto start = std::chrono::system_clock::now();

            char binTag[1024];
            sprintf(binTag, "%s(bin%d)", prependTag, binID); //writes the string pointed to by format to buffer.
            // The format specifiers starting with % which are replaced by the values of variables that are passed to the sprintf() function as additional arguments.

            if (verbose) {
                std::cout << "Bin " << binID << " has " << imageBins.at(binID).size()
                          << " images" << std::endl;
            }

            algo->SetImages(imageBins.at(binID));

            algo->InitializeMatChain(binTag);
            if (verbose) {
                std::cout<<"Chain initialized"<<std::endl;
            }

            if (!algo->MakeMCMC(binTag)) { //if possible, mcmc is done
                std::cout << "No MCMC done" << std::endl;
            }

            if (!algo->SaveOutput(outputPath, binTag)) { //if output is there, it is saved
                std::cout << "No Output Generated" << std::endl;
            }

            if (verbose) {
                auto end = std::chrono::system_clock::now();
                auto elapsed =
                        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

                std::cout << "Finished bin " << binID << " in " << elapsed.count() <<" ms"
                          << std::endl;
            }

            for (TImage *image : imageBins[binID]) {
                delete image;
            }
        }

        if (verbose) {
            auto end = std::chrono::system_clock::now();
            auto elapsed =
                    std::chrono::duration_cast<std::chrono::milliseconds>(end - startBins);

            std::cout << "Finished calculations in " << elapsed.count() << "ms"<<std::endl;
        }
    }
    else { //ie if splitDayNight=True we did not delete the images yet
        for (TImage *image : images) {
            delete image;
        }
    }




#ifdef GPERFTOOLS_PROF
    ProfilerStop();
#endif

    // algo->SaveAverage("average.rgb");
    // algo->SaveStandardDeviation("sd.rgb");
    delete algo;

    return 0;
}
