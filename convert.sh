for file in Debug/*.rgb
do
  filename=$(basename "$file")
  extension="${filename##*.}"
  filename="${filename%.*}"
  convert -size "652x489" -depth 8 "$file" "Debug/${filename}.png"
  rm "$file"
done
