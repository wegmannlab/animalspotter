'''This script should be run from the command line. It is used to optimize the parameters given within the created experiments.
For further information, see the SigOpt website.
Please note that running this script will use some of the monthly SigOpt budget.
Also note, that the same can be done parallelized; if you wish to parallelize the optimization, please use the
master_sigopt_parallel_*.py scripts. (In the latter, the results of the best parameter run are not automatically generated, though;
use the script AS_opt_sigopt_best.py with the detected best parameters to get them)
At last, please note that the parameter parallel within this script is nevertheless set to True; if you are absolutely sure, that
no other script is using the same gt data, you can change it to False to increase speed (then, gt data will be moved forth and back during the accuracy
evaluations, instead of copying them)'''
from sigopt import Connection
import AS_evaluation
import argparse
from pathlib import Path
import json
from rescaling import rescale

conn = Connection(client_token="CQRRDMSHOYYMIQYMKKLBKACDNQJAMWHPGRXCQQNRARFMHLNR")

def evaluate_model(assignments, best_parameter_run):
    return AS_evaluation.main(assignments, best_parameter_run, '', count=100, parallel=True)
    #in case you are absolutely certain that you are the only one using the gt files at the moment, you can change parallel to False
    # (then they'll be moved during the evaluation; in the end they'll be moved back)

def main(static_input):
    best_parameter_run=False
    if static_input['iou']:
        static_input['Euclidean']=False
    else:
        static_input['Euclidean']=True
    #add analysis folder
    static_input['a']=static_input['o']+'/analysis/'
    #add burnin specification: use only the last 500-1000 iterations if there are enough mcmc steps
    if static_input['chainlength']>1150:
        static_input['b_nr']=4 #burnin nr
        static_input['burnin']=(static_input['chainlength']-750)/4
    elif static_input['chainlength']>700:
        static_input['b_nr']=4
        static_input['burnin']=(static_input['chainlength']-300)/4
    else:
        static_input['b_nr']=1 #to short mcmc for multiple burnins
        static_input['burnin']=int(static_input['chainlength']/2)

    if static_input['reduce']!=1:
        rescale(static_input)


    experiment = conn.experiments().create(
        name='AS_optimization',
        # Define which parameters you would like to tune
        parameters=[
        dict(name='heuristic', type='double', bounds=dict(min=0.05, max=0.3)),
        dict(name='alpha', type='double', bounds=dict(min=0.5, max=2.0)),
        dict(name='min_bin_size', type='int', bounds=dict(min=3, max=50)),
        dict(name='maxbindiff', type='int', bounds=dict(min=10, max=1800)),
        #dict(name='magick_contrast_stretching_min', type='int', bounds=dict(min=0, max=15)),
        #dict(name='magick_contrast_stretching_max', type='int', bounds=dict(min=0, max=15)),
        ],
        metrics=[dict(name='function_value', objective='maximize')],
        parallel_bandwidth=1,
        # Define an Observation Budget for your experiment (recommended: 10-20X nr parameters; this is often enough for convergence)
        observation_budget=3,
        project="as_optimization",
    )
    print("Created experiment: https://app.sigopt.com/as_optimization/" + experiment.id)

    # Evaluate your model with the suggested parameter assignments
    print('Starting optimization loop...')
    count=0
    # Run the Optimization Loop until the Observation Budget is exhausted
    while experiment.progress.observation_count < experiment.observation_budget:
        suggestion = conn.experiments(experiment.id).suggestions().create()
        assignments = str(suggestion.assignments).split('(')[1].split(')')[0]
        assignments = json.loads(assignments)
        static_input.update(assignments)
        input = static_input
        value = evaluate_model(input, best_parameter_run)
        conn.experiments(experiment.id).observations().create(
            suggestion=suggestion.id,
            value=value,
        )

        # Update the experiment object
        experiment = conn.experiments(experiment.id).fetch()
        count+=1
        print('optimization loop nr', count, 'finished')

    print('Observation budget exhausted')
    # Fetch the best configuration and explore your experiment
    all_best_assignments = conn.experiments(experiment.id).best_assignments().fetch()
    # Returns a list of dict-like Observation objects
    best_assignments = all_best_assignments.data[0].assignments

    best_parameter_run=True
    static_input.update(best_assignments)
    input = static_input
    value=evaluate_model(input, best_parameter_run)
    print('Highest accuracy found is', value)
    print("Best Assignments: " + str(best_assignments))
    print("Best heuristics value: " + str(best_assignments['heuristic']))
    print("Best alpha value: " + str(best_assignments['alpha']))
    print("Best minimum bin size: " + str(best_assignments['min_bin_size']))
    # print("Best to discard smaller bins: True(1) or False(0): " + str(best_assignments['discard_very_small_bins']))
    print("Recommended max number of seconds between 2 pictures to possibly be in same bin: " + str(
        best_assignments['maxbindiff']))
    #print("Recommended image contrast stretching using ImageMagick with: " + str(
        #best_assignments['magick_contrast_stretching_min'])+'and'+str(best_assignments['magick_contrast_stretching_max']))
    print("Explore your experiment: https://app.sigopt.com/as_optimization/" + experiment.id + "/analysis")

    print('Now you can have a look at the worst and best AS outputs (directories \' worst \' and \' best \' in the directory', input['path_to_sigopt_output'],'to see which images are problematic')

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--path_to_truth_mask',
                        help='Path to where the dataset is located in Darwin format',
                        required=True,
                        type=str)
    parser.add_argument('-i',
                        help='Path to the folder which contains the animal spotter input',
                        required=True,
                        type=str)
    parser.add_argument('-o',
                        help='Path to a temporary folder where AS output goes, no one cares',
                        required=True,
                        type=str)
    parser.add_argument('--path_to_sigopt_output',
                        help='Path to not yet existent folder',
                        required=True,
                        type=str)
    parser.add_argument('--binary',
                        help='Path to animalspotter executable',
                        required=True,
                        type=str)
    parser.add_argument('--chainlength',
                        help='Length of the mcmc',
                        required=True,
                        type=int)
    parser.add_argument('--iou',
                        help='Score to evaluate accuracy is the iou; if False, score to evaluate is euclidean dist',
                        required=True,
                        type=bool)
    parser.add_argument('-reduce',
                        help='Factor by which the imgs are reduced --> AS will run faster',
                        required=True,
                        type=int)


    args = parser.parse_args()

    # run the actual code
    main(vars(args))


#command line command: python3.8 AS_opt_sigopt.py --path_to_truth_mask /data/darwin/annotations/Dona_polygons/ -i /data/darwin/sigopt_pics/ -o /data/darwin/sigopt_pics/AS_output_waste/ --path_to_sigopt_output /data/darwin/sigopt_AS_output_waste --chainlength 10 --binary ~/git/animalspotter/build/animalspotter