import socket
from sigopt import Connection
import AS_evaluation
import json


def evaluate_model(assignments, worker_nr, count, best_parameter_run):
  return AS_evaluation.main(assignments, best_parameter_run, worker_nr, count, parallel=True)

# Each worker runs the same optimization loop
# for the experiment created on master
def run_worker(api_token, experiment_id, static_input, worker_nr):
  # Create the SigOpt connection
  conn = Connection(client_token=api_token) #deleted args.????

  # Keep track of the hostname for logging purposes
  hostname = socket.gethostname()

  experiment = conn.experiments(experiment_id).fetch()
  count=0
  while experiment.progress.observation_count < experiment.observation_budget:

    # Receive a Suggestion
    suggestion = conn.experiments(experiment.id).suggestions().create()

    assignments = str(suggestion.assignments).split('(')[1].split(')')[0]
    assignments = json.loads(assignments)
    static_input.update(assignments)
    input = static_input

    # Evaluate Your Metric
    # You implement this function
    value = evaluate_model(input, worker_nr, count, best_parameter_run=False)

    # Report an Observation
    # Include the hostname so that you can track
    # progress on the web interface
    conn.experiments(experiment.id).observations().create(
      suggestion=suggestion.id,
      value=value,
      metadata=dict(hostname=hostname),
    )

    # Update the experiment object
    experiment = conn.experiments(experiment.id).fetch()
    count += 1

