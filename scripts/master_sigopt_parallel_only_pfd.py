'''parallelized parameter optimization of pfd only'''
from sigopt import Connection
from worker_sigopt_parallel import run_worker
import time
import argparse
from rescaling import rescale

def master(api_token, num_workers, static_input):

  start=time.time() #the starting time
  # Create the SigOpt connection
  conn = Connection(client_token=api_token)

  # Create the experiment on master
  experiment = conn.experiments().create(
    project="as_optimization",
    name='AS_optimization',
    # Define which parameters you would like to tune
    parameters=[
      dict(name='pfd', type='double', bounds=dict(min=0.4, max=1.0)),
    ],
    metrics=[dict(name='function_value', objective='maximize')],
    observation_budget=15,
    parallel_bandwidth=num_workers,
  )
  print("Created experiment: https://app.sigopt.com/as_optimization/" + experiment.id)

  for _ in range(num_workers):
    # Launch a worker and run the run_worker
    # function (below) on the worker machine
    # You implement this function
    spin_up_worker(
      api_token=api_token,
      experiment_id=experiment.id,
      static_input=static_input, worker_nr=_
    )
  print('Observation budget exhausted')

  # Fetch the best configuration and explore your experiment
  all_best_assignments = conn.experiments(experiment.id).best_assignments().fetch()
  # Returns a list of dict-like Observation objects

  #I don't want to generate the AS output on the cluster, therefore it is not run again with the best
  #assignments (cause I cannot open images on the cluster, would first have to download them again)
  best_assignments = all_best_assignments.data[0].assignments
  print("Best Assignments: " + str(best_assignments))
  print("Best pfd value: " + str(best_assignments['pfd']))
  print("Explore your experiment: https://app.sigopt.com/as_optimization/" + experiment.id + "/analysis")

  end=time.time()
  runtime=end-start
  print('The runtime of the program is: {}s'.format(runtime))


def spin_up_worker(api_token, experiment_id,static_input, worker_nr):
  run_worker(api_token, experiment_id, static_input, worker_nr)


def set_parameters(static_input):
    if static_input['iou']:
        static_input['Euclidean']=False
    else:
        static_input['Euclidean']=True
    # add analysis folder
    static_input['a'] = static_input['o'] + '/analysis/'
    # add burnin specification: use only the last 500-1000 iterations if there are enough mcmc steps
    if static_input['chainlength'] > 1150:
        static_input['b_nr'] = 4  # burnin nr
        static_input['burnin'] = (static_input['chainlength'] - 750) / 4
    elif static_input['chainlength'] > 700:
        static_input['b_nr'] = 4
        static_input['burnin'] = (static_input['chainlength'] - 300) / 4
    else:
        static_input['b_nr'] = 1  # to short mcmc for multiple burnins
        static_input['burnin'] = int(static_input['chainlength'] / 2)
    if static_input['reduce']!=1:
        rescale(static_input)
    static_input['heuristic']=0.39 #seems to be fine
    static_input['min_bin_size']=20 #make merging important
    static_input['maxbindiff']=10 #make merging important
    static_input['hmp']=1 #do not use rough distance merging
    return static_input



client_token="CQRRDMSHOYYMIQYMKKLBKACDNQJAMWHPGRXCQQNRARFMHLNR"

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--path_to_truth_mask',help='Path to where the dataset is located in Darwin format',
                        required=True,
                        type=str)
parser.add_argument('-i',help='Path to the folder which contains the animal spotter input',
                        required=True,
                        type=str)
parser.add_argument('-o',help='Path to a temporary folder where AS output goes, no one cares',
                        required=True,
                        type=str)
parser.add_argument('--path_to_sigopt_output',
                        help='Path to not yet existent temporary folder, no one cares',
                        required=True,
                        type=str)
parser.add_argument('--binary',
                        help='Path to animalspotter executable',
                        required=True,
                        type=str)
parser.add_argument('--chainlength',
                        help='Length of the mcmc',
                        required=False,
                        type=int)
parser.add_argument('--nr_workers', help='Nr of workers for parallelisation', required=True, type=int)
parser.add_argument('--iou',
                    help='Score to evaluate accuracy is the iou; if False, score to evaluate is euclidean dist',
                    required=True,
                    type=bool)
parser.add_argument('-reduce',
                    help='Factor by which the imgs are reduced --> AS will run faster',
                    required=True,
                    type=int)

args = parser.parse_args()

# run the actual code
static_input=set_parameters(vars(args))
master(api_token=client_token, num_workers=static_input['nr_workers'], static_input=static_input)
