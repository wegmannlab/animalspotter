'''
This script was written due to the initial problem of mean bin images containing ghost-like animals.
Note that it takes a veeeery long time to run.
As the distribution of mean between-image distances of each pixel j is thought to be bimodal (1 peak for small
distances comprising the foreground pixels and one peak for large distances comprising the background pixels), these
distances can be used for a K-means clustering with K=2.
The initial cluster centers are chosen deterministically. Clusters are recalculated as long as the clustering error
is still decreasing across 2 steps.
 In the end, the new representative image is saved as 'new\_mean\_img\_{current bin number}.png.'''

###VAR TO CHANGE!!!
path_to_imgs='/data/probably_waste/animalspotter_core_set_for_tanja_6-40_until_end/outputs/6-402810N_24-045610E/2016-03-11_to_2016-03-14/'
###########################################################3

import numpy as np
import os, glob
from PIL import Image


def Euclidean_dist(mat1,mat2):
    '''Takes 2 matrices and calculates the Euclidean distance between them'''
    diff_mat = np.subtract(mat1, mat2)
    sq_diff_mat=np.square(diff_mat)
    dist = np.sqrt(np.sum(sq_diff_mat))
    return dist

def load_imgs_sep_bins_create_dic(path_to_dir_with_imgs, string_filenames):
    '''Takes a path and a filename string; loads all images in the given path that fulfill the filename conditions
    (The AS output original Distortion images should be loaded as here, the bins are made already)
    Returns a dictionary with the bin nr as key and a list of filenames of the corresponding bin as value'''

    os.chdir(path_to_dir_with_imgs)
    #create dictionary with bin nr as key and a list of all file names of the files in this bin as value
    my_dic={}
    for file in glob.glob(string_filenames):
        bin_nr=file.split('_')[5].split('(')[1].split(')')[0]
        if bin_nr in my_dic:
            my_dic[bin_nr].append(file)
        else:
            my_dic[bin_nr]=[file]
    return my_dic


def create_coord_dic(img_list):
    '''Takes a list of image names, and returns a dictionary with a specific coordinate as key and a list of pixel values
    (of the pixel at this coordinate, of all images) as value'''
    #create a dictionary with a specific coordinate as key and all the pixel values as value
    coord_dic={}
    for img in img_list:
        img=Image.open(img)
        img_array = np.array(img)
        shape=np.shape(img_array)
        for x in range(np.shape(img_array)[0]):
            for y in range(np.shape(img_array)[1]):
                coord='{}_{}'.format(x, y)
                if coord in coord_dic:
                    coord_dic[coord].append(img_array[x][y])
                else:
                    coord_dic[coord] = [img_array[x][y]]
    return (coord_dic, shape)



def calc_dist_pxl_position_btw_imgs(pxl_values):
    ''' Takes a list of all pixel values at a certain coordinate of all images of the same bin; for each image, it
    calculates the distance of the pixel to the same pixel  of
    all other images; it returns a list containing the mean distances'''
    mean_dist_all_imgs=[]
    for pxl_value in pxl_values:
        dist=[]
        for other_pxl_value in pxl_values:
            dist.append(Euclidean_dist(pxl_value, other_pxl_value))
        mean_dist=np.mean(dist)
        mean_dist_all_imgs.append(mean_dist)
    return(mean_dist_all_imgs)




def initial_clusters(mean_dist_all_imgs, K=2):
    '''takes a list of pxl distances and the wished nr of clusters, and returns deterministically chosen initial cluster centers'''
    m1=np.mean(mean_dist_all_imgs)
    initial_cluster_list=[m1]
    for i in range(2, K+1): #find K clusters
        maximum = 0  # start with maximum= zero
        differences_exist=False #in case all pxl distances are same
        for x in mean_dist_all_imgs:
            #make sure x is not one of our clusters already:
            if x in initial_cluster_list:
                continue
            else:
                differences_exist=True
            # I will have to find the x that maximizes the worst case distance
            worst_case_dist = np.inf  # start with worst case distance = infinity
            for previous_chosen in initial_cluster_list:  # calculate distance of x to each previous chosen cluster
                dist = Euclidean_dist(previous_chosen, x)
                if dist < worst_case_dist and dist != 0:  # cant be 0 (otherwise it could be a problem to have 2 times the same image in the dataset)
                    worst_case_dist = dist  # find the minimal distance (distance to nearest center)

            if worst_case_dist > maximum:
                maximum = worst_case_dist
                mi = x
        if differences_exist==False:
            mi=initial_cluster_list[0]+200 #cluster center, zu dem nie etwas zugeordnet wird
        initial_cluster_list.append(mi)
    return initial_cluster_list

def assign_to_right_cluster(mean_dist_all_imgs, clusters):
    '''Takes 1) a list of the mean distance of a pxl at a certain position of each image i to the same pxl in all other images of the same bin
    and 2) a list of the cluster centers
    Returns 1) a dictionary ('dic') containing the nearest cluster index as key and all the mean distances as values
    and 2) a dictionary ('img_index_dic') containing again the nearest cluster index as key and the indeces of the images
    in the 'mean_dist_all_imgs' list as value
    The second dictionary is to store the info from which images the distances come'''
    dic = {}
    img_index_dic={}
    for x in range(len(clusters)):
        dic[x]=[] #initialize dictionary
        img_index_dic[x]=[]
    count=0
    for i in mean_dist_all_imgs:
        count+=1
        min = np.inf
        for x in range(len(clusters)):
            if Euclidean_dist(i, clusters[x]) < min:
                min = Euclidean_dist(i, clusters[x])
                index_nearest_cluster = x
        dic[index_nearest_cluster].append(i)
        img_index_dic[index_nearest_cluster].append((count-1)) #to store info which image has which distance
    return (dic, img_index_dic)



def recompute_clusters(dic,K):
    '''Takes 1) a dictionary containing the nearest cluster index as key and all the mean distances as values, and
    2) the nr of clusters
    Computes the mean of the arrays saved in a certain key of the dictionary'''
    new_clusters=[]
    for key in dic:
        if dic[key]!=[]: #if there are more than 2 clusters to begin with
            new_clusters.append(np.mean(dic[key], axis=0))
    while len(new_clusters)<K:
        new_clusters.append(new_clusters[0]+200) #this artificially generated cluster will never be chosen, it's just a placeholder so that 2 clusters exist
    return new_clusters



def clustering_error(dic, clusters):
    '''Takes 1) a dictionary containing the nearest cluster index as key and all the mean distances as values, and
    2) the cluster centers
    Returns the clustering error'''
    e=0 #the clustering error; start with 0, will add it for every image
    for x in range(len(clusters)):
        for i in dic[x]:
            e+=np.square(Euclidean_dist(x,i))
    return e


string_filenames='20*Distortion*.jpg.png'
my_dic = load_imgs_sep_bins_create_dic(path_to_imgs, string_filenames=string_filenames)
for bin in my_dic:  # for every bin
    img_list = my_dic[bin]
    coord_dic, shape = create_coord_dic(img_list=img_list)
    new_mean_img=np.zeros((shape), dtype=np.uint8)

    for key in coord_dic:  # for every coordinate
        pxl_values = coord_dic[key]
        mean_dist_all_imgs=calc_dist_pxl_position_btw_imgs(pxl_values=pxl_values)
        clusters=initial_clusters(mean_dist_all_imgs, K=2)
        mydic,  img_index_dic=assign_to_right_cluster(mean_dist_all_imgs,clusters)
        e0=clustering_error(mydic,clusters)
        #print("the starting clustering error is:", e0)
        clusters = recompute_clusters(mydic,K=2)
        mydic,  img_index_dic = assign_to_right_cluster(mean_dist_all_imgs, clusters)
        e1 = clustering_error(mydic, clusters)
        clusters = recompute_clusters(mydic, K=2)
        mydic,  img_index_dic = assign_to_right_cluster(mean_dist_all_imgs, clusters)
        e2 = clustering_error(mydic, clusters)
        while e0>e1 or e0>e2: #while error is still decreasing across 2 steps
            e0=e1
            e1=e2
            clusters=recompute_clusters(mydic, K=2)
            mydic,  img_index_dic=assign_to_right_cluster(mean_dist_all_imgs,clusters)
            e2=clustering_error(mydic,clusters)
        #print("the end clustering error is:", e1)



        #Find out which cluster is the background cluster (smaller distances)
        if mydic[1]==[] or np.mean(mydic[0]) < np.mean(mydic[1]): #if cluster 1 is empty or has higher distances, cluster 0 is background
            #set the pxl value to the mean rgb pxl value of the corresponding imgs
            background_imgs_indeces=img_index_dic[0]
        else:
            background_imgs_indeces=img_index_dic[1]

        background_r = []
        background_g = []
        background_b = []
        for i in background_imgs_indeces:
            background_r.append(pxl_values[i][0])
            background_g.append(pxl_values[i][1])
            background_b.append(pxl_values[i][2])
        background_r=np.mean(background_r)
        background_g=np.mean(background_g)
        background_b = np.mean(background_b)


        x=int(key.split('_')[0])
        y=int(key.split('_')[1])
        new_mean_img[x][y]=(background_r, background_g, background_b)


    filename = 'new_mean_image_{}'.format(bin)
    img = Image.fromarray(new_mean_img)
    img.save('{}.png'.format(filename))








