# coding=utf-8
from PIL import Image
from PIL.ExifTags import TAGS
import os
import re

def decode_user_comment(raw_data):
    decoded = ""
    if isinstance(raw_data, str):
        decoded = None
    else:
        if raw_data[:5]==b'ASCII':
            print('user tag was written in ASCII')
            return (raw_data[8:].decode("ascii"))
        decoded = raw_data[8:].decode("utf-16").strip('\x00').strip()

        # handle case where the encoding was wrong, try to recover
        if '䬀' in decoded:
            decoded = raw_data[8:].decode("utf-8").strip('\x00').strip()

            decoded = ''.join(filter(lambda c: ord(c) < 128 and ord(c) != 0, decoded))
    return decoded

def extract_labels(image):
    """
    Extract all the tags of a particular image and return it as a list.

    :param image: Path to Image
    :return: List of tags in that image
    """
    try:
        img = Image.open(image)

        img.info.keys()

        exifd = img._getexif()
        print(exifd)

        keys = list(exifd.keys())
        keys = [k for k in keys if k in TAGS]

        # print("\n".join([TAGS[k] for k in keys]))

        commentID = -1
        dateID = -1

        for k in keys:
            if TAGS[k] == 'UserComment':
                commentID = k
            if TAGS[k] == 'DateTimeOriginal':
                dateID = k

        tags = []
        date = ""

        if dateID >= 0:
            date = exifd[dateID]

        if commentID >= 0:
            var = exifd[commentID]

            decoded = decode_user_comment(var)
            if decoded is None:
                print("Warning, wrong usercomment tag in " + image)
                return None, ""

            keywords_keyword = "Keywords: "
            if keywords_keyword in decoded:
                index = decoded.index(keywords_keyword)
                decoded = decoded[index + len(keywords_keyword):]

            tags = decoded.split(';')



        return tags, date

    except IOError:
        filename, file_extension = os.path.splitext(image)

        if file_extension:
            if file_extension != ".txt":
                print("Could not open file " + image)
    except:
        print("Fatal error while reading tags from file " + image)

    return None, ""
