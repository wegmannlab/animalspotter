import os

import subprocess

import random

import hashlib

import argparse

from ImageTools import *
from make_day_night_dirs import mk_daynight_dirs
    
minBoxWeight = 0.1

def opencv_contours(binaryImage):
    return 0, 0, 0, 0

def saveBoxes(mypath, outputAnalysis, decoded, tags, imageTags):
    base = decoded.split(' : ')

    boxInfo = base[1].split(' ')

    size = base[0].split(' ')[3].split('/')

    width = int(size[0])
    height = int(size[1])

    objectIndex = -1

    if imageTags is not None:
        for t in imageTags:
            if t in tags:
                objectIndex = tags.index(t)
                break;

    offset = 0

    imageName = ""
    for part in boxInfo:
        if part.isdigit():
            break
        else:
            if offset > 0:
                imageName += " "

            imageName += part

        offset += 1

    labelOutput = os.path.join(mypath, imageName.rsplit('.', 1)[0] + ".txt")
    labelOutputRaw = os.path.join(outputAnalysis, imageName.rsplit('.', 1)[0] + ".raw.txt")

    with open(labelOutput, 'w') as output_file, open(labelOutputRaw, 'w') as output_raw:
        output_raw.write(imageName + '\n')

        for i in range(int(boxInfo[offset])):
            boxWeight = float(boxInfo[2 + i * 4 + offset])
            connected = float(boxInfo[3 + i * 4 + offset])
            foreground = float(boxInfo[4 + i * 4 + offset])

            box = boxInfo[1 + i * 4 + offset]

            boxWidth = int(box.split('x')[0])
            boxHeight = int(box.split('x')[1].split('+')[0])

            boxX = int(box.split('x')[1].split('+')[1])
            boxY = int(box.split('x')[1].split('+')[2])

            output_raw.write(str(boxWeight) + " " +
                            str(connected) + " " +
                            str(foreground) + " " +
                          str((boxX + boxWidth / 2.) / width) + " " +
                          str((boxY + boxHeight / 2.) / height) + " " +
                          str(float(boxWidth) / width) + " " +
                          str(float(boxHeight) / height) + '\n')

            if objectIndex >= 0 and boxWeight > minBoxWeight:
                output_file.write(str(objectIndex) + " " +
                                  str((boxX + boxWidth / 2.) / width) + " " +
                                  str((boxY + boxHeight / 2.) / height) + " " +
                                  str(boxWidth / width) + " " +
                                  str(boxHeight / height) + '\n')

def analyse(options, mypath, output, outputAnalysis, tags):
    params = [options.binary, '-i', mypath + "/", '-o', output + "/", '-di',
                      '-v', '-mb', str(options.min_bin_size), '-di']
                      

    if options.burnin >= 0:
        params.append('-b')
        params.append(str(options.burnin))

    if options.burnin_nr >= 0:
        params.append('-b_nr')
        params.append(str(options.burnin_nr))

    if options.chainlength >= 0:
        params.append('-c')
        params.append(str(options.chainlength))

    if options.threads > 0:
        params.append('-th')
        params.append(str(options.threads))

    if options.alpha > 1: #AS takes the log of this to weight influence of neighbour pxls. Cause log of something between 0 and 1 is neg, this would not make any sense
        params.append('--alpha')
        params.append(str(options.alpha))

    if options.beta > 1: #same reason as above with alpha
        params.append('--beta')
        params.append(str(options.beta))
        
    if options.prior > 0:
        params.append('-p')
        params.append(str(options.prior))
        
    if options.min_box_prob >= 0:
        params.append('-mpt')
        params.append(str(options.min_box_prob))
        
    if options.std_background_means > 0:
        params.append('-u')
        params.append(str(options.std_background_means))
        
    if options.std_background_sigma > 0:
        params.append('-w')
        params.append(str(options.std_background_sigma))

    if options.std_foreground_means > 0:
        params.append('-x')
        params.append(str(options.std_foreground_means))
        
    if options.std_foreground_sigma > 0:
        params.append('-y')
        params.append(str(options.std_foreground_sigma))

    if options.maxbindiff > 0:
        params.append('-td')
        params.append(str(options.maxbindiff))

    if options.rgb:
        params.append('--color')
        params.append(options.rgb)

    if options.reduce >= 0:
        params.append('-r')
        params.append(str(options.reduce))

    if options.night >= 0:
        params.append('-gn')
        params.append(str(options.night))
        
    if options.std_img_distortion >= 0:
        params.append('-im')
        params.append(str(options.std_img_distortion))

    if options.heuristic >= 0:
        params.append('-he')
        params.append(str(options.heuristic))

    if options.pxlFractionDist > 0 and options.pxlFractionDist<=1:
        params.append('-pfd')
        params.append(str(options.pxlFractionDist))

    if options.seed != -1:
        params.append('-s')
        params.append(str(options.seed))

    if options.how_maniest_pxl_used > 0:
        params.append('-hmp')
        params.append(str(options.how_maniest_pxl_used))

    if not options.no_bounding_boxes:
        params.append('--bounding-boxes')
        
    if options.discard_very_small_bins:
        params.append('--discardSmallBins')

    if options.separate_day_night_only:
        params.append('--daynightonly')

    print(' '.join(params))

    process = subprocess.Popen(params, stdout=subprocess.PIPE)

    images = []

    for line in iter(process.stdout.readline, b''):  # replace '' with b'' for Python 3
        decoded = line.decode('UTF-8').strip('\n')

        print(decoded)

        if decoded.startswith('Bounding boxes'):
            base = decoded.split(' : ')

            boxInfo = base[1].split(' ')

            image = mypath

            offset = 0

            imageName = ""
            for part in boxInfo:
                if part.isdigit():
                    break
                else:
                    if offset > 0:
                        imageName += " "

                    imageName += boxInfo[offset]

                offset += 1

            image = os.path.join(image, imageName)

            images.append(image)

            imageTags = extract_labels(image)[0]

            saveBoxes(mypath, outputAnalysis, decoded, tags, imageTags)
    #get the return code of AS to check that it worked
    process.communicate()
    rc=process.returncode
    print('AS ended with return code', rc)
    if rc!=0:
        print('An error occured while running AnimalSpotter; this might be because loading the images used too much memory -- an estimate how much memory is needed using a specific input can be found in the file min_estimated_memory_needed.txt, located in the corresponding output directory (within', options.output.rstrip(' '), '). If too much memory is required, try to increase the parameter \'reduce\' or split the input directory in a reasonable way (i.e. try splitting somewhere where consecutive images look very different. If this is not possible, try splitting in a way that ensures that enough, i.e. at least x=minBinSize, similar images are in each resulting new input directory)')
        raise ValueError('AnimalSpotter did not return 0') #raise an error
    return images

def get_folders(outputBase, analysis, input):

    inputFolders = []
    outputFolders = []
    analysisFolders = []

    dirs = [x[0] for x in os.walk(input)]

    absolute_subdirs = []
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            absolute_subdirs.append(i)

    # now I have the whole path for the subdirs; but I only want to have the part that is not in input
    subdirs = []
    for absolute_subdir in absolute_subdirs:
        nr_subdirs = len(absolute_subdir.split('/')) - len(input.rstrip('/').split('/'))
        if nr_subdirs != 0:
            subdirs.append('/'.join(absolute_subdir.split('/')[-nr_subdirs:]))

    for folder in subdirs:
        inputFolders.append(os.path.join(input, folder))
        outputFolders.append(os.path.join(outputBase, folder))
        analysisFolders.append(os.path.join(analysis, folder))

    if not inputFolders:
        print("No subfolders found for specified input directory, falling back to only analyse input directory")
        inputFolders.append(input)
        outputFolders.append(outputBase)
        analysisFolders.append(analysis)

    print('inputFolders are', inputFolders)

    return outputFolders, analysisFolders, inputFolders


def parse_args():
    parser = argparse.ArgumentParser()
    

    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        type=str,
        help="Output folder",
    )
    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        type=str,
        help="Input folder",
    )
    parser.add_argument(
        "-a",
        "--analysis-folder",
        dest="analysis_folder",
        type=str,
        help="Output of the analysis",
    )
    parser.add_argument(
        "-l",
        "--labels-file",
        dest="labels_file",
        type=str,
        help="File containing the labels to be analysed. One label per line",
    )
    
    parser.add_argument(
        "--heuristic",
        dest="heuristic",
        type=float,
        default=-1,
        help="File containing the labels to be analysed. One label per line",
    )

    parser.add_argument(
        "--pxlFractionDist",
        dest="pxlFractionDist",
        type=float,
        default=-1,
        help="Fraction of smallest pixel-wise distances that should be considered for bin merging",
    )

    binary = '../cmake-build-debug/animalspotter'
    parser.add_argument(
        "--binary",
        dest="binary",
        default=binary,
        type=str,
        help="Path to animal spotter binary [Default: {}]".format(
            os.path.relpath(binary)
        ),
    )
    
    threads = 0
    parser.add_argument(
        "-t",
        "--threads",
        dest="threads",
        default=threads,
        type=int,
        help="Threads to use ",
    )
    
    parser.add_argument(
        "--no-bounding-boxes",
        dest="no_bounding_boxes",
        default=False,
        action='store_true',
        help="Disable bounding boxes"
    )
    
    burnin = -1
    parser.add_argument(
        "-b",
        dest="burnin",
        default=burnin,
        type=int,
        help="Burnin length ",
    )

    burnin_nr = -1
    parser.add_argument(
        "-b_nr",
        dest="burnin_nr",
        default=burnin_nr,
        type=int,
        help="Nr of burnins ",
    )
    
    std_img_distortion = -1
    parser.add_argument(
        "-im",
        dest="std_img_distortion",
        default=std_img_distortion,
        type=float,
        help="The standard deviation for the image distortions .",
    )

    std_background_means=-1
    parser.add_argument(
        "-u",
        dest="std_background_means",
        default=std_background_means,
        type=float,
        help=" The standard deviation for the background means.",
    )
    
    std_foreground_means=-1
    parser.add_argument(
        "-x",
        dest="std_foreground_means",
        default=std_foreground_means,
        type=float,
        help=" The standard deviation for the foreground means.",
    )
    
    std_background_sigma=-1
    parser.add_argument(
        "-w",
        dest="std_background_sigma",
        default=std_background_sigma,
        type=float,
        help=" The standard deviation for the background sigma.",
    )
    
    std_foreground_sigma=-1
    parser.add_argument(
        "-y",
        dest="std_foreground_sigma",
        default=std_foreground_sigma,
        type=float,
        help=" The standard deviation for the foreground sigma.",
    )
    
    maxbindiff=-1
    parser.add_argument(
        "-td",
        dest="maxbindiff",
        default=maxbindiff,
        type=int,
        help=" Maximum difference in seconds between two pictures to be in the same bin."
    )
  
    
    chainlength = -1
    parser.add_argument(
        "-c",
        dest="chainlength",
        default=chainlength,
        type=int,
        help="MCM chain length ",
    )
    parser.add_argument(
        "--test-samples",
        dest="test_samples",
        default=0.8,
        type=float,
        help="Separation of test/validation samples",
    )

    prior = -1
    parser.add_argument(
        "--prior",
        dest="prior",
        default=prior,
        type=float,
        help="Prior",
    )
    
    alpha = -1
    parser.add_argument(
        "--alpha",
        dest="alpha",
        default=alpha,
        type=float,
        help="Animalspotter alpha value",
    )

    beta = -1
    parser.add_argument(
        "--beta",
        dest="beta",
        default=beta,
        type=float,
        help="Animalspotter beta value",
    )
    
    rgb = ""
    parser.add_argument(
        "--box-color",
        dest="rgb",
        default=rgb,
        type=str,
        help="Output box color",
    )
    
    min_bin_size=5
    parser.add_argument(
        "--mb",
        dest="min_bin_size",
        default=min_bin_size,
        type=int,
        help="Minimum bin size",
    )
    
    parser.add_argument(
        "--discard_very_small_bins",
        dest="discard_very_small_bins",
        default=False,
        action='store_true',
        help="Discard bins smaller than number specified in parameter 'min_bin_size' ('--mb')"
    )

    parser.add_argument(
        "--dn",
        dest="separate_day_night_only",
        default=False,
        action='store_true',
        help="Get a list of all day images and one for all night images -- AS is not actually run"
    )
    
    min_box_prob = 0.35
    parser.add_argument(
        "--min-box-prob",
        dest="min_box_prob",
        default=min_box_prob,
        type=float,
        help="Minimum probability of a pixel to be considered foreground in the bb calculation",
    )
    
    reduce = 1
    parser.add_argument(
        "--reduce",
        dest="reduce",
        default=reduce,
        type=int,
        help="Reduction of original image size",
    )

    hmp=-1
    parser.add_argument(
        "--hmp",
        dest="how_maniest_pxl_used",
        default=hmp,
        type=int,
        help="How maniest pxl should be used to calculate a rough distance to get candidates for merging",
    )
    
    night = 0.75
    parser.add_argument(
        "--min-night-grayscale",
        dest="night",
        default=night,
        type=float,
        help="Minumum amount of pixels in percentage needing to be grayscale for the image to be nightmode. 0 to disable",
    )

    parser.add_argument(
        "-s",
        "--seed",
        dest="seed",
        default=-1,
        type=int,
        help="This is the seed",
    ) #, defaults to -1, which sets no seed (and uses the default animalspotter one)


    return parser.parse_args()

def main():

    options = parse_args()

    if not os.path.exists(options.binary):
        print("No valid binary specified, binary " + options.binary + " does not exist")
        return

    if not options.input:
        print("You need to specify an input folder")
        return

    if not os.path.exists(options.input):
        print("Specified input folder "+options.input +" does not exist")
        return

    if not options.output:
        print("You need to specify an output folder")
        return

    if not os.path.exists(options.output):
        os.makedirs(options.output)

    if not options.analysis_folder:
        print("You need to specify an analysis folder")
        return

    if options.analysis_folder and not os.path.exists(options.analysis_folder):
        os.makedirs(options.analysis_folder)

    output, analysisFolders, inputPaths = get_folders(options.output, options.analysis_folder, options.input)

    tags = []


    if options.labels_file and os.path.exists(options.labels_file):
        with open(options.labels_file) as f:
            tags = f.readlines()
        tags = [x.strip() for x in tags]

    with open(os.path.join(options.analysis_folder, "dataset.names"), 'w') as output_file:
        for tag in tags:
            output_file.write(tag + '\n')

    paths = []
    
   
    for i in range(0, len(output)):
        if not os.path.exists(output[i]):
            os.makedirs(output[i])

        if not os.path.exists(analysisFolders[i]):
            os.makedirs(analysisFolders[i])
        print(inputPaths[i])
        print(output[i])
        paths.extend(analyse(options, inputPaths[i], output[i], analysisFolders[i], tags))

    random.shuffle(paths)

    split = int(len(paths) * options.test_samples)

    train_data = paths[:split]
    test_data = paths[split:]

    with open(os.path.join(options.analysis_folder, "training.txt"), 'w') as output_file:
        for img in train_data:
            output_file.write(img + '\n')

    with open(os.path.join(options.analysis_folder, "test.txt"), 'w') as output_file:
        for img in test_data:
            output_file.write(img + '\n')

    if options.separate_day_night_only:
        print('starting dir creation')
        mk_daynight_dirs(options.input, inputPaths, output)


if __name__ == "__main__":
    main()

