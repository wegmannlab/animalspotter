'''
This script changes the id of all imgs without gt and of all imgs that should not be considered to have gt to 0
Which images are still unchanged is decided using the time they were taken; the x images in the middle are kept unchanged.
(we want to study the effect of the nr of imgs with gt on accuracy and parameter choice)
Be sure to give a copy of the folder containing the original images as input, as they will be renamed,
or if there are less than the wished nr of images in a folder, deleted.



'''
# PARAMETERS TO ADAPT#
gt_path='/home/dona/Documents/all'
img_path='/home/dona/Documents/AS_input/night_AS_input_osw/50imgs' #contains images in whatever folder structure (if the end subdirectory does not contain the nr of wished images, it is deleted, though!)
nr_imgs_with_gt=50 #adjacent imgs (those are probably more similar, but this is the natural choice for expanding gt)

##################################
import os, glob, shutil
from IsingBeta import get_timeIdsDict

####################################

gt_ids=[]
os.chdir(gt_path)
for file in glob.glob('*.json'):
    gt_ids.append(file.split('_')[0])

#get all end subdirs first
dirs = [x[0] for x in os.walk(img_path)]

absolute_subdirs = []
for i in dirs:
    if len([x[0] for x in os.walk(i)]) == 1:
        absolute_subdirs.append(i)

for subdir in absolute_subdirs:
    #first set the id of all imgs without gt to 0 and move them in a new subdir
    new_dir = '/'.join(subdir.split('/')[:-1]) + '/no_gt_' + subdir.split('/')[-1]
    os.mkdir(new_dir)
    os.chdir(subdir)
    for img in glob.glob('*.jpg'):
        if img.split('_')[0] not in gt_ids:
            new_img_name = '0_' + '_'.join(img.split('_')[1:])
            os.rename(img, new_img_name)
            shutil.move(new_img_name, new_dir)

    # now that I got rid of these imgs, go through all remaining imgs and keep only the nr specified in nr_imgs_with_gt of gt imgs
    # make sure that the imgs we keep are in the middle (time-wise)
    # rename the other imgs by changing the id to 0, so it doesn't match any gt json files anymore and cannot be used for accuracy calculations
    idTimesDict = get_timeIdsDict(subdir)  # ordered by time:)
    dictsize = len(idTimesDict)
    count = 0
    del_ids = []
    if dictsize<nr_imgs_with_gt:
        print('{} does not contain enough annotated images; it has therefore been deleted'.format(subdir))
        os.system('rm -r {}'.format(new_dir))
        os.chdir('/'.join(subdir.split('/')[:-1]))
        os.system('rm -r {}'.format(subdir))
        subdir = '/'.join(subdir.split('/')[:-1])
        while len([name for name in os.listdir(subdir)]) == 0:
            os.chdir('/'.join(subdir.split('/')[:-1]))
            os.system('rm -r {}'.format(subdir))
            subdir = '/'.join(subdir.split('/')[:-1])
        continue
    lower_border = (dictsize - nr_imgs_with_gt) / 2
    upper_border = lower_border + nr_imgs_with_gt
    for key, value in idTimesDict:
        count += 1
        if count < lower_border:
            del_ids.append(key)
        if count >= upper_border:
            del_ids.append(key)

    for img in glob.glob('*jpg'):
        if img.split('_')[0] in del_ids:
            new_img_name = '0_' + '_'.join(img.split('_')[1:])
            os.rename(img, new_img_name)


    # move back the imgs without gt; they should be used as AS input still
    os.chdir(new_dir)
    for img in glob.glob('*'):
        shutil.move(img, subdir)

    #and then remove the new dir
    os.system('rm -r {}'.format(new_dir))


