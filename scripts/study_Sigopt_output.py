
'''This script was written to 1) separate the empty vs animal imgs output and get corresponding distribution plots, and
2) to obtain the dictionary with all accuracies for every image studied with gt (so you can have a look at what different
accuracies mean)'''




###ADAPT THIS!
path_to_truth_mask='/home/dona/Documents/all_with_animals' # ground truth for all imgs, or only empty or only animal imgs
directory_with_AS_output_and_mask='/home/dona/Documents/best_full_sigopt_ccf_night' #not yet existent directory, where the results should go
#can also already exist, but then it should contain: the gt, the mcmc output, both together, the original imgs
sigopt_out_directory='/home/dona/Documents/best_sigopt_ccf_night100' #sigopt output of the run we'd like to study

#########################################################################################################3





from evaluation_pxl_level import evaluate_AS

import shutil, os
from pathlib import Path
import matplotlib.pyplot as plt


def get_gt(path_to_truth_mask, sigopt_out_directory, directory_to_put_them, parallel):
    '''Puts all AS output images where I have a ground truth mask in a separate folder, together with the ground truth masks
    Returns the nr of images that fulfill this condition
    This function is necessary as we do not have a ground truth mask for all images'''
    if os.path.exists(directory_to_put_them):
        print('Directory with gt, AS output, original imgs and AS output together with gt already exists')
        return
    ids_AS=[]
    for file in Path(sigopt_out_directory+'/best').glob('*'):
        shutil.move(str(file), sigopt_out_directory)
    for file in Path(sigopt_out_directory+'/worst').glob('*'):
        shutil.move(str(file), sigopt_out_directory)
    for file in Path(sigopt_out_directory).glob('mcmc*.png'):
        filename = str(file).split('/')[-1]
        ids_AS.append(filename.split('_')[9])
    ids_mask = []
    for file in Path(path_to_truth_mask).glob('*.json'):
        filename = str(file).split('/')[-1]
        ids_mask.append(filename.split('_')[0])
    #get intersection, i.e. id of images where we have AS output
    #and the truth mask
    ids=list(set(ids_AS) & set(ids_mask))
    if len(ids)==0:
        print('There is no gt for any of the input mcmc imgs')
        return
    if not os.path.exists(directory_to_put_them):
        os.mkdir(directory_to_put_them)
    for file in Path(sigopt_out_directory).glob('mcmc*.jpg.png'):
        filename = str(file).split('/')[-1]
        if filename.split('_')[9] in ids:
            shutil.move(str(file), directory_to_put_them)
    for file in Path(sigopt_out_directory).glob('*.jpg'):
        filename=str(file).split('/')[-1]
        if filename.split('_')[0] in ids:
            shutil.move(str(file), directory_to_put_them)
    for file in Path(sigopt_out_directory).glob('AS_and_ground_truth*.png'):
        filename = str(file).split('/')[-1]
        if filename.split('_')[4].rstrip('.png') in ids:
            shutil.move(str(file), directory_to_put_them)
    for file in Path(path_to_truth_mask).glob('*.json'):
        filename = str(file).split('/')[-1]
        if filename.split('_')[0] in ids:
            if parallel:
                if '(' in filename:  # because os.system cannot handle a paranthesis in the file name (gives a syntax error and skips these files)--> I rename them, apply imagemagick, name them back
                    initial_img_name = str(file)
                    new_img_name = str(file).replace('(', '')
                    new_img_name = new_img_name.replace(')', '_renamed')
                    os.rename(initial_img_name, new_img_name)

                    os.system('cp {} {}'.format(new_img_name, directory_to_put_them))
                    #files are not given back their initial names because this might disturb during parallelization
                    #os.rename(directory_to_put_them + '/' + new_img_name, directory_to_put_them + '/' + initial_img_name)
                    #os.rename(path_to_truth_mask.rstrip('/')+'/'+new_img_name, path_to_truth_mask.rstrip('/')+'/'+initial_img_name)
                else:
                    #print(str(file))
                    #print(directory_to_put_them)
                    os.system('cp {} {}'.format(str(file), directory_to_put_them))

            else:
                shutil.move(str(file), directory_with_AS_output_and_mask)
    return

parallel=True
best_parameter_run=False
iou_bool=True
Euclidean=False
get_gt(path_to_truth_mask, sigopt_out_directory, directory_with_AS_output_and_mask, parallel)
score, scores_dict = evaluate_AS(directory_with_AS_output_and_mask, best_parameter_run, iou_bool, Euclidean)
print('The overall score of imgs of interest is', score)

# see how accuracies are distributed
plt.hist(scores_dict.values())
plt.title('AS accuracy distribution')
plt.xlabel('accuracy')
plt.ylabel('number')
plt.savefig(directory_with_AS_output_and_mask + '/AS_accuracy_distr.png')
bad_count=0
good_count=0
other_count=0
for id in scores_dict.keys():
    if scores_dict[id]==0:
        bad_count+=1
    elif scores_dict[id]==1:
        good_count+=1
    else:
        other_count+=1
print(bad_count)
print(good_count)
print(other_count)
print(scores_dict)
