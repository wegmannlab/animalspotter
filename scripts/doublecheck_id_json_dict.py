'''This file is to check that the id json dictionary is really correct
(it should be, but one might have messed it up by using the wrong input json dictionary)
So this script has been written as it's very important that this json dict is correct'''

###ADAPT THIS!!!
mypath='/home/dona/Documents/check_id_error' #path to where the json dict is located
json_dict_name='final_updated_ids.json' #actual json dict name
new_json_dict_name='new_final_updated_ids.json' #in case you want to add the missing entries
gt_path='/home/dona/Documents/all' #path to all gt data for which you want to check that the entry in the json dict is present and correct

Add_missing_entries=False #first you should check why this happened! Afterwards, you can set this to True for correction
#############################################################################################################################################

from pathlib import Path
import json, os

all_ids=[]
for file in Path(mypath).glob(json_dict_name):
    with open(file, 'r') as f:
        id_dicts = json.load(f)

mydict={}
for adict in id_dicts:
    mydict[adict['id']]=adict['original_filename'].strip('.jpg')


#get all json

missing=[]
wrong_filenames=0
missing_CTs=[]
present_CTs=[]

for file in Path(gt_path).glob('*json'):
    file=str(file)
    filename=file.split('/')[-1]
    id=filename.split('_')[0]
    filename='_'.join(filename.split('_')[1:]).strip('.json')
    CT=filename.split('_')[0]
    if str(id) not in mydict.keys():
        print(id, 'is not in mydict')
        missing.append(id)
        if CT not in missing_CTs:
            missing_CTs.append(CT)
        if Add_missing_entries:
            mydict[id]=filename #add it to the dictionary
        continue
    else:
        if CT not in present_CTs:
            present_CTs.append(CT)
    if mydict[id]!=filename:
        print('actual filename is:' ,filename)
        print('entry in dict is:',mydict[id])
        wrong_filenames+=1

print(len(missing))
print(wrong_filenames)
print(missing_CTs)
print(present_CTs)
print(set(missing_CTs).intersection(set(present_CTs)))



dictlist=[]
for mydictkey in mydict.keys():
    newdict={}
    newdict['id']=mydictkey
    newdict['original_filename']=mydict[mydictkey]
    dictlist.append(newdict)

if Add_missing_entries:
    os.chdir(mypath)
    with open(new_json_dict_name, 'w') as json_file:
        json.dump(dictlist, json_file)
