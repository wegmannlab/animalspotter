'''This script changes the name of the input images by copying the initial img name (eg ICT0001) to the front,
before the id. This is useful, because then the imgs will be displayed in the right order in the dir.
This is necessary if we do not have a gt for the whole cam trap, but instead must only use the ones with gt and the subsequent 20
imgs (or less, in case of day-night transition or cam is moved) as AS input'''

#####ADAPT THIS!!!!
img_path='/home/dona/Documents/partly_annotated/partly_annotated/use_for_AS/JFIFs_converted/6.366733N_24.023500E_mwg_tag_spc/2012-03-25_to_2012-04-01_RECNX15'
cp_to_front=False #true if we want to copy the initial img name to the front
#else, we want to delete the initial img name in the front
#########################################################################333
import os, glob

os.chdir(img_path)
for img in glob.glob('*.jpg'):
    if cp_to_front==True:
        initial_imgname=img.rstrip('.jpg').split('_')[-1]
        os.rename(img, initial_imgname+'_'+img)
    else:
        img_name='_'.join(img.split('_')[1:])
        os.rename(img, img_name)