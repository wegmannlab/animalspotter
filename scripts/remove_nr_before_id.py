'''In the inherited darwin truthset (before summer 2020, can be found on the cluster; the ones annotated are the files
containing the string 'polygon'; only those are of interest), each filename contains a nr at the beginning, that is not
the id, is not useful and has to be stripped so the files can be used for accuracy evaluation. (the originals remain on
the cluster)'''

####ADAPT THIS

path_to_truth_set='/data/darwin/annotations_with_subdirectories/polygons/'

###############################################################################
import os, glob

os.chdir(path_to_truth_set)
for file in glob.glob('*.json'):
    if len(file.split('_')[0])!=6: #if the first nr is not 6 digits (as the id)
        cleaned_filename=file.split('_')[1:]
        separator = '_'
        cleaned_filename = separator.join(cleaned_filename)
        print(cleaned_filename)


        path_to_file = path_to_truth_set + file
        new_path_to_file = path_to_truth_set + cleaned_filename
        os.rename(path_to_file, new_path_to_file)


