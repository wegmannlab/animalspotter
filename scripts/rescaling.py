
###ADAPT THIS!!!
input_dir='/home/dona/Documents/rescale_locally' #contains imgs to be rescaled in whatever directory structure
reduce_factor=10
run_from_this_script=False
#note: only images in subfolders without any other folder in it are processed!
#############################################################################################################


import os, glob
from pathlib import Path

def rescale(static_input):
    '''If not yet existent, it creates a dir rescaled_{input dir name} with all the rescaled images.Then, the entry 'i' in the dictionary
    static_input is changed to this new directory, and the entry 'reduce' is changed to 1 as reducing must not be performed anymore lateron.'''

    input=static_input['i']
    reduction_factor=static_input['reduce']
    reduction_percentage=100/reduction_factor


    new_input_bone = '/'.join(static_input['i'].rstrip('/').split('/')[:-1])+'/rescaled_'+static_input['i'].rstrip('/').split('/')[-1]
    print(new_input_bone)
    if not Path(new_input_bone).exists():
        os.mkdir(new_input_bone)
    else:
        print('Using existent folder {} (contains already rescaled imgs) instead of rescaling again'.format(new_input_bone))
        static_input['i']=new_input_bone
        static_input['reduce']=1
        return

    dirs = [x[0] for x in os.walk(input)]

    absolute_subdirs = []
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            absolute_subdirs.append(i)
    # now I have the whole path for the subdirs; but I only want to have the part that is not in input
    subdirs = []
    for absolute_subdir in absolute_subdirs:
        nr_subdirs = len(absolute_subdir.split('/')) - len(input.rstrip('/').split('/'))
        if nr_subdirs != 0:
            subdirs.append('/'.join(absolute_subdir.split('/')[-nr_subdirs:]))
        absolute_subdir = Path(absolute_subdir)
        assert absolute_subdir.exists() and absolute_subdir.is_dir()
        if subdirs == []:
            dir_to_put_rescaled_img = new_input_bone
        else:
            dir_to_put_rescaled_img = new_input_bone + '/'+subdirs[-1]
            count=0
            new_dir = new_input_bone
            while not Path(dir_to_put_rescaled_img).exists():
                new_dir=new_dir+'/'+subdirs[-1].split('/')[count]
                if not Path(new_dir).exists():
                    os.mkdir(new_dir)
                count+=1


        #for each img in the current subfolder, rescale it
        for img in absolute_subdir.glob('*.jpg'):
            img_name = str(img).split('/')[-1]  # because img is the img name with the absolute path

            replaced_paranthesis = False
            if '(' in img_name: # because imagemagick cannot handle a paranthesis in the file name (gives a syntax error and skips these files)--> I rename them, apply imagemagick, name them back
                replaced_paranthesis = True
                initial_img_name = img_name
                initial_img = img
                img_name = img_name.replace('(', '')
                img_name = img_name.replace(')', '')
                img = '/'.join(str(img).split('/')[:-1]) + '/' + img_name
                os.rename(initial_img, img)

            os.system('convert {a} -resize {b}% -define colorspace:auto-grayscale=false -type truecolor {c}/{d}'.format(a=img, b=reduction_percentage, c=dir_to_put_rescaled_img, d=img_name))
            if replaced_paranthesis:
                os.rename(dir_to_put_rescaled_img + '/' + img_name, dir_to_put_rescaled_img + '/' + initial_img_name)
                os.rename(img, initial_img)


    #set the value of input in the dictionary 'static_input' to the dictionary with the rescaled imgs, and reduce to 1 as we will not have to reduce anymore thereafter
    static_input['i']=new_input_bone
    static_input['reduce']=1

if run_from_this_script==True:
    static_input = {'i': '/home/dona/Documents/rescale_locally', 'reduce': 10}
    rescale(static_input=static_input)
