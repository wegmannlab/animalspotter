import argparse

import os
from collections import Counter

from ImageTools import *
from datetime import datetime, timedelta

import calendar

"""
This script extracts all labels from the pictures and writes them to a file
"""

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        default="",
        type=str,
        help="Input folder",
    )

    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        default="",
        type=str,
        help="Output file",
    )

    parser.add_argument(
        "-f",
        "--filters",
        dest="filters",
        default="",
        type=str,
        help="Ignore all tags starting with a certain string. This is a comma separated list",
    )

    parser.add_argument(
        "--fix-dates",
        dest="fix_dates",
        action="store_true",
        help="Adjust dates based on time_error tag",
    )

    parser.add_argument(
        "-t",
        "--max-time-difference",
        dest="max_time_difference",
        default=3,
        type=float,
        help="Maximum time difference between two images to be considered to be in same group",
    )

    parser.add_argument(
        "-m",
        "--max-group-size",
        dest="max_group_size",
        default=3,
        type=int,
        help="Maximum group size for images",
    )

    return parser.parse_args()

def path_to_list(path):
    '''
    Based on https://stackoverflow.com/questions/3167154/how-to-split-a-dos-path-into-its-components-in-python
    :param path:
    :return:
    '''
    folders = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)

            break

    return folders

def get_time_error_tag(tags):
    for tag in tags:
        if tag.startswith("time_error_"):
            return tag
    return None

def add_months(sourcedate, months):
    """
    https://stackoverflow.com/questions/4130922/how-to-increment-datetime-by-custom-months-in-python-without-using-library
    """
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return datetime(year, month, day, sourcedate.hour, sourcedate.minute, sourcedate.second)

def fix_date_time(tags, event_date, event_time):
    # time_error_plus_00sec_03min_14h_06d_02m_0001y

    time_error_tag = get_time_error_tag(tags)

    if time_error_tag is not None:
        original_datetime = datetime.strptime(event_date+" "+event_time, "%Y:%m:%d %H:%M:%S")

        time_error_tag = time_error_tag.replace("time_error_", '')

        parts = time_error_tag.split('_')

        delta = timedelta(seconds=int(parts[1].replace('sec', '')), minutes= int(parts[2].replace('min', '')) ,
                          hours= int(parts[3].replace('h', '')), days= int(parts[4].replace('d', '')))


        difference_months = int(parts[5].replace('m', ''))
        difference_months += int(parts[6].replace('y', '')) * 12

        if parts[0] == "plus":
            original_datetime = original_datetime + delta
        else:
            original_datetime = original_datetime - delta
            difference_months *= -1

        original_datetime = add_months(original_datetime, difference_months)

        fixed_string = original_datetime.strftime("%Y:%m:%d %H:%M:%S")

        return fixed_string.split(' ')

    return event_date, event_time

def save_file_info(writer, root, file, fix_date):
    '''
    Extract the relevant information from an image and write it to the output stream
    :param writer:
    :param root:
    :param file:
    :return:
    '''
    fullPath = os.path.join(root, file)

    tags, date = extract_labels(fullPath)

    if tags == None:
        return [], "", "", ""

    event_date = ""
    event_time = ""
    if " " in date:
        event_date = date.split(" ")[0]
        event_time = date.split(" ")[1]

        if fix_date:
            event_date, event_time = fix_date_time(tags, event_date, event_time)

    path = path_to_list(root)

    if len(path) >= 2:
        # event_date
        writer.write(event_date)
        writer.write("\t")

        # event_time
        writer.write(event_time)
        writer.write("\t")

        # event_tags
        writer.write(", ".join(tags))
        writer.write("\t")

        # location_id
        writer.write(path[1])
        writer.write("\t")

        # recording_id
        writer.write(path[0])
        writer.write("\t")

        # full_path
        writer.write(os.path.join(root, file))
        writer.write("\n")

        return tags, event_date, event_time, path[1]
    return [], "", "", ""

def create_tag_tuples(tags):
    """
    Create all possible tuples out of a list of strings.
    The tuples have as the first element the smaller of the two
    :param tags:
    :return:
    """

    tags = list(dict.fromkeys(tags)) #Workaround for duplicate tags

    tuples = []

    if not tags:
        tuples.append(('', ''))
    if len(tags) == 1:
        tuples.append((tags[0], ''))

    for a in range(0, len(tags)):
        for b in range(a + 1, len(tags)):
            if a != b:
                tag_one = tags[a]
                tag_two = tags[b]
                if tag_one > tag_two:
                    tuples.append((tag_two, tag_one))
                else:
                    tuples.append((tag_one, tag_two))

    return tuples

def filter_on_prefix(tags, prefixes):
    """
    Remove all elements in a list that start with a certain prefix
    :param tags:
    :param prefixes:
    :return:
    """
    newList = []

    for tag in tags:
        found_prefix = False

        for prefix in prefixes:
            if prefix != '' and tag.startswith(prefix):
                found_prefix = True
                break

        if not found_prefix:
            newList.append(tag)

    return newList


def main():
    options = parse_args()

    if not options.output:
        print("No output file specified")
        return

    prefix_filters = options.filters.split(",")

    tags_info = {}
    tags_tuple_info = {}
    camera_counter = Counter()

    image_sets = []

    with open(os.path.join(options.output, "tags.tsv"), 'w') as output_file:

        output_file.write("event_date\tevent_time\tevent_tags\tlocation_id\trecording_id\tfull_path\n")

        for root, dirs, files in os.walk(options.input):
            path = root.split(os.sep)
            print((len(path) - 1) * 'XXX', os.path.basename(root))

            files = sorted(files)

            timestamped_images = []

            for file in files:

                filename, file_extension = os.path.splitext(file)

                if file_extension.lower() == ".jpg" or file_extension.lower() == ".jpeg":
                    tags, event_date, event_time, camera = save_file_info(output_file, root, file, options.fix_dates)

                    if camera != "":

                        if event_date and event_time:
                            image_timestamp = datetime.strptime(event_date + " " + event_time, "%Y:%m:%d %H:%M:%S")

                            timestamped_images.append((image_timestamp, os.path.join(root, file), tags))

                        camera_counter[camera] += 1

                        tags = filter_on_prefix(tags, prefix_filters)

                        for tag in tags:
                            counter = tags_info.get(tag, Counter())
                            counter[camera] += 1
                            tags_info[tag] = counter

                        for tup in create_tag_tuples(tags):
                            counter = tags_tuple_info.get(tup, Counter())
                            counter[camera] += 1
                            tags_tuple_info[tup] = counter

            def get_key(image):
                return image[0]

            timestamped_images = sorted(timestamped_images, key=get_key)

            last_image_timestamp = None
            current_set = []
            current_tags = set()

            for timestamp, image, tags in timestamped_images:

                if last_image_timestamp is not None:
                    time_diff = timestamp - last_image_timestamp
                    seconds_since_last_in_set = time_diff.total_seconds()

                    if seconds_since_last_in_set > options.max_time_difference:  # Start new group
                        image_sets.append((current_set, current_tags))
                        current_set = []
                        current_tags = set()

                    current_set.append(image)
                    current_tags.update(tags)

                else:
                    current_set.append(image)
                    current_tags.update(tags)

                last_image_timestamp = timestamp

                if len(current_set) >= options.max_group_size:
                    image_sets.append((current_set, current_tags))
                    current_set = []
                    current_tags = set()
                    last_image_timestamp = None

    print("TAGS")
    for tag in tags_info:
        print(tag + " " + str(tags_info[tag].most_common()))

    print("TUPLES")
    for tup in tags_tuple_info:
        print(str(tup) + " " + str(tags_tuple_info[tup].most_common()))

    print("CAMERAS")
    for camera in camera_counter:
        print(camera + " " + str(camera_counter[camera]))

    cameras = list(camera_counter.keys())

    #Save tag counter per camera
    with open(os.path.join(options.output, "camera_tag_count.tsv"), 'w') as output_file:

        output_file.write("Tag\t" + "\t".join(cameras) + "\n")

        for tag in tags_info:
            output_file.write(tag + "\t")

            for camera in cameras:
                output_file.write(str(tags_info[tag][camera]) + "\t")

            output_file.write("\n")

    # Save tag counter per camera
    with open(os.path.join(options.output, "camera_tuple_count.tsv"), 'w') as output_file:

        output_file.write("Tag\t" + "\t".join(cameras) + "\n")

        sorted_tuples = sorted(tags_tuple_info.keys())

        for tup in sorted_tuples:
            output_file.write(tup[0]+", " + tup[1] + "\t")

            for camera in cameras:
                output_file.write(str(tags_tuple_info[tup][camera]) + "\t")

            output_file.write("\n")

    #Save count per camera
    with open(os.path.join(options.output, "camera_infos.tsv"), 'w') as output_file:
        for camera in camera_counter:
            output_file.write(camera + "\t" + str(camera_counter[camera]) + "\n")

    # Save all image sets
    with open(os.path.join(options.output, "image_sets.tsv"), 'w') as output_file:

        output_file.write("Image count\tTag count\tImages and Tags\n")

        for image_set, set_tags in image_sets:
            output_file.write(str(len(image_set)) + "\t")
            output_file.write(str(len(set_tags)) + "\t")

            for image in image_set:
                output_file.write(image + "\t")

            for tag in set_tags:
                output_file.write(tag + "\t")
            output_file.write("\n")

if __name__ == "__main__":
    main()
