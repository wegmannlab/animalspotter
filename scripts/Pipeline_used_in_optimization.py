import os

import subprocess

import random
import sys, argparse, getpass
import hashlib

import argparse

from ImageTools import *
    
minBoxWeight = 0.1

def opencv_contours(binaryImage):
    return 0, 0, 0, 0

def saveBoxes(mypath, outputAnalysis, decoded, tags, imageTags):
    base = decoded.split(' : ')

    boxInfo = base[1].split(' ')

    size = base[0].split(' ')[3].split('/')

    width = int(size[0])
    height = int(size[1])

    objectIndex = -1

    if imageTags is not None:
        for t in imageTags:
            if t in tags:
                objectIndex = tags.index(t)
                break;

    offset = 0

    imageName = ""
    for part in boxInfo:
        if part.isdigit():
            break
        else:
            if offset > 0:
                imageName += " "

            imageName += part

        offset += 1

    labelOutput = os.path.join(mypath, imageName.rsplit('.', 1)[0] + ".txt")
    labelOutputRaw = os.path.join(outputAnalysis, imageName.rsplit('.', 1)[0] + ".raw.txt")

    with open(labelOutput, 'w') as output_file, open(labelOutputRaw, 'w') as output_raw:
        output_raw.write(imageName + '\n')

        for i in range(int(boxInfo[offset])):
            boxWeight = float(boxInfo[2 + i * 4 + offset])
            connected = float(boxInfo[3 + i * 4 + offset])
            foreground = float(boxInfo[4 + i * 4 + offset])

            box = boxInfo[1 + i * 4 + offset]

            boxWidth = int(box.split('x')[0])
            boxHeight = int(box.split('x')[1].split('+')[0])

            boxX = int(box.split('x')[1].split('+')[1])
            boxY = int(box.split('x')[1].split('+')[2])

            output_raw.write(str(boxWeight) + " " +
                            str(connected) + " " +
                            str(foreground) + " " +
                          str((boxX + boxWidth / 2.) / width) + " " +
                          str((boxY + boxHeight / 2.) / height) + " " +
                          str(float(boxWidth) / width) + " " +
                          str(float(boxHeight) / height) + '\n')

            if objectIndex >= 0 and boxWeight > minBoxWeight:
                output_file.write(str(objectIndex) + " " +
                                  str((boxX + boxWidth / 2.) / width) + " " +
                                  str((boxY + boxHeight / 2.) / height) + " " +
                                  str(boxWidth / width) + " " +
                                  str(boxHeight / height) + '\n')

def analyse(options, mypath, output, outputAnalysis, tags):
    params = [options['binary'], '-i', mypath + "/", '-o', output + "/", '-di',
                      '-v', '-mb', str(options['min_bin_size']), '-di']

    if options['burnin'] >= 0:
        params.append('-b')
        params.append(str(options['burnin']))

    if options['b_nr'] >= 0:
        params.append('-b_nr')
        params.append(str(options['b_nr']))

    if options['chainlength'] >= 0:
        params.append('-c')
        params.append(str(options['chainlength']))

    if options['threads'] > 0:
        params.append('-th')
        params.append(str(options['threads']))

    if options['alpha'] > 1: #AS takes the log of this to weight influence of neighbour pxls. Cause log of something between 0 and 1 is neg, this would not make any sense
        params.append('--alpha')
        params.append(str(options['alpha']))

    if options['beta'] > 1: #same reason as above with alpha
        params.append('--beta')
        params.append(str(options['beta']))

    if options['prior'] > 0:
        params.append('-p')
        params.append(str(options['prior']))

    if options['min_box_prob'] >= 0:
        params.append('-mpt')
        params.append(str(options['min_box_prob']))
        
    if options['std_background_means'] > 0:
        params.append('-u')
        params.append(str(options['std_background_means']))

    if options['std_background_sigma'] > 0:
        params.append('-w')
        params.append(str(options['std_background_sigma']))

    if options['std_foreground_means'] > 0:
        params.append('-x')
        params.append(str(options['std_foreground_means']))

    if options['std_foreground_sigma'] > 0:
        params.append('-y')
        params.append(str(options['std_foreground_sigma']))

    if options['maxbindiff'] > 0:
        params.append('-td')
        params.append(str(options['maxbindiff']))

    if options['rgb']:
        params.append('--color')
        params.append(options['rgb'])

    if options['reduce'] >= 0:
        params.append('-r')
        params.append(str(options['reduce']))

    if options['night'] >= 0:
        params.append('-gn')
        params.append(str(options['night']))

    if options['std_img_distortion'] >= 0:
        params.append('-im')
        params.append(str(options['std_img_distortion']))


    if options['heuristic'] >= 0:
        params.append('-he')
        params.append(str(options['heuristic']))

    if options['pxlFractionDist'] > 0 and options['pxlFractionDist']<=1:
        params.append('-pfd')
        params.append(str(options['pxlFractionDist']))

    if options['hmp']>0:
        params.append('-hmp')
        params.append(str(options['hmp']))

    if options['seed'] != -1:
        params.append('-s')
        params.append(str(options['seed']))

    if not options['no_bounding_boxes']:
        params.append('--bounding-boxes')

    if options['discard_very_small_bins']:
        params.append('--discardSmallBins')

    print(' '.join(params))

    process = subprocess.Popen(params, stdout=subprocess.PIPE)

    images = []

    for line in iter(process.stdout.readline, b''):  # replace '' with b'' for Python 3
        decoded = line.decode('UTF-8').strip('\n')

        print(decoded)

        if decoded.startswith('Bounding boxes'):
            base = decoded.split(' : ')

            boxInfo = base[1].split(' ')

            image = mypath

            offset = 0

            imageName = ""
            for part in boxInfo:
                if part.isdigit():
                    break
                else:
                    if offset > 0:
                        imageName += " "

                    imageName += boxInfo[offset]

                offset += 1

            image = os.path.join(image, imageName)

            images.append(image)

            imageTags = extract_labels(image)[0]

            saveBoxes(mypath, outputAnalysis, decoded, tags, imageTags)

    # get the return code of AS to check that it worked
    process.communicate()
    rc = process.returncode
    print('AS ended with return code', rc)
    if rc != 0:
        print('An error occured while running AnimalSpotter; this might be because loading the images used too much memory -- an estimate how much memory is needed using a specific input can be found in the file min_estimated_memory_needed.txt, located in the corresponding output directory (within',options.output.rstrip(' '), ')')
        raise ValueError('AnimalSpotter did not return 0')  # raise an error

    return images

def get_folders(outputBase, analysis, input):

    inputFolders = []
    outputFolders = []
    analysisFolders = []

    dirs = [x[0] for x in os.walk(input)]

    absolute_subdirs = []
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            absolute_subdirs.append(i)

    # now I have the whole path for the subdirs; but I only want to have the part that is not in input
    subdirs = []
    for absolute_subdir in absolute_subdirs:
        nr_subdirs = len(absolute_subdir.split('/')) - len(input.rstrip('/').split('/'))
        if nr_subdirs != 0:
            subdirs.append('/'.join(absolute_subdir.split('/')[-nr_subdirs:]))

    for folder in subdirs:
        inputFolders.append(os.path.join(input, folder))
        outputFolders.append(os.path.join(outputBase, folder))
        analysisFolders.append(os.path.join(analysis, folder))

    if not inputFolders:
        print("No subfolders found for specified input directory, falling back to only analyse input directory")
        inputFolders.append(input)
        outputFolders.append(outputBase)
        analysisFolders.append(analysis)


    return outputFolders, analysisFolders, inputFolders


def complete_dict(options):
    if 'heuristic' not in options:
        options['heuristic']=-1

    if 'pxlFractionDist' not in options:
        options['pxlFractionDist']=-1

    if 'binary' not in options:
        options['binary']=-'../cmake-build-debug/animalspotter'

    if 'threads' not in options:
        options['threads']=0

    if 'no_bounding_boxes' not in options:
        options['no_bounding_boxes']=True #faster and bb are not needed for optimization

    if 'discard_very_small_bins' not in options:
        options['discard_very_small_bins']=False

    if 'burnin' not in options:
        options['burnin']=-1

    if 'b_nr' not in options:
        options['b_nr']=-1

    if 'std_img_distortion' not in options:
        options['std_img_distortion']=-1

    if 'std_background_means' not in options:
        options['std_background_means'] = -1

    if 'std_foreground_means' not in options:
        options['std_foreground_means'] = -1

    if 'std_background_sigma' not in options:
        options['std_background_sigma'] = -1

    if 'std_foreground_sigma' not in options:
        options['std_foreground_sigma'] = -1

    if 'maxbindiff' not in options:
        options['maxbindiff'] = -1

    if 'chainlength' not in options:
        options['chainlength'] = -1

    if 'test_samples' not in options:
        options['test_samples'] = 0.8

    if 'prior' not in options:
        options['prior'] = -1

    if 'alpha' not in options:
        options['alpha'] = -1

    if 'beta' not in options:
        options['beta'] = -1

    if 'rgb' not in options:
        options['rgb'] = ''

    if 'min_bin_size' not in options:
        options['min_bin_size'] = 5

    if 'min_box_prob' not in options:
        options['min_box_prob'] = 0.35

    if 'reduce' not in options:
        options['reduce'] = 10
    
    if 'night' not in options:
        options['night'] = 0.75
    
    if 'seed' not in options:
        options['seed'] = -1
    if 'hmp' not in options:
        options['hmp']=-1

    return options

def main(options):

    if not os.path.exists(options['binary']):
        print("No valid binary specified, binary " + options['binary'] + " does not exist")
        return

    if not options['i']:
        print("You need to specify an input folder")
        return

    if not os.path.exists(options['i']):
        print("Specified input folder "+options['i'] +" does not exist")
        return

    print('current input is',options['i'])

    if not options['o']:
        print("You need to specify an output folder")
        return

    if not os.path.exists(options['o']):
        os.makedirs(options['o'])
        print('output dir has been generated')
    else:
        print('no output dir!')

    if not options['a']:
        print("You need to specify an analysis folder")
        return

    if options['a'] and not os.path.exists(options['a']):
        os.makedirs(options['a'])

    output, analysisFolders, inputPaths = get_folders(options['o'], options['a'], options['i'])

    tags = []


    if 'l' in options and os.path.exists(options['l']):
        with open(options['l']) as f:
            tags = f.readlines()
        tags = [x.strip() for x in tags]

    with open(os.path.join(options['a'], "dataset.names"), 'w') as output_file:
        for tag in tags:
            output_file.write(tag + '\n')

    paths = []

    options = complete_dict(options)
    for i in range(0, len(output)):
        if not os.path.exists(output[i]):
            os.makedirs(output[i])

        if not os.path.exists(analysisFolders[i]):
            os.makedirs(analysisFolders[i])

        paths.extend(analyse(options, inputPaths[i], output[i], analysisFolders[i], tags))

    random.shuffle(paths)

    split = int(len(paths) * options['test_samples'])

    train_data = paths[:split]
    test_data = paths[split:]

    with open(os.path.join(options['a'], "training.txt"), 'w') as output_file:
        for img in train_data:
            output_file.write(img + '\n')

    with open(os.path.join(options['a'], "test.txt"), 'w') as output_file:
        for img in test_data:
            output_file.write(img + '\n')




