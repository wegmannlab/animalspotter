import Pipeline_used_in_optimization
import apply_magick
from move_and_del_stuff import check_existence, delete_waste
from evaluation_pxl_level import evaluate_AS
from move_best_worst_output import move
from draw_mask import add_originals, draw_mask_for_interesting_AS_output
import os, glob, shutil
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


def main(assignmentsold, best_parameter_run, worker_nr, count=100, parallel=True):
    print('entered main evaluation function')
    assignments=dict(assignmentsold)
    iou_bool=assignments['iou']
    Euclidean=assignments['Euclidean']
    directory_with_AS_output_and_mask = assignments['path_to_sigopt_output']+str(worker_nr)+str(count)
    path_to_truth_mask = assignments['path_to_truth_mask']
    assignments['o']=assignments['o']+str(worker_nr)+str(count)
    assignments['a']=assignments['o']+'/analysis'
    print('analysis folder will be', assignments['a'])
    path_to_AS_output = assignments['o']
    os.mkdir(directory_with_AS_output_and_mask)
    temp_storage=assignments['i']
    scores=[]
    opt_dict={}
    total_data_size=0
    #list of directories within the specified input directory
    dirs = [x[0] for x in os.walk(assignments['i'])]
    subdirs=[]
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            subdirs.append(i)
    print('subdirs are', subdirs)
    for i in subdirs:
        assignments['i']=i
        print(assignments['o'], 'is current output dir')
        print('Running AS...')
        Pipeline_used_in_optimization.main(assignments)

        #move all AS output in same dir
        AS_output_dirs=[x[0] for x in os.walk(path_to_AS_output)]
        for AS_output_dir in AS_output_dirs:
            if AS_output_dir!=path_to_AS_output:
                for file in Path(AS_output_dir).glob('mcmc*.jpg.png'):
                    shutil.move(str(file), path_to_AS_output)


        # depending on parameters, it might be that I do not have any output of the AS (discard very small bins)
        nr_files = 0
        for file in Path(path_to_AS_output).glob('mcmc*.jpg.png'):
            nr_files += 1
        if nr_files == 0:
            print('No AS output generated for subdirectory',i)
            continue
        else:
            # move the mcmc files (AS output) and the mask files (ground truth) where we have both in the specified directory
            print('Moving (or in case of parallelisation: copying gt and moving AS output) files where we have ground truth and AS output into separate directory...')
            data_size=check_existence(path_to_AS_output, path_to_truth_mask, directory_with_AS_output_and_mask, parallel)
            if data_size==0:
                print('there is no ground truth for any of the AS output images')
                #return -1 if there is no ground truth for any of the data in assignments['i']
                if scores==[] and i==subdirs[-1]:
                    print('There is no ground truth for any of the AS output images for the suggested parameters in the whole input dir ')
                    shutil.rmtree(directory_with_AS_output_and_mask) #else making this dir in the next iteration of the sigopt optimization loop will throw an error
                    return(-1)
                assignments['i']=temp_storage
                continue

            # evaluation of AS accuracy:
            print('Evaluating AS accuracy...')
            if not best_parameter_run:
                score, scores_dict = evaluate_AS(directory_with_AS_output_and_mask, best_parameter_run, iou_bool, Euclidean)
                print(score)

                # delete the generated outputs (& move back the truth_mask_images); I do not want to completely waste my laptop
                print('Deleting generated waste and moving truth mask images back to initial directory (in case of parallelisation, delete previously copied gt files)...')
                delete_waste(path_to_AS_output, path_to_truth_mask, directory_with_AS_output_and_mask, parallel)
            else:
                score, opt_scores_dict = evaluate_AS(directory_with_AS_output_and_mask, best_parameter_run, iou_bool, Euclidean )
                opt_dict.update(opt_scores_dict)
                print(score)
        scores.append(score*data_size) #I want to calculate the weighted mean of the scores
        total_data_size+=data_size

    assignments['i'] = temp_storage
    if best_parameter_run:
        draw_mask_for_interesting_AS_output(directory_with_AS_output_and_mask)
        add_originals(directory_with_AS_output_and_mask, assignments['i'])
        print('Moving truth mask images back to initial directory (in case of parallelisation: delete the previously copied gt files), then generating a dictionary with the worst and best AS output, ...')
        #print(opt_dict)
        move(opt_dict, path_to_AS_output, path_to_truth_mask, directory_with_AS_output_and_mask, parallel)

        #see how accuracies are distributed
        plt.hist(opt_dict.values())
        plt.title('AS accuracy distribution')
        plt.xlabel('accuracy')
        plt.ylabel('number')
        plt.savefig(directory_with_AS_output_and_mask+'/AS_accuracy_distr.png')


    for i in range(len(scores)):
        scores[i]=scores[i]/total_data_size

    print('score is ', np.sum(scores))


    return (np.sum(scores))