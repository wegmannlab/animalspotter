import os, glob
import shutil
from pathlib import Path


def mk_daynight_dirs(input, img_paths, output_paths):
    #Note: images that are not in end-subfolders (not containing another subfolder) are ignored
    #make day & night folders on same level as input dir
    backup_original=input.rstrip('/')+'_original'
    os.system('cp -r {} {}'.format(input, backup_original))

    day_path='/'.join(input.rstrip('/').split('/')[:-1])+'/day_'+input.rstrip('/').split('/')[-1]
    night_path = '/'.join(input.rstrip('/').split('/')[:-1]) + '/night_'+input.rstrip('/').split('/')[-1]
    os.mkdir(day_path)
    os.mkdir(night_path)



    # img_paths= the whole path for the subdirs; but I only want to have the part that is not in input
    subdirs = []
    for i in range(len(img_paths)):

        # first get a list of day and night imgs
        dn_file_path=output_paths[i].rstrip('/')+'/daynight.txt'
        count = 0
        with open(dn_file_path, 'r') as file:
            for line in file:
                count += 1
                if count == 2:
                    night = line.rstrip().split(';')
                if count == 4:
                    day = line.rstrip().split(';')

        #make the directory structure in the day and night dir same as in the input dir
        absolute_subdir=img_paths[i]
        nr_subdirs = len(absolute_subdir.split('/')) - len(input.rstrip('/').split('/'))
        if nr_subdirs != 0:
            subdirs.append('/'.join(absolute_subdir.split('/')[-nr_subdirs:]))
        absolute_subdir = Path(absolute_subdir)
        assert absolute_subdir.exists() and absolute_subdir.is_dir()
        if subdirs == []:
            dir_to_put_day_img = day_path
            dir_to_put_night_img=night_path
        else:
            #create all subdirs for day
            dir_to_put_day_img = day_path + '/' + subdirs[-1]
            count = 0
            new_day_dir = day_path
            while not Path(dir_to_put_day_img).exists():
                new_day_dir = new_day_dir + '/' + subdirs[-1].split('/')[count]
                if not Path(new_day_dir).exists():
                    os.mkdir(new_day_dir)
                count += 1

            #create all subdirs for night
            dir_to_put_night_img = night_path + '/' + subdirs[-1]
            count=0
            new_night_dir=night_path
            while not Path(dir_to_put_night_img).exists():
                new_night_dir = new_night_dir + '/' + subdirs[-1].split('/')[count]
                if not Path(new_night_dir).exists():
                    os.mkdir(new_night_dir)
                count += 1

        # for each img in the current subfolder, move it in the right folder (corresponding folder in day or night parent folder)
        for img in absolute_subdir.glob('*.jpg'):
            img_name = str(img).split('/')[-1]  # because img is the img name with the absolute path
            if img_name in night:
                shutil.move(img, dir_to_put_night_img+'/'+img_name)
            elif img_name in day:
                shutil.move(img, dir_to_put_day_img+'/'+img_name)

    #remove the now empty input folder
    os.system('rm -r {}'.format(input))
    #rename the backup of the input folder to the initial input name
    os.system('mv {} {}'.format(backup_original, input))
    print('All night images of the initial input were copied to {}, all day images were copied to {}'.format(night_path, day_path))

####################################################################################33
#to call this script from here:

#input='/home/dona/Documents/selected_imgs'
#img_paths=['/home/dona/Documents/selected_imgs/6-402810N_24-045610E_aoe_bag_ssr/2016-03-11_to_2016-03-14_15_16', '/home/dona/Documents/selected_imgs/6-402810N_24-045610E_aoe_bag_ssr/2016-02-18_to_2016-03-04_15_16', '/home/dona/Documents/selected_imgs/6-283073N_24-058211E_ccf_dry_ser/2016-02-10_to_2016-03-12_16_02', '/home/dona/Documents/selected_imgs/6-406080N_24-070680E_osw_dsc_dsc/2016-02-11_to_2016-03-14-15_10', '/home/dona/Documents/selected_imgs/6-348367N_24-063000E_osw_dsc_dsc/2012-03-29_to_2012-04-05_B23', '/home/dona/Documents/selected_imgs/6-447062N_24-058630E_osw_dsc_dsc/2016-02-19_to_2016-02-26-16_07', '/home/dona/Documents/selected_imgs/6-483499N_24-096579E_ccf_dry_ser/2016-03-01_to_2016-03-04_16_13', '/home/dona/Documents/selected_imgs/6-483499N_24-096579E_ccf_dry_ser/2016-02-26_to_2016-03-01_16_13', '/home/dona/Documents/selected_imgs/6-372172N_24-137423E_ccf_riv_ser/2016-02-25_to_2016-03-13_16_30', '/home/dona/Documents/selected_imgs/6-304117N_24-077667E_osw_dsc_dsc/2012-03-05_to_2012-03-29_B30', '/home/dona/Documents/selected_imgs/6-409809N_24-142780E_ccf_riv_gaf/2016-02-21_to_2016-03-15_16_01', '/home/dona/Documents/selected_imgs/6-409809N_24-142780E_ccf_riv_gaf/2016-03-15_to_2016-03-30_16_01']
#output_paths=['/home/dona/Documents/AS_waste/6-402810N_24-045610E_aoe_bag_ssr/2016-03-11_to_2016-03-14_15_16', '/home/dona/Documents/AS_waste/6-402810N_24-045610E_aoe_bag_ssr/2016-02-18_to_2016-03-04_15_16', '/home/dona/Documents/AS_waste/6-283073N_24-058211E_ccf_dry_ser/2016-02-10_to_2016-03-12_16_02', '/home/dona/Documents/AS_waste/6-406080N_24-070680E_osw_dsc_dsc/2016-02-11_to_2016-03-14-15_10', '/home/dona/Documents/AS_waste/6-348367N_24-063000E_osw_dsc_dsc/2012-03-29_to_2012-04-05_B23', '/home/dona/Documents/AS_waste/6-447062N_24-058630E_osw_dsc_dsc/2016-02-19_to_2016-02-26-16_07', '/home/dona/Documents/AS_waste/6-483499N_24-096579E_ccf_dry_ser/2016-03-01_to_2016-03-04_16_13', '/home/dona/Documents/AS_waste/6-483499N_24-096579E_ccf_dry_ser/2016-02-26_to_2016-03-01_16_13', '/home/dona/Documents/AS_waste/6-372172N_24-137423E_ccf_riv_ser/2016-02-25_to_2016-03-13_16_30', '/home/dona/Documents/AS_waste/6-304117N_24-077667E_osw_dsc_dsc/2012-03-05_to_2012-03-29_B30', '/home/dona/Documents/AS_waste/6-409809N_24-142780E_ccf_riv_gaf/2016-02-21_to_2016-03-15_16_01', '/home/dona/Documents/AS_waste/6-409809N_24-142780E_ccf_riv_gaf/2016-03-15_to_2016-03-30_16_01']

#mk_daynight_dirs(input, img_paths, output_paths)