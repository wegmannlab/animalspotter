'''This script serves to split the ground truth data of images not containing animals from the others.
This is needed to understand the accuracies obtained better (got 2 peaks, 1 for low and 1 for high accuracies -- maybe
accuracies are low just for empty images?)
Thereafter, AS_opt_sigopt_best.py can be run once using the gt of empty imgs, and once using the gt of imgs with animals'''

###VARIABLES TO CHANGE!
gt_path='/home/dona/Documents/all'

###########################################################################
import os, glob
import json
from pathlib import Path

directory_emptyimgs=gt_path+"_empty"
directory_animalimgs=gt_path+"_with_animals"
os.mkdir(directory_emptyimgs)
os.mkdir(directory_animalimgs)

for file in Path(gt_path).glob('*json'):
    with file.open() as f:
        content = json.load(f)
    annotation=content['annotations']
    if annotation==[]:
        os.system('cp {} {}'.format(str(file), directory_emptyimgs))
    else:
        os.system('cp {} {}'.format(str(file), directory_animalimgs))

