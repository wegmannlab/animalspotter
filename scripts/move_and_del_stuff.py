
import glob, os
import shutil
from pathlib import Path


def check_existence(path_to_AS_output, path_to_truth_mask,directory_to_put_them, parallel):
    '''Puts all AS output images where I have a ground truth mask in a separate folder, together with the ground truth masks
    Returns the nr of images that fulfill this condition
    This function is necessary as we do not have a ground truth mask for all images'''
    ids_AS=[]
    for file in Path(path_to_AS_output).glob('mcmc*.png'):
        filename = str(file).split('/')[-1]
        ids_AS.append(filename.split('_')[9])
    ids_mask = []
    for file in Path(path_to_truth_mask).glob('*.json'):
        filename = str(file).split('/')[-1]
        ids_mask.append(filename.split('_')[0])
    #get intersection, i.e. id of images where we have AS output
    #and the truth mask
    ids=list(set(ids_AS) & set(ids_mask))
    if len(ids)==0:
        return(len(ids))
    if not os.path.exists(directory_to_put_them):
        os.mkdir(directory_to_put_them)
    for file in Path(path_to_AS_output).glob('mcmc*.jpg.png'):
        filename = str(file).split('/')[-1]
        if filename.split('_')[9] in ids:
            shutil.move(str(file), directory_to_put_them)
    for file in Path(path_to_truth_mask).glob('*.json'):
        filename = str(file).split('/')[-1]
        if filename.split('_')[0] in ids:
            if parallel:
                if '(' in filename:  # because os.system cannot handle a paranthesis in the file name (gives a syntax error and skips these files)--> I rename them, apply imagemagick, name them back
                    initial_img_name = str(file)
                    new_img_name = str(file).replace('(', '')
                    new_img_name = new_img_name.replace(')', '_renamed')
                    os.rename(initial_img_name, new_img_name)

                    os.system('cp {} {}'.format(new_img_name, directory_to_put_them))
                    #files are not given back their initial names because this might disturb during parallelization
                    #os.rename(directory_to_put_them + '/' + new_img_name, directory_to_put_them + '/' + initial_img_name)
                    #os.rename(path_to_truth_mask.rstrip('/')+'/'+new_img_name, path_to_truth_mask.rstrip('/')+'/'+initial_img_name)
                else:
                    #print(str(file))
                    #print(directory_to_put_them)
                    os.system('cp {} {}'.format(str(file), directory_to_put_them))

            else:
                shutil.move(str(file), directory_to_put_them)
    return(len(ids))

def delete_waste(path_to_AS_output, path_to_truth_mask, directory_to_put_them, parallel):
    '''moves the ground truth images back in their initial folder, then removes the generated AS output
    This is necessary during optimization as I do not want to store the output of all the non-optimal AS runs'''
    print('Dir with AS output that is deleted', path_to_AS_output)
    shutil.rmtree(path_to_AS_output)
    if parallel:
        print('dir with sigopt output that is deleted', directory_to_put_them)
        shutil.rmtree(directory_to_put_them)
    else:
        for file in Path(directory_to_put_them).glob('*json'):
            shutil.move(str(file), path_to_truth_mask)
        shutil.rmtree(directory_to_put_them)

