'''This script should be run from the command line. Please enter the detected best parameters in the dictionary
'best_assignments' within this script; then, the accuracy will be calculated and the results will automatically be generated
Please note that the parameter parallel within this script is set to True even though it is not parallelized; if you are absolutely sure, that
no other script is using the same gt data, you can change it to False to increase speed (then, gt data will be moved forth and back during the accuracy
evaluations, instead of copying them)
'''
from sigopt import Connection
import AS_evaluation
import argparse
from pathlib import Path
import json

################
#just to run with chosen parameters, in case I would like to test something or to just get an impression how good it is
###############

def evaluate_model(assignments, best_parameter_run):
    return AS_evaluation.main(assignments, best_parameter_run, '', count=100, parallel=True)
    #in case you are absolutely certain that you are the only one using the gt files at the moment, you can change parallel to False
    # (then they'll be moved during the evaluation; in the end they'll be moved back)

def main(static_input):
    if static_input['iou']:
        static_input['Euclidean']=False
    else:
        static_input['Euclidean']=True
    # add burnin specification: use only the last 500-1000 iterations if there are enough mcmc steps
    if static_input['chainlength'] > 1150:
        static_input['b_nr'] = 4 #burnin nr
        static_input['burnin'] = (static_input['chainlength'] - 750) / 4
    elif static_input['chainlength'] > 700:
        static_input['b_nr'] = 4
        static_input['burnin'] = (static_input['chainlength'] - 300) / 4
    else:
        static_input['b_nr'] = 1  # to short mcmc for multiple burnins
        static_input['burnin'] = int(static_input['chainlength'] / 2)

    # best observation for day ccf mcmc 10'000 (accuracy: 0.395397)
    best_assignments={'heuristic':0.15941302350913486, 'reduce':1, 'min_bin_size':6, 'maxbindiff':665, 'pfd':0.7521391469797809, 'alpha':1.2199076152435249, 'beta':0.5926047527947509}

    #best observation for night ccf mcmc 10'000
    #best_assignments={'heuristic':0.053184065324417745, 'reduce':1, 'min_bin_size':6, 'maxbindiff':270, 'pfd':0.9487172509976204, 'alpha':2.324489394965603, 'beta':0.7381887663518523}

    #best observation for day osw mcmc 10'000
    #best_assignments = {'heuristic': 0.21200557539012055, 'reduce': 1, 'min_bin_size': 6, 'maxbindiff':1074 ,'pfd': 0.8116080647373508, 'alpha': 1.3839920643595622, 'beta': 1.0189540771023597}

    #best observation for night osw mcmc 10'000
    #best_assignments = {'heuristic': 0.05, 'reduce': 1, 'min_bin_size':3 , 'maxbindiff': 10,'pfd':1.0 , 'alpha': 2.5, 'beta': 0.5}

    #use maxbindiff 10 and min bin size 10 to increase the influence of merging

    best_parameter_run=True
    static_input.update(best_assignments)
    input = static_input
    value=evaluate_model(input, best_parameter_run)
    print('best is', value)
    print('Now you can have a look at the worst and best AS outputs (directories \' worst \' and \' best \' in the directory', input['path_to_sigopt_output'],'to see which images are problematic')

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--path_to_truth_mask',
                        help='Path to where the dataset is located in Darwin format',
                        required=True,
                        type=str)
    parser.add_argument('-i',
                        help='Path to the folder which contains the animal spotter input',
                        required=True,
                        type=str)
    parser.add_argument('-o',
                        help='Path to a temporary folder where AS output goes, no one cares',
                        required=True,
                        type=str)
    parser.add_argument('--path_to_sigopt_output',
                        help='Path to not yet existent folder',
                        required=True,
                        type=str)
    parser.add_argument('--binary',
                        help='Path to animalspotter executable',
                        required=True,
                        type=str)

    parser.add_argument('--chainlength',
                        help='Length of the mcmc',
                        required=True,
                        type=int)
    parser.add_argument('--iou',
                        help='Score to evaluate accuracy is the iou; if False, score to evaluate is euclidean dist',
                        required=True,
                        type=bool)

    args = parser.parse_args()

    # run the actual code
    main(vars(args))
