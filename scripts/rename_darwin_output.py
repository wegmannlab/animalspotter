'''This script is to rename the exported json files from darwin v7 to what is written inside them, under \'original filename\''''

###########################################################
#ADAPT THIS!!!
#path to where the darwin output is
path='/home/dona/Documents/selected_imgs/gt/all/'
############################################################

import os, glob, json

os.chdir(path)
for file in glob.glob('*.json'):
    with open(file, 'r') as f:
        f = json.load(f)
        original_filename = f['image']['original_filename']
        original_filename=original_filename.strip('jpg')+'json'


        path_to_file = path + file
        new_path_to_file = path + original_filename
        os.rename(path_to_file, new_path_to_file)