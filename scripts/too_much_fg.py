'''This script was written to check whether pxls that are fg in too many imgs of a bin
(in more than 85%;this value was chosen in the heuristics calculation in AS) are wrongly classified as fg by the AS
(This was though not the case for the tested input)'''

###ADAPT THIS!!!
darwin_path = "/home/dona/Documents/all"
imgs_path = "/home/dona/Documents/AS_out_best_osw_day100" #should contain all mcmc output!
newData='/home/dona/Documents/too_much_fg_test/' #not yet existing folder
#############################################################################################################################3

#const
max_images_foreground = 0.85
#in the heuristics, a pxl can only be fg in at most 85% of all imgs in the bin; else it is classified as bg everywhere.

##############
import numpy as np
from PIL import Image
import json
import shutil
import glob,os
from pathlib import Path
from evaluation_pxl_level import convert_polygons_to_mask




def make_CT_dirs(newData):
    CT_dirs=[]
    ids_by_CT={}
    for file in Path(newData).glob('mcmc*jpg.png'):
        file=str(file)
        filename=file.split('/')[-1]
        id=filename.split('_')[9]
        CT='_'.join(filename.split('_')[10:-2])
        assert CT[0]=='6' and CT.split('_')[0][-1]=='N'
        CT_dir = newData + CT+'/'
        if CT_dir not in CT_dirs:
            os.mkdir(CT_dir)
            CT_dirs.append(CT_dir)
            ids_by_CT[CT_dir]=[id]
        else:
            ids_by_CT[CT_dir].append(id)
        shutil.move(file, CT_dir)
    print(newData)
    for file in Path(newData).glob('*.json'):
        filename = str(file).split('/')[-1]
        id = filename.split('_')[0]
        for CT_dir in ids_by_CT.keys():
            if id in ids_by_CT[CT_dir]:
                shutil.move(str(file), CT_dir)
                break
    return CT_dirs

def make_bin_dirs(CT_dir):
    bin_dirs=[]
    ids_by_bin = {}
    for file in Path(CT_dir).glob('mcmc*.jpg.png'):
        filename = str(file).split('/')[-1]
        id = filename.split('_')[9]
        binID = filename.split('(bin')[-1]
        binID = binID.split(')')[0]
        bin_dir=CT_dir+binID
        if bin_dir not in bin_dirs:
            os.mkdir(bin_dir)
            bin_dirs.append(bin_dir)
            ids_by_bin[bin_dir]=[id]
        else:
            ids_by_bin[bin_dir].append(id)
        shutil.move(str(file), bin_dir)

    for file in Path(CT_dir).glob('*json'):
        filename = str(file).split('/')[-1]
        id = filename.split('_')[0]
        for bin_dir in ids_by_bin.keys():
            if id in ids_by_bin[bin_dir]:
                shutil.move(str(file), bin_dir)
                break
    return(bin_dirs)


def main(darwin_path, AS_output_path, newData):
    os.mkdir(newData)
    all_ids = []
    for file in Path(AS_output_path).glob('mcmc*.jpg.png'):
        shutil.move(str(file), newData)
        filename = str(file).split('/')[-1]
        id=filename.split('_')[9]
        all_ids.append(id)
    print(len(all_ids))
    for file in Path(darwin_path).glob('*.json'):
        filename = str(file).split('/')[-1]
        id=filename.split('_')[0]
        if id in all_ids:
            os.system('cp {} {}'.format(file, newData))
    CT_dirs = make_CT_dirs(newData)
    for CT_dir in CT_dirs:
        count = 0
        annotation_present = False
        bin_dirs=make_bin_dirs(CT_dir)
        first_bin = True
        for bin_dir in bin_dirs:

            # get a dictionary with id as key and the ground truth json file as value
            annotations = {}
            for annotation_file in Path(bin_dir).glob("*.json"):
                with annotation_file.open() as f:
                    annotation = json.load(f)
                image_id = str(annotation['image']['original_filename'].split("_")[0])
                annotations[image_id] = annotation
            if len(annotations) == 0:
                continue

            # get all AS output files
            AS_files = list(Path(bin_dir).glob("mcmc*.jpg.png"))
            #print('length of AS files is', len(AS_files))
            # for each mcmc img, get annotation
            for AS_file_nr in range(len(AS_files)):
                AS_file_whole_path = AS_files[AS_file_nr]
                AS_file = str(AS_files[AS_file_nr]).split('/')[-1]
                image_id = AS_file.split('_')[9]
                if image_id not in list(annotations):  # if there is no gt, continue to next AS output file
                    # print('no annotation')
                    if AS_file_nr + 1 == len(AS_files) and annotation_present == False:
                        print('no annotation for any of the input mcmc files found; current input was:', bin_dir)
                        break
                    continue
                annotation_present = True
                annotation = annotations[image_id]
                count = count + 1

                # Extract all the polygons
                polygons = [a['polygon']['path'] for a in annotation['annotations'] if 'polygon' in a]

                # Create binary mask with the polygons contained in the annotation
                mask = convert_polygons_to_mask(
                    polygons=polygons,
                    height=annotation['image']['height'],
                    width=annotation['image']['width']
                )

                # Load the jpg.png file and resize the mask to be matching
                AS_img = Image.open(AS_file_whole_path)
                mask = mask.resize(AS_img.size)

                # Create booleans masks
                AS_array = np.array(AS_img)
                AS_array = np.divide(AS_array, 255)  # pixel values from 0 to 255
                mask_array = np.array(mask)
                mask_array = np.divide(mask_array, 255)
                assert AS_array.shape == mask_array.shape

                right_classification_mask = np.zeros(mask_array.shape)
                right_classification_mask[(np.where((AS_array > 0.5) & (mask_array == 1)))] = 1
                if count == 1:
                    right_classification = np.zeros(mask_array.shape)
                right_classification += right_classification_mask

                misclassification_mask = np.zeros(mask_array.shape)
                misclassification_mask[np.where((AS_array > 0.5) & (mask_array == 0))] = 1
                if count == 1:
                    misclassification = np.zeros(mask_array.shape)
                misclassification += misclassification_mask

            if not annotation_present:
                continue

            if first_bin:  # first iteration: cond_accuracy must be created first
                cond_right_classification = np.zeros(mask_array.shape)
                cond_misclassification = np.zeros(mask_array.shape)
                fg_in_AS = right_classification + misclassification
                nr_of_AS_files = len(AS_files)
            else:
                fg_in_AS += misclassification
                fg_in_AS+= right_classification
                nr_of_AS_files += len(AS_files)
            max_fg_imgs = nr_of_AS_files * max_images_foreground
            cond_right_classification[np.where(fg_in_AS > max_fg_imgs)] += right_classification[
                np.where(fg_in_AS > max_fg_imgs)]
            cond_misclassification[np.where(fg_in_AS > max_fg_imgs)] += misclassification[
                np.where(fg_in_AS > max_fg_imgs)]
            first_bin = False
        if not annotation_present:
            continue

        # output that for every CT differently because they might contain imgs of different sizes
        if np.sum(cond_right_classification) + np.sum(cond_misclassification) == 0:
            print('No pxl has been classified as fg too often within the CT', CT_dir.split('/')[-1])
            continue

        cond_prob = np.sum(cond_right_classification) / (
                    np.sum(cond_right_classification) + np.sum(cond_misclassification))
        prob = np.sum(right_classification) / (np.sum(right_classification) + np.sum(misclassification))

        print('Current CT:' + CT_dir + 'prob to be fg if classified as fg in too many imgs is ' + str(cond_prob))
        print('Current CT:' + CT_dir + ' prob to be fg if classified as fg is ' + str(prob))


main(darwin_path, imgs_path, newData)