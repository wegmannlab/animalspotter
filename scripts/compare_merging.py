'''This script is to compare the old merging with the new merging (faster one, but based on rough distances)
Use the AS version on branch roughDistanceTesting to obtain the Merging1.txt and Merging2.txt file for each AS output folder
Put all these Merging*.txt files in a separate folder, that can then be used as input in this script.
When running AS, use the parameters td (maxbindifference) =10, minbinsize=20 (--> merging becomes important)
pfd=1 (faster)
(mcmc length can be set to 0 as merging is done independent from the mcmc)
'''

##ADAPT THIS!!
mypath='/home/dona/Documents/compareMerging/minbinsize20/ccf' #path to all txt files containing the merging info (this input dir was created manually, all the files were intially located in the corresponding AS output dirs)
#mypath='/home/dona/Documents/compareMerging/minbinsize20/osw'
###############################################################################33


import os, glob
import matplotlib.pyplot as plt

roughDistances=[]
roughIndecesA=[]
roughIndecesB=[]
exactDistances=[]
exactIndecesA=[]
exactIndecesB=[]

count=-1
os.chdir(mypath)
for file in glob.glob("Merging*.txt"):
    with open(file, 'r') as old:
        for line in old:
            count+=1
            if '(' in line:
                distance=line.split('(')[1]
                distance=distance.split(')')[0]
                binA=int(line.split(' ')[1])
                binB=int(line.split(' ')[2])
                if binA==-1:
                    continue
                if count%2==0:
                    exactDistances.append(float(distance))
                    exactIndecesA.append(binA)
                    exactIndecesB.append(binB)
                else:
                    roughDistances.append(float(distance))
                    roughIndecesA.append(binA)
                    roughIndecesB.append(binB)



true=0
wrong=0
for i in range(len(roughIndecesB)):
    if roughIndecesA[i]==exactIndecesA[i] and roughIndecesB[i]==exactIndecesB[i]:
        true+=1
    elif roughIndecesA[i]==exactIndecesB[i] and roughIndecesB[i]==exactIndecesA[i]:
        true+=1
    else:
        wrong+=1


print('true are', true)
print('wrong are', wrong)
print('total', true+wrong)

#plot distances
labels = ['using exact distances', 'using rough distances']
plt.hist(exactDistances, bins=50, alpha=0.5)
plt.hist(roughDistances, bins=50,  alpha=0.5, color='r')
plt.xlim([0,0.15])
plt.legend(labels)
#plt.xlim(0,1)
plt.xlabel('Distance')
plt.ylabel('Quantity')
plt.title('Distances between merged bins')
plt.show()

