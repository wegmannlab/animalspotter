'''parallelized parameter optimization of all parameters considered to be important'''
from sigopt import Connection
import time
import argparse
from rescaling import rescale
import threading
import AS_evaluation
import json


class Master(threading.Thread):
    """
    Shows what a master machine does when running SigOpt in a distributed setting.
    """

    def __init__(self, static_input):  # gets called when an object of this class is created
        """
        Initialize the master thread, creating the SigOpt API connection and the experiment.
        We use the observation_budget field on the experiment to keep track of approximately
        how many total Observations we want to report. We recommend using a number between 10-20x
        the number of parameters in an experiment.
        """
        threading.Thread.__init__(self)
        # Create the SigOpt connection
        self.conn = Connection(client_token="CQRRDMSHOYYMIQYMKKLBKACDNQJAMWHPGRXCQQNRARFMHLNR")
        experiment = self.conn.experiments().create(
            project="as_optimization",
            name='AS_optimization',
            parameters=[
                dict(name='alpha', type='double', bounds=dict(min=0.5, max=2.5)),
                dict(name='beta', type='double', bounds=dict(min=0.5, max=2.0)),
                dict(name='min_bin_size', type='int', bounds=dict(min=3, max=10)),
                dict(name='maxbindiff', type='int', bounds=dict(min=10, max=1800)),
                # note: in case opt is large maxbindiff, the others will probably not be optimized (cause they're not important in that case)
                dict(name='pfd', type='double', bounds=dict(min=0.4, max=1.0)),
                dict(name='heuristic', type='double', bounds=dict(min=0.05, max=0.4)),
            ],
            metrics=[dict(name='accuracy', objective='maximize')],
            observation_budget=90,
            parallel_bandwidth=static_input['nr_workers'],
        )
        print(
            "View your experiment progress: https://app.sigopt.com/client/10966/project/as_optimization/experiments/{}".format(
                experiment.id))
        self.experiment_id = experiment.id
        self.static_input = static_input

    @property
    def remaining_observations(
            self):  # this function is called every time when we want to get the var remaining observation (due to the @property decorater)
        """
        Re-fetch the experiment and calculate how many Observations we need to run until
        we reach the observation budget
        """
        experiment = self.conn.experiments(self.experiment_id).fetch()
        return experiment.observation_budget - experiment.progress.observation_count

    def run(self):
        """
        Attempt to run NUM_WORKERS worker machines. If any machines fail, retry up to
        three times, deleting openSuggestions before proceeding.
        """
        tries = 3
        while (tries > 0 and self.remaining_observations > 0):
            workers = [Worker(self.experiment_id, self.static_input) for _ in
                       range(self.static_input['nr_workers'])]  # changed xrange to range!!!
            for worker in workers:
                worker.start()
            for worker in workers:
                worker.join()
            self.conn.experiments(self.experiment_id).suggestions().delete(state='open')
            tries -= 1


class Worker(threading.Thread):
    """
    Shows what a worker machine does when running SigOpt in a distributed setting.
    """

    def __init__(self, experiment_id, static_input):
        """
        Initialize a worker thread, creating the SigOpt API connection and storing the previously
        created experiment's id
        """
        threading.Thread.__init__(self)
        self.experiment_id = experiment_id
        # Create the SigOpt connection
        self.conn = Connection(client_token="CQRRDMSHOYYMIQYMKKLBKACDNQJAMWHPGRXCQQNRARFMHLNR")
        self.static_input = dict(
            static_input)  # copy of static_input; else, different workers are changing the same dictionary

    @property
    def metadata(self):
        """
        Use metadata to keep track of the host that each Suggestion and Observation is created on.
        Learn more: https://sigopt.com/docs/overview/metadata
        """
        return dict(
            host=threading.current_thread().name)  # by default, a unique name is constructed of the form “Thread-N” where N is a small decimal number

    @property
    def remaining_observations(self):
        """
        Re-fetch the experiment and calculate how many Observations we need to run until
        we reach the observation budget
        """
        experiment = self.conn.experiments(self.experiment_id).fetch()  # Update the experiment object
        return experiment.observation_budget - experiment.progress.observation_count

    def run(self):
        """
        SigOpt acts as the scheduler for the Suggestions, so all you need to do is run the
        optimization loop until there are no remaining Observations to be reported.
        We handle exceptions by reporting failed Observations. Learn more about handling
        failure cases: https://sigopt.com/docs/overview/metric_failure
        """
        while self.remaining_observations > 0:  # within getting the remaining observations var, the exp object is updated; see above
            suggestion = self.conn.experiments(self.experiment_id).suggestions().create(
                metadata=self.metadata)  # Receive a Suggestion
            assignments = str(suggestion.assignments).split('(')[1].split(')')[0]
            assignments = json.loads(assignments)
            self.static_input.update(assignments)
            try:
                print('Try evaluating metric')
                print('input is', self.static_input)
                print('entry for worker nr is ', self.metadata['host'])
                print('entry for count is',
                      self.conn.experiments(self.experiment_id).fetch().progress.observation_count)
                # Evaluate Your Metric
                value = AS_evaluation.main(self.static_input, best_parameter_run=False, worker_nr=self.metadata['host'],
                                           count=self.conn.experiments(
                                               self.experiment_id).fetch().progress.observation_count, parallel=True)
                failed = False
            except Exception:
                print('Exception; evaluating metric did not work')
                value = None
                failed = True

            # Report an Observation
            # Include the hostname so that you can track
            # progress on the web interface
            self.conn.experiments(self.experiment_id).observations().create(
                suggestion=suggestion.id,
                value=value,
                failed=failed,
                metadata=self.metadata,
            )


def set_parameters(static_input):
    if static_input['iou']:
        static_input['Euclidean'] = False
    else:
        static_input['Euclidean'] = True
    # add burnin specification: use only the last 500-1000 iterations if there are enough mcmc steps
    if static_input['chainlength'] > 1150:
        static_input['b_nr'] = 4  # burnin nr
        static_input['burnin'] = (static_input['chainlength'] - 750) / 4
    elif static_input['chainlength'] > 700:
        static_input['b_nr'] = 4
        static_input['burnin'] = (static_input['chainlength'] - 300) / 4
    else:
        static_input['b_nr'] = 1  # to short mcmc for multiple burnins
        static_input['burnin'] = int(static_input['chainlength'] / 2)
    if static_input['reduce'] != 1:
        rescale(static_input)

    return static_input


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--path_to_truth_mask', help='Path to where the dataset is located in Darwin format',
                    required=True,
                    type=str)
parser.add_argument('-i', help='Path to the folder which contains the animal spotter input',
                    required=True,
                    type=str)
parser.add_argument('-o', help='Path to a temporary folder where AS output goes, no one cares',
                    required=True,
                    type=str)
parser.add_argument('--path_to_sigopt_output',
                    help='Path to not yet existent temporary folder, no one cares',
                    required=True,
                    type=str)
parser.add_argument('--binary',
                    help='Path to animalspotter executable',
                    required=True,
                    type=str)
parser.add_argument('--chainlength',
                    help='Length of the mcmc',
                    required=False,
                    type=int)
parser.add_argument('--nr_workers', help='Nr of workers for parallelisation', required=True, type=int)
parser.add_argument('--iou',
                    help='Score to evaluate accuracy is the iou; if False, score to evaluate is euclidean dist',
                    required=True,
                    type=bool)
parser.add_argument('-reduce',
                    help='Factor by which the imgs are reduced --> AS will run faster',
                    required=True,
                    type=int)

args = parser.parse_args()

# run the actual code
start = time.time()  # the starting time
static_input = set_parameters(vars(args))
master = Master(static_input=static_input)
master.start()
master.join()
end = time.time()
runtime = end - start
print('The runtime of the program is: {}s'.format(runtime))
