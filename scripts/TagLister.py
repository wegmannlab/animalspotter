import argparse

from collections import Counter

from ImageTools import *

"""
This script extract all labels from the pictures in a specific folder and its subfolders
"""

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        default="",
        type=str,
        help="Input folder",
    )

    parser.add_argument(
        "-f",
        "--filter",
        dest="filter",
        default="",
        type=str,
        help="Only analyse pictures with a certain tag",
    )

    return parser.parse_args()

def get_folders(input):
    """
    Return the subfolder in the input folder
    :param input:
    :return:
    """

    inputFolders = []

    for folder in next(os.walk(input))[1]:
        inputFolders.append(input + folder + "/")

    inputFolders.append(input)

    return inputFolders

def main():
    options = parse_args()

    inputFolders = get_folders(options.input)

    counter = Counter()

    for folder in inputFolders:
        print("Analyse folder " + folder)
        for file in os.listdir(folder):
            labels = extract_labels(folder + file)[0]

            if (not options.filter or options.filter in labels) and labels != None:
                for label in labels:
                    counter[label] += 1

    labels = []

    for key in counter:
        labels.append((key, counter[key]))

    sorted_labels = sorted(labels, key=lambda tup: tup[1])

    for value in sorted_labels:
        print(value[0] + " " + str(value[1]))

if __name__ == "__main__":
    main()
