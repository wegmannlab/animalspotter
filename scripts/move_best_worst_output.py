
import os
import shutil
import math
from heapq import nsmallest, nlargest
from pathlib import Path

def move(scores_dict, path_to_AS_output,path_to_truth_mask, directory_with_AS_output_and_mask, parallel):
    ''' takes the score dict of form {ids:scores}, the path to the truth masks, the path where the AS_output and the
     truth masks were put if both were available as input'''
    # moving truth mask images back to initial directory
    if parallel:
        for file in Path(directory_with_AS_output_and_mask).glob('*json'):
            os.remove(str(file))
    else:
        for file in Path(directory_with_AS_output_and_mask).glob('*json'):
            shutil.move(str(file), path_to_truth_mask)
    #if >1000 keys in dict, find the 100 best and 100 worst
    #else: find the 10% best and worst
    if len(scores_dict)>1000:
        nr=100
    else:
        nr=math.ceil(len(scores_dict)/10)
    #print(scores_dict)
    worst_id_list = nsmallest(nr, scores_dict, key = scores_dict.get)
    best_id_list=nlargest(nr, scores_dict, key=scores_dict.get)

    #move the jpg.png images into path /best and path/worst
    worst_dir = directory_with_AS_output_and_mask + '/worst/'
    os.mkdir(worst_dir)
    best_dir = directory_with_AS_output_and_mask + '/best/'
    os.mkdir(best_dir)
    print('worst ids',worst_id_list)
    print('best ids', best_id_list)
    for file in Path(directory_with_AS_output_and_mask).glob('mcmc*.jpg.png'):
        filename = str(file).split('/')[-1]
        if int(filename.split('_')[9]) in worst_id_list:
            shutil.move(str(file), worst_dir)
    #also move ground truth image (the mask)
    for file in Path(directory_with_AS_output_and_mask).glob('AS_and_ground_truth_*.png'):
        filename = str(file).split('/')[-1]
        if int(filename.split('_')[4].strip('.png')) in worst_id_list:
            shutil.move(str(file), worst_dir)
    #and the original image
    for file in Path(directory_with_AS_output_and_mask).glob('*.jpg'):
        filename = str(file).split('/')[-1]
        if int(filename.split('_')[0]) in worst_id_list:
            shutil.move(str(file), worst_dir)
    #same for the best output
    for file in Path(directory_with_AS_output_and_mask).glob('mcmc*.jpg.png'):
        filename = str(file).split('/')[-1]
        if int(filename.split('_')[9]) in best_id_list:
            shutil.move(str(file), best_dir)
        # also move ground truth image (the mask)
        for file in Path(directory_with_AS_output_and_mask).glob('AS_and_ground_truth_*.png'):
            filename = str(file).split('/')[-1]
            if int(filename.split('_')[4].strip('.png')) in best_id_list:
                shutil.move(str(file), best_dir)
        # and the original image
        for file in Path(directory_with_AS_output_and_mask).glob('*.jpg'):
            filename = str(file).split('/')[-1]
            if int(filename.split('_')[0]) in best_id_list:
                shutil.move(str(file), best_dir)

    #attention with next line -- maybe need this directory?
    #shutil.rmtree(path_to_AS_output) #AS output where no ground truth exists

