'''This script is to check whether we already have generated masks for the imgs of the suggested camera trap'''


#ADAPT THIS!!!
#Path to the json files (masks/ truth set) (make sure here are all json files we have!)
path_to_already_existing_truth_set='/home/dona/Documents/all'
#Path_to_imgs_in_progress (loaded onto darwin, being tagged at the moment)
path_to_imgs_in_progress='/home/dona/Documents/AS_input_ccf'

#Cam trap that looks good, for which I would like to generate ground truth. Write it here in the format: 6-406080N_24-070680E_osw_dsc_dsc_2016-02-11_to_2016-03-14
#note: it is important to replace dots in the name by -
#also, only use here the cam name and the time (if there is something after the time, do not add it here)
cam_trap_potential_new_imgs='6-266600N_24-074783E_osw_dsc_dsc_2012-03-05_to_2012-03-30'

#############################################################################################################################

import os, glob
import string

already_existing_truthset=[]

os.chdir(path_to_already_existing_truth_set)
for file in glob.glob('*.json'):
    cam=file.split('_')[1]+'_'+file.split('_')[2]+'_'+file.split('_')[3]+'_'+file.split('_')[4]+'_'+file.split('_')[5]
    time=file.split('_')[6:9]
    separator='_'
    time=separator.join(time)
    time = time.split('-')[0:5]
    separator='-'
    time=separator.join(time)
    cam_and_time = cam + '_' + time
    cam_and_time = cam_and_time.replace('.', '-')
    if cam_and_time not in already_existing_truthset:
        already_existing_truthset.append(cam_and_time)

os.chdir(path_to_imgs_in_progress)
dirs = [x[0] for x in os.walk(path_to_imgs_in_progress)]
subdirs = []
for i in dirs:
    if len([x[0] for x in os.walk(i)]) == 1:
        subdirs.append(i)
for i in subdirs:
    if i.split('/')[-1] != 'diva':
        cam = i.split('/')[-2]
        cam = cam.replace('.', '-')
        time = i.split('/')[-1]
        time = time.replace(' ', '_')
        time=time.split('_')[0:3]
        separator = '_'
        time = separator.join(time)
        cam_and_time = cam + '_' + time
        if cam_and_time not in already_existing_truthset:
            already_existing_truthset.append(cam_and_time)

print(already_existing_truthset)

if cam_trap_potential_new_imgs in already_existing_truthset:
    print('already used')
else:
    print('Good choice')






