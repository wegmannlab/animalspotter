'''This script has been written to pre-process images. As the AS accounts for image distortions, this script is probably only
useful to preprocess images before upload to darwin v7, in order to make animals better visible ie simplify the annotation.
The pre-processing step chosen here is contrast stretching, ignoring a certain percentage of the brightest and darkest pixels
(percentage can be specified by the user)
Maybe there is also a better pre-processing step. Then, this script here can just serve as a template;) '''
###ADAPT THIS!!!
assignments={'magick_contrast_stretching_min':3, 'magick_contrast_stretching_max':10, 'i':'/home/dona/Documents/stretch_contrast'}
#'i' is the input: contains imgs for which contrast stretching should be performed. Can have arbitrary directory structure, BUT:
#only images in subfolders without any other folder in it are processed!!!!
#min and max here: I chose the min 3% and the max 10% as this seemed to be quite fine. But I did not check much.
#############

import os
from pathlib import Path

def contrastStretch(static_input):
    '''If not yet existent, it creates a dir contrast_stretched_{input dir name} with all the contrast stretched images.Then, the entry 'i' in the dictionary
    static_input is changed to this new directory
    This function performs the contrast-stretching from image magick; a certain percentage of the brightest and darkest pixels are
    not considered for the contrast stretching (keys 'magick_contrast_stretching_min' and 'magick_contrast_stretchin_max' in the input directionary 'static_input'
    The other key required is 'i' which should contain the path to the input directory'''

    input=static_input['i']


    new_input_bone = '/'.join(static_input['i'].rstrip('/').split('/')[:-1])+'/contrast_stretched_'+static_input['i'].rstrip('/').split('/')[-1]
    print(new_input_bone)
    if not Path(new_input_bone).exists():
        os.mkdir(new_input_bone)
    else:
        print('Using existent folder {} (contains already rescaled imgs) instead of rescaling again'.format(new_input_bone))
        static_input['i']=new_input_bone
        return

    dirs = [x[0] for x in os.walk(input)]

    absolute_subdirs = []
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            absolute_subdirs.append(i)
    # now I have the whole path for the subdirs; but I only want to have the part that is not in input
    subdirs = []
    for absolute_subdir in absolute_subdirs:
        nr_subdirs = len(absolute_subdir.split('/')) - len(input.rstrip('/').split('/'))
        if nr_subdirs != 0:
            subdirs.append('/'.join(absolute_subdir.split('/')[-nr_subdirs:]))
        absolute_subdir = Path(absolute_subdir)
        assert absolute_subdir.exists() and absolute_subdir.is_dir()
        if subdirs == []:
            dir_to_put_rescaled_img = new_input_bone
        else:
            dir_to_put_rescaled_img = new_input_bone + '/'+subdirs[-1]
            count=0
            new_dir = new_input_bone
            while not Path(dir_to_put_rescaled_img).exists():
                new_dir=new_dir+'/'+subdirs[-1].split('/')[count]
                if not Path(new_dir).exists():
                    os.mkdir(new_dir)
                count+=1


        #for each img in the current subfolder, rescale it
        for img in absolute_subdir.glob('*.jpg'):
            img_name = str(img).split('/')[-1]  # because img is the img name with the absolute path

            replaced_paranthesis = False
            if '(' in img_name: # because imagemagick cannot handle a paranthesis in the file name (gives a syntax error and skips these files)--> I rename them, apply imagemagick, name them back
                replaced_paranthesis = True
                initial_img_name = img_name
                initial_img = img
                img_name = img_name.replace('(', '')
                img_name = img_name.replace(')', '')
                img = '/'.join(str(img).split('/')[:-1]) + '/' + img_name
                os.rename(initial_img, img)

            os.system('convert -contrast-stretch {a}x{b}% {c} {d}/{e}'.format(a=assignments['magick_contrast_stretching_min'],b=assignments['magick_contrast_stretching_max'],c=img, d=dir_to_put_rescaled_img, e=img_name))
            if replaced_paranthesis:
                os.rename(dir_to_put_rescaled_img + '/' + img_name, dir_to_put_rescaled_img + '/' + initial_img_name)
                os.rename(img, initial_img)


    #set the value of input in the dictionary 'static_input' to the dictionary with the contrast stretched imgs
    static_input['i']=new_input_bone


contrastStretch(assignments)
