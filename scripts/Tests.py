import unittest

from ChinkoTagExtracter import create_tag_tuples, filter_on_prefix
from DateFixer import normalize_date

class MyTest(unittest.TestCase):

    def test_tuples(self):
        self.assertEqual([('a', 'b'),('a','c'),('b','c')], create_tag_tuples(['a', 'b', 'c']))
        self.assertEqual([('a', 'b'),('a','c'),('b','c')], create_tag_tuples(['a', 'a', 'b', 'c']))
        self.assertEqual([('a', '')], create_tag_tuples(['a']))
        self.assertEqual([('', '')], create_tag_tuples([]))


    def test_filter(self):
        self.assertEqual(['c'], filter_on_prefix(['ab', 'a', 'c'], ['a']))
        self.assertEqual(['ab', 'a', 'c'], filter_on_prefix(['ab', 'a', 'c'], ['']))

    def test_date_normalization(self):
        self.assertEqual('2012-03-21', normalize_date('2012-—-03-=—-21'))
        self.assertEqual('2012-03-21', normalize_date('2012-03-21'))
