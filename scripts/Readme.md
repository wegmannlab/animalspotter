# Readme

This folder has multiple tools to make it easier to work with animal spotter.

## Tools

### Image tag statistics

To easily get an overview of the different tags used in the pictures of a dataset, use the TagLister tool.
Simply execute the following command:

'''
python3 TagLister.py -i FOLDER
'''

### Pipeline

The pipeline scans a folder for all subfolders with pictures in them.
Those pictures are then analysed with animalspotter to determine the background/foreground for every image.
Based on this a grayscale image of every image is created as well as a list of bounding boxes.

Here is an example command to run the pipeline:

'''
python3 Pipeline.py -i /media/beat/ElementsSE/Pictures_Chinko/ -o /home/beat/temp/pip/a/ -a /home/beat/temp/pip/b/ -l tags.txt
'''

We use the folder -i as input, output the raw analysis of animalspotter to -o and the analysed output to -a.
To do the analysis we use the tags -l

Available options:
* -b Burnin steps for the MCMC
* -c MCMC chain length
* -th Amount of threads to use during the analysis
* --alpha Ising alpha value 
* --box-color Color of the debug output bounding box
* --min-box-prob Minimum probability of a pixel to be considered foreground


## Data analysis

Here we describe some tools that use the output of the pipeline to do further analysis of the data.

## Animal recognition
