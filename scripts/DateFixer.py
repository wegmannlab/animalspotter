import argparse

import os
from collections import Counter
import re

from ImageTools import *

import pytesseract

from PIL import Image
from PIL import ImageFilter
from PIL import ImageOps

from datetime import datetime

import piexif
import piexif.helper

"""
This script extracts all labels from the pictures and writes them to a file
"""

pattern = re.compile("^20[01][0-9]-[0-9]{2}-[0-9]{2}$")
pattern_time = re.compile("^1?[0-9]:[0-6]?[0-9]:[0-6]?[0-9]$")
correction_tag = "recovered_time_date_original"

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        default="",
        type=str,
        help="Input folder",
    )

    parser.add_argument(
        "--skip-fixed",
        dest="skip_fixed",
        action="store_true",
        help="Do not recalculate already fixed images",
    )

    return parser.parse_args()

def normalize_date(date):
    date = date.replace('—', '-')
    date = date.replace('=', '-')

    while True:
        new_date = date.replace('--', '-')

        if new_date == date:
            break

        date = new_date

    return date

def normalize_time(time):

    time = time.replace("%", ":")

    while True:
        new_time = time.replace('::', ':')

        if new_time == time:
            break

        time = new_time

    return time

def extract_date_time(strings):

    am_or_pm = None
    time = None
    date = None

    for part in strings:
        if part.strip() != '':
            subParts = part.split(' ')

            for subPart in subParts:
                if subPart == 'AM':
                    am_or_pm = 'AM'
                elif subPart == 'PM':
                    am_or_pm = 'PM'

                #Handle common decoding issues
                subPart_time = normalize_time(subPart)
                if pattern_time.match(subPart_time):
                    time = subPart_time

                #Handle common decoding issues
                subPart_date = normalize_date(subPart)
                if pattern.match(subPart_date):
                    date = subPart_date

    if am_or_pm is not None and time is not None:
        if am_or_pm == "PM":
            hours, minutes, seconds = time.split(":")

            hours = int(hours) + 12

            if hours == 24:
                hours = 12

            time = str(hours) + ':' + minutes + ':' + seconds
        elif am_or_pm == "AM":
            hours, minutes, seconds = time.split(":")
            if int(hours) == 12:
                hours = '0'

            time = hours + ':' + minutes + ':' + seconds

    #if date is None or time is None:
        #print(strings)

    return date, time, strings

def find_date(image, psm):
    extracted = pytesseract.image_to_string(image,
                                            lang='eng',
                                            config='--psm ' + str(psm) )

    # + '--oem 0 --tessdata-dir /usr/share/tesseract-ocr/4.00/tessdata')
    # ' -c tessedit_char_whitelist=APM0123456789-:'
    parts = extracted.split('\n')

    return extract_date_time(parts)

def adapted_area(area, image):
    return (area[0] * image.width, area[1] * image.height, area[2] * image.width, area[3] * image.height)

def find_date_in_image(image, modes):

    all_strings = []

    date = None
    time = None

    for psm in modes:
        date_temp, time_temp, strings = find_date(image, psm)
        all_strings.append(strings)

        if date_temp is not None:
            date = date_temp
        if time_temp is not None:
            time = time_temp

        if date is not None and time is not None:
            break

    return date, time

def update_time(image_path, date_time, override = False):

    datetimeoriginal = None
    try:
        datetimeoriginal = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S").strftime("%Y:%m:%d %H:%M:%S")
    except ValueError as e:
        print(image_path + " " + str(e))

    if datetimeoriginal is not None:
        img = Image.open(image_path)
        exif_dict = piexif.load(img.info["exif"])

        exif_dict["Exif"][piexif.ExifIFD.DateTimeOriginal] = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S").strftime("%Y:%m:%d %H:%M:%S")

        try:
            temp = decode_user_comment(exif_dict["Exif"][piexif.ExifIFD.UserComment])

            if correction_tag not in temp:
                temp += ';' + correction_tag

            exif_dict["Exif"][piexif.ExifIFD.UserComment] = piexif.helper.UserComment.dump(temp, encoding='unicode')

        except KeyError:
            exif_dict["Exif"][piexif.ExifIFD.UserComment] = piexif.helper.UserComment.dump(u"Keywords: " + correction_tag, encoding='unicode')

            print("No user comment")
        except UnicodeDecodeError as e:
            print(image_path + " " + str(e))

        exif_bytes = piexif.dump(exif_dict)

        output = image_path

        if not override:
            output += '.corrected.JPG'

        img.save(output, "jpeg", exif=exif_bytes)

def main():
    options = parse_args()

    modes = [3, 11, 12, 7, 4, 6] ##Find minimal list, as this is excessive

    area = (0, 0, 0.4, 0.03) #(left, upper, right, lower)

    total_checked = 0
    total_corrected = 0

    debug_images = None #['IMG_0478.JPG']

    for root, dirs, files in os.walk(options.input):
        path = root.split(os.sep)
        print((len(path) - 1) * 'XXX', os.path.basename(root))
        for file in files:

            filename, file_extension = os.path.splitext(file)

            if (file_extension.lower() == ".jpg" or file_extension.lower() == ".jpeg") and (debug_images is None or any(file in s for s in debug_images)):

                full_path = os.path.join(root, file)

                tags, date = extract_labels(full_path)

                if tags is not None and (date == "" or (not options.skip_fixed and correction_tag in tags)):
                    print(file)

                    total_checked += 1
                    original = Image.open(full_path)

                    date = None
                    time = None

                    if area is not None:
                        original_crop = original.crop(adapted_area(area, original))

                        #Add black border, this helps tesseract a lot!
                        new_size = (int(original_crop.width * 1.35), int(original_crop.height * 1.35))
                        new_im = Image.new("RGB", new_size)  ## luckily, this is already black!
                        new_im.paste(original_crop, (int((new_size[0] - original_crop.width) / 2),
                                             int((new_size[1] - original_crop.height) / 2)))

                        original_crop = new_im

                        date, time = find_date_in_image(original_crop, modes)

                        #If nothing was found, check with cropped image
                        if date is None or time is None:
                            print('Fall back to cropped smooth image')
                            image = original_crop.filter(ImageFilter.SMOOTH)

                            date_temp, time_temp = find_date_in_image(image, modes)

                            if date_temp is not None:
                                date = date_temp
                            if time_temp is not None:
                                time = time_temp

                        # If nothing was found, check with advanced enhance
                        if date is None or time is None:
                            print('Fall back to cropped advanced enhance')
                            image = original_crop.filter(ImageFilter.SMOOTH)
                            image = image.filter(ImageFilter.SMOOTH_MORE)
                            image = image.filter(ImageFilter.SMOOTH_MORE)
                            image = image.filter(ImageFilter.SHARPEN)

                            date_temp, time_temp = find_date_in_image(image, modes)

                            if date_temp is not None:
                                date = date_temp
                            if time_temp is not None:
                                time = time_temp

                        # If nothing was found, check with enlarged image
                        if date is None or time is None:
                            print('Fall back to cropped smoother enlarged image')
                            image = original_crop.resize((image.width * 3, image.height * 3), Image.ANTIALIAS)

                            image = image.filter(ImageFilter.SMOOTH)

                            date_temp, time_temp = find_date_in_image(image, modes)

                            if date_temp is not None:
                                date = date_temp
                            if time_temp is not None:
                                time = time_temp

                        # If nothing was found, check with full image
                        if date is None or time is None:
                            print('Fall back to full image')
                            date_temp, time_temp = find_date_in_image(original, modes)

                            if date_temp is not None:
                                date = date_temp
                            if time_temp is not None:
                                time = time_temp

                    if date is not None and time is not None:
                        total_corrected += 1
                        update_time(full_path, date + " " + time, True)
                        #print(date + " " + time)
                    '''
                    else:
                        if date is not None:
                            print(date)

                        if time is not None:
                            print(time)
                    '''


                    print("Corrected " + str(total_corrected) + " out of " + str(total_checked))

    print("Corrected " + str(total_corrected) + " out of " + str(total_checked))

if __name__ == "__main__":
    main()
