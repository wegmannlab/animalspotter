import json
from typing import List
import sys

import argparse
from pathlib import Path
import numpy as np
from PIL import Image, ImageDraw
import os
import csv


def convert_polygons_to_mask(polygons: List, height: int , width: int):
    """ Convert a polygon into a 2D binary mask

    Parameters
    ----------
    polygons: list
        List of list of coordinates in the format [[{x: x1, y:y1}, ..., {x: xn, y:yn}], ...,
        [{x: x1, y:y1}, ..., {x: xn, y:yn}]].
    height: int
        Height of the output mask
    width: int
        Width of the output mask

    Returns
    -------
    PIL.Image.Image
        Mask with the polygons are drown on it
    """

    img = Image.new('L', (width, height), 0)
    nr=0
    for polygon in polygons:
        nr+=1
        # Polygons needs to be in the format [(x1, y1), (x2, y2), ...] or [x1, y1, x2, y2, ...]
        polygon = [(point['x'], point['y']) for point in polygon]
        # Draw polygon
        ImageDraw.Draw(img).polygon(polygon, outline=255, fill=255)

    return img

def evaluate_foreground(annotations: dict, path_to_darwin_dataset_and_AS_output: Path, best_parameter_run: bool, iou_bool:bool, Euclidean:bool):
    """
    Given a list of annotations and a path containing the [image_id].jpg.png files,
    evaluates the number of foreground pixels correctly predicted by the animal spotter using
    the masks contained in the annotations

    Parameters
    ----------
    annotations: dict
        List of annotations, mapped as {image_id: annotation} i.e. the image_id is the key
    path_to_darwin_dataset_and_AS_output: Path
        Path to the folder which contains the AS output & the ground truth darwin imgs
    best_parameter_run: bool
        True if this is the run with the best parameters from the sigopt optimization; if iou_bool, some extra values are calculated and saved in
        a file called 'contingency_table'
    iou_bool: bool
        Score to calculate is the iou
    Euclidean: bool
        Score to calculate is the euclidean distance

    Returns
    -------
    score: float
         iou: Score which evaluates how many of the foreground pixels detected were in fact animals
         Euclidean: Score which evaluates how big the distance between the ground truth img and the AS output is; reversed so a higher value is better
    """
    assert annotations is not None
    assert iou_bool!=Euclidean

    score = 0.0
    foreground_files = list(path_to_darwin_dataset_and_AS_output.glob("mcmc*.jpg.png"))
    assert len(foreground_files) == len(annotations)


    mask_arrays=[]
    AS_arrays=[]
    scores=[]
    scores_dict={}



    for foreground_file in foreground_files:
        assert foreground_file.exists() and foreground_file.suffix == ".png"

        # Retrieve corresponding annotation
        foreground_file2=foreground_file.stem
        foreground_file2=foreground_file2.split('_')
        image_id=int(foreground_file2[9])
        annotation = annotations[image_id]

        # Extract all the polygons
        polygons = [a['polygon']['path'] for a in annotation['annotations'] if 'polygon' in a]

        # Create binary mask with the polygons contained in the annotation
        mask = convert_polygons_to_mask(
            polygons=polygons,
            height=annotation['image']['height'],
            width=annotation['image']['width']
        )

        # Load the jpg.png file and resize the mask to be matching
        AS_img = Image.open(foreground_file)
        mask = mask.resize(AS_img.size)


        # Create booleans masks
        AS_array = np.array(AS_img)
        AS_array = np.divide(AS_array, 255)  # pixel values from 0 to 255
        mask_array = np.array(mask)
        mask_array=np.divide(mask_array, 255)
        assert AS_array.shape == mask_array.shape

        if iou_bool:
            # Compute the intersection over union (IoU), a bit adapted from official definition
            union = (np.sum(AS_array[np.nonzero(AS_array)]) + np.sum(mask_array[np.nonzero(mask_array)]))/2
            # intersection_def=np.where(AS_array!=0) and np.where(np.array(mask_array)!=0)
            intersection_mask = np.zeros(mask_array.shape)
            intersection_mask[(np.where((AS_array !=0) & (mask_array != 0)))] = 1
            intersection = np.sum(np.multiply(AS_array[intersection_mask == 1], mask_array[intersection_mask == 1]))


            if best_parameter_run:  # I only want to analyze the run for the best parameters in detail
                # write the title
                mylist = ['TP', 'FP', 'FN', 'TN']
                with open('contingency_table', 'a') as output:
                    wr = csv.writer(output, quoting=csv.QUOTE_ALL)
                    wr.writerow(mylist)

                # calculate values for contingency table: FP, TP, TN, FN
                one_array = np.full((np.shape(AS_array)), 1)
                reversed_AS_array = np.subtract(one_array, AS_array)
                reversed_mask_array = np.subtract(one_array, mask_array)

                # TN (true neg): nr of pixels not found by AS & not in ground truth + uncertainty of AS of FP
                # (AS returns probs, e.g. 0.6 prob to be foreground --> counted as 0.4)
                TN_mask = np.zeros(mask_array.shape)
                TN_mask[(mask_array == 0)] = 1
                TN = np.sum(reversed_AS_array[TN_mask == 1]) + np.sum(reversed_mask_array[TN_mask == 1])

                # FN where mask is and (1-AS prob)
                FN_mask = np.zeros(mask_array.shape)
                FN_mask[(mask_array == 1)] = 1
                FN = np.sum(reversed_AS_array[FN_mask == 1])

                # TP
                TP = np.sum(AS_array[intersection_mask == 1])

                # FP where AS detected foreground but there is no animal in ground truth mask
                FP_mask = np.zeros(mask_array.shape)
                FP_mask[(np.where((AS_array != 0) & (mask_array == 0)))] = 1
                FP = np.sum(AS_array[FP_mask == 1])

                mylist = [TP, FP, FN, TN]

                with open(str(path_to_darwin_dataset_and_AS_output)+'/contingency_table', 'a') as output:
                    wr = csv.writer(output, quoting=csv.QUOTE_ALL)
                    wr.writerow(mylist)
                # but I could also calculate an alternative measure for how good it is:
                # eg precision (TP/(TP+FP)
                # or recall (TN/(TN+FN)


            assert (intersection <= union)

            if intersection == 0 and union == 0:
                iou = 1
            elif intersection == 0:
                iou = 0
            else:
                iou = intersection / union

            # Integrate result
            score += iou
            scores.append(iou)
            scores_dict[image_id] = iou


        elif Euclidean:
            euclidean_array=np.abs(np.substract(AS_array, mask_array))
            euclidean_dist=np.sum(euclidean_array)
            #to get values between 0 &1:
            euclidean_dist=euclidean_dist/np.size(euclidean_array)
            #because everything has been written for the iou (i.e. for maximizing the score), I reverse the euclidean dist
            # so that a bigger value is better
            euclidean_dist=1-euclidean_dist

            # Integrate result
            score += euclidean_dist
            scores.append(euclidean_dist)
            scores_dict[image_id] = euclidean_dist





        AS_arrays.append(AS_array)
        mask_arrays.append(mask_array)



    # Average the score
    score /= len(foreground_files)


    return (foreground_files, scores, score, AS_arrays, mask_arrays, scores_dict)


def evaluate_AS(
        path_to_darwin_dataset_and_AS_output: Path,
        best_parameter_run: bool,
        iou_bool: bool,
        Euclidean: bool
):
    path_to_darwin_dataset_and_AS_output=Path(path_to_darwin_dataset_and_AS_output)
    assert path_to_darwin_dataset_and_AS_output.exists() and path_to_darwin_dataset_and_AS_output.is_dir()
    foreground_pixels_score= float('nan')

    # Load and map the annotations

    annotations = {}
    for annotation_file in path_to_darwin_dataset_and_AS_output.glob("*.json"):
        with annotation_file.open() as f:
            annotation = json.load(f)
        image_id = int(annotation['image']['original_filename'].split("_")[0])
        annotations[image_id] = annotation
    assert len(annotations) > 0

    filenames, scores, foreground_pixels_score, AS_arrays, mask_arrays, scores_dict = evaluate_foreground(
        annotations=annotations, path_to_darwin_dataset_and_AS_output=path_to_darwin_dataset_and_AS_output, best_parameter_run=best_parameter_run, iou_bool=iou_bool, Euclidean=Euclidean
    )
    #mean_precisions, mean_recalls, mean_FP_rates=recall_precision_FP_rate_calc(AS_arrays, mask_arrays)
    #generate_recall_precision_curve(mean_precisions, mean_recalls)
    #generate_roc_curve(mean_precisions, mean_FP_rates)
    return(foreground_pixels_score, scores_dict)

