'''This script is to test the parallelisation not using SigOpt'''

import time
import argparse
from rescaling import rescale
import threading
import AS_evaluation
import json


class Master(threading.Thread):
  """
  Shows what a master machine does when running SigOpt in a distributed setting.
  """

  def __init__(self, static_input): #gets called when an object of this class is created
    """
    Initialize the master thread,
    We use the observation_budget field on the experiment to keep track of approximately
    how many total Observations we want to report.
    """
    threading.Thread.__init__(self)
    # Create the SigOpt connection

    self.static_input=static_input
    self.observation_budget=30
    self.observations_done=0

  @property
  def remaining_observations(self): #this function is called every time when we want to get the var remaining observation (due to the @property decorater)

    return self.observation_budget - self.observations_done

  def run(self):
    """
    Attempt to run NUM_WORKERS worker machines. If any machines fail, retry up to
    three times, deleting openSuggestions before proceeding.
    """
    tries = 3
    while (tries > 0 and self.remaining_observations > 0):
      workers = [Worker(self.static_input) for _ in range(self.static_input['nr_workers'])] #changed xrange to range!!!
      for worker in workers:
        worker.start()
      for worker in workers:
        worker.join()
      tries -= 1

class Worker(threading.Thread):
  """
  Shows what a worker machine does when running SigOpt in a distributed setting.
  """

  def __init__(self, static_input):
    """
    Initialize a worker thread
    """
    threading.Thread.__init__(self)
    self.worker_observations_done=0
    self.static_input=dict(static_input) #copy of static_input; else, different workers are changing the same dictionary

  @property
  def metadata(self):
    """
    Use metadata to keep track of the host that each Suggestion and Observation is created on.
    Learn more: https://sigopt.com/docs/overview/metadata
    """
    return dict(host=threading.current_thread().name) #by default, a unique name is constructed of the form “Thread-N” where N is a small decimal number

  @property
  def remaining_observations(self):
    """
    calculate how many Observations we need to run until
    we reach the observation budget
    """
    master.observations_done+=1
    return master.observation_budget-master.observations_done

  def run(self):
    """
    SigOpt acts as the scheduler for the Suggestions, so all you need to do is run the
    optimization loop until there are no remaining Observations to be reported.
    We handle exceptions by reporting failed Observations. Learn more about handling
    failure cases: https://sigopt.com/docs/overview/metric_failure
    """
    while self.remaining_observations > 0: #within getting the remaining observations var, the exp object is updated; see above
        assignments={'alpha':1.2, 'beta':1.3}
        self.static_input.update(assignments)
        try:
            print('Try evaluating metric')
            print('input is', self.static_input)
            print('entry for worker nr is ', self.metadata['host'])
            print('entry for count is', self.worker_observations_done)
            # Evaluate Your Metric
            value=AS_evaluation.main(self.static_input, best_parameter_run=False, worker_nr=self.metadata['host'], count=self.worker_observations_done, parallel=True)
            failed = False
            self.worker_observations_done+=1
        except Exception:
            print('Exception; evaluating metric did not work')
            value = None
            failed = True
        if failed:
            break






def set_parameters(static_input):
    if static_input['iou']:
        static_input['Euclidean']=False
    else:
        static_input['Euclidean']=True
    # add burnin specification: use only the last 500-1000 iterations if there are enough mcmc steps
    if static_input['chainlength'] > 1150:
        static_input['b_nr'] = 4  # burnin nr
        static_input['burnin'] = (static_input['chainlength'] - 750) / 4
    elif static_input['chainlength'] > 700:
        static_input['b_nr'] = 4
        static_input['burnin'] = (static_input['chainlength'] - 300) / 4
    else:
        static_input['b_nr'] = 1  # to short mcmc for multiple burnins
        static_input['burnin'] = int(static_input['chainlength'] / 2)
    if static_input['reduce']!=1:
        rescale(static_input)
    static_input['heuristic']=0.15 #0.2973 #found in first heuristic tests
    static_input['min_bin_size']=3 #seems to be fine (parallel tests)
    static_input['maxbindiff']=1200 #seems not to be very important (parallel tests) & I prefer a large value as then we don't waste too much time with the merging
    static_input['pfd']=1 #faster calculation than another value, and merging is not important for alpha & beta optimization
    static_input['threads']=1
    return static_input



parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--path_to_truth_mask',help='Path to where the dataset is located in Darwin format',
                        required=True,
                        type=str)
parser.add_argument('-i',help='Path to the folder which contains the animal spotter input',
                        required=True,
                        type=str)
parser.add_argument('-o',help='Path to a temporary folder where AS output goes, no one cares',
                        required=True,
                        type=str)
parser.add_argument('--path_to_sigopt_output',
                        help='Path to not yet existent temporary folder, no one cares',
                        required=True,
                        type=str)
parser.add_argument('--binary',
                        help='Path to animalspotter executable',
                        required=True,
                        type=str)
parser.add_argument('--chainlength',
                        help='Length of the mcmc',
                        required=False,
                        type=int)
parser.add_argument('--nr_workers', help='Nr of workers for parallelisation', required=True, type=int)
parser.add_argument('--iou',
                    help='Score to evaluate accuracy is the iou; if False, score to evaluate is euclidean dist',
                    required=True,
                    type=bool)
parser.add_argument('-reduce',
                    help='Factor by which the imgs are reduced --> AS will run faster',
                    required=True,
                    type=int)

args = parser.parse_args()

# run the actual code
start=time.time() #the starting time
static_input=set_parameters(vars(args))
master=Master(static_input=static_input)
master.start()
master.join()
end=time.time()
runtime=end-start
print('The runtime of the program is: {}s'.format(runtime))
