import argparse

import os
import random

from os import listdir

from ImageTools import *

"""
This script extract all labels from the pictures in a specific folder and its subfolders
"""

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--image-folder",
        dest="image_folder",
        default="",
        type=str,
        help="Folder containing the original images folder",
    )

    parser.add_argument(
        "-a",
        "--analysis-folder",
        dest="analysis_folder",
        default="",
        type=str,
        help="Folder containing the analysis output",
    )

    parser.add_argument(
        "-l",
        "--labels-file",
        dest="labels_file",
        type=str,
        help="File containing the labels to be analysed. One label per line",
    )

    parser.add_argument(
        "-w",
        "--weight",
        dest="weight",
        default=0.05,
        type=float,
        help="Minimal weight of a bounding box to be considered as an animal. Multiple tags can be defined with a comma separated list",
    )

    parser.add_argument(
        "--disallow-multilabel",
        dest="no_multilabel",
        action="store_true",
        help="Disallow multiple labels on the same image",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Enable verbose mode",
    )

    parser.add_argument(
        "--allow-multi-label",
        dest="allow_multi_label",
        action="store_true",
        help="Allow images to have multiple labels at once",
    )
    parser.add_argument(
        "--test-samples",
        dest="test_samples",
        default=0.8,
        type=float,
        help="Separation of test/validation samples",
    )

    return parser.parse_args()

def get_label_indexes(labels, imageLabels):

    indexes = []

    for label in imageLabels:
        if label in labels:
            indexes.append(labels.index(label))

    return indexes

def readBoxes(inputFile):
    file = open(inputFile, 'r')

    name = ""

    boxes = []

    for line in file:
        if not name:
            name = line.strip()
        else:
            boxes.append(line.strip())

    return name, boxes

def readLabels(labelsFile):
    labels = []

    if labelsFile:
        file = open(labelsFile, "r")
        for line in file:
            cleaned = line.strip()

            if cleaned:
                labels.append(cleaned)

    return labels

def analyseImage(labels, imageLabels, boxes, imagePath, minWeight):
    yoloOutput = imagePath.rsplit('.', 1)[0] + ".txt"
    print(yoloOutput + " " + str(imageLabels))

    labelIndex = get_label_indexes(labels, imageLabels)

    with open(yoloOutput, 'w') as output_file:
        for labelIndex in labelIndex:
            for box in boxes:

                split = box.split(" ")

                if float(split[0]) > minWeight:
                    split[0] = str(labelIndex)

                    for i in range(0, len(split)):
                        output_file.write(split[i])

                        if i < len(split) - 1:
                            output_file.write(' ')

                    output_file.write('\n')

def main():
    options = parse_args()

    labels = readLabels(options.labels_file)

    minWeight = float(options.weight)

    images = []

    #Get folders in analysis folder
    for folder in next(os.walk(options.analysis_folder))[1]:

        absoluteFolderAnalysis = os.path.join(options.analysis_folder, folder)
        absoluteFolderImage = os.path.join(options.image_folder, folder)

        for file in listdir(absoluteFolderAnalysis):
            if file.endswith(".raw.txt"):
                absoluteFileAnalysis = os.path.join(absoluteFolderAnalysis, file)

                imageName, boxes = readBoxes(absoluteFileAnalysis)
                absoluteFileImage = os.path.join(absoluteFolderImage, imageName)

                imageLabels = []

                if len(boxes) > 0:
                    imageLabels = extract_labels(absoluteFileImage)

                analyseImage(labels, imageLabels, boxes, absoluteFileImage, minWeight)
                images.append(os.path.join(folder, imageName))

    random.shuffle(images)

    split = int(len(images) * options.test_samples)

    train_data = images[:split]
    test_data = images[split:]

    with open(os.path.join(options.analysis_folder, "training.txt"), 'w') as output_file:
        for img in train_data:
            output_file.write(os.path.join(absoluteFolderImage, img) + '\n')

    with open(os.path.join(options.analysis_folder, "test.txt"), 'w') as output_file:
        for img in test_data:
            output_file.write(os.path.join(absoluteFolderImage, img) + '\n')


if __name__ == "__main__":
    main()