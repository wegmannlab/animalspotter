import matplotlib.pyplot as plt
import numpy as np


######recall precision curve and roc curve
def recall_precision_FP_rate_calc(AS_arrays, mask_arrays):
    thresholds=range(100, 0, -1)
    thresholds=np.divide(thresholds, 100)
    mean_precisions=[]
    mean_recalls=[]
    mean_FP_rates=[]
    for threshold in thresholds:
        precisions = []
        recalls = []
        FP_rates=[]
        for i in range(len(AS_arrays)):
            AS_array=AS_arrays[i]
            mask_array=mask_arrays[i]
            cond1=AS_array>threshold
            cond2=mask_array==1
            cond3=AS_array<threshold
            cond4=mask_array==0
            TP=np.sum(np.where((cond1&cond2), 1,0))
            FP=np.sum(np.where((cond1&cond4), 1,0))
            FN=np.sum(np.where((cond3&cond2), 1,0))
            TN=np.sum(np.where((cond3&cond4), 1,0))
            precision=TP/(TP+FP+0.000000001) #add small value to avoid division by zero
            recall=TP/(TP+FN+0.000000001)
            FP_rate=FP/(TN+FP)
            precisions.append(precision)
            recalls.append(recall)
            FP_rates.append(FP_rate)
        mean_recalls.append(np.mean(recalls))
        mean_precisions.append(np.mean(precisions))
        mean_FP_rates.append(np.mean(FP_rates))
    return (mean_precisions, mean_recalls, mean_FP_rates)


def generate_recall_precision_curve(mean_precisions, mean_recalls):
    plt.plot(mean_recalls, mean_precisions, marker='o', markersize=3, color="red")
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.xlabel('recall')
    plt.ylabel('precision')
    plt.title('Recall-Precision curve /AP')
    plt.savefig("Recall-Precision_curve")

def generate_roc_curve(mean_precisions, mean_FP_rates):
    plt.plot(mean_FP_rates, mean_precisions, marker='o', markersize=3, color="red")
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.xlabel('FP-rate')
    plt.ylabel('precision')
    plt.title('ROC curve')
    plt.savefig("ROC_curve")