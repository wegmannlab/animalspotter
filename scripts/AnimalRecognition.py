import argparse

import os
from ImageTools import *

from collections import Counter

"""
This script detects the animals in the pictures annotated with the Pipeline script
"""

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        default="",
        type=str,
        help="Input folder",
    )
    parser.add_argument(
        "-a",
        "--analysis-folder",
        dest="analysis_folder",
        type=str,
        help="Output of the analysis",
    )
    parser.add_argument(
        "-f",
        "--filter",
        dest="filter",
        default="",
        type=str,
        help="Only analyse pictures with a certain tag",
    )

    parser.add_argument(
        "-w",
        "--weight",
        dest="weight",
        default=0.05,
        type=float,
        help="Minimal weight of a bounding box to be considered as an animal. Multiple tags can be defined with a comma separated list",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Enable verbose mode",
    )

    return parser.parse_args()


def box_weight(image):
    filename, file_extension = os.path.splitext(image)

    if file_extension:
        file = open(filename + ".raw.txt", "r")

        file.readline() #First line is filename

        for line in file.read().splitlines():
            weight = float(line.split(" ")[0])
            return weight

    return 0

def image_has_any_tag(filters, labels):
    found = []

    for filter in filters:
        if labels is not None and filter in labels:
            found.append(filter)

    return found

def main():
    options = parse_args()

    filters = options.filter.split(",")

    if not options.input:
        print("You need to specify an input folder")
        return

    if not os.path.exists(options.input):
        print("Specified input folder "+options.input +" does not exist")
        return

    if not options.analysis_folder:
        print("You need to specify an analysis folder")
        return

    if not os.path.exists(options.analysis_folder):
        print("Specified analysis folder "+options.analysis_folder +" does not exist")
        return

    total = Counter()
    animals = Counter()

    average = Counter()
    maximum = Counter()

    folderNames = []

    for folder in next(os.walk(options.input))[1]:
        folderNames.append(folder)

    if not folderNames:
        print("No subfolders found in input folder, falling back to directly use input folder")
        folderNames.append("")

    for folder in folderNames:
        absoluteImageFolder = os.path.join(options.input, folder)
        for file in os.listdir(absoluteImageFolder):
            image = os.path.join(absoluteImageFolder, file)

            if not image.endswith(".txt"):
                labels = extract_labels(image)[0]

                matchingLabels = image_has_any_tag(filters, labels)
                if len(filters) == 0 or len(matchingLabels) > 0:

                    weight = box_weight(os.path.join(options.analysis_folder, os.path.join(folder, file)))

                    for mL in matchingLabels:
                        maximum[mL] = max(maximum[mL], weight)
                        average[mL] += weight
                        total[mL] += 1

                    if weight > options.weight:
                        if(options.verbose):
                            print("Found animal " + str(weight) + " in "+image + " with labels " + str(matchingLabels))

                        for mL in matchingLabels:
                            animals[mL] += 1

    for filter in filters:
        print("Found " + str(animals[filter]) + " pictures with a bounding box in " + str(total[filter]) + " images with the label " + filter)
        if total[filter] > 0:
            print("Max weight: " + str(maximum[filter]) + ", average: " + str(average[filter] / total[filter]))
            print( str(animals[filter] / total[filter]) + " total")

if __name__ == "__main__":
    main()
