'''This script is to give each img for which a ground truth will be generated an identical id and store the resulting dictionary (id:imgname) into a json file.'''

#READ EVERYTHING BEFORE THE IMPORT STATEMENTS BEFORE RUNNING THIS SCRIPT

###################################################################
 #SECTION: ADAPT THIS

#Where the json file is located
id_json_path='/home/dona/Documents/check_id_error'
#the json file (contains all ids and filenames of the images that were already renamed)
name_json_file='final_updated_ids.json'
#name that we will give the actualized json file (just dont overwrite the old one for safety reasons)
name_to_give_new_json_file='updated_ids_WASTE.json'
#path to the images that should be renamed and for which an id should be generated
path_to_all_img_dirs='/home/dona/Documents/check_id_error/test_ondarwin' #this path should contain the whole cam trap dir (6.....); giving the cam trap directly (6......)does not work
#############################################################################

#IMPORTANT INFO!!!
'''please make sure that there are no spaces in directory names (replace them with an underscore)
Also, if you already generated the gt files but somehow lost the renamed original imgs (but you still have the imgs), make sure that their directories 
have the same name as in the json (gt data) files'''

#######################################################################

import glob
import json
import os
import random
import numpy as np




#read json files with all ids & filenames that we already have
os.chdir(id_json_path)
with open(name_json_file, 'r') as f:
    id_dicts = json.load(f)


ids = []  # list of all existing ids
for mydict in id_dicts:
    ids.append(mydict['id'])

# walk through all images
# get original filename
# list of directories within the specified input directory
dirs = [x[0] for x in os.walk(path_to_all_img_dirs)]
subdirs=[]
for i in dirs:
    if len([x[0] for x in os.walk(i)]) == 1:
        subdirs.append(i)

for subdir in subdirs:
    temporary=subdir
    subfolder_index_border=np.inf
    name=''
    for i in range(0, len(temporary.split('/'))):
        path = '/'.join(temporary.split('/')[:i+1])
        if path in dirs and path.split('/')[-1][0] == '6':
            name=path.split('/')[-1]
            subfolder_index_border = i
        if i > subfolder_index_border:
            name += '_' + temporary.split('/')[i]


    os.chdir(subdir)
    for file in glob.glob('*'):
        already_there = False
        if file[0]=='6':
            original_filename=file.rstrip('.JPG')
            original_filename=original_filename.rstrip('.jpg')
        else: #which cam trap is not yet in name
            original_filename = name + '_' + file.rstrip('.JPG')
            original_filename=original_filename.rstrip('.jpg')
        original_filename = original_filename.replace(' ', '_')
        original_filename2 = original_filename.replace('.', '-')
        original_filename=original_filename
        # print(original_filename)
        # break

        # original_filename2 is necessary, because some cam traps are named
        # with a dot while some are with a '-'
        # (As long as its the same sign in the input for animal spotter and the
        # ground truth data, this is not a problem for AS; therefore I don't rename
        # #it here but just check whether both strings don't match a already taken
        # #cam trap)

        # if original_filename in there, get corresponding id
        # this ensures that this script can be run 2 times on the same img set without messing up the ids
        for mydict in id_dicts:
            #print('dict',mydict['original_filename'])


            if original_filename == mydict['original_filename']:
                id = mydict['id']
                already_there = True
            if original_filename2 == mydict['original_filename']:
                id = mydict['id']
                already_there = True
        #break
        # else, generate random 6 Stellen id that is not yet used

        #break #remove later again
        if not already_there:
            #print(original_filename)
            # create new id
            id = random.randint(100000, 999999)
            while id in ids:
                id = random.randint(100000, 999999)
            # create new dict and append to id_dicts
            new_dict = {'id': id, 'original_filename': original_filename}
            id_dicts.append(new_dict)
            ids.append(id)

        # rename images: initial name_id_originalfilename
        path=path.rstrip('/')+'/'
        path_to_file = path.rstrip('/') +'/'+ file
        os.rename(path_to_file, path + str(id) + '_' + original_filename+'.jpg')

# write new file containing all ids & original filenames (for safety reasons, don't
# just append it to the old one)
os.chdir(id_json_path)
with open(name_to_give_new_json_file, 'w') as json_file:
    json.dump(id_dicts, json_file)

