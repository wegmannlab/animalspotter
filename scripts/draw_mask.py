
import json
from typing import List
import sys

import argparse
from pathlib import Path
import numpy as np
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import os, glob, shutil



def convert_polygons_to_mask(img: Image, polygons: List, height: int, width: int, filename: int,
                             path_to_interesting_AS_output: str):
    """ Convert a polygon into a 2D binary mask

    Parameters
    ----------
    polygons: list
        List of list of coordinates in the format [[{x: x1, y:y1}, ..., {x: xn, y:yn}], ...,
        [{x: x1, y:y1}, ..., {x: xn, y:yn}]].
    height: int
        Height of the output mask
    width: int
        Width of the output mask

    Returns
    -------
    PIL.Image.Image
        Mask with the polygons drawn on it
    """

    mcmc_width, mcmc_height = img.size
    img2=img.resize((width,height))
    nr = 0
    for polygon in polygons:
        nr += 1
        # Polygons needs to be in the format [(x1, y1), (x2, y2), ...] or [x1, y1, x2, y2, ...]
        polygon = [(point['x'], point['y']) for point in polygon]
        # Draw polygon
        #ImageDraw.Draw(img2).polygon(polygon, outline ="red")
        ImageDraw.Draw(img2).line(polygon, fill='blue', width=9)


    img2.resize((mcmc_width, mcmc_height))
    img2.save("{}/AS_and_ground_truth_{}.png".format(path_to_interesting_AS_output, filename))



def draw_mask_for_interesting_AS_output(path_to_interesting_AS_output):
    '''creates new pngs with the ground truth on top of the AS output (in the original size of the images as it's more comfortable for visual inspection)

    Takes the path to the interesting AS output images and the ground truth annotation files (must be in same folder) as input
    The interesting AS output is the optimum found by sigopt'''
    # have a look at how the worst AS outputs should have looked
    for file in Path(path_to_interesting_AS_output).glob('mcmc*.png'):
        filename = str(file).split('/')[-1]
        id = filename.split('_')[9]
        img = Image.open(file).convert('RGBA')

        for file in Path(path_to_interesting_AS_output).glob('*{}*.json'.format(id)):
            filename = str(file).split('/')[-1]
            if id == filename.split('_')[0]:
                with open(str(file), 'r') as f:
                    annotation = json.load(f)
                    width, height = annotation["image"]['width'], annotation["image"]['height']
                    # # Extract all the polygons
                    polygons = [a['polygon'] for a in annotation['annotations'] if
                        'polygon' in a]  # this is now a list of length= #polygons
                    polygon_list = []
                    for i in range(0, len(polygons)):
                        polygon_list.append(polygons[i]['path'])
                    mask = convert_polygons_to_mask(img=img, polygons=polygon_list, height=height, width=width, filename=id,
                                            path_to_interesting_AS_output=path_to_interesting_AS_output)



def add_originals(path_to_interesting_AS_output, path_to_original_images):
    '''Copies the original images into the folder with the interesting AS output, to allow for visual inspection'''
    ids=[]
    for file in Path(path_to_interesting_AS_output).glob('mcmc*.png'):
        filename = str(file).split('/')[-1]
        ids.append(filename.split('_')[9])
    dirs = [x[0] for x in os.walk(path_to_original_images)]
    subdirs = []
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            subdirs.append(i)
    for i in subdirs:
        for file in Path(i).glob('*.jpg'):
            filename = str(file).split('/')[-1]
            if filename.split('_')[0] in ids:
                shutil.copy(str(file), path_to_interesting_AS_output)

#in case you would like to run it from here and not from the command line,
#just change the paths of the following
# def main():
#     path_to_worst_AS_output = '/data/darwin/sigopt_best_AS_output2_test2/worst/'
#     path_to_best_AS_output = '/data/darwin/sigopt_best_AS_output2_test2/best/'
#     #path_to_all_output='/data/darwin/sigopt_best_AS_output2/'
#     path_to_original_images='/data/darwin/sigopt_pics'
#     path_to_masks = '/data/darwin/annotations/'
#     draw_mask_for_interesting_AS_output(path_to_worst_AS_output, path_to_masks)
#     draw_mask_for_interesting_AS_output(path_to_best_AS_output, path_to_masks)
#     #draw_mask_for_interesting_AS_output(path_to_all_output, path_to_masks)
#     add_originals(path_to_best_AS_output, path_to_original_images)
# #main()
