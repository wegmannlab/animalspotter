'''This script is to find out whether an IsingBeta will be useful (i.e. whether the classification of the same pixel in
a neighbouring image should be considered)
This script calculates how many times the probability of a pixel to be foreground is higher/same/lower given this same
particular pixel was foreground in the preceding image than the probability of this pixel to be foreground overall.
To study the influence of the time gap between consecutive images, the function takes a maximal and minimal time difference;
only images within the given time difference range are considered to be neighbours. .


'''



###ADAPT THIS!!!!!!!!!!!
#path to darwin masks of cam traps with complete ground truth
darwin_path="/home/dona/Documents/darwin_masks_by_CT" #dir with gt json files; contains different dirs for the different cam traps (no time subfolders)
imgs_path="/home/dona/Documents/AS_input" #dir with the images; contains different dirs for the different cam traps (can have time subfolders, or not, this is not important)
#set the time range in which images are considered neighbours
min_time_diff=0
max_time_diff=5
#min_time_diff=5
#max_time_diff=60

####################################################################################################

import os
import glob
import numpy as np
import json
from PIL import Image, ImageDraw
from typing import List
import datetime
import PIL.ExifTags
import sys
import pandas as pd
'''
Overview of what is done:
1) I need the darwin masks and the true imgs
2) I need to get the time tag exif information from the true imgs
3) Then I need to order the darwin masks according to this time tag
4) Calculate the prob to be fg if one of the neighbour imgs is fg there for each pxl
5) Calculate overall prob to be fg for each pxl

Note: this has to be done separately for each pxl because certain pxls are more likely to be fg than others'''

def main(darwin_path,imgs_path, min_time_diff, max_time_diff):
    '''max_time_diff [s]: the max time difference between two subsequent imgs to be considered neighbours here (else they are not used to see whether IsingBeta is useful)'''
    gt_cams_path, imgs_cams_path=get_cam_names(darwin_path, imgs_path)
    both_fg=0
    preceding_fg=0
    total_nr_pxls=0
    fg_pxls=0
    sum_beta_larger=0
    sum_beta_smaller=0
    sum_beta_same=0
    for x in range(0,len(gt_cams_path)):
        idTimesDict=get_timeIdsDict(imgs_cams_path[x]) #get the dictionary id:time
        os.chdir(gt_cams_path[x])
        width, height=getWidthHeight(gt_cams_path[x])
        npixels=width*height
        all_ids=[]
        neighbour_ids=[]
        previous_value=datetime.datetime.strptime('1960:01:01 00:00:00', "%Y:%m:%d %H:%M:%S") #to make sure the very first image is not added to neighbour_ids
        for key, value in idTimesDict:
            all_ids.append(key)
            value=datetime.datetime.strptime(value, "%Y:%m:%d %H:%M:%S")
            if (value-previous_value).total_seconds()<max_time_diff and (value-previous_value).total_seconds()>min_time_diff:
                neighbour_ids.append(key)
            previous_value=value

        #get ids for which we have a gt mask
        gt_ids=get_ids_gt()
        #get the mask of the first id of which we have a mask!
        firstindex=0
        while all_ids[firstindex] not in gt_ids:
            firstindex+=1
        precedingMask = get_masks(all_ids[firstindex])
        fg_pxls += int(np.sum(precedingMask))

        #print('starting loop')
        count=0
        sum_intersection_masks = np.zeros(precedingMask.shape)
        sum_preceding_masks=np.zeros(precedingMask.shape)
        sum_masks=precedingMask
        for i in range(firstindex, len(all_ids)):
            if all_ids[i] not in gt_ids:
                #print('continuing')
                continue
            count+=1
            #print("id is",all_ids[i])
            mask=get_masks(all_ids[i])
            sum_masks+=mask

            if all_ids[i] not in neighbour_ids: #time difference too large to be considered for IsingBeta
                precedingMask = mask  # for next iteration
                continue

            #find where both are fg
            intersection_mask = np.zeros(mask.shape)
            intersection_mask[(np.where((precedingMask != 0) & (mask != 0)))] = 1

            #find where preceding is fg
            sum_preceding_masks+=precedingMask
            precedingMask=mask #for next iteration
            sum_intersection_masks+=intersection_mask
        #


        #how many times each pxl is fg is stored in sum_masks --> divide by count to get prob to be fg for each pxl
        fg_prob=sum_masks/count
        #how many times each pxl & its corresponding preceding pxl is fg is stored in sum_intersection_masks; how many times each preceding pxl is fg is stored in sum_preceding_masks
        sum_preceding_masks[(np.where(sum_preceding_masks==0))] = 1#replace 0 entries by 1, else dividing will raise an error
        beta_fg_prob=np.divide(sum_intersection_masks, sum_preceding_masks)
        #I cannot average over CT of course, cause this will be camera specific!



        #count how many times beta_fg_prob is higher than fg_prob
        beta_larger = np.zeros(beta_fg_prob.shape)
        beta_larger[(np.where(beta_fg_prob>fg_prob))] = 1
        sum_beta_larger+=np.sum(beta_larger)
        beta_smaller = np.zeros(beta_fg_prob.shape)
        beta_smaller[(np.where(beta_fg_prob < fg_prob))] = 1
        sum_beta_smaller += np.sum(beta_smaller)
        beta_same = np.zeros(beta_fg_prob.shape)
        beta_same[(np.where(beta_fg_prob == fg_prob))] = 1
        sum_beta_same += np.sum(beta_same)

        #print(beta_fg_prob.shape)
        print('Cam trap {}: Probability to be fg higher if preceding (time diff < {}, but> {}) corresponding pxl was foreground in {} cases, lower in {} cases'.format(gt_cams_path[x], max_time_diff,min_time_diff, sum_beta_larger, sum_beta_smaller))


    print('Overall:', 'Probability to be fg higher if preceding (time diff < {} but > {}) corresponding pxl was foreground in {} cases, lower in {} cases, same in {} cases'.format(max_time_diff, min_time_diff, sum_beta_larger, sum_beta_smaller, sum_beta_same))

    # over all imgs; but this is not differentiating between the model where some pixels have a  higher prob to be fg and
    #the model where the prob to be fg depends on whether it was fg in the preceding img
    #fg_prob=fg_pxls/total_nr_pxls
    #
    #ifIsingBetaProb=both_fg/preceding_fg
    #print ('fg probability is '+ str(fg_prob))
    #print('fg probability if pixel at same position in preceding img was fg is'+ str(ifIsingBetaProb))






def get_dirs_in_dir(path):
    '''returns a list of directories in the given directory'''
    dirs = [x[0] for x in os.walk(path)] #this though still includes the ground truth dir itself
    subdirs=[]
    for i in dirs:
        if len([x[0] for x in os.walk(i)]) == 1:
            subdirs.append(i)
    return(subdirs)

def getWidthHeight(subdir):
    '''checks whether all imgs for which we have json files in the given subdir are of same dimension. Returns the width and height of the imgs'''
    for file in glob.glob('*json'):
        with open(file, 'r') as f:
            annotation = json.load(f)
        height= annotation['image']['height']
        width = annotation['image']['width']
        break
    for file in glob.glob('*json'):
        with open(file, 'r') as f:
            annotation = json.load(f)
        if height!= annotation['image']['height'] or width != annotation['image']['width']:
            raise Exception ("Not all images in subdir {} are of same height and width".format(subdir))
    return(width, height)

def get_ids_gt():
    'returns a list of ids for which we have gt'
    ids_mask=[]
    for file in glob.glob('*.json'):
        ids_mask.append(file.split('_')[0])
    return(ids_mask)

def get_masks(id):
    '''returns the binary mask of the given id'''
    for file in glob.glob('{}_*.json'.format(id)):
        with open(file, 'r') as f:
            annotation = json.load(f)
        # Extract all the polygons
        polygons = [a['polygon']['path'] for a in annotation['annotations'] if 'polygon' in a]
        height = annotation['image']['height']
        width = annotation['image']['width']

        # Create binary mask with the polygons contained in the annotation
        mask = convert_polygons_to_mask(polygons=polygons, height=height, width=width)

        # create boolean masks
        mask_array = np.array(mask)
        mask_array = np.divide(mask_array, 255)
        return(mask_array)



def convert_polygons_to_mask(polygons: List, height: int , width: int ):
    """ Convert a polygon into a 2D binary mask

    Parameters
    ----------
    polygons: list
        List of list of coordinates in the format [[{x: x1, y:y1}, ..., {x: xn, y:yn}], ...,
        [{x: x1, y:y1}, ..., {x: xn, y:yn}]].
    height: int
        Height of the output mask
    width: int
        Width of the output mask

    Returns
    -------
    PIL.Image.Image
        Mask with the polygons are drown on it
    """

    img = Image.new('L', (width, height), 0)
    nr=0
    for polygon in polygons:
        nr+=1
        # Polygons needs to be in the format [(x1, y1), (x2, y2), ...] or [x1, y1, x2, y2, ...]
        polygon = [(point['x'], point['y']) for point in polygon]
        # Draw polygon
        ImageDraw.Draw(img).polygon(polygon, outline=255, fill=255)
    return img


def get_cam_names(darwin_path, imgs_path):
    """returns an ordered list of paths to images and json files for which we have both, imgs and json files in the given paths"""
    gt_cams = []
    json_subdirs = get_dirs_in_dir(darwin_path)
    for dir in json_subdirs:
        gt_cams.append(dir.split("/")[-1])

    imgs_subdirs = get_dirs_in_dir(imgs_path)
    img_cam_paths = [] #whole path
    cams=[] #only cams
    for dir in imgs_subdirs:
        if dir.split("/")[-2]=='analysis': #this is only used if we call this function from too_much_fg.py
            continue
        list = dir.split("/")
        gt = False
        for i in list:
            if i in gt_cams:
                gt = True
                cams.append(i)
                img_cam_paths.append(dir)
                break
        if gt == False:
            print("There is no ground truth for subdir {}".format(dir))

    for i in gt_cams:
        if i not in cams:
            print("No images found corresponding to the ground truth of {}".format(i))

    gt_cam_paths=[]

    #now make list of paths, where the cams are in the same order in both lists
    for cam in cams:
        for dir in json_subdirs:
            if dir.split("/")[-1]==cam:
                gt_cam_paths.append(dir)


    return(gt_cam_paths,img_cam_paths)

def get_timeIdsDict(img_cam_path):
    '''takes a path to original imgs and returns a dictionary with id as key and datetime as value, sorted
    by datetime'''
    os.chdir(img_cam_path)
    mydict={}
    for file in glob.glob('*.jpg'):
        img = Image.open(file)
        exif = {
            PIL.ExifTags.TAGS[k]: v
            for k, v in img._getexif().items()
            if k in PIL.ExifTags.TAGS
        }
        date=exif['DateTimeOriginal'] #this is the right time!! (not 'DateTime' and also not DigitizedDateTime')
        id=file.split("_")[0]
        mydict[id]=date

    #sort dict by value
    mydict = sorted(mydict.items(), key=lambda x: datetime.datetime.strptime(x[1], "%Y:%m:%d %H:%M:%S"))
    return mydict





if __name__ == "__main__":
    main(darwin_path, imgs_path, min_time_diff, max_time_diff)

'''
output of all at least partly annotated cam traps: within the 3 img series only (min time diff=0, max time diff=5: 
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.349817N_24.117650E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 1485445.0 cases, lower in 1863022.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.366733N_24.023500E_mwg_tag_spc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 3438257.0 cases, lower in 1873568.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.336917N_24.104017E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 6081926.0 cases, lower in 3242976.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-402810N_24-045610E_aoe_bag_ssr: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 9375270.0 cases, lower in 3676190.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-283073N_24-058211E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 15043254.0 cases, lower in 4545507.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-406080N_24-070680E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 18401162.0 cases, lower in 7726657.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-348367N_24-063000E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 21387348.0 cases, lower in 8617358.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-447062N_24-058630E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 23984672.0 cases, lower in 10339639.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-483499N_24-096579E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 26889611.0 cases, lower in 11681913.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-372172N_24-137423E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 31752320.0 cases, lower in 13105938.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-304117N_24-077667E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 32436506.0 cases, lower in 16012521.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-409809N_24-142780E_ccf_riv_gaf: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 36560932.0 cases, lower in 16674457.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.331400N_24.137583E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 38510439.0 cases, lower in 16692719.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.277950N_24.049117E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 40166774.0 cases, lower in 16972305.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.353533N_24.058983E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 43656814.0 cases, lower in 17138759.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.266600N_24.074783E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 48297567.0 cases, lower in 17284513.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.346233N_24.012717E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 50478085.0 cases, lower in 18451282.0 cases
Overall: Probability to be fg higher if preceding (time diff < 5) corresponding pxl was foreground in 50478085.0 cases, lower in 18451282.0 cases, same in 32505001.0 cases

output of all at least partly annotated cam traps: Within the images put in same bin by AS only but not in same img serie(max time between bins parameter used= 60s, min time between imgs: 5, cause imgs of same serie have a smaller time difference between them & imgs that are not in same serie have a larger time difference):
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.349817N_24.117650E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 461866.0 cases, lower in 2886601.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.366733N_24.023500E_mwg_tag_spc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 1308251.0 cases, lower in 4003574.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.336917N_24.104017E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 2550393.0 cases, lower in 6774509.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-402810N_24-045610E_aoe_bag_ssr: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 4623888.0 cases, lower in 8422222.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-283073N_24-058211E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 5407138.0 cases, lower in 14175290.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-406080N_24-070680E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 5655364.0 cases, lower in 20466122.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-348367N_24-063000E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 6898054.0 cases, lower in 23100128.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-447062N_24-058630E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 7682049.0 cases, lower in 26635404.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-483499N_24-096579E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 7796354.0 cases, lower in 30768312.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-372172N_24-137423E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 9398683.0 cases, lower in 35452674.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-304117N_24-077667E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 9901404.0 cases, lower in 38540722.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-409809N_24-142780E_ccf_riv_gaf: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 10199302.0 cases, lower in 43029186.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.331400N_24.137583E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 11237865.0 cases, lower in 43958368.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.277950N_24.049117E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 11650339.0 cases, lower in 45481815.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.353533N_24.058983E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 13845073.0 cases, lower in 46943575.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.266600N_24.074783E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 17057055.0 cases, lower in 48517313.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.346233N_24.012717E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 60, but> 5) corresponding pxl was foreground in 18309317.0 cases, lower in 50612102.0 cases
Overall: Probability to be fg higher if preceding (time diff < 60 but > 5) corresponding pxl was foreground in 18309317.0 cases, lower in 50612102.0 cases, same in 32512949.0 cases

Because an Ising Beta would not be useful here, expanding the time range further is not useful.
Still, as I already produced the following output, I kept it here.


output of all at least partly annotated cam traps: Within the images put in same bin by AS only but not in same img serie(max time between bins parameter used= 1800s, because this is the max of the range testet by sigopt, min time between imgs: 5, cause imgs of same serie have a smaller time difference between them & imgs that are not in same serie have a larger time difference):
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.349817N_24.117650E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 461866.0 cases, lower in 2886601.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.366733N_24.023500E_mwg_tag_spc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 1308251.0 cases, lower in 4003574.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.336917N_24.104017E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 2663906.0 cases, lower in 6660996.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-402810N_24-045610E_aoe_bag_ssr: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 4959347.0 cases, lower in 8088747.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-283073N_24-058211E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 5742597.0 cases, lower in 13841815.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-406080N_24-070680E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 5991055.0 cases, lower in 20132415.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-348367N_24-063000E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 7280169.0 cases, lower in 22719997.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-447062N_24-058630E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 8067688.0 cases, lower in 26251749.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-483499N_24-096579E_ccf_dry_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 8182588.0 cases, lower in 30384062.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-372172N_24-137423E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 9822199.0 cases, lower in 35031038.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-304117N_24-077667E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 10385857.0 cases, lower in 38058149.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6-409809N_24-142780E_ccf_riv_gaf: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 10683389.0 cases, lower in 42546979.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.331400N_24.137583E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 11737506.0 cases, lower in 43460541.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.277950N_24.049117E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 12149980.0 cases, lower in 44983988.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.353533N_24.058983E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 14344049.0 cases, lower in 46446413.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.266600N_24.074783E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 17553589.0 cases, lower in 48022555.0 cases
Cam trap /home/dona/Documents/darwin_masks_by_CT/6.346233N_24.012717E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 1800, but> 5) corresponding pxl was foreground in 18805851.0 cases, lower in 50117344.0 cases
Overall: Probability to be fg higher if preceding (time diff < 1800 but > 5) corresponding pxl was foreground in 18805851.0 cases, lower in 50117344.0 cases, same in 32511173.0 cases
'''






'''
output of only the fully annotated camera traps: Within the 3 img series only:
Cam trap /home/dona/Documents/darwin_masks_onlyCompleteCT/6.331400N_24.137583E_ccf_riv_ser: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 1949495.0 cases, lower in 18274.0 cases
Cam trap /home/dona/Documents/darwin_masks_onlyCompleteCT/6.277950N_24.049117E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 3543952.0 cases, lower in 359738.0 cases
Cam trap /home/dona/Documents/darwin_masks_onlyCompleteCT/6.353533N_24.058983E_ccf_swa_ser: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 7036208.0 cases, lower in 523976.0 cases
Cam trap /home/dona/Documents/darwin_masks_onlyCompleteCT/6.266600N_24.074783E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 11763615.0 cases, lower in 583076.0 cases
Cam trap /home/dona/Documents/darwin_masks_onlyCompleteCT/6.346233N_24.012717E_osw_dsc_dsc: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 14132268.0 cases, lower in 1561771.0 cases
Overall: Probability to be fg higher if preceding (time diff < 3) corresponding pxl was foreground in 14132268.0 cases, lower in 1561771.0 cases, same in 3569705.0 cases
'''