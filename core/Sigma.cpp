//
// Created by Beat Wolf on 11.05.18.
//

#include "Sigma.h"
#include "TMemoryHelper.h"

#include <cmath>
#include <iostream>
#include <algorithm>

static const double cTerm1 = sqrt(pow(2.0 * 3.14159265359, 3.0));//y is the nr of dimensions
//the cTerm1 is the const (in case dimensionality is const) part of the denominator in the density function
// (non-degenerate case) of the multivariate normal distribution
//see https://en.wikipedia.org/wiki/Multivariate_normal_distribution
static const double minMatrixDeterminant = 1.0e-10;
static const bool normalizeLij = true;

Sigma::Sigma(const double * pixelLUT, double** var){
    _pixelLUT = pixelLUT;

    double **inv = inverse(var);//we'll always need the inverse matrix only

    decompose(inv);//Cholesky decomposition (works for a var covar matrix because such a matrix is symmetric. Maybe also other reasons, too?)
    //The Cholesky decomposition of a matrix in an upper and lower triangular matrix (upper=lower triangular matrix gespiegelt)
    //is useful, because now, values inside the matrix are not dependant anymore (as it would be the case in a var covar matrix!)
    //ie they can be updated independently in the mcmc!
    //Also, note that the scaling factor l which we created here, is useful because it gives the mcmc a way to change
    //the size of the whole var covar matrix in 1 step (else this would be very difficult, as for each entry of the
    //triangular matrix such a move would have to be accepted separately-- this is not very likely, would
    //take more time for the mcmc to get to good values)
    mem::FreeArray2d(&inv, 3, 3);

    _term2 = log(GetDeterminantSimplified() / cTerm1);//determinant of the inverse of matrix X = 1/determinant of matrix X
    //ie _term2 is the log of the whole denominator in the density function of the multivariate normal distr (non-denerate case)
    // again, see https://en.wikipedia.org/wiki/Multivariate_normal_distribution
    //note: instead of a simple var, we need to use the var covar matrix in higher dimensionalities


    if(_term2 != _term2){ // in cpp, this is if term2 is not a nr (not each matrix is invertible)
        std::cout << "nan" << std::endl;
    }
}

Sigma::Sigma(Sigma* sigma){
    //this is a copy constructor
    CopySigma(sigma);
}

void Sigma::CopySigma(Sigma * sigma){
    //this function copies the var from the obj given as parameter into the owns object (through which this is called) variables
    _l = sigma->_l; //this is equivalent to _l=(*sigma)._l i.e. I copy the _l of my obj and save it as _l
    _l11 = sigma->_l11;
    _l21 = sigma->_l21;
    _l31 = sigma->_l31;
    _l22 = sigma->_l22;
    _l32 = sigma->_l32;
    _l33 = sigma->_l33;

    _lPow2 = sigma->_lPow2;
    _lPow3 = sigma->_lPow3;
    _term2 = sigma->_term2;
    _pixelLUT = sigma->_pixelLUT;

    _lChanges = sigma->_lChanges;

    _l11Changes = sigma->_l11Changes;
    _l22Changes = sigma->_l22Changes;
    _l33Changes = sigma->_l33Changes;
    _l21Changes = sigma->_l21Changes;
    _l31Changes = sigma->_l31Changes;
    _l32Changes = sigma->_l32Changes;

    _singular = sigma->_singular;
}

//https://rosettacode.org/wiki/Cholesky_decomposition
void Sigma::decompose(double** var){
    _l11 = sqrt(var[0][0]);
    _l21 = (1. / _l11) * var[0][1];
    _l22 = sqrt(var[1][1] - (_l21 * _l21));
    _l31 = (1. / _l11) * var[0][2];
    _l32 = (1. / _l22) * (var[1][2] - _l31 * _l21);
    _l33 = sqrt(var[2][2] - (_l31 * _l31 + _l32 * _l32));

   // _l = 1;
    if(normalizeLij){
        _l = std::max(std::abs(_l11), std::abs(_l21));
        _l = std::max(_l, std::abs(_l22));
        _l = std::max(_l, std::abs(_l31));
        _l = std::max(_l, std::abs(_l32));
        _l = std::max(_l, std::abs(_l33));
        //so _l is just the max entry of the triangular matrix

        if(_l > 0){ //this is always true as we took the absolute value above
            _l11 = _l11 / _l;
            _l21 = _l21 / _l;
            _l22 = _l22 / _l;
            _l31 = _l31 / _l;
            _l32 = _l32 / _l;
            _l33 = _l33 / _l;
        }
    }else{
        _l = 1;
    }


    _lPow2 = _l * _l;
    _lPow3 = _lPow2 *_l;
}

void correctSingularMatrix(double ** matrix){
    //this just increases the var by a small value
    for(int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            //std::cout << matrix[x][y] << " ";
            if(x == y){
                matrix[x][y] += 0.00001;
            }
        }
        //std::cout << std::endl;
    }
}

//https://www.dr-lex.be/random/matrix-inv.html
double ** inverse(double ** matrix){
    double **inverseMatrix;
    mem::InitializeArray2d(&inverseMatrix, 3, 3);

    double det = determinantSymetric(matrix);
    //std::cout << det << std::endl;

    //Check for singular matrix

    if(abs(det) <= minMatrixDeterminant){

        //TODO: This is a heuristic to no longer have a singular matrix
        do{
            correctSingularMatrix(matrix);
            det = determinantSymetric(matrix);
        }while(abs(det) <= minMatrixDeterminant);

        //std::cout << "Corrected to " << det << " " << abs(det) << " " << (abs(det) <= minMatrixDeterminant)  << std::endl;
    }

    //now, as we are sure to not have a singular matrix (not invertible), we can calculate the inverse matrix
    //matrix*inverse matrix= inverse matrix* matrix= identity matrix (diagonal is filled with 1's, all other entries are 0)

    //the inverse matrix= 1/det * the adjugate matrix
    //First, the adjugate matrix is calculated: (https://en.wikipedia.org/wiki/Adjugate_matrix#3_%C3%97_3_generic_matrix)

     //|  a33a22-a32a23  -(a33a12-a32a13)   a23a12-a22a13 |
    inverseMatrix[0][0] = matrix[2][2] * matrix[1][1] - matrix[1][2] * matrix[2][1];
    inverseMatrix[1][0] = -(matrix[2][2] * matrix[1][0] - matrix[1][2] * matrix[2][0]);
    inverseMatrix[2][0] = matrix[2][1] * matrix[1][0] - matrix[1][1] * matrix[2][0];

    //|-(a33a21-a31a23)   a33a11-a31a13  -(a23a11-a21a13)|
    inverseMatrix[0][1] = -(matrix[2][2] * matrix[0][1] - matrix[0][2] * matrix[2][1]);
    inverseMatrix[1][1] = matrix[2][2] * matrix[0][0] - matrix[0][2] * matrix[2][0];
    inverseMatrix[2][1] = -(matrix[2][1] * matrix[0][0] - matrix[0][1] * matrix[2][0]);

    //|  a32a21-a31a22  -(a32a11-a31a12)   a22a11-a21a12 |
    inverseMatrix[0][2] = matrix[1][2] * matrix[0][1] - matrix[0][2] * matrix[1][1];
    inverseMatrix[1][2] = -(matrix[1][2] * matrix[0][0] - matrix[0][2] * matrix[1][0]);
    inverseMatrix[2][2] = matrix[1][1] * matrix[0][0] - matrix[0][1] * matrix[1][0];

    //then we divide by the det
    for(int x = 0; x < 3; x++){
        for(int y = 0; y < 3; y++){
            inverseMatrix[x][y] /= det;
            //std::cout << inverseMatrix[x][y] << " " << det << std::endl;
        }
    }

    return inverseMatrix;
}

/**
 * https://math.stackexchange.com/questions/603213/simplified-method-for-symmetric-matrix-determinants
 */
double determinantSymetric(double **matrix){
    //a(df−e2)+b(ce−bf)+c(be−dc).

    double a, b, c, d, e, f;

    a = matrix[0][0];
    d = matrix[1][1];
    f = matrix[2][2];

    b = matrix[0][1];
    c = matrix[0][2];
    e = matrix[1][2];

    return a*(d*f - e*e) + b*(c*e-b*f) + c*(b*e-d*c);
}

double Sigma::GetDeterminantSimplified() const{
    double absLs = _l11 * _l22 * _l33;

    return _lPow3 * absLs;
}

void Sigma::SetL(double l){
    _l = l;
    _lPow2 = l * l;
    _lPow3 = _lPow2 * l;

    _term2 = log(GetDeterminantSimplified() / cTerm1);
    _lChanges++;
}

double Sigma::GetL() const{
    return _l;
}

int Sigma::GetLChanges() const{
    return _lChanges;
}

int Sigma::GetLChanges(int i, int j) const{

    if(i == 0){

        if(j == 0){
            return _l11Changes;
        }else if(j == 1){
            return _l21Changes;
        }else if(j == 2){
            return _l31Changes;
        }

    }else if(i == 1){
        if(j == 1){
            return _l22Changes;
        }else if(j == 2){
            return _l32Changes;
        }

    }else if(i == 2){

        if(j == 2){
            return _l33Changes;
        }

    }

    std::cout << "L " << i << " " << j << " does not exist! (GetLChanges)" << std::endl;
}

double Sigma::GetL(int i, int j) const{
    //TODO: This is not pretty at all
    //double _l11, _l21, _l31, _l22, _l32, _l33;
    if(i == 0){

        if(j == 0){
            return _l11;
        }else if(j == 1){
            return _l21;
        }else if(j == 2){
            return _l31;
        }

    }else if(i == 1){
        if(j == 1){
            return _l22;
        }else if(j == 2){
            return _l32;
        }

    }else if(i == 2){

        if(j == 2){
            return _l33;
        }

    }

    std::cout << "L " << i << " " << j << " does not exist! (GetL)" << std::endl;
}

void Sigma::SetL(int i, int j, double value){
    //TODO: This is not pretty at all

    if(i == 0){

        if(j == 0){
            _l11 = value;
            _l11Changes++;
        }else if(j == 1){
            _l21 = value;
            _l21Changes++;
        }else if(j == 2){
            _l31 = value;
            _l31Changes++;
        }

    }else if(i == 1){
        if(j == 1){
            _l22 = value;
            _l22Changes++;
        }else if(j == 2){
            _l32 = value;
            _l32Changes++;
        }

    }else if(i == 2){

        if(j == 2){
            _l33 = value;
            _l33Changes++;
        }
    }

    if(i == j){
        _term2 = log(GetDeterminantSimplified() / cTerm1);
    }
}


void Sigma::ResetChangeCounters(){
    _lChanges = 0;
    _l11Changes = 0;
    _l22Changes = 0;
    _l33Changes = 0;
    _l21Changes = 0;
    _l31Changes = 0;
    _l32Changes = 0;
}

bool Sigma::IsSingular(){
    return _singular;
}

void Sigma::SetIsSingular(bool singular){
    _singular = singular;
}

double ** Sigma::GetSigma(){
    //https://ncalculators.com/matrix/3x3-matrix-multiplication-calculator.htm
    //note: sigma is the true var covar matrix (ie its the scale factor l* the lower triangular matrix* the scale factor l * the upper triangular matrix)
    double **sigma;
    mem::InitializeArray2d(&sigma, 3, 3);

    double lSquared = _lPow2;

    sigma[0][0] = lSquared * _l11 * _l11;
    sigma[1][0] = lSquared * _l11 * _l21;
    sigma[2][0] = lSquared * _l11 * _l31;

    sigma[0][1] = lSquared * _l21 * _l11;
    sigma[1][1] = lSquared * _l21 * _l21 + lSquared * _l22 * _l22;
    sigma[2][1] = lSquared * _l21 * _l31 + lSquared * _l22 * _l32;

    sigma[0][2] = lSquared * _l31 * _l11;
    sigma[1][2] = lSquared * _l31 * _l21 + lSquared * _l32 * _l22;
    sigma[2][2] = lSquared * _l31 * _l31 + lSquared * _l32 * _l32 + lSquared * _l33 * _l33;

    double **inv = inverse(sigma);
    mem::FreeArray2d(&sigma, 3, 3);
    return inv;
}

double Sigma::LogDensity(const unsigned char * const x, const double * const mean) const{
    //calculates density of the multivariate normal distr
    //https://en.wikipedia.org/wiki/Multivariate_normal_distribution

    //x is the pxl j and mean is what we should expect the pxl j to be in case it is bg

    //x-mü
    const double d1 = _pixelLUT[x[0]] - mean[0]; //difference from what is expected to the actual value--> converted to value between 0 and 1
    const double d2 = _pixelLUT[x[1]] - mean[1];
    const double d3 = _pixelLUT[x[2]] - mean[2];

    //calculate the nominator:
    //0.5*(x-mü) transposed (dh als Zeilenvektor) * l(scale factor) * lower triangular matrix (of the inverse matrix of course)
    // * l(scale factor)*upper triangular matrix * (x-mü)

    //note that (x-mü) transposed * lower triangular matrix = upper triangular matrix * (x-mü) = Spaltenvektor (a, b, c)
    const double a = _l11 * d1 + _l21 * d2 + _l31 * d3;
    const double b = _l22 * d2 + _l32 * d3;
    const double c = _l33 * d3;

    double val = _lPow2; //l^2
    val *= (a * a + b * b + c * c);

    val *= -0.5;//val is the whole nominator in the log scale

    return _term2 + val;//_term2 is the log of 1/denominator of the multivariate normal distribution
    //ie this returns the density of the multivariate normal distr (of the differences of pxls from their expected value) for pxl j
}

double Sigma::LogDensityDist(const unsigned char* x, const double * const mean, const double * const dist) const{
    //x=pixel, mean=_meanPixel[j] =average col of pixel j across all imgs, dist=_distImage[i] ie the distortion of col m across all pixels j in img i

    double mDist[3];

    //for the 3 color channels:
    mDist[0] = std::min(1.0, std::max(0.0, mean[0] * dist[0])); //dist * the mean = what we should expect the pxl j to be in case it is bg
    mDist[1] = std::min(1.0, std::max(0.0, mean[1] * dist[1]));
    mDist[2] = std::min(1.0, std::max(0.0, mean[2] * dist[2]));

    return LogDensity(x, mDist);
}

bool isSingularMatrix(double **matrix){
    return determinantSymetric(matrix) <= minMatrixDeterminant;
}