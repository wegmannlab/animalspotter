#ifndef TMEMORYHELPER_H
#define TMEMORYHELPER_H

#include <stdlib.h>

namespace mem
{
    // Templates must be declared and implemented
    // in the header file.

    template<class T>
    void InitializeArray2d(T*** a, unsigned int x, unsigned int y)
    {
        *a = (T**)malloc(x * sizeof(T*));//*a contains an array of x elements, each of them has size T* (ie each
        //element is an array). This is casted to T** (a 2D array)

        for (unsigned int i = 0; i < x; ++i)
        {
			//if (i % 1000 == 0) std::cout << i << " of " << x << std::endl;
            (*a)[i] = (T*)calloc(y, sizeof(T)); //Allocates a block of memory for an array of num(=y) elements,
            // each of them size (=T) bytes long, and initializes all its bits to zero (as opposed to malloc, which
            // does not initialize anything). calloc returns a pointer of type
            //void, which is casted here to a T* object.
        }
    }

    template<class T>
    void FreeArray2d(T*** a, unsigned int x, unsigned int y)
    {
        for (unsigned int i = 0; i < x; ++i)
        {
            free((*a)[i]); //(*a)[i] is an array with y elements
        }

        free(*a); //the now empty 2d array is deleted
    }

    template<class T>
    void InitializeArray3d(T**** a, unsigned int x, unsigned int y, unsigned int z)
    //see method InitializeArray2d, should be clear then
    {
        *a = (T***)malloc(x * sizeof(T**));

        for (unsigned int i = 0; i < x; ++i)
        {
            (*a)[i] = (T**)calloc(y, sizeof(T*));

            for (unsigned int j = 0; j < y; ++j)
            {
                (*a)[i][j] = (T*)calloc(z, sizeof(T));
            }
        }
    }

    template<class T>
    void FreeArray3d(T**** a, unsigned int x, unsigned int y, unsigned int z)
    {
        //see method FreeArray2d, should be clear then
        for (unsigned int i = 0; i < x; ++i)
        {
            for (unsigned int j = 0; j < y; ++j)
            {
                free((*a)[i][j]);
            }

            free((*a)[i]);
        }

        free(*a);
    }
}

#endif
