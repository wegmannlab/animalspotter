#include "TAlgorithm.h"
#include "tinydir.h"
#include "THeuristic.h"
#include "TObjectDetector.h"
#include <algorithm>

//Output more information about a specific image

//#define DEBUG_IMAGE 0
//#define DEBUG_FG
//#define DEBUG_Z
//#define DEBUG_BG_MEAN

//Allows to disable specific parts of the algorithm
static const bool update_z = true;
static const bool update_bg_mean = true;
static const bool update_bg_sigma = true;
static const bool update_bg_sigma_l = true;
static const bool update_fg_mean = true;
static const bool update_fg_sigma = true;
static const bool update_fg_sigma_l = true;
static const bool update_image_distortion = true;
static const bool use_neighbours = true; //Setting alpha to 0 has same effect

static const double min_sd_adjustement_ratio = 0.2;
static const double max_sd_adjustement_ratio = 4;
static const double target_acceptance_rate = 1. / 3.;
static const bool force_singular_matrix_bg = true;

TAlgorithm::TAlgorithm(double lambda,
                       unsigned int chainLength,
                       unsigned int burnin,
                       unsigned int burnin_nr,
                       double *standardDeviations, //*standardDeviations means the value stored there
                       unsigned int seed,
                       double prior,
                       int numThreads) {
    _heuristic = 0;
    _lambda = lambda;
    _chainLength = chainLength;
    _burnin = burnin;
    _burnin_nr=burnin_nr;
    _standardDeviations = standardDeviations;

    _output_box_colors[0] = 255;
    _output_box_colors[1] = 69;
    _output_box_colors[2] = 0;

    _fastMath = new TFastMath(1000);

    _outputPath = nullptr; //null pointer

    // Pixel lookup table
    _pixelLut = CreateLUT();

    #if defined(_OPENMP)
        omp_set_num_threads(numThreads);
    #else
        numThreads = 1; //Force num threads to 1 if openmp is disabled
    #endif

    _numThreads = numThreads;

    _random = (TRandom**)malloc(_numThreads * sizeof(TRandom*)); //TODO: Do this the C++11 way
    //malloc() allocates a block of uninitialized memory (size must be given to it as an argument) & returns a void pointer
    //to the first byte of the allocated memory block if the allocation succeeds (else ie if there is no free space left, it returns NULL)
    //That the pointer is void should clarify that the datatype of the allocated memory is not known.
    //Here, we store _numThreads (var saying how many) elements of type TRandom*. Then the resulting void pointer is casted
    // to be of type TRandom* (would be done automatically but it's clearer to write it)
    //TRandom* means the size of an object of the class TRandom

    for(int i = 0; i < _numThreads; i++){
        double rx, ry;

        if(i == 0){
            rx = seed;
            ry = seed * 2;
        }else{
            rx = _random[i - 1]->RandomUniform();
            ry = _random[i - 1]->RandomUniform();
        }
        _random[i] = new TRandom(_fastMath, rx, ry); //array of length _numThreads
    }

    // Probabilities of z
    _probZBG = log(1 - prior);
    _probZFG = log(prior); //probability to be foreground is given by the log of the prior
    _nImages = 0;
}

TAlgorithm::~TAlgorithm() { //this is the destructor (same name as constructor but with a tilde in front)
    if(_inited){
        CleanupMatrices();
    }

    delete _fastMath;
    delete [] _pixelLut;

    for(int i = 0; i < _numThreads; i++){
        delete _random[i];
    }

    free(_random);
}


void TAlgorithm::SetDebugImages(bool debug){
    _debugImage = debug;
}

void TAlgorithm::SetDebugPixel(int pixel){
    _debugPixel = pixel;
}


void TAlgorithm::CleanupMatrices(){
    // Per pixel across images
    mem::FreeArray2d(&_meanPixel, _nPixels, nChannels); //the array containing the mean pixel values across all images is deleted

    // Per image
    mem::FreeArray2d(&_meanImage, _nImages, nChannels);// the array containing the mean pixel value of each image is deleted
    mem::FreeArray2d(&_distImage, _nImages, nChannels);

    // Optimize by using bitmasks -> uses 1/8 memory !!!
    // Boolean value (1 / 0) per pixel per image
    mem::FreeArray2d(&_z, _nImages, _nPixels);
    mem::FreeArray2d(&_tmpZ, _nImages, _nPixels); //temporary z; this is used for calculation purposes but does not hold
    //any info we should keep after the calculations

    // For testing
    mem::FreeArray2d(&_sumZ, _nImages, _nPixels);
    //

    mem::FreeArray2d(&_debugColors, _chainLength, nChannels);

    delete [] _meanPixelAcceptance;
    delete [] _meanImageAcceptance;
    delete [] _distImageAcceptance;

    for(int i = 0; i < _nImages; i++){
        delete _varCovarFG[i];
        delete _proposedVarCovarFG[i];
    }

    for(int j = 0; j < _nPixels; j++){
        delete _varCovarBG[j];
        delete _proposedVarCovarBG[j];
    }


    delete [] _varCovarBG;
    delete [] _varCovarFG;
    delete [] _proposedVarCovarBG;
    delete [] _proposedVarCovarFG;

    _inited = false;
}


void TAlgorithm::InitializeMatChain(char *outputTag) {
    _inited = true;
    InitializeMatrices();
    InitializeChain(outputTag);
}

void TAlgorithm::InitializeMatrices() {
    mem::InitializeArray2d(&_meanPixel, _nPixels, nChannels); //creates an array containing the mean color values of each pixel across all images
    mem::InitializeArray2d(&_meanImage, _nImages, nChannels); //creates an array containing the mean color values for every image

    _varCovarBG = new Sigma*[_nPixels]; //creates an array containing a pointer to a sigma object for each pixel
    _varCovarFG = new Sigma*[_nImages]; //creates an array containing a pointer to a sigma object for each image
    _proposedVarCovarBG = new Sigma*[_nPixels];
    _proposedVarCovarFG = new Sigma*[_nImages];

    _meanPixelAcceptance = new int[_nPixels];
    std::fill(_meanPixelAcceptance, _meanPixelAcceptance + _nPixels, 0); //the array _meanPixelAcceptance is filled with zeroes

    _meanImageAcceptance = new int[_nImages];
    std::fill(_meanImageAcceptance, _meanImageAcceptance + _nImages, 0);

    _distImageAcceptance = new int[_nImages];
    std::fill(_distImageAcceptance, _distImageAcceptance + _nImages, 0);

    mem::InitializeArray2d(&_distImage, _nImages, nChannels); //creates a 2d array of size _nImages x nChannels,
    // I guess that each element has space for 1 &_distImage (ie a double)

    // Optimize by using bitmasks -> uses 1/8 memory !!!
    // Boolean value (1 / 0) per pixel per image
    mem::InitializeArray2d(&_z, _nImages, _nPixels);
    mem::InitializeArray2d(&_tmpZ, _nImages, _nPixels);

    // sum of z for grayscale image
    mem::InitializeArray2d(&_sumZ, _nImages, _nPixels);

    // Set all z to zero, could be done using calloc, but
    // speed isn't an issue here.
    // Also set all distortions to zero.
    for (unsigned int i = 0; i < _nImages; ++i) {
        for (unsigned int m = 0; m < nChannels; ++m) {
            _distImage[i][m] = 1; //this distortion is a factor ie 1 means that there is no distortion
        }

        for (unsigned int j = 0; j < _nPixels; ++j) {
            _z[i][j] = 0;
            _sumZ[i][j] = 0;
        }
    }

    mem::InitializeArray2d(&_debugColors, _chainLength, nChannels);
}

double * CreateLUT(){
    // Pixel lookup table: array containing values between 0 and 1 corresponding to the color values 0-255
    double * pixelLut = new double[256];
    for (int i = 0; i < 256; ++i) {
        pixelLut[i] = (double) i / 255.0;
    }
    return pixelLut;
}

void TAlgorithm::InitMeanBG(int nPixels, int nImages, int nChannels, unsigned char** z, double** meanPixel, const std::vector<TImage *> images) {
    int *pixelCounter = new int[nPixels];
    std::fill(pixelCounter, pixelCounter+nPixels, 0);

    //first set all entries of the meanPixel array to 0 (this is necessary because foreground pixels should be 0)
    for (unsigned int j = 0; j < nPixels; ++j) {
        for (unsigned char m = 0; m < nChannels; ++m) {
            meanPixel[j][m] = 0;
        }
    }

    for (unsigned int i = 0; i < nImages; ++i) {
        for (unsigned int j = 0; j < nPixels; ++j) {
            if(z[i][j] == 0){ //Only use background pixels
                for (unsigned char m = 0; m < nChannels; ++m) {
                    double pixel = (double) images[i]->_image[j*nChannels + m] / 255.0; //get the color as value between 0 and 1
                    meanPixel[j][m] += pixel; // add the pixel value of color m of all background pixels at position j (ie across all images)
                }
                pixelCounter[j]++; //counts how many times a particular pixel position is background
            }
        }
    }

    for (unsigned int j = 0; j < nPixels; ++j) {
        if(pixelCounter[j] > 0){
            for (unsigned char m = 0; m < nChannels; ++m) {
                meanPixel[j][m] /= (double) pixelCounter[j]; //divide by the number of images where this pixel was background to get the mean:)
            }
        }//TODO: Init BG color to something smart if all pixels are FG at this pos
    }

    delete [] pixelCounter;
}

void TAlgorithm::InitializeChain(char *outputTag) {
    /////////////////////////////////////////////////////////////////////////////////
    // Calculate the meanPixel and meanImage of pixel values.
    // Summing the values each pixel across all images and
    // the values of each pixel in an image

    //Calculate mean background image (_meanPixel)
    InitMeanBG(_nPixels, _nImages, nChannels, _z, _meanPixel, _images);

    //Calculate mean foreground color per image

    //first just calculate the meanImage (considering bg and fg)
    for (unsigned int i = 0; i < _nImages; ++i) {
        for (unsigned int j = 0; j < _nPixels; ++j) {
            for (unsigned char m = 0; m < nChannels; ++m) {
                _meanImage[i][m] += ((double) _images[i]->_image[j*nChannels + m] / 255.0); //divide by 255 to get a value between 0 and 1
            }
        }
    }

    for (unsigned int i = 0; i < _nImages; ++i) {
        for (unsigned char m = 0; m < nChannels; ++m) {
            _meanImage[i][m] /= (double) _nPixels;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Print out the _meanPixel of all images (mu)
    unsigned int rgbPixels = _imageWidth * _imageHeight * nChannels;
    unsigned char *bufImg = new unsigned char[rgbPixels];
    int k = 0;
    /////////////////////////////////////////////////////////////////////////////////

    for (unsigned int j = 0; j < _nPixels; ++j) {
        //bufImg is a 1d array & holds the values of the mean bg image
        bufImg[k] = _meanPixel[j][0] * 255.0;
        ++k;
        bufImg[k] = _meanPixel[j][1] * 255.0;
        ++k;
        bufImg[k] = _meanPixel[j][2] * 255.0;
        ++k;
    }

    if(_outputPath != nullptr && outputTag != nullptr){
        char buf[1024];
        snprintf(buf, sizeof buf, "%s/%s_meanPixelImageOriginal.%s", _outputPath,
                 outputTag, "png");
        if (!io::SavePNG_RGB(bufImg, _imageWidth, _imageHeight, &buf[0])) {
            std::cout << "image %s has not been printed out" << buf;
        }
    }

    delete [] bufImg;

    if(_heuristic > 0){
        std::cout << "Perform initial heuristic with " << _heuristic << " foreground percentage" << std::endl;
        THeuristic heuristic(_z, _meanImage, _distImage, _meanPixel, _sumZ, _images, _pixelLut,
        _nImages, _nPixels, _imageWidth, nChannels); //create a THeuristic obj called heuristic
        heuristic.ExtractForeground(_heuristic); //classifies the pxls into fg & bg. Fills meanImage with the mean fg value in case
        //at least 1 pxl at pos j is fg, else it fills this position with the mean (bg) value
    }

    //Background sigma
    for (unsigned int j = 0; j < _nPixels; ++j) {
        double **tempMatrix;
        mem::InitializeArray2d(&tempMatrix, 3, 3);

        bool allImagesBG = estimateSigmaPixel(tempMatrix, _meanPixel[j], _images, j); //meanPixel is the mean bg color at pxl j
        //this calculates the var covar (color channels) --> tempMatrix
        //allImagesBG tells whether all imgs are bg at pixel j (or at most 1 pxl is bg at pixel j)

        bool isSingular = false;
        if(allImagesBG){
            isSingular = isSingularMatrix(tempMatrix); //is the matrix det above a certain min (ie is there enough var); A square matrix is singular if and only if its determinant is zero
        }

        _varCovarBG[j] = new Sigma(_pixelLut, tempMatrix); //creates a new Sigma object;
        //the triangular matrix of the tempMatrix is calculated (Cholesky decomposition), entries are stored as _l11 and so on.
        //_l is the scaling factor
        _varCovarBG[j]->SetIsSingular(isSingular);

        _proposedVarCovarBG[j] = new Sigma(_varCovarBG[j]); //copy of the sigma obj '_varCovarBG[j]'

        mem::FreeArray2d(&tempMatrix, 3, 3);
    }

    //Foreground sigma
    for (unsigned int i = 0; i < _nImages; ++i) {
        double **tempMatrix;
        mem::InitializeArray2d(&tempMatrix, 3, 3);

        estimateSigmaImage(tempMatrix, _meanImage[i], _images, i); //calculates the var covar matrix for the fg // note that _meanImage[i] holds the
        //mean fg pxl value in case there is >1 pxl classified as fg (else it just holds the mean bg color there)

        _varCovarFG[i] = new Sigma(_pixelLut, tempMatrix);
        _proposedVarCovarFG[i] = new Sigma(_varCovarFG[i]);

        mem::FreeArray2d(&tempMatrix, 3, 3);
    }
}

bool TAlgorithm::estimateSigmaPixel(double **sigmaMatrix,
                               double *means,
                               std::vector<TImage *> values,
                               int pixel) {
    //I guess that this actually calculates the var covar matrix. It returns whether each pxl at the given pos is classified as bg
    int bgPixels = 0;
    for (int k = 0; k < _nImages; k++) {
        if (_z[k][pixel] == 0) {
            bgPixels++;
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {

            double total = 0;

            for (int k = 0; k < _nImages; k++) {
                if (bgPixels <= 1 || _z[k][pixel] == 0) { //if there are <=1 bg pixels at 'pixel', or if the pxl in the img k is bg
                    total += (_pixelLut[values[k]->_image[pixel * nChannels + i]] - means[i]) *
                             (_pixelLut[values[k]->_image[pixel * nChannels + j]] - means[j]);
                }
            }

            sigmaMatrix[i][j] = total;
        }
    }

    if (bgPixels <= 1) { //if there are <=1 bg Pixels, all Pixels are counted as bg
        bgPixels = _nImages;
    }


    double prefix = 1. / (bgPixels - 1);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            sigmaMatrix[i][j] *= prefix;
        }
    }

    return bgPixels == _nImages; //
}

bool TAlgorithm::estimateSigmaImage(double **sigmaMatrix, double *means, std::vector<TImage *> values, int image){
    //like estimateSigmaPixel but for the fg not the bg

    int fgPixels = 0;
    for(int k = 0; k < _nPixels; k++){
        if(_z[image][k] == 1){
            fgPixels++;
        }
    }

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            double total = 0;

            for(int k = 0; k < _nPixels; k++){
                if(fgPixels <= 1 || _z[image][k] == 1){
                    total += (_pixelLut[values[image]->_image[k * nChannels + i]] - means[i]) * (_pixelLut[values[image]->_image[k*nChannels + j]] - means[j]);
                }
            }

            sigmaMatrix[i][j] = total;
        }
    }

    if(fgPixels <= 1){
        fgPixels = _nPixels;
    }

    double prefix = 1. / (fgPixels - 1);

    for(int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            sigmaMatrix[i][j] *= prefix;
        }
    }

    return fgPixels == _nPixels;
}

void TAlgorithm::AnalyseAcceptanceRates(const int steps, const double targetAcceptanceRate){
    //The acceptance rates of the mcmc are the nr of changes/(nr of images*nr of steps)
    //(target acceptance rate for mcmc's is 1/3)
    int totalMeanPixelChanges = 0;
    int totalMeanImageChanges = 0;
    int totalDistImageChanges = 0;
    int totalFGLChanges = 0;
    int totalBGLChanges = 0;

    int totalFGL11Changes = 0;
    int totalFGL22Changes = 0;
    int totalFGL33Changes = 0;
    int totalBGL11Changes = 0;
    int totalBGL22Changes = 0;
    int totalBGL33Changes = 0;

    int totalBGChanges = 0;
    int totalFGChanges = 0;

    for(int i = 0; i < _nImages; i++){
        totalMeanImageChanges += _meanImageAcceptance[i];
        totalDistImageChanges += _distImageAcceptance[i];
        totalFGLChanges += _varCovarFG[i]->GetLChanges();

        totalFGL11Changes += _varCovarFG[i]->GetLChanges(0, 0);
        totalFGL22Changes += _varCovarFG[i]->GetLChanges(1, 1);
        totalFGL33Changes += _varCovarFG[i]->GetLChanges(2, 2);

        totalFGChanges += _varCovarFG[i]->GetLChanges(0, 1);
        totalFGChanges += _varCovarFG[i]->GetLChanges(0, 2);
        totalFGChanges += _varCovarFG[i]->GetLChanges(1, 2);
    }

    totalFGChanges += totalFGL11Changes;
    totalFGChanges += totalFGL22Changes;
    totalFGChanges += totalFGL33Changes;
    totalFGChanges += totalFGLChanges;

    for(int j = 0; j < _nPixels; j++){
        totalMeanPixelChanges += _meanPixelAcceptance[j];
        totalBGLChanges += _varCovarBG[j]->GetLChanges();

        totalBGL11Changes += _varCovarBG[j]->GetLChanges(0, 0);
        totalBGL22Changes += _varCovarBG[j]->GetLChanges(1, 1);
        totalBGL33Changes += _varCovarBG[j]->GetLChanges(2, 2);

        totalBGChanges += _varCovarBG[j]->GetLChanges(0, 1);
        totalBGChanges += _varCovarBG[j]->GetLChanges(0, 2);
        totalBGChanges += _varCovarBG[j]->GetLChanges(1, 2);
    }

    totalBGChanges += totalBGL11Changes;
    totalBGChanges += totalBGL22Changes;
    totalBGChanges += totalBGL33Changes;
    totalBGChanges += totalBGLChanges;

    double meanImageAcceptance = (totalMeanImageChanges / ( (double)_nImages * steps));
    double distImageAcceptance = (totalDistImageChanges / ((double)_nImages * steps));
    double meanPixelAcceptance = (totalMeanPixelChanges / ((double)_nPixels * steps));

    double fgLAcceptance = (totalFGLChanges / ( (double)_nImages * steps));
    double bgLAcceptance = (totalBGLChanges / ( (double)_nPixels * steps));

    double fgL11Acceptance = (totalFGL11Changes / ( (double)_nImages * steps));
    double fgL22Acceptance = (totalFGL22Changes / ( (double)_nImages * steps));
    double fgL33Acceptance = (totalFGL33Changes / ( (double)_nImages * steps));
    double bgL11Acceptance = (totalBGL11Changes / ( (double)_nPixels * steps));
    double bgL22Acceptance = (totalBGL22Changes / ( (double)_nPixels * steps));
    double bgL33Acceptance = (totalBGL33Changes / ( (double)_nPixels * steps));

    int sigmaMultiplier = update_bg_sigma_l ? 7 : 6; //ternary operator: as update_bg_sigma_l is always true, sigmaMultiplier is always=7
    //6 because I have 6 elements in the triangular matrix that are not 0 (beat also implemented a way to work without
    //the scale factor l, but we never actually use it

    //7 because I also have the factor l in front of the triangular matrix
    double bgSigmaAcceptance = (totalBGChanges / ( (double)_nPixels * steps * sigmaMultiplier));

    sigmaMultiplier = update_fg_sigma_l ? 7 : 6; //also 7 always
    double fgSigmaAcceptance = (totalFGChanges / ( (double)_nImages * steps * sigmaMultiplier));

    if(_verbose){
        std::cout << "**** Acceptance rates ****" << std::endl;
        std::cout << "Foreground mean acceptance : " << meanImageAcceptance << std::endl;
        std::cout << "Image distortion acceptance : " << distImageAcceptance << std::endl;
        std::cout << "Pixel background mean acceptance : " << meanPixelAcceptance << std::endl;

        std::cout << "Foreground sigma acceptance : " << fgSigmaAcceptance << std::endl;
        std::cout << "Background sigma acceptance : " << bgSigmaAcceptance << std::endl;
        std::cout << "Foreground l acceptance : " << fgLAcceptance << std::endl;
        std::cout << "Background l acceptance : " << bgLAcceptance << std::endl;
        std::cout << "Foreground lii acceptance : " << fgL11Acceptance << " " << fgL22Acceptance << " " << fgL33Acceptance << std::endl;
        std::cout << "Background lii acceptance : " << bgL11Acceptance << " " << bgL22Acceptance << " " << bgL33Acceptance << std::endl;
    }

    if(targetAcceptanceRate >= 0){ //this is always true when called from MCMCStepDebug as the targetAcceptanceRate=1/3 & is const
        if(_verbose) {
            std::cout << "**** Old SDs ****" << std::endl;
            std::cout << "Old foreground mean SD : " << _meanImageSD << std::endl;
            std::cout << "Old image distortion SD : " << _distImageSD << std::endl;
            std::cout << "Old pixel background mean SD : " << _meanPixelSD << std::endl;
            std::cout << "Old FG sigma SD : " << _fgSigmaSD << std::endl;
            std::cout << "Old BG sigma SD : " << _bgSigmaSD << std::endl;
        }

        //Remember that a too high acceptance in the mcmc means that the steps are too small; a too small acceptance on
        //the other side means the steps are too large. An acceptance rate of ~ 1/3 is optimal (targetAcceptanceRate)
        //the step size is adapted in the following:

        double ratio = meanPixelAcceptance / targetAcceptanceRate;//>1 if acceptance is too high (ie too small steps)
        ratio = std::min(max_sd_adjustement_ratio, std::max(min_sd_adjustement_ratio, ratio)); //set ratio to be between 0.2 and 4
        _meanPixelSD *= ratio;//if acceptance is too high, the step size in the mcmc is increased

        ratio = distImageAcceptance / targetAcceptanceRate;
        ratio = std::min(max_sd_adjustement_ratio, std::max(min_sd_adjustement_ratio, ratio));
        _distImageSD *= ratio;

        ratio = meanImageAcceptance / targetAcceptanceRate;
        ratio = std::min(max_sd_adjustement_ratio, std::max(min_sd_adjustement_ratio, ratio));
        _meanImageSD *= ratio;

        ratio = bgSigmaAcceptance / targetAcceptanceRate;
        ratio = std::min(max_sd_adjustement_ratio, std::max(min_sd_adjustement_ratio, ratio));
        _bgSigmaSD *= ratio;

        ratio = fgSigmaAcceptance / targetAcceptanceRate;
        ratio = std::min(max_sd_adjustement_ratio, std::max(min_sd_adjustement_ratio, ratio));
        _fgSigmaSD *= ratio;

        if(_verbose) {
            std::cout << "**** New SDs ****" << std::endl;
            std::cout << "New foreground mean SD : " << _meanImageSD << std::endl;
            std::cout << "New image distortion SD : " << _distImageSD << std::endl;
            std::cout << "New pixel background mean SD : " << _meanPixelSD << std::endl;
            std::cout << "New FG sigma SD : " << _fgSigmaSD << std::endl;
            std::cout << "New BG sigma SD : " << _bgSigmaSD << std::endl;
        }
    }
}

void TAlgorithm::SetVerbose(bool verbose){
    _verbose = verbose;
}


void TAlgorithm::SetOutputBoxColor(unsigned char color[3]){
    _output_box_colors[0] = color[0];
    _output_box_colors[1] = color[1];
    _output_box_colors[2] = color[2];
}

void TAlgorithm::WriteDebugOutputPixel(int step, std::ofstream &logFile, int debugPixel){
    if(_nPixels > debugPixel && debugPixel >= 0){
        logFile << step << "\t";
        logFile << (step < _burnin*_burnin_nr ? "true" : "false");
        logFile << "\t";

        int fgPixels = 0;
        for(int i = 0; i < _nImages; i++){
            if(_z[i][debugPixel] == 1){
                fgPixels++;
            }
        }

        logFile << fgPixels << "\t";

        for(int m = 0; m < nChannels; m++){
            logFile << _meanPixel[debugPixel][m] << "\t"; //average value of color m at pxl 'debugPixel' across all imgs
        }

        logFile << _varCovarBG[debugPixel]->GetL() << "\t";;

        logFile << _varCovarBG[debugPixel]->GetL(0, 0) << "\t";
        logFile << _varCovarBG[debugPixel]->GetL(1, 1) << "\t";
        logFile << _varCovarBG[debugPixel]->GetL(2, 2) << "\t";

        logFile << _varCovarBG[debugPixel]->GetL(0, 1) << "\t";
        logFile << _varCovarBG[debugPixel]->GetL(0, 2) << "\t";
        logFile << _varCovarBG[debugPixel]->GetL(1, 2) << "\t";

        double ** sigma = _varCovarBG[debugPixel]->GetSigma();

        logFile << sigma[0][0] << "\t";
        logFile << sigma[1][1] << "\t";
        logFile << sigma[2][2] << "\t";
        logFile << sigma[0][1] << "\t";
        logFile << sigma[0][2] << "\t";
        logFile << sigma[1][2] << "\t";


        logFile << (_meanPixelAcceptance[debugPixel] / (double)step) << "\t";
        logFile << (_varCovarBG[debugPixel]->GetLChanges() / (double)step);

        mem::FreeArray2d(&sigma, 3, 3);

        logFile << std::endl;
    }
}

void TAlgorithm::WriteDebugOutputImage(int step, std::ofstream &logFile, int debugImage) {
    //debugImage is the index of the img we are looking at
    if(_images.size() > debugImage && debugImage >= 0){ //if debugImage is a possible nr
        logFile << step << "\t";
        logFile << (step < _burnin*_burnin_nr ? "true" : "false"); //if we are still in the burning, write true else write false
        logFile << "\t";

        int fgPixels = 0;
        for(int j = 0; j < _nPixels; j++){
            if(_z[debugImage][j] == 1){
                fgPixels++; //count nr of fg pixels in img 'debugImage'
            }
        }
        logFile << fgPixels << "\t";

        for(int m = 0; m < nChannels; m++){
            logFile << _distImage[debugImage][m] << "\t"; //distImage =distortion of color m across all pxls in img i
        }

        for(int m = 0; m < nChannels; m++){
            logFile << _meanImage[debugImage][m] << "\t"; //mean value of col m across all pxls in the img 'debugImage'
        }

        //then write the triangular matrix and the scale factor to the logFile
        logFile << _varCovarFG[debugImage]->GetL() << "\t";;

        logFile << _varCovarFG[debugImage]->GetL(0, 0) << "\t";
        logFile << _varCovarFG[debugImage]->GetL(1, 1) << "\t";
        logFile << _varCovarFG[debugImage]->GetL(2, 2) << "\t";

        logFile << _varCovarFG[debugImage]->GetL(0, 1) << "\t";
        logFile << _varCovarFG[debugImage]->GetL(0, 2) << "\t";
        logFile << _varCovarFG[debugImage]->GetL(1, 2) << "\t";

        //and the sigma
        double ** sigma = _varCovarFG[debugImage]->GetSigma();

        logFile << sigma[0][0] << "\t";
        logFile << sigma[1][1] << "\t";
        logFile << sigma[2][2] << "\t";
        logFile << sigma[0][1] << "\t";
        logFile << sigma[0][2] << "\t";
        logFile << sigma[1][2] << "\t";

        mem::FreeArray2d(&sigma, 3, 3);

        //also write how much changed to the logfile
        logFile << (_meanImageAcceptance[debugImage] / (double)step) << "\t";
        logFile << (_distImageAcceptance[debugImage] / (double)step) << "\t";

        logFile << (_varCovarFG[debugImage]->GetLChanges() / (double)step);

        logFile << std::endl;
    }

}

void TAlgorithm::InitDebugFiles(char* outputTag, std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog){
    if(_chainLength > 1){
        if(_nPixels > _debugPixel && _debugPixel >= 0){
            char buf[1024];
            snprintf(buf, sizeof buf, "%s/logPixel_%s_%d.csv", _outputPath, outputTag, _debugPixel);
            //the resulting Cstring is stored in 'buf'.
            //'buf' is the max buffer capacity to fill (1 place is needed for the terminating null char).

            //pixelLog is one of the parameters; it is an obj of ofstream (stream class to write on files)
            pixelLog.open(buf); //this opens a file called 'buf' in the mode 'out' (default for ofstream obj)

            //the following is written to the file:
            pixelLog << "step\tburning\t";
            pixelLog << "FG\tr mean BG\tg mean BG\tb mean BG\tl BG\tl11 BG\tl22 BG\tl33 BG\tl21 BG\tl31 BG\tl32 BG\t";
            pixelLog << "s11 BG\ts22 BG\ts33 BG\ts21 BG\ts31 BG\ts32 BG\tmean Acceptance\tl Acceptance";

            pixelLog << std::endl;
        }

        if(_debugImage){
            for(int i = 0; i < _nImages;i++){
                char buf[1024];
                snprintf(buf, sizeof buf, "%s/logImage_%s_%s(%d).csv", _outputPath, outputTag, _images[i]->GetName().c_str(), i);

                std::ofstream *log = new std::ofstream(); //ofstream=stream class to write on files
                log->open(buf);

                (*log) << "step\tburning\tFG\tr dist\tg dist\tb dist\tr mean FG\tg mean FG\tb mean FG\t";
                (*log) << "l FG\tl11 FG\tl22 FG\tl33 FG\tl21 FG\tl31 FG\tl32 FG\t";
                (*log) << "s11 FG\ts22 FG\ts33 FG\ts21 FG\ts31 FG\ts32 FG\tmean Acceptance\tdist Acceptance\tl Acceptance";

                (*log) << std::endl;

                imageLogs.push_back(log); //add log at the end of the array 'imageLogs'
            }
        }
    }
}

void TAlgorithm::CloseDebugFiles(std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog){
    if(_chainLength > 0){
        if(_verbose){
            std::cout << "MCMC acceptance rates:" << std::endl;
        }
        AnalyseAcceptanceRates(_chainLength - _burnin*_burnin_nr, -1); //here, the std are not adapted
    }

    if(_chainLength > 1){
        if(_nPixels > _debugPixel && _debugPixel >= 0) {
            pixelLog.close();
        }

        if(_debugImage) {
            for (int i = 0; i < _nImages; i++) {
                imageLogs[i]->close();
                delete imageLogs[i];
            }
        }
    }
}

bool TAlgorithm::MCMCStepDebug(int step, char* outputTag, std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog){
    if(_debugImage) {
        //Debug outputs
        for (int i = 0; i < _nImages; i++) {
            WriteDebugOutputImage(step, *imageLogs[i], i); //wievielter step in the mcmc
        }
    }
    if(_nPixels > _debugPixel && _debugPixel >= 0) {
        WriteDebugOutputPixel(step, pixelLog, _debugPixel);
    }

    for (int i=1;i<=_burnin_nr;i++){
        if(step == _burnin*i){
            if(_verbose) {
                std::cout <<"Step "<<step<< ": Burnin acceptance rates:" << std::endl;
            }
            AnalyseAcceptanceRates(step, target_acceptance_rate); //here the std are adapted in order to achieve the target acceptance rate

            //Propose new SDs after burnin
            std::fill(_meanPixelAcceptance, _meanPixelAcceptance + _nPixels, 0);
            std::fill(_meanImageAcceptance, _meanImageAcceptance + _nImages, 0);
            std::fill(_distImageAcceptance, _distImageAcceptance + _nImages, 0);

            for(int i = 0; i < _nImages; i++){
                _varCovarFG[i]->ResetChangeCounters();
            }

            for(int j = 0; j < _nPixels; j++){
                _varCovarBG[j]->ResetChangeCounters();
            }
        }
    }




    return true;
}

bool TAlgorithm::MakeMCMC(char* outputTag) {
    //Reset the SD values for each new bin to their original values (as they are modified after the burnin)
    _meanImageSD = _standardDeviations[3];
    _distImageSD = _standardDeviations[1];
    _meanPixelSD = _standardDeviations[0];
    _fgSigmaSD = _standardDeviations[4];
    _bgSigmaSD = _standardDeviations[2];

    std::vector<std::ofstream*> imageLogs;
    std::ofstream pixelLog;

    InitDebugFiles(outputTag, imageLogs, pixelLog);

    for (unsigned int step = 1; step <= _chainLength; ++step) {
        MCMCStepDebug(step, outputTag, imageLogs, pixelLog);
        ChainStep(step - 1);

        #ifdef  DEBUG_BG_MEAN
            SaveBGMean("step", step); //save the bg img
        #endif

        if (_verbose && step % 100 == 0) { //if verbose, print when each 100. step is done
            std::cout << "Step " << step << " done." << std::endl;
        }
    }

    CloseDebugFiles(imageLogs, pixelLog);

    return true;
}

void TAlgorithm::ChainStep(unsigned int _step) {
    TStopWatch stopWatch;
    stopWatch.Start(); //saves actual time

    TAlgorithm::PixelFgBgCalc(_step); //changes pxl classification with prob=hastings ratio
    // how many times a pxl is classified as fg is stored in sumZ[i][j]

    // TODO: Rename function similar to TAlgorithm::UpdateFgVarCorDist();
    // Updating background (per pixel)
    TAlgorithm::UpdateBgPixels();

    // Get meanPix for testing.

    // Updating foreground (per image) and distortions (used in background)
    //_step variable for debugging
    TAlgorithm::UpdateFgVarCorDist(_step);

    stopWatch.StopPrint(); //prints how long this took
}

TRandom * TAlgorithm::GetThreadRandom(){
#if defined(_OPENMP)
    return _random[omp_get_thread_num()];
#else
    return _random[0];
#endif
}

/**
 * Pixel foreground background calculation
 */
void TAlgorithm::PixelFgBgCalc(unsigned int _step) {

    /////////////////////////////////////////////////////////////////////////////////
    // The old values are always calculated again. -> Old values can be stored!
    /////////////////////////////////////////////////////////////////////////////////

    const int nImages = _nImages;
    const int nPixels = _nPixels;

    if(update_z){
        #pragma omp parallel for //This code divides the following into multiple threads, which are run simultaneously.
        for (int i = 0; i < nImages; ++i) {
            TRandom *random = GetThreadRandom();
            for (unsigned int j = 0; j < nPixels; ++j) {

                if(!force_singular_matrix_bg || !_varCovarBG[j]->IsSingular()){ //if the varCovarBG[j] is not singular
                    unsigned char z = _z[i][j];
                    const unsigned char *pixel = &_images[i]->_image[j * nChannels]; //The three colors of the pixel
                    unsigned char zProposed = 1 - z; //propose move to other classification

                    double bgProb = _varCovarBG[j]->LogDensityDist(pixel, _meanPixel[j], _distImage[i]);
                    double fgProb = _varCovarFG[i]->LogDensity(pixel, _meanImage[i]);

                    double newProb = (zProposed == 0) ? bgProb : fgProb; //if proposed is bg, the newProb is the bgProb
                    double oldProb = (zProposed == 0) ? fgProb : bgProb; //and the oldProb is the fgProb

                    double newProbZ = (zProposed == 0) ? _probZBG : _probZFG; //_probZFG = log(prior); //probability to be foreground is given by the log of the prior
                    double oldProbZ = (zProposed == 0) ? _probZFG : _probZBG; //_probZBG = log(1 - prior);



                    double h = (newProb + newProbZ) - (oldProb + oldProbZ); //this is the hastings ratio but in log scale

                    if(use_neighbours){
                        double logEasingAlpha = LogIsingAlphaEnergy(i, j);//how good is proposed move judged on neighbour pxls classification

                        h += logEasingAlpha;

                        //check time difference
                        double logEasingBeta=LogIsingBetaEnergy(i,j);//how good is proposed move judged on neighbour img classification of same pxl

                        h+=logEasingBeta;
                    }


                    //double he = std::min(1.0, exp(h));//the hastings ratio does not need to be explicitely calculated

                    //accept move with prob=hastings ratio
                    if (h > log(random->RandomUniform())) { //in case log hastings ratio is larger than a random nr between 0 and 1
                        _tmpZ[i][j] = zProposed;
                    }else{
                        _tmpZ[i][j] = z;
                    }
                }else{
                    _tmpZ[i][j] = 0; //Singular matrices for BG are always BG (cause it is singular if there is ~no variation)
                }

            }
        }
    }else{ //this is just for testing purposes
        for (unsigned int j = 0; j < nPixels; ++j) {
            for (unsigned int i = 0; i < nImages; ++i) {
                _tmpZ[i][j] = _z[i][j];
            }
        }
    }

    //Move temporary z to general z. This is controversial, maybe z should be updated directly
    //Dona: yeah I dont see why we need the _tmpZ? is this just for security reasons?
    for (unsigned int i = 0; i < nImages; ++i) {
        for (unsigned int j = 0; j < nPixels; ++j) {
            unsigned char z = _tmpZ[i][j];

            _z[i][j] = z;

            if (_step >= _burnin*_burnin_nr && z > 0) {//TODO: Why z > 0? its either 0 or 1
                _sumZ[i][j] += z; //Sum of all z's ie stores how many times the pxl j of an img i belonged to the fg in the mcmc after the burnin, I guess
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
#ifdef DEBUG_Z
    if(update_z){
        unsigned char bufGray[_nPixels];

            // Print out grayscale images of markov chain _z
            char buf[1024];
            double checksum = 0;
            for (unsigned int j = 0; j < _nPixels; ++j) {
                bufGray[j] = _z[DEBUG_IMAGE][j] * 255;
                checksum += _sumZ[DEBUG_IMAGE][j] / (DEBUG_IMAGE * j + 1);

            }
            snprintf(buf, sizeof buf, "%s/%s_%s_%s_%i.%s", _outputPath, "DEBUG",
                     "pixels", _images[DEBUG_IMAGE]->GetName()->c_str(), _step, "tiff");

            if (!io::SaveTiffGrayscale(&bufGray[0], _imageWidth, _imageHeight,
                                       &buf[0])) {
                std::cout << "image %s has not been printed out" << buf;
            }
    }
#endif

}

double TAlgorithm::proposeNewMean(TRandom * random, unsigned int j) {
    //random=the random var object for the current thread, j=the pixel
    double propBgMean[nChannels];
    for (unsigned char m = 0; m < nChannels; m++) {
        propBgMean[m] = random->MoveRandomNormal(_meanPixel[j][m],
                                                 _meanPixelSD, 0, 1);
    }

    // Sum over images to get hastings ratio
    double proposedSumMean = 0;
    double oldSumMean = 0;

    int bgPixels = 0;

    for (unsigned int i = 0; i < _nImages; i++) {
        if (_z[i][j] == 0) {
            proposedSumMean += _varCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], propBgMean, _distImage[i]);
            //This calculates the difference of the pxl value to the expected pxl value and then does something with the varCovarBG[j] matrix
            oldSumMean += _varCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], _distImage[i]);
            bgPixels++;
        }
    }

    //Only adjust mean if at least one pixels is in the background
    if(bgPixels > 0){
        double h = proposedSumMean - oldSumMean;

        if (h > log(random->RandomUniform())){
            _meanPixelAcceptance[j]++;
            memcpy(_meanPixel[j], propBgMean, nChannels * sizeof(double));
            return proposedSumMean;
        }
    }

    return oldSumMean;
}

void TAlgorithm::proposeSigmaBG(TRandom * random, double oldSum, unsigned int j) {

    //Propose new l
    double currentL = _varCovarBG[j]->GetL();

    double proposedL = random->MoveRandomNormalNoBounds(currentL, _bgSigmaSD);

    if(proposedL < 0){
        proposedL = - proposedL;
    }

    double sumCurrent = 0;
    double sumProposed = 0;

    _proposedVarCovarBG[j]->CopySigma(_varCovarBG[j]);
    _proposedVarCovarBG[j]->SetL(proposedL);
    int bgCounter = 0;

    for (unsigned int i = 0; i < _nImages; i++) {
        if (_z[i][j] == 0) {
            bgCounter++;
            if(oldSum != oldSum){ //Dona:?? is this just for testing purposes?
                sumCurrent += _varCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], _distImage[i]);
            }
            sumProposed += _proposedVarCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], _distImage[i]);
        }
    }

    if(bgCounter == 0){
        return;
    }

    if(!(oldSum != oldSum)){ //?? this is always true, no?
        sumCurrent = oldSum;
    }

    double h = sumProposed - sumCurrent;

    if (update_bg_sigma_l && h > log(random->RandomUniform())) { //accept move with prob=h
        Sigma *temp = _varCovarBG[j];
        _varCovarBG[j] = _proposedVarCovarBG[j];
        _proposedVarCovarBG[j] = temp; //I just have to make sure that _varCovarBG & _proposedVarCovarBG do not point to
        // the same matrix else if I change one of them I would also change the other
        sumCurrent = sumProposed;
    }

    //actualize sumCurrent
    //Propose new lii
    sumCurrent = proposeNewLBG(random, sumCurrent, j, 0, 0);
    sumCurrent = proposeNewLBG(random, sumCurrent, j, 1, 1);
    sumCurrent = proposeNewLBG(random, sumCurrent, j, 2, 2);

    //Propose new lij
    sumCurrent = proposeNewLBG(random, sumCurrent, j, 0, 1);
    sumCurrent = proposeNewLBG(random, sumCurrent, j, 0, 2);
    proposeNewLBG(random, sumCurrent, j, 1, 2);

}

double TAlgorithm::proposeNewLBG(TRandom *random, double currentSum, int j, int li, int lj){
    double currentL = _varCovarBG[j]->GetL(li, lj); //this is the lij

    double proposedL = random->MoveRandomNormalNoBounds(currentL, _bgSigmaSD);

    if(li == lj){ //then its the variance, so the correlation can be set to be pos
        proposedL = std::fabs(proposedL); //abs value
    }

    double sumProposed = 0;

    _proposedVarCovarBG[j]->CopySigma(_varCovarBG[j]);
    _proposedVarCovarBG[j]->SetL(li, lj, proposedL);

    for (unsigned int i = 0; i < _nImages; i++) {
        if (_z[i][j] == 0) {
            sumProposed += _proposedVarCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], _distImage[i]);
        }
    }

    double prior = 0;
    //find wishart prior for cholesky decomposed inverse matrix (good that the wishart prior can be found through the
    //inverse matrix, because calculating the inverse (done in Sigm::Sigma) is very expensive
    // --> we always want to work with it & directly update it in the mcmc!)
    //The following is just how you find the wishart prior:
    if(li == lj){//Chi square density distribution, degrees of freedom 4
        prior = ((4. - li) - 1.) * (log(proposedL) - log(currentL)) + 0.5 * (currentL*currentL -proposedL*proposedL);
        //l11 has degree of freedom=3, l22 df=2, l33 df=1
    }else{ //Normal distribution N(0,1)
        prior = (-proposedL * proposedL + currentL * currentL) / 2.;
    }

    double h =sumProposed - currentSum + prior;

    if (h > log(random->RandomUniform())) {
        Sigma *temp = _varCovarBG[j];
        _varCovarBG[j] = _proposedVarCovarBG[j];
        _proposedVarCovarBG[j] = temp;

        return sumProposed;//if move was accepted
    }

    return currentSum; //if not accepted
}

double TAlgorithm::proposeNewLFG(TRandom *random, double currentSum, int i, int li, int lj){
    double currentL = _varCovarFG[i]->GetL(li, lj);

    double proposedL = random->MoveRandomNormalNoBounds(currentL, _fgSigmaSD);

    if(li == lj){
        proposedL = std::fabs(proposedL);
    }

    double sumProposed = 0;

    _proposedVarCovarFG[i]->CopySigma(_varCovarFG[i]);
    _proposedVarCovarFG[i]->SetL(li, lj, proposedL);

    for (unsigned int j = 0; j < _nPixels; j++) {
        if (_z[i][j] == 1) {
            sumProposed += _proposedVarCovarFG[i]->LogDensity(&_images[i]->_image[j * nChannels], _meanImage[i]);
        }
    }

    double prior = 0;
    if(li == lj){//Chi square density distribution, degrees of freedom 4
        prior = ((4. - li) - 1.) * (log(proposedL) - log(currentL)) + 0.5 * (currentL*currentL - proposedL*proposedL);
    }else{ //Normal distribution N(0,1)
        prior = (-proposedL * proposedL + currentL * currentL) / 2.;
    }

    double h = sumProposed - currentSum + prior;

    if (h > log(random->RandomUniform())) {
        Sigma *temp = _varCovarFG[i];
        _varCovarFG[i] = _proposedVarCovarFG[i];
        _proposedVarCovarFG[i] = temp;
        return sumProposed;
    }

    return currentSum;
}

void TAlgorithm::UpdateBgPixels() {

    const int nPixels = _nPixels;

    #pragma omp parallel for
    for (int j = 0; j < nPixels; ++j) {
        TRandom * random = GetThreadRandom();

        double oldSum = NAN;

        // Propose new mean
        if(update_bg_mean){ //this is always true
            oldSum = proposeNewMean(random, j);
        }

        /////////////////////////////////////////////////////////////////////////////////
        // Propose Sigma (ie new var covar matrix)

        if(update_bg_sigma && (!force_singular_matrix_bg || !_varCovarBG[j]->IsSingular())){ //if varCovarBG[j] is not singular
            proposeSigmaBG(random, oldSum, j);
        }
    }
}

void TAlgorithm::UpdateFgVarCorDist(unsigned int _step) {

    const int nImages = _nImages;
    const int nPixels = _nPixels;

    #pragma omp parallel for
    for (int i = 0; i < nImages; i++) {
        double *proposedValue = new double[nChannels];
        TRandom *random = GetThreadRandom();
        double h;

        double oldSum = 0;

        if(update_fg_mean){
            // Propose new mean
            for (int m = 0; m < nChannels; m++) {
                proposedValue[m] = random->MoveRandomNormal(_meanImage[i][m],
                                                            _meanImageSD, 0, 1);
            }

            double proposedSumMean = 0;

            int fgPixels = 0;

            for (unsigned int j = 0; j < nPixels; j++) {
                if (_z[i][j] == 1) {
                    proposedSumMean += _varCovarFG[i]->LogDensity(&_images[i]->_image[j * nChannels], proposedValue);
                    oldSum += _varCovarFG[i]->LogDensity(&_images[i]->_image[j * nChannels], _meanImage[i]);
                    fgPixels++;
                }
            }

            if(fgPixels > 0){
                h = proposedSumMean - oldSum;//prior cancels out (probably because its uniform)

                if (h > log(random->RandomUniform())) {
                    _meanImageAcceptance[i]++;
                    memcpy(_meanImage[i], proposedValue, nChannels * sizeof(double)); //proposed values for all col channels are copied in meanimg
                }
            }else{
                delete [] proposedValue;
                continue; //there are no foreground pixels for this image, we can't calculate anything. go to next image
            }
        }

        #ifdef DEBUG_FG
            if(i == DEBUG_IMAGE){
                for(int m = 0; m < nChannels; m++){
                    _debugColors[_step][m] = _meanImage[i][m];
                }
            }
        #endif

        if(update_fg_sigma){
            //Propose new l
            double currentL = _varCovarFG[i]->GetL();

            double proposedL = random->MoveRandomNormalNoBounds(currentL, _fgSigmaSD);
            if(proposedL < 0){
                proposedL = - proposedL;
            }

            double sumCurrent = 0;
            double sumProposed = 0;

            _proposedVarCovarFG[i]->CopySigma(_varCovarFG[i]);
            _proposedVarCovarFG[i]->SetL(proposedL);

            int counter = 0;
            for (unsigned int j = 0; j < nPixels; ++j) {
                if (_z[i][j] == 1) {
                    if(oldSum != oldSum){
                        sumCurrent += _varCovarFG[i]->LogDensity(&_images[i]->_image[j * nChannels], _meanImage[i]);
                    }

                    sumProposed += _proposedVarCovarFG[i]->LogDensity(&_images[i]->_image[j * nChannels], _meanImage[i]);
                    counter++;
                }
            }

            if(!(oldSum != oldSum)){ //always true?
                sumCurrent = oldSum;
            }

            h = sumProposed - sumCurrent;

            if (h > log(random->RandomUniform()) && update_fg_sigma_l) {
                Sigma *temp = _varCovarFG[i];
                _varCovarFG[i] = _proposedVarCovarFG[i];
                _proposedVarCovarFG[i] = temp;
                sumCurrent = sumProposed;
            }

            //Propose new lii
            sumCurrent = proposeNewLFG(random, sumCurrent, i, 0, 0);
            sumCurrent = proposeNewLFG(random, sumCurrent, i, 1, 1);
            sumCurrent = proposeNewLFG(random, sumCurrent, i, 2, 2);

            //Propose new lij
            sumCurrent = proposeNewLFG(random, sumCurrent, i, 0, 1);
            sumCurrent = proposeNewLFG(random, sumCurrent, i, 0, 2);
            proposeNewLFG(random, sumCurrent, i, 1, 2);
        }

        /////////////////////////////////////////////////////////////////////////////////
        // Propose new distortions
        if(update_image_distortion){
            for (int m = 0; m < nChannels; ++m) {
                // Casting just truncates, is rounding needed?
                proposedValue[m] = random->MoveRandomNormal(_distImage[i][m], _distImageSD, 0.2, 3);
            }

            double proposedSumDist = 0;
            double oldSumDist = 0;

            int bgPixels = 0;
            //int ifChecker= 0;
            for (unsigned int j = 0; j < nPixels; ++j) {
                if (_z[i][j] == 0) {
                    proposedSumDist += _varCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], proposedValue);
                    oldSumDist += _varCovarBG[j]->LogDensityDist(&_images[i]->_image[j * nChannels], _meanPixel[j], _distImage[i]);
                    bgPixels++;
                }
            }

            if(bgPixels > 0){
                // add Priors (because of log scale!)
                proposedSumDist += TAlgorithm::LogNormalDensity(proposedValue, 1, 0.1);
                oldSumDist += TAlgorithm::LogNormalDensity(_distImage[i], 1, 0.1);

                h = proposedSumDist - oldSumDist;

                if (h > log(random->RandomUniform())) {
                    _distImageAcceptance[i]++;
                    memcpy(_distImage[i], proposedValue, nChannels * sizeof(double));
                }
            }
        }

        delete [] proposedValue;
    }
}

// TODO: pow ersetzen. Zwischen speichern und 2 mal berechnen
double TAlgorithm::LogNormalDensity(double *x, double mean, double sigma) {
    double term1 = _fastMath->Log(1.0 / (sigma * 2.50662827463));
    double result = (term1 - (0.5 * pow((x[0] - mean) / sigma, 2.0)))
                    + (term1 - (0.5 * pow((x[1] - mean) / sigma, 2.0)))
                    + (term1 - (0.5 * pow((x[2] - mean) / sigma, 2.0)));

    return result;
}

double TAlgorithm::LogIsingAlphaEnergy(unsigned int imageIndex, unsigned int pixelJ) {
    int currentSum = 1;
    int proposedSum = 1;
    double currentZ = _z[imageIndex][pixelJ];
    const bool canGoLeft = pixelJ % _imageWidth != 0;
    const bool canGoRight = pixelJ % _imageWidth != _imageWidth - 1;

    //Top
    int pixelIndex = pixelJ - _imageWidth;
    if(pixelIndex >= 0){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;//if the pixel above is classified the same--> currentZ++, else proposedSum++
    }

    //TopRight
    pixelIndex = pixelJ - _imageWidth + 1;
    if(pixelIndex >= 0 && canGoRight){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //Right
    pixelIndex =pixelJ + 1;
    if(pixelIndex < _nPixels && canGoRight){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //BottomRight
    pixelIndex = pixelJ + 1 + _imageWidth;
    if(pixelIndex < _nPixels && canGoRight){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //bottom
    pixelIndex = pixelJ + _imageWidth;
    if(pixelIndex < _nPixels){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //bottomLeft
    pixelIndex = pixelJ + _imageWidth - 1;
    if(pixelIndex < _nPixels && canGoLeft){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //left
    pixelIndex = pixelJ - 1;
    if(pixelIndex >= 0 && canGoLeft){
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    //topLeft
    pixelIndex = pixelJ - 1 - _imageWidth;
    if(pixelIndex >= 0 && canGoLeft) {
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    return (proposedSum - currentSum) * _logIsingAlpha; //logIsingAlpha =log(alpha) //proposedSum-currentSum is like how good is the move judged by the neighbours. logIsingAlpha is a weight how important neighbours should be
}

double TAlgorithm::LogIsingBetaEnergy(unsigned int imageI, unsigned int pixelIndex) {
    int currentSum = 1;
    int proposedSum = 1;
    double currentZ = _z[imageI][pixelIndex];
    bool canGoBack=false;
    bool canGoForward=false;

    if (imageI!=0){ //make sure we're not at the first img of a bin
        //note: duration cast always rounds towards zero (ie postive values are rounded down ie truncated)
        std::chrono::seconds previousTimeGap= std::chrono::duration_cast<std::chrono::seconds>(_images[imageI]->GetOriginalTime()-_images[imageI-1]->GetOriginalTime());
        canGoBack = previousTimeGap<std::chrono::seconds(maxNeighbourDiff);
    }

    if (imageI!=_images.size()-1){ //not at the last img of a bin
        std::chrono::seconds nextTimeGap= std::chrono::duration_cast<std::chrono::seconds>(_images[imageI+1]->GetOriginalTime()-_images[imageI]->GetOriginalTime());
        canGoForward =nextTimeGap<std::chrono::seconds(maxNeighbourDiff);
    }

    //before
    if (canGoBack){
        int imageIndex= imageI - 1;
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;//if the pixel in img before is classified the same--> currentZ++, else proposedSum++
    }

    //after
    if (canGoForward){
        int imageIndex = imageI + 1;
        _z[imageIndex][pixelIndex] == currentZ ? currentSum++ : proposedSum++;
    }

    return (proposedSum - currentSum) * _logIsingBeta; //logIsingBeta =log(beta) //proposedSum-currentSum is like how good is the move judged by the neighbour imgs. logIsingBeta is a weight how important neighbours should be
}

bool TAlgorithm::SaveOutput(char *outputPath, char* outputTag) {
    /////////////////////////////////////////////////////////////////////////////////
    _outputPath = outputPath;
    unsigned char *bufGray = new unsigned char[_nPixels];

    /////////////////////////////////////////////////////////////////////////////////

    // Print out grayscale images of markov chain _z

    unsigned long maxZ = _chainLength - _burnin*_burnin_nr;//std::max(1, (_chainLength - _burnin));
    if(maxZ == 0){//This code is used when debugging heuristics, where sumZ is not related to the chain length
        for (unsigned int i = 0; i < _nImages; ++i) {
            for (unsigned int j = 0; j < _nPixels; ++j) {
                maxZ = std::max(maxZ, _sumZ[i][j]);
            }
        }
    }
    if(maxZ <= 0){
        maxZ = 1;
    }

    char buf[1024];
    for (unsigned int i = 0; i < _nImages; ++i) {
        for (unsigned int j = 0; j < _nPixels; ++j) {
            // basically: _sumZ[i][j] / (_chainlength - _burnin) * 255.0;
            const unsigned long t = std::min(maxZ, _sumZ[i][j]);
            const unsigned char gray = (t * 255) / maxZ;
            bufGray[j] = gray;
            //bufGray[j] = _z[i][j] * 255;

        }
        snprintf(buf, sizeof buf, "%s/mcmc_%s_%s_(%d)_%s.%s", _outputPath, outputTag,
                 "pixels", i, _images[i]->GetName().c_str(), "png");
        if (!io::SavePNGGrayscale(bufGray, _imageWidth, _imageHeight,
                                   &buf[0])) {
            std::cout << "image %s has not been printed out" << buf;
            break;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Print out grayscale images of markov chain _z

    for (unsigned int m = 0; m < nChannels; ++m) {
        for (unsigned int j = 0; j < _nPixels; ++j) {
            double **sigma = _varCovarBG[j]->GetSigma();
            // basically: _sumZ[i][j] / (_chainlength - _burnin) * 255.0;
            bufGray[j] = sqrt(sigma[m][m]) * 255.0;

            mem::FreeArray2d(&sigma, 3, 3);
        }
        snprintf(buf, sizeof buf, "%s/%s_%s%s%d.%s", _outputPath, outputTag,
                 "SDofImages", "channel", m, "png");
        if (!io::SavePNGGrayscale(&bufGray[0], _imageWidth, _imageHeight,
                                   &buf[0])) {
            std::cout << "image %s has not been printed out" << buf;
            break;
        }
    }

    delete [] bufGray;

    /////////////////////////////////////////////////////////////////////////////////
    // Print out the _meanPixel of all images (mu)
    SaveBGMean(std::string("meanPixelImage").c_str(), -1, outputTag);

    /////////////////////////////////////////////////////////////////////////////////
    // Print out the debug pixels

    int k = 0;

    unsigned int rgbPixels = _imageWidth * _imageHeight * 3;
    unsigned char *bufImg = new unsigned char[rgbPixels];
    #ifdef DEBUG_FG
    /////////////////////////////////////////////////////////////////////////////////
        unsigned int debugPixels = _chainLength * 3;
        unsigned char *bufImgDebug = new unsigned char[debugPixels];

        for (unsigned int j = 0; j < _chainLength; ++j) {
            bufImgDebug[k] = _debugColors[j][0] * 255.0;
            ++k;
            bufImgDebug[k] = _debugColors[j][1] * 255.0;
            ++k;
            bufImgDebug[k] = _debugColors[j][2] * 255.0;
            ++k;
        }

        snprintf(buf, sizeof buf, "%s/%s_DebugMean.%s", _outputPath,
                 outputTag, "tiff");
        if (!io::SaveTiffRGB(bufImgDebug, _chainLength, 1, &buf[0])) {
            std::cout << "image %s has not been printed out" << buf;
        }
        delete [] bufImgDebug;
    #endif

    /////////////////////////////////////////////////////////////////////////////////
    // Original Images + distortion factor per pixel & image
    // There is still something weird here!

    snprintf(buf, sizeof buf, "%s/%s_distValues.%s", _outputPath, outputTag, "csv");
    FILE *distValues = fopen(buf, "w");

    if(distValues == nullptr){
        std::cout << "Could not open output file " << buf << std::endl;
    }

    fprintf(distValues, "image;red;green;blue\n");

    for (unsigned int i = 0; i < _nImages; ++i) {
        const double distR = _distImage[i][0];
        const double distG = _distImage[i][1];
        const double distB = _distImage[i][2];

        fprintf(distValues, "%d;%f;%f;%f\n", i, distR, distG, distB);

        k = 0;
        for (unsigned int j = 0; j < _nPixels; ++j) {
            // Check if really minus dist[RGB]
            bufImg[k] = std::min(255, std::max((int)(_pixelLut[_images[i]->_image[j * nChannels]] / distR * 255), 0));
            ++k;
            bufImg[k] = std::min(255, std::max((int)(_pixelLut[_images[i]->_image[j * nChannels + 1]] / distG * 255), 0));
            ++k;
            bufImg[k] = std::min(255, std::max((int)(_pixelLut[_images[i]->_image[j * nChannels + 2]] / distB * 255), 0));
            ++k;
        }

        if(_output_bounding_boxes){
            double * normalizedSumZ = new double[_nPixels];
            for(int j = 0; j < _nPixels; j++){
                normalizedSumZ[j] = std::min(_sumZ[i][j], maxZ) / (double)maxZ;
            }

            TObjectDetector detector(_images[i], 0.0, normalizedSumZ, _output_box_colors, _min_box_probability, false);

            std::vector<TBoundingBox *> boxes = detector.GetBoundingBoxes();

            if(_verbose){
                std::cout << "Bounding boxes ( " << _images[i]->GetOriginalWidth() << "/" << _images[i]->GetOriginalHeight() << " ) : " << _images[i]->GetName().c_str() << " " << boxes.size();
            }

            for(TBoundingBox *box: boxes){

                if(_verbose){
                    TBoundingBox originalBox = box->GetOriginalBox(_images[i]);
                    std::cout << " "  <<  originalBox.GetWidth() << "x" << originalBox.GetHeight() << "+" << originalBox.GetX() << "+" << originalBox.GetY() << " " <<
                              box->GetWeight() <<
                              " " << ((double)box->GetConnectedCount() / (box->GetHeight() * box->GetWidth())) <<
                              " " << ((double)box->GetForegroundCount() / (box->GetHeight() * box->GetWidth()));
                }

                const double colorModifier = (0.7 + box->GetWeight() * 0.3);

                for(int x = box->GetX(); x < box->GetX() + box->GetWidth(); x++){
                    for(int y = box->GetY(); y < box->GetY() + box->GetHeight(); y++){
                        if(x == box->GetX() || y == box->GetY() || x == box->GetX() + box->GetWidth() - 1 || y == box->GetY() + box->GetHeight() - 1){
                            int pixelID = (x + _images[i]->GetWidth() * y) * nChannels;

                            bufImg[pixelID] = box->GetColor()[0] * colorModifier;
                            bufImg[pixelID + 1] = box->GetColor()[1] * colorModifier;
                            bufImg[pixelID + 2] = box->GetColor()[2] * colorModifier;
                        }
                    }
                }

                delete box;
            }

            if(_verbose){
                std::cout << std::endl;
            }

            delete [] normalizedSumZ;
        }

        snprintf(buf, sizeof buf, "%s/%s_originalDistortion_%s.%s", _outputPath,
                 outputTag, _images[i]->GetName().c_str(), "png");
        if (!io::SavePNG_RGB(bufImg, _imageWidth, _imageHeight, &buf[0])) {
            std::cout << "image %s has not been printed out" << buf;
        }
    }

    delete [] bufImg;

    fclose(distValues);

    /////////////////////////////////////////////////////////////////////////////////
    // log... File

    return true;
}

void TAlgorithm::SaveBGMean(const char *baseName, int id, char* outputTag){
    char buf[1024];
    unsigned int rgbPixels = _imageWidth * _imageHeight * 3;
    unsigned char *bufImg = new unsigned char[rgbPixels];
    int k = 0;
    /////////////////////////////////////////////////////////////////////////////////

    for (unsigned int j = 0; j < _nPixels; ++j) {
        bufImg[k] = _meanPixel[j][0] * 255.0;
        ++k;
        bufImg[k] = _meanPixel[j][1] * 255.0;
        ++k;
        bufImg[k] = _meanPixel[j][2] * 255.0;
        ++k;
    }

    snprintf(buf, sizeof buf, "%s/%s_%s_%d.%s", _outputPath,
             outputTag, baseName, id, "png");
    if (!io::SavePNG_RGB(bufImg, _imageWidth, _imageHeight, &buf[0])) {
        std::cout << "image %s has not been printed out" << buf;
    }

    delete [] bufImg;
}

void TAlgorithm::SetOutputPath(char *outputPath){
    _outputPath = outputPath;
}

void TAlgorithm::SetNeighborAlpha(double alpha){
    _logIsingAlpha = log(alpha);
}

void TAlgorithm::SetNeighborBeta(double beta){
    _logIsingBeta = log(beta);
}

void TAlgorithm::SetHeuristic(double heuristic){
    _heuristic = heuristic;
}


void TAlgorithm::SetOutputBoundingBoxes(bool outputBoundingBoxes){
    _output_bounding_boxes = outputBoundingBoxes;
}

void TAlgorithm::SetLogPath(char *logPath) {
    _logPath = logPath;
}

int TAlgorithm::GetImageCount() {
    return _images.size();
}

void TAlgorithm::SetImages(std::vector<TImage *> images){
    if(_inited){
        CleanupMatrices();  //deletes all previous initializations
    }

    _images = images;

    TImage * first = images.front(); //images.front() returns a reference to the first element in the vector images
    _imageHeight = first->GetHeight();
    _imageWidth = first->GetWidth();
    _nPixels = _imageWidth * _imageHeight;
    _nImages = images.size(); //nr of elements in the vector images
}


void TAlgorithm::SetMinOutputBoxProbability(double min_box_probability){
    _min_box_probability = min_box_probability;
}