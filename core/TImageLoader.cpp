//
// Created by beat on 26.07.18.
//

#include "TImageLoader.h"
#include "TMemoryHelper.h"
#include "TAlgorithm.h"

#include "../libs/date/date.h"
#include "../libs/easyexif/exif.h"
#include "../libs/jpeg_decoder/jpeg_decoder.h"

#include <cmath>
#include <iterator>
#include <algorithm>


using namespace date;

TImage* TImageLoader::CreateImage(unsigned char * decoded,
                          unsigned int originalImageWidth,
                          unsigned originalImageHeight,
                          unsigned int imageReduction,
                          std::string &name){

    if (imageReduction==1){
        int originalnPixels = originalImageWidth * originalImageHeight;
        unsigned char *dataSmall = new unsigned char[originalnPixels * nChannels];
        //Write every entry of decoded into dataSmall (decoded is an object that is overwritten for every new img; that's why I cannot directly use decoded here)

        //Loop over every pixel
        for (int y = 0; y < originalImageHeight; y++) {
            for (int x = 0; x < originalImageWidth; x++) {
                //Loop over RGB colors
                for (unsigned char rgb = 0; rgb < nChannels; rgb++) {
                    dataSmall[(y * originalImageWidth + x) * nChannels + rgb] = decoded[(y * originalImageWidth + x) * nChannels + rgb];
                }
            }
        }
        return new TImage(name, dataSmall, originalImageWidth, originalImageHeight, originalImageWidth, originalImageHeight);
    }
    else{
        int imageWidth = floor(originalImageWidth / imageReduction);
        int imageHeight = floor(originalImageHeight / imageReduction);
        int nPixels = imageWidth * imageHeight;
        unsigned char *dataSmall = new unsigned char[nPixels * nChannels];
        // resize the image


        double xDisplacement = std::max(1.0, (double) originalImageWidth / imageWidth);
        double yDisplacement = std::max(1.0, (double) originalImageHeight / imageHeight);
        //Loop over every reduced pixel

        #ifdef _WIN32
            #pragma omp parallel for
        #else
            #pragma omp parallel for collapse(2)
        #endif
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {

                //Calculate start region in original picture
                int originalY = (int) (y * yDisplacement);
                int originalX = (int) (x * xDisplacement);

                //Loop over RGB colors
                for (unsigned char rgb = 0; rgb < nChannels; rgb++) {

                    int totalRGB = 0;
                    int totalPixels = 0;

                    //Loop over area around pixel
                    for (int yReduced = originalY;
                         yReduced < originalImageHeight && yReduced < originalY + yDisplacement;
                         yReduced++) {

                        for (int xReduced = originalX;
                             xReduced < originalImageWidth && xReduced < originalX + xDisplacement;
                             xReduced++) {
                            totalRGB += decoded[(yReduced * originalImageWidth + xReduced) * nChannels + rgb];
                            totalPixels++;
                        }
                    }

                    if (totalPixels > 0) {
                        dataSmall[(y * imageWidth + x) * nChannels + rgb] = totalRGB / totalPixels;
                    }
                }
            }
        }

        return new TImage(name, dataSmall, imageWidth, imageHeight, originalImageWidth, originalImageHeight);
    }
}

std::vector<TImage *> TImageLoader::LoadImages(std::string &outputPath, std::string &inputPath, std::vector<std::string> &fileNames, unsigned int imageReduction) {


    std::vector<std::string> imagePaths;
    std::vector<std::string> imageNames;

    if(fileNames.empty()){
        tinydir_dir dir;
        tinydir_open_sorted(&dir, inputPath.c_str());
        int fileCount = dir.n_files;

        for (int i = 0; i < fileCount; i++) {
            tinydir_file f;
            tinydir_readfile_n(&dir, &f, i);

            if (!f.is_dir && (f.extension[0] == 'J' || f.extension[0] == 'j')) {
                imagePaths.push_back(std::string(f.path));
                imageNames.push_back(std::string(f.name));
            }
        }

        try {
            tinydir_close(&dir);
        } catch (int e) {
            // std::cout << "tinydir_close failed with error " << std::cout << e << std::endl;
        }
    }else{
        for(std::string &name : fileNames){
            imagePaths.push_back(inputPath + name);
            imageNames.push_back(name);
        }
    }

    //TODO: Scan if really three Channels are here (RGB) & check for order.
    // Otherwise throw error...

    const int imagePathsSize = imagePaths.size();

    TImage** imagesArray = new TImage*[imagePathsSize];
    #pragma omp parallel for
    for(int i = 0; i < imagePathsSize; i++) {
        imagesArray[i] = nullptr;

        size_t size;
        unsigned char *buffer;
        FILE *file = fopen(imagePaths[i].c_str(), "rb");

        if (!file) {
            std::cout << "Error opening the input file (" << imagePaths[i] << ")."
                      << std::endl;
            continue;
        }

        fseek(file, 0, SEEK_END);
        size = ftell(file);
        buffer = (unsigned char *) malloc(size);
        fseek(file, 0, SEEK_SET);
        fread(buffer, 1, size, file);
        fclose(file);

        //Read image data
        Jpeg::Decoder decoder(buffer, size);

        if (decoder.GetResult() != Jpeg::Decoder::OK) {
            std::cout << "Error decoding the input file " << decoder.GetResult() << " :" << imagePaths[i] << std::endl;
            continue;
        }

        unsigned int originalImageWidth = decoder.GetWidth();
        unsigned int originalImageHeight = decoder.GetHeight();
        unsigned int originalnPixels = decoder.GetImageSize();

        if (originalnPixels <= 0) {
            std::cout << "Input file empty : " << imagePaths[i] << std::endl;
            continue;
        }

        unsigned char *decoded = decoder.GetImage();

        TImage* image = CreateImage(decoded, originalImageWidth, originalImageHeight, imageReduction, imageNames[i]);

        //create a file with approximate memory used
        if (i==0){ //if we are in the first iteration ie reading the first img
            unsigned int memory_needed=sizeof(unsigned char)*image->GetWidth()*image->GetHeight()*3*imagePathsSize;
            std::string myString = outputPath;
            std::stringstream ss;
            ss << myString << "min_estimated_memory_needed.txt";
            std::string path = ss.str();


            std::ofstream file(path); //open in constructor

            std::string data("The min estimated memory [bytes] used to just load the images into AnimalSpotter using input " + inputPath);
            file << data << "\n";
            file << memory_needed<<"bytes"<<std::endl;

            file << std::endl;
            file.close();

        }

        //Read exif tags using easyexif
        easyexif::EXIFInfo result;
        int code = result.parseFrom(buffer, size);
        if (code) {
            printf("Error parsing EXIF: code %d\n", code);
        }else if(!result.DateTimeOriginal.empty()){
            std::chrono::system_clock::time_point tp;
            std::istringstream ss{result.DateTimeOriginal};

            ss >> parse("%Y:%m:%d %H:%M:%S", tp);

            image->SetOriginalTime(tp);
        }

        free(buffer);

        imagesArray[i] = image;
    }

    std::vector<TImage *> images;

    for(int i = 0; i < imagePathsSize; i++) {
        if(imagesArray[i] != nullptr){
            images.push_back(imagesArray[i]);
        }
    }

    delete [] imagesArray;

    return images;

}
