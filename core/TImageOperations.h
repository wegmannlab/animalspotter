#ifndef TIMAGEOPERATIONS_H
#define TIMAGEOPERATIONS_H

#include <stdlib.h>
#include <string>
#include <cstring>
#include "math.h"
#include <iostream>

class TImageOperations
{
public:
	TImageOperations();
	~TImageOperations();

	static void GenerateGaussianKernel(double sigma, double size, double** kernel);
	static void GaussianFilter(unsigned char** image, int width, int height, double** kernel, int kernelSize);
	static void GaussianFilter(unsigned char** image, int width, int height, double** kernel, int kernelSize, int channel);
	static void MeanFilter(unsigned char ** image, int width, int height, int kernelSize, int channel);
	static void EqualizeHistogram(unsigned char** image, int width, int height, double factor);
	static void EqualizeHistogram(unsigned char** image, int width, int height, double factor, int channel);
	static void CLAHE(unsigned char** image, int channel, int width, int height, double limit);
	static void ClampRange(unsigned char** image, int width, int height, unsigned char min, unsigned char max, int channel);
	static void EnhanceRedGreenDifference(unsigned char** image, int width, int height);
	static void RGBToHSL(unsigned char** image, int width, int height);
	static void HSLToRGB(unsigned char** image, int width, int height);
	static double HueToRGB(double p, double q, double t);
	// Max
	// static void TImageOperations::SaveBitmapToFile( BYTE* pBitmapBits, LONG lWidth, LONG lHeight,WORD wBitsPerPixel, LPCTSTR lpszFileName );
	// static bool SaveBMP ( BYTE* Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile );

private:
	static void MakeHistogram(unsigned char* image, int width, int contextualRegionSizeX, int contextualRegionSizeY, unsigned long* hist, unsigned int nBins, unsigned char* lut);
	static void ClipHistogram(unsigned long* hist, unsigned int nBins, double limit);
	static void MapHistogram(unsigned long* hist, unsigned char min, unsigned char max,	int nBins, unsigned long nPixels);
	static void Interpolate(unsigned char* imagePointer, int width, unsigned long* topLeft, unsigned long* topRight, unsigned long* bottomLeft, unsigned long* bottomRight, unsigned int xSub, unsigned int ySub, unsigned char* lut);
};

#endif
