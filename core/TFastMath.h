//
// Created by beat on 13.06.18.
//

#ifndef ANIMALSPOTTER_TFASTMATH_H
#define ANIMALSPOTTER_TFASTMATH_H

#include <cmath>

class TFastMath {


public:
    /**
     * Create a TFastmath object which can guarantees a certain precision in its results
     */
    TFastMath(int precision);
    ~TFastMath();

    /**
     * Returns the log of value x.
     * x has to be between 0 and 1
     *
     * TODO: rename to log
     */
    double Log(double x);

private:
    //lookup table for log values
    double* _logLut;
};



#endif //ANIMALSPOTTER_TFASTMATH_H
