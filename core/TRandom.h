//
// Created by beat on 13.06.18.
//

#ifndef ANIMALSPOTTER_TRANDOM_H
#define ANIMALSPOTTER_TRANDOM_H


#include <stdint.h>

#include "TFastMath.h"

/**
 * This class provides non threadsafe functions to generate random values
 */
class TRandom {

public:
    TRandom(TFastMath *fastMath, int x, int y);

    double RandomUniform();
    double RandomUniformInRange(double min, double max);
    double RandomNormal(double mean, double sd);

    double MoveRandomNormal(double mean, double sd, double min, double max);
    double MoveRandomNormal(double mean, double sd, double min, double max, double randomValue1, double randomValue2);
    void MoveRandomNormal(double* mean, double* sigma, double min, double max, double* &out, double randomValue1, double randomValue2, double randomValue3, double randomValue4, double randomValue5, double randomValue6);
    double MoveRandomUniform(double mean, double factor, double min, double max);
    double MoveRandomUniform(double mean, double factor, double min, double max, double randomValue);

    double MoveRandomNormalNoBounds(double mean, double sd);

private :
    uint32_t _x, _y, _z, _w;
    TFastMath *_fastMath;
};


#endif //ANIMALSPOTTER_TRANDOM_H
