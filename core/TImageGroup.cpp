//
// Created by beat on 24.09.18.
//

#include "TImageGroup.h"
#include "TIOHelper.h"
#include <iostream>
#include <numeric>
#include <cmath>
#include "TAlgorithm.h"

TImageGroup::TImageGroup(std::vector<TImage *> images) : _images(images) {
    CalculateAverage();
    CalculateMedian();
}

TImageGroup::TImageGroup(const TImageGroup &group) {
    _images = group._images;
    _average = new TImage(*group._average);
    _median = new TImage(*group._median);
}

TImageGroup & TImageGroup::operator=(const TImageGroup &group){
    _images = group._images;

    if (_average != nullptr) {
        delete _average;
        _average = nullptr; //This should not be needed if this deconstructor is not called twice
    }
    _average = new TImage(*group._average);

    if (_median != nullptr) {
        delete _median;
        _median = nullptr; //This should not be needed if this deconstructor is not called twice
    }
    _median = new TImage(*group._median);

    return *this;
}

TImageGroup::~TImageGroup() {
    if (_average != nullptr) {
        delete _average;
        _average = nullptr; //This should not be needed if this deconstructor is not called twice
    }
    if (_median != nullptr) {
        delete _median;
        _median = nullptr;
    }
}

std::vector<TImage *> TImageGroup::GetImages() const {
    return _images;
}

void TImageGroup::AddImage(TImage *image) {
    _images.push_back(image);
}

void TImageGroup::CalculateAverage() {
    if (_average != nullptr) {
        delete _average;
        _average = nullptr;
    }

    //TODO: Verify that all other images are really the same size
    const int width = _images[0]->GetWidth();
    const int height = _images[0]->GetHeight();

    const int nBytes = width * height * 3;
    long *total = new long[width * height * 3];
    std::fill(total, total + nBytes, 0);//initialize with zeroes

    for (TImage *image : _images) {
        for (int i = 0; i < nBytes; i++) {//for each pxl for each col channel
            total[i] += image->_image[i];//add value[i] to total[i]
        }
    }

    unsigned char *pixels = new unsigned char[width * height * 3];
    const int nImages = _images.size();

    for (int i = 0; i < nBytes; i++) {
        pixels[i] = (unsigned char) (total[i] / nImages);//get average value at i (across all imgs)
    }

    delete[] total;

    _average = new TImage("Average", pixels, width, height, width, height);
}

void TImageGroup::CalculateMedian() {
    if (_median != nullptr) {
        delete _median;
        _median = nullptr;
    }

    //TODO: Verify that all other images are really the same size
    const int width = _images[0]->GetWidth();
    const int height = _images[0]->GetHeight();

    const int nImages = _images.size();

    const int nBytes = width * height * 3;
    unsigned char *median = new unsigned char[width * height * 3];
    std::fill(median, median + nBytes, 0);//initialize with zeroes

    for (int i = 0; i < nBytes; i++) {//for each pxl for each col channel
        std::vector<int> pxlValues;
        //int *pxlValues= new int[nImages];
        //std::fill(pxlValues, pxlValues+nImages, 0); //initialize with zeroes
        //int count=0;
        for (TImage *image : _images) {
            pxlValues.push_back(image->_image[i]);
            //pxlValues[count] += image->_image[i];//add value[i] to total[i]
        }
        std::sort(pxlValues.begin(), pxlValues.end());

        double pxlmedian=0;
        //odd case
        if (nImages%2!=0){
            pxlmedian=pxlValues[nImages/2];
        }
        else{ //even case
            pxlmedian=(pxlValues[(nImages-1)/2]+pxlValues[(nImages/2)])/2.0;
        }
        median[i]=(unsigned  char) pxlmedian;
        pxlValues.clear(); //empty the vector for next iteration

    }

    _median = new TImage("Median", median, width, height, width, height); //create the median img
}

const TImage & TImageGroup::GetAverage() {
    return *_average;
}

const TImage & TImageGroup::GetMedian() {
    return *_median;
}

int TImageGroup::size() const {
    return _images.size();
}