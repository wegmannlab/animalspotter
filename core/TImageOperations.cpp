#include "TImageOperations.h"


TImageOperations::TImageOperations()
{
}


TImageOperations::~TImageOperations()
{
}

void TImageOperations::GenerateGaussianKernel(double sigma, double size, double** kernel)
{
	// Standard nvidia implementation
	// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch40.html
	// int kernelSize = (int)(sigma * 3.0);

	double r = 0.0;
	double s = 2.0 * sigma * sigma;
	double sum = 0.0;

	int k = (int)((size - 1.0) / 2.0);

	for (int x = -k; x <= k; x++)
	{
		for (int y = -k; y <= k; y++)
		{
			r = sqrt(x * x + y * y);
			kernel[x + k][y + k] = (exp(-(r * r) / s)) / (3.141592 * s);
			sum += kernel[x + k][y + k];
		}
	}

	// Normalization
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			kernel[i][j] /= sum;
		}
	}
}

void TImageOperations::GaussianFilter(unsigned char** image, int width, int height, double** kernel, int kernelSize)
{
	unsigned char** tmpImage = (unsigned char**)malloc(width * height * sizeof(unsigned char*));//freed in same function
	for (int i = 0; i < width * height; i++)
	{
		tmpImage[i] = (unsigned char*)malloc(3 * sizeof(unsigned char)); //freed in same function
	}

	int k = (int)((kernelSize - 1.0) / 2.0);

	for (int i = 0; i < width * height; i++)
	{
		int x = i % width;
		int y = i / width;

		for (int m = 0; m < 3; m++)
		{
			double value = 0.0;
			int kx = 0;

			for (int xx = (x - k); xx <= (x + k); xx++)
			{
				int ky = 0;

				for (int yy = (y - k); yy <= (y + k); yy++)
				{
					int pos = yy * width + xx;

					if (xx >= 0 && xx < width && yy >= 0 && yy < height)
					{
						value += image[pos][m] * kernel[kx][ky];
					}

					ky++;
				}

				kx++;
			}

			tmpImage[i][m] = (unsigned char)value;
		}
	}

	for (int i = 0; i < width * height; i++)
	{
		std::memcpy(image[i], tmpImage[i], 3 * sizeof(unsigned char));
	}

	for (int i = 0; i < width * height; i++) free(tmpImage[i]);
	free(tmpImage);
}

void TImageOperations::GaussianFilter(unsigned char** image, int width, int height, double** kernel, int kernelSize, int channel)
{
	unsigned char** tmpImage = (unsigned char**)malloc(width * height * sizeof(unsigned char*)); //freed in same function
	for (int i = 0; i < width * height; i++)
	{
		tmpImage[i] = (unsigned char*)malloc(3 * sizeof(unsigned char)); //freed in same function
	}

	int k = (int)((kernelSize - 1.0) / 2.0);

	for (int i = 0; i < width * height; i++)
	{
		int x = i % width;
		int y = i / width;

		double value = 0.0;
		int kx = 0;

		for (int xx = (x - k); xx <= (x + k); xx++)
		{
			int ky = 0;

			for (int yy = (y - k); yy <= (y + k); yy++)
			{
				int pos = yy * width + xx;

				if (xx >= 0 && xx < width && yy >= 0 && yy < height)
				{
					value += image[pos][channel] * kernel[kx][ky];
				}

				ky++;
			}

			kx++;
		}

		tmpImage[i][channel] = (unsigned char)value;
	}

	for (int i = 0; i < width * height; i++)
	{
		std::memcpy(image[i], tmpImage[i], 3 * sizeof(unsigned char));
	}

	for (int i = 0; i < width * height; i++) free(tmpImage[i]);
	free(tmpImage);
}

void TImageOperations::MeanFilter(unsigned char ** image, int width, int height, int kernelSize, int channel)
{
	unsigned char** tmpImage = (unsigned char**)malloc(width * height * sizeof(unsigned char*)); //freed in same function
	for (int i = 0; i < width * height; i++)
	{
		tmpImage[i] = (unsigned char*)malloc(3 * sizeof(unsigned char));//freed in same function
	}

	for (int i = 0; i < width * height; i++)
	{
		int x = i % width;
		int y = i / width;

		double value = 0.0;
		int c = 0;

		for (int xx = (x - kernelSize); xx <= (x + kernelSize); xx++)
		{
			for (int yy = (y - kernelSize); yy <= (y + kernelSize); yy++)
			{
				int pos = yy * width + xx;

				if (xx >= 0 && xx < width && yy >= 0 && yy < height)
				{
					value += image[pos][channel];
					c++;
				}
			}
		}

		tmpImage[i][channel] = (unsigned char)(value / (double)c);
	}

	for (int i = 0; i < width * height; i++)
	{
		image[i][channel] = tmpImage[i][channel];
	}

	for (int i = 0; i < width * height; i++) free(tmpImage[i]);
	free(tmpImage);
}

void TImageOperations::EqualizeHistogram(unsigned char** image, int width, int height, double factor)
{
	for (int m = 0; m < 3; m++)
	{
		double* equ = (double*)calloc(256, sizeof(double)); //freed in same function

		for (int i = 0; i < width * height; i++)
		{
			equ[image[i][m]] += 1.0;
		}

		double area = width * height;
		area *= area;

		for (int i = 0; i < 256; i++)
		{
			equ[i] = pow(equ[i] / area, factor);
		}

		for (int i = 1; i < 256; i++)
		{
			equ[i] += equ[i - 1];
		}

		for (int i = 0; i < 256; i++)
		{
			equ[i] *= 255.0 / equ[255];
		}

		// Equilization step
		for (int i = 0; i < width * height; i++)
		{
			image[i][m] = (unsigned char)equ[image[i][m]];
		}

		free(equ);
	}
}

void TImageOperations::EqualizeHistogram(unsigned char** image, int width, int height, double factor, int channel)
{
	double* equ = (double*)calloc(256, sizeof(double));//freed in same function

	for (int i = 0; i < width * height; i++)
	{
		equ[image[i][channel]] += 1.0;
	}

	double area = width * height;
	area *= area;

	for (int i = 0; i < 256; i++)
	{
		equ[i] = pow(equ[i] / area, factor);
	}

	for (int i = 1; i < 256; i++)
	{
		equ[i] += equ[i - 1];
	}

	for (int i = 0; i < 256; i++)
	{
		equ[i] *= 255.0 / equ[255];
	}

	// Equilization step
	for (int i = 0; i < width * height; i++)
	{
		image[i][channel] = (unsigned char)equ[image[i][channel]];
	}

	free(equ);
}

void TImageOperations::CLAHE(unsigned char** image, int channel, int width, int height, double limit)
{
	unsigned char* tmpImage = (unsigned char*)malloc(width * height * sizeof(unsigned char)); //freed in same function

	for (int i = 0; i < width * height; i++)
	{
		tmpImage[i] = image[i][channel];
	}


	// Get min and max gray levels for channel
	int min = 1000;
	int max = 0;
	int nBins = 256;
	int nContextualRegionsX = 4;
	int nContextualRegionsY = 4;
	double clipLimit = limit;

	unsigned char* imagePointer;

	unsigned long* hist;
	unsigned long* histMap = (unsigned long*)malloc(nContextualRegionsX * nContextualRegionsY * nBins * sizeof(unsigned long));//freed in same function
	unsigned char* lut = (unsigned char*)malloc(256 * sizeof(unsigned char)); //freed in same function

	for (int i = 0; i < width * height; i++)
	{
		if (image[i][channel] < min) min = image[i][channel];
		if (image[i][channel] > max) max = image[i][channel];
	}

	int contextualRegionSizeX = width / nContextualRegionsX;
	int contextualRegionSizeY = height / nContextualRegionsY;
	int contextualRegionSize = contextualRegionSizeX * contextualRegionSizeY;

	clipLimit = (clipLimit * (contextualRegionSizeX * contextualRegionSizeY) / nBins);

	// Create lookup table
	unsigned char binSize = (unsigned char)(1.0 + ((double)max - (double)min) / (double)nBins);

	for (int i = min; i <= max; i++)
	{
		lut[i] = (unsigned char)((double)i - (double)min) / (double)binSize;
	}

	imagePointer = tmpImage;
	for (int y = 0; y < nContextualRegionsY; y++)
	{
		for (int x = 0; x < nContextualRegionsX; x++, imagePointer += contextualRegionSizeX)
		{
			hist = &histMap[nBins * (y * nContextualRegionsX + x)];

			// Make histogram
			MakeHistogram(imagePointer, width, contextualRegionSizeX, contextualRegionSizeY, hist, nBins, lut);
			ClipHistogram(hist, nBins, clipLimit);
			MapHistogram(hist, min, max, nBins, contextualRegionSize);
		}

		imagePointer += (contextualRegionSizeY - 1) * width;
	}

	// Interpolation
	unsigned int xLeft, xRight, yTop, yBottom;
	unsigned int xSub, ySub;

	imagePointer = tmpImage;
	for (int y = 0; y <= nContextualRegionsY; y++)
	{
		if (y == 0)
		{
			// Top row
			ySub = contextualRegionSizeY >> 1;
			yTop = 0;
			yBottom = 0;
		}
		else
		{

			if (y == nContextualRegionsY) {
				// Bottom row
				ySub = contextualRegionSizeY >> 1;
				yTop = nContextualRegionsY - 1;
				yBottom = yTop;
			}
			else
			{
				ySub = contextualRegionSizeY;
				yTop = y - 1;
				yBottom = yTop + 1;
			}
		}

		for (int x = 0; x <= nContextualRegionsX; x++)
		{
			if (x == 0)
			{
				// Outermost left
				xSub = contextualRegionSizeX >> 1;
				xLeft = 0;
				xRight = 0;
			}
			else
			{
				if (x == nContextualRegionsX)
				{
					// Outermost right
					xSub = contextualRegionSizeX >> 1;
					xLeft = nContextualRegionsX - 1;
					xRight = xLeft;
				}
				else
				{
					/* default values */
					xSub = contextualRegionSizeX;
					xLeft = x - 1;
					xRight = xLeft + 1;
				}
			}

			unsigned long* topLeft = &histMap[256 * (yTop * nContextualRegionsX + xLeft)];
			unsigned long* topRight = &histMap[256 * (yTop * nContextualRegionsX + xRight)];
			unsigned long* bottomLeft = &histMap[256 * (yBottom * nContextualRegionsX + xLeft)];
			unsigned long* bottomRight = &histMap[256 * (yBottom * nContextualRegionsX + xRight)];

			Interpolate(imagePointer, width, topLeft, topRight, bottomLeft, bottomRight, xSub, ySub, lut);
			imagePointer += xSub;
		}

		imagePointer += (ySub - 1) * width;
	}

	for (int i = 0; i < width * height; i++)
	{
		image[i][channel] = tmpImage[i];
	}

	free(histMap);
	free(lut);
	free(tmpImage);
}

void TImageOperations::ClampRange(unsigned char** image, int width, int height, unsigned char min, unsigned char max, int channel)
{
	for (int i = 0; i < width * height; i++)
	{
		if (image[i][channel] < min)
		{
			image[i][channel] = min;
		}
		else if (image[i][channel] > max)
		{
			image[i][channel] = max;
		}
	}
}

void TImageOperations::EnhanceRedGreenDifference(unsigned char ** image, int width, int height)
{
	for (int i = 0; i < width * height; i++)
	{
		if (image[i][2] > 175) continue;

		if (image[i][0] > image[i][1])
		{
			image[i][0] = (image[i][0] * 1.25 > 255) ? 255 : image[i][0] * 1.25;
		}

		if (image[i][0] < image[i][1])
		{
			image[i][1] = (image[i][1] * 1.25 > 255) ? 255 : image[i][1] * 1.25;
		}
	}
}

void TImageOperations::RGBToHSL(unsigned char** image, int width, int height)
{
	for (int i = 0; i < width * height; i++)
	{
		double r = (double)image[i][0] / 255.0;
		double g = (double)image[i][1] / 255.0;
		double b = (double)image[i][2] / 255.0;

		double max = r;
		if (max < g) max = g;
		if (max < b) max = b;

		double min = r;
		if (min > g) min = g;
		if (min > b) min = b;

		double h, s, l = (max + min) / 2.0;

		if (max == min)
		{
			h = s = 0;
		}
		else
		{
			double d = max - min;
			s = (l > 0.5) ? d / (2.0 - max - min) : d / (max + min);

			if (max == r) h = (g - b) / d + ((g < b) ? 6.0 : 0);
			if (max == g) h = (b - r) / d + 2.0;
			if (max == b) h = (r - g) / d + 4.0;

			h /= 6.0;
		}

		image[i][0] = h * 255;
		image[i][1] = s * 255;
		image[i][2] = l * 255;
	}
}

void TImageOperations::HSLToRGB(unsigned char** image, int width, int height)
{
	for (int i = 0; i < width * height; i++)
	{
		double r, g, b;

		double h = (double)image[i][0] / 255.0;
		double s = (double)image[i][1] / 255.0;
		double l = (double)image[i][2] / 255.0;

		if (s == 0) {
			r = g = b = l;
		}
		else {
			double q = l < 0.5 ? l * (1 + s) : l + s - l * s;
			double p = 2 * l - q;
			r = HueToRGB(p, q, h + 1.0f / 3.0f);
			g = HueToRGB(p, q, h);
			b = HueToRGB(p, q, h - 1.0f / 3.0f);
		}

		image[i][0] = r * 255.0;
		image[i][1] = g * 255.0;
		image[i][2] = b * 255.0;
	}
}

double TImageOperations::HueToRGB(double p, double q, double t)
{
	if (t < 0) t += 1;
	if (t > 1) t -= 1;
	if (t < 1.0 / 6.0) return p + (q - p) * 6.0 * t;
	if (t < 1.0 / 2.0) return q;
	if (t < 2.0 / 3.0) return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
	return p;
}

void TImageOperations::MakeHistogram(unsigned char* image, int width, int contextualRegionSizeX, int contextualRegionSizeY, unsigned long* hist, unsigned int nBins, unsigned char* lut)
{
	unsigned char* imagePointer;

	for (unsigned int i = 0; i < nBins; i++)
	{
		hist[i] = 0;
	}

	for (int i = 0; i < contextualRegionSizeY; i++)
	{
		imagePointer = &image[contextualRegionSizeX];
		while (image < imagePointer)
		{
			hist[lut[*image++]]++;
		}

		imagePointer += contextualRegionSizeX;
		image = imagePointer - contextualRegionSizeX;
	}
}

void TImageOperations::ClipHistogram(unsigned long* hist, unsigned int nBins, double limit)
{
	unsigned long* bin;
	unsigned long* end;
	unsigned long* hst;

	unsigned long nExcess, nExcessOld, upper, increment, step;

	nExcess = 0;
	bin = hist;

	for (unsigned int i = 0; i < nBins; i++)
	{
		long excess = (long)bin[i] - (long)limit;
		if (excess > 0) nExcess += excess;
	};


	// Clip and redistribute pixels
	increment = nExcess / nBins;
	upper = limit - increment;

	for (unsigned int i = 0; i < nBins; i++)
	{
		if (hist[i] > limit)
		{
			// Clip
			hist[i] = limit;
		}
		else
		{
			if (hist[i] > upper)
			{
				nExcess -= (hist[i] - upper);
				hist[i] = limit;
			}
			else
			{
				nExcess -= increment;
				hist[i] += increment;
			}
		}
	}

	do {
		// Redistribute remaining excess
		end = &hist[nBins]; hst = hist;

		nExcessOld = nExcess;

		while (nExcess && hst < end)
		{
			step = nBins / nExcess;

			if (step < 1)
			{
				step = 1;
			}

			for (bin = hst; bin < end && nExcess; bin += step)
			{
				if (*bin < limit)
				{
					(*bin)++;
					nExcess--;
				}

			}

			hst++;
		}

	} while ((nExcess) && (nExcess < nExcessOld));
}

void TImageOperations::MapHistogram(unsigned long * hist, unsigned char min, unsigned char max, int grayLevels, unsigned long nPixels)
{
	unsigned long sum = 0;

	double scale = ((float)(max - min)) / nPixels;
	const unsigned long lmin = (unsigned long)min;

	for (int i = 0; i < grayLevels; i++)
	{
		sum += hist[i];
		hist[i] = (unsigned long)(lmin + sum * scale);
		if (hist[i] > max) hist[i] = max;
	}
}

void TImageOperations::Interpolate(unsigned char* imagePointer, int width, unsigned long* topLeft, unsigned long* topRight, unsigned long* bottomLeft, unsigned long* bottomRight, unsigned int xSub, unsigned int ySub, unsigned char* lut)
{
	unsigned int increment = width - xSub;
	unsigned char val;
	unsigned int n = xSub * ySub;

	unsigned int shift = 0;

	if (n & (n - 1))
	{
		for (unsigned int y = 0, iy = ySub; y < ySub; y++, iy--, imagePointer += increment)
		{
			for (unsigned int x = 0, ix = xSub; x < xSub; x++, ix--)
			{
				val = lut[*imagePointer];
				*imagePointer++ = (unsigned char)((iy * (ix*topLeft[val]
					+ x * topRight[val])
					+ y * (ix * bottomLeft[val]
					+ x * bottomRight[val])) / n);
			}
		}
	}
	else {
		while (n >>= 1) shift++;

		for (unsigned int y = 0, iy = ySub; y < ySub; y++, iy--, imagePointer += increment)
		{
			for (unsigned int x = 0, ix = xSub; x < xSub;
			x++, ix--)
			{
				val = lut[*imagePointer];
				*imagePointer++ = (unsigned char)((iy* (ix * topLeft[val]
					+ x * topRight[val])
					+ y * (ix * bottomLeft[val]
					+ x * bottomRight[val])) >> shift);
			}
		}
	}
}

