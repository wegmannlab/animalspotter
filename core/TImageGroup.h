//
// Created by beat on 24.09.18.
//

#ifndef ANIMALSPOTTER_TIMAGEGROUP_H
#define ANIMALSPOTTER_TIMAGEGROUP_H

#include "TImage.h"
#include <vector>

class TImageGroup {

public:

    TImageGroup(std::vector<TImage *> images);
    TImageGroup(const TImageGroup & group);

    ~TImageGroup();

    std::vector<TImage *> GetImages() const;

    TImageGroup & operator=(const TImageGroup &group);

    void AddImage(TImage * image);

    void CalculateAverage();
    void CalculateMedian();

    const TImage & GetAverage();
    const TImage & GetMedian();

    int size() const;

private:
    std::vector<TImage *> _images;
    TImage * _average = nullptr;
    TImage * _median = nullptr;
};


#endif //ANIMALSPOTTER_TIMAGEGROUP_H
