#ifndef TIOHELPER_H
#define TIOHELPER_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

namespace io {

	bool SavePNGGrayscale(unsigned char* _img, const int _w, const int _h, char* _path);
	bool SavePNG_RGB(unsigned char* _img, const int _w, const int _h, const char* _buf);
}

#endif
