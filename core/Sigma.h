//
// Created by Beat Wolf on 11.05.18.
//

#ifndef ANIMALSPOTTER_SIGMA_H
#define ANIMALSPOTTER_SIGMA_H
#include <math.h>

class Sigma {

    public:

    Sigma(const double * pixelLUT, double** var);
    Sigma(Sigma * sigma);

    void CopySigma(Sigma * sigma);

    double LogDensity(const unsigned char * const x, const double * const mean) const;

    double LogDensityDist(const unsigned char * const x, const double * const mean, const double * const dist) const;

    void SetL(double l);

    double GetL() const;

    int GetLChanges() const;

    int GetLChanges(int i, int j) const;

    void ResetChangeCounters();

    double GetL(int i, int j) const;

    void SetL(int i, int j, double value);

    double ** GetSigma();

    bool IsSingular();
    void SetIsSingular(bool singular);

    private:
    //L matrix, by col (triangular)

    void decompose(double** var);
    double GetDeterminantSimplified() const;

    double _l11, _l21, _l31, _l22, _l32, _l33;
    double _l;
    double _lPow3;
    double _lPow2;
    double _term2;

    //TODO: maybe an unsigned short is enough? (65,535)
    int _lChanges = 0;
    int _l11Changes = 0;
    int _l22Changes = 0;
    int _l33Changes = 0;
    int _l21Changes = 0;
    int _l31Changes = 0;
    int _l32Changes = 0;

    const double * _pixelLUT;
    bool _singular = false;
};

double ** inverse(double ** matrix);
double determinantSymetric(double **matrix);
bool isSingularMatrix(double **matrix);

void correctSingularMatrix(double ** matrix);

#endif //ANIMALSPOTTER_SIGMA_H
