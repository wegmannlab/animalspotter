//
// Created by beat on 13.06.18.
//

#include "TFastMath.h"

#include <cstdlib>

TFastMath::TFastMath(int precision) {
    // Log lookup table
    //double precision = 1000;
    _logLut = (double *) malloc(precision * sizeof(double));

    for (int i = 0; i < precision; ++i) {
        _logLut[i] = log((double) i / (double)precision);
    }
}

TFastMath::~TFastMath() {
    free(_logLut);
}

double TFastMath::Log(double x)
{
    if (x > 1 || x < 1.0 / 1000.0)
    {
        return log(x);
    }

    int index = (int)(1000 * x);
    return _logLut[index];
}
