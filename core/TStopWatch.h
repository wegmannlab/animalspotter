#ifndef TSTOPWATCH_H
#define TSTOPWATCH_H

#include <iostream>
#include <chrono>

class TStopWatch {
private:
	double _start;
public:
	TStopWatch();

	void Start();
	double Stop();
	void StopPrint();
	void StopPrint(const char* message);
	double GetTime();

	~TStopWatch();
};

#endif
