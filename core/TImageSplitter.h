//
// Created by beat on 24.09.18.
//

#ifndef ANIMALSPOTTER_TIMAGESPLITTER_H
#define ANIMALSPOTTER_TIMAGESPLITTER_H


#include "TImage.h"
#include "TImageGroup.h"

#include <vector>

class TImageSplitter {

public:
    TImageSplitter(int maxTimeDifferenceSeconds, size_t minBinSize, double minGrayscaleForNight, bool splitDayNight, float pxlFractionDist, int howManiestPxl);

    /**
     * Split images in groups based on the settings of the TImageSplitter object
     */
    std::vector<std::vector<TImage *>> SplitImages(std::vector<TImage *> & images);

    /**
     * Sets the TImageLoader instance to verbose or not (thus outputing more debug information)
     * Defaults to false.
     *
     * @param verbose
     */
    void SetVerbose(bool verbose);

    /**
     * If set to true, bins smaller than minBinSize are discarded and not merged with their closest neighbour
     */
    void SetDiscardSmallBins(bool discard);
    void SetOutputPath(char *outputPath);

private:
    int DistanceSeconds(const TImage *imageA, const TImage *imageB) const;
    double Distance(const TImage *imageA, const TImage *imageB) const;
    double ImageDifference(const TImage &imageA, const TImage &imageB) const;
    double ImageRoughDifference(const TImage &imageA, const TImage &imageB) const;
    std::vector<std::vector<TImage *>> MergeBins(std::vector<std::vector<TImage *>> &bins, int minBinSize, int nrCalls) const;
    bool IsNighttime(const TImage *imageA) const;
    std::vector<unsigned int> FindSmallBins(const std::vector <TImageGroup> & bins, int minBinSize) const;
    std::tuple<int, int, double> FindMostSimilar(std::vector <TImageGroup> bins, int minBinSize) const;
    std::tuple<int, int, double> FindRoughlyMostSimilar(std::vector <TImageGroup> &bins, int minBinSize,const std::vector <unsigned int> &smallBins, const std::vector <int> &deletedBins, std::vector<std::vector<double>> &roughDistances, std::vector<std::vector<double>> &exactDistances) const;

    bool _verbose = false;
	size_t _minBinSize;
    double _minGrayscaleForNight = 0;
    double _pxlFractionDist;
    bool _discardSmallBins = false;
    int _maxTimeDifferenceSeconds = 10 * 60;
    bool _splitDayNight;
    const char* _outputPath;
    int _howManiestPxl;
};


#endif //ANIMALSPOTTER_TIMAGESPLITTER_H
