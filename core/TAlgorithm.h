#ifndef TALGORITHM_H
#define TALGORITHM_H

#include "tinydir.h"
#include "TIOHelper.h"
#include "TMemoryHelper.h"
#include "TImage.h"
#include "math.h"
#include "float.h"
#include "Sigma.h"
#include <vector>
#include "omp.h"
#include "TStopWatch.h"
#include "TImageOperations.h"
#include "TFastMath.h"
#include <stdint.h>
#include <algorithm>
#include <time.h>
#include <iostream>
#include <iomanip> // to print cout with higher precision
#include <fstream>
#include "TRandom.h"

// The number of colors per pixel // channels per image (3 = RGB)
static const int nChannels = 3;
static const std::chrono::seconds maxNeighbourDiff = std::chrono::seconds(5); //5s are chosen to make sure that only images of the same capture event are considered as direct neighbours
//That expanding this is not useful was shown using the script IsingBeta.py.


class TAlgorithm
{
public:
    TAlgorithm(double lambda, unsigned int chainLength,
                    unsigned int burnin, unsigned int burnin_nr,
                    double* standardDeviations,
           unsigned int seed,
                       double prior,
           int numThreads);

    ~TAlgorithm();

    void InitializeMatChain(char *outputTag);
    bool MakeMCMC(char* outputTag);
    bool SaveOutput(char* outputPath, char* outputTag);
    void SetOutputPath(char *outputPath);
    void SetImages(std::vector<TImage *> images);
    void SetOutputBoundingBoxes(bool outputBoundingBoxes);

    void SetNeighborAlpha(double alpha);
    void SetNeighborBeta(double beta);

    //Percentage of each image that should become foreground based on a heuristic
    //This is done before the MCMC
    void SetHeuristic(double heuristic);

    void SetLogPath(char *logPath);

    void SetVerbose(bool verbose);
    void SetOutputBoxColor(unsigned char color[3]);

    int GetImageCount();

    static void InitMeanBG(int nPixels, int nImages, int nChannels, unsigned char** z, double** meanPixel, const std::vector<TImage*> images);

    void SetDebugImages(bool debug);
    void SetDebugPixel(int pixel);

    void SetMinOutputBoxProbability(double min_box_probability);

private:
    const char* _outputPath;
    const char* _logPath;
    bool _verbose;

    unsigned int _nImages;
    unsigned int _nPixels;
    unsigned int _imageWidth;
    unsigned int _imageHeight;


    std::vector<TImage *> _images;

    long _seed;
    double _lambda;

    int _chainLength;
    int _burnin;
    int _burnin_nr;

    double _isingGamma;
    double* _standardDeviations;

    bool _pixellog;
    double _logIsingAlpha;
    double _logIsingBeta;

    // Store the alpha and the 1 - alpha
    double _probZBG;
    double _probZFG;

    double _meanImageSD;
    double _distImageSD;
    double _meanPixelSD;

    // For extra speed, check what would be faster,
    // to have this 2d and 3d stuff in 1d and do a mapping
    // or having small 2d and 3d matrices (overhead ...)

    Sigma** _varCovarBG; //Sigma 0, for every pixel in the image
    double _bgSigmaSD;
    Sigma** _varCovarFG; //Sigma 1, for every image

	Sigma** _proposedVarCovarBG; //Sigma 0, for every pixel in the image
    double _fgSigmaSD;
	Sigma** _proposedVarCovarFG; //Sigma 1, for every image

	// mu_j,m: average value of color m at pixel j across all images i
	double** _meanPixel;
	int* _meanPixelAcceptance; //Counter to determine acceptance rate

	// phi_i,m: average value of color m across all pixels j in image i
	double** _meanImage;
	int* _meanImageAcceptance; //Counter to determine acceptance rate

	// delta_i,m: distortion of color m across all pixels j in image i
	double** _distImage;
	int* _distImageAcceptance; //Counter to determine acceptance rate

	// stores whether a pixel j of an image i belongs to the foreground
	// or the background
	unsigned char** _z;
	unsigned char** _tmpZ;

	double** _debugColors;

	// stores how many pixel j of an image i belonged to the foreground (Max)
	unsigned long ** _sumZ;

	// Lookup tables
	double* _pixelLut;

	TFastMath * _fastMath;
	TRandom ** _random;

	int _numThreads;

	double _heuristic;
	bool _inited = false;

    bool _debugImage = false;
    int _debugPixel = -1;

    unsigned char _output_box_colors[3];
    double _min_box_probability = 0.51;

    bool _output_bounding_boxes = true;

	void InitializeMatrices();
	void InitializeChain(char *outputTag);

	void ChainStep(unsigned int _step);

	void PixelFgBgCalc(unsigned int _step);
	double proposeNewMean(TRandom * random, unsigned int j);
	void proposeSigmaBG(TRandom * random, double oldSum, unsigned int j);

	double proposeNewLBG(TRandom *random, double currentSum, int j, int li, int lj);
	double proposeNewLFG(TRandom *random, double currentSum, int i, int li, int lj);

	void WriteDebugOutputImage(int step, std::ofstream &logFile, int debugImage);
    void WriteDebugOutputPixel(int step, std::ofstream &logFile, int debugPixel);

	void UpdateBgPixels();

	void UpdateFgVarCorDist(unsigned int _step);

	double LogNormalDensity(double* x, double mean, double sigma);
	double LogIsingAlphaEnergy(unsigned int i, unsigned int j);
	double LogIsingBetaEnergy(unsigned int i, unsigned int j);

	void SaveBGMean(const char *baseName, int id, char* outputTag);

	bool estimateSigmaPixel(double **sigmaMatrix, double *means, std::vector<TImage *> values, int pixel);
    bool estimateSigmaImage(double **sigmaMatrix, double *means, std::vector<TImage *> values, int image);

	void CleanupMatrices();

    void InitDebugFiles(char* outputTag, std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog);
    void CloseDebugFiles(std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog);
    bool MCMCStepDebug(int step, char* outputTag, std::vector<std::ofstream*> &imageLogs, std::ofstream &pixelLog);

	//Get the random variable object for the current thread
	TRandom * GetThreadRandom();

	void AnalyseAcceptanceRates(const int steps, const double targetAcceptanceRate);
};

double * CreateLUT();

#endif
