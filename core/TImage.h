//
// Created by beat on 26.07.18.
//

#ifndef ANIMALSPOTTER_TIMAGE_H
#define ANIMALSPOTTER_TIMAGE_H

#include <vector>
#include <string>
#include <chrono>

/**
 * Holds all information about a specific image.
 */
class TImage{
public:

    TImage(const std::string &name, unsigned char* image, int width, int height, int originalWidth, int originalHeight);
    TImage(const TImage & image);
    ~TImage();

    /**
     * Returns the name of the image
     */
    const std::string & GetName() const;

    /**
     * Returns the width of the size reduced image.
     */
    int GetWidth() const;

    /**
     * Returns the height of the size reduced image.
     */
    int GetHeight() const;

    /**
     * Returns the width of the original image
     */
    int GetOriginalWidth() const;

    /**
     * Returns the size of the height of the original image
     */
    int GetOriginalHeight() const;

    /**
     * Set the time at which the image was taken
     */
    void SetOriginalTime(std::chrono::system_clock::time_point &time);

    /**
     * Returns the time at which the image was taken.
     * Defaults to "now"
     */
    std::chrono::system_clock::time_point GetOriginalTime() const;

    const unsigned char * _image;

private:
    const std::string _name;
    const int _width, _height;
    const int _originalWidth, _originalHeight;

    std::chrono::system_clock::time_point _originalImageTime = std::chrono::system_clock::now(); //TODO: Make constant?
};


#endif //ANIMALSPOTTER_TIMAGE_H
