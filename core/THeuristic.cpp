//
// Created by beat on 26.07.18.
//


#define _USE_MATH_DEFINES
#include "THeuristic.h"
#include "TMemoryHelper.h"
#include "TAlgorithm.h"

#include <algorithm>
#include <cstring>
#include <cmath>


//Heuristic options
static const bool debug_t_test = false;

static const int heuristic_loops = 5; //5
static const double max_smoothing_passes = 3; //3

static const bool init_bg_mean_from_heuristic = true;
static const bool init_fg_mean_from_heuristic = true;
static const bool init_z_from_heuristic = true;

static const bool use_neighbours = true;

static const double max_fg_distortion_from_heuristic[2] = {0.5, 2};

static const double max_images_foreground = 0.85;//0.7; //Maximum amount of images in which a pixel is allowed to be part of the foreground
static const double min_tvalue = 0.75;//0.6
static const double max_t_flexibility = 0.7;
static const double min_pixel_sd = 0.02;//0.015

THeuristic::THeuristic(unsigned char** z, double** meanImage, double** distImage, double** meanPixel, unsigned long ** sumZ, std::vector<TImage *> images, double* pixelLut,
                       int nImages, int nPixels, int imageWidth, int nChannels) :
        _z(z), _meanImage(meanImage), _distImage(distImage), _meanPixel(meanPixel), _sumZ(sumZ), _images(images), _pixelLut(pixelLut),
        _nImages(nImages), _nPixels(nPixels), _imageWidth(imageWidth), _nChannels(nChannels){
}
//the above is an initializer list (could also be written as _z=z and so on)

void THeuristic::ExtractForeground(double foregroundPercentage){
    for(int i = 0; i < heuristic_loops; i++){
        Heuristic(foregroundPercentage, i);
    }
}


static bool sort_using_greater_than(double u, double v) {
    return u > v;
}

double getMean(double originalMean, int images, double imgColor){
    return (originalMean * images - imgColor) / (images - 1); //take one image out of the mean calculation
}

double getSD(double orginalSD, int images, double originalMean, double imageColor){
    //this function just removes one image (imageColor) from the sd calculation
    double diff = imageColor - originalMean;
    double diffS = diff * diff;

    double temp2 = orginalSD * orginalSD * images;

    double temp = (temp2 - diffS) / (images - 1);

    return sqrt(temp);
}

void THeuristic::Heuristic(double foregroundPercentage, int step){

    //Calculate average color for every image and deduce distortion from mean
    if(step == 0){ //before : and max_fg_distortion_from_heuristic > 0 ; but this is never true as max_fg_distortion_from_heuristic is an array
        //note: we can only do this for the 1.step because meanImage is changed in this function
        // There is no need for recalculating distImage, because it is not changed in this function
        double *meanAll = new double[_nChannels];
        std::fill(meanAll, meanAll + _nChannels, 0);
        for(int i = 0; i < _nImages; i++){
            for (int m = 0; m < _nChannels; m++) {
                meanAll[m] += _meanImage[i][m];
            }
        }
        for(int m = 0; m < _nChannels; m++){
            meanAll[m] /= _nImages; //so this is just the mean of color m across all imgs
        }

        for(int i = 0; i < _nImages; i++){

            for(int m = 0; m < _nChannels; m++) {
                double dist = _meanImage[i][m] / meanAll[m];
                if(dist > max_fg_distortion_from_heuristic[1]){
                    dist = max_fg_distortion_from_heuristic[1];//because a brightness effect cannot be THAT large
                    //if the distance would be larger than this, it's probably because an animal is covering
                    //a large part of the img (not a distortion effect)
                }else if(dist < max_fg_distortion_from_heuristic[0]){
                    dist = max_fg_distortion_from_heuristic[0];
                }

                _distImage[i][m] = dist;
            }
        }
        delete [] meanAll;
    }

    double ** sdPixel;
    mem::InitializeArray2d(&sdPixel, _nPixels, 3);

    //Calculate SD
    for(int i = 0; i < _nImages; i++) {
        for (int j = 0; j < _nPixels; j++) {
            for(int m = 0; m < _nChannels; m++) {
                const double diff = _pixelLut[_images[i]->_image[j * _nChannels + m]] - _meanPixel[j][m];//meanPixel is the mean background value at this pixel& channel
                //pixelLut is the lookup table: pixelLut[color_value] is the corresponding number between 0 and 1
                sdPixel[j][m] += diff * diff;
            }
        }
    }

    for (int j = 0; j < _nPixels; j++) {
        for(int m = 0; m < _nChannels; m++) {
            sdPixel[j][m] = sqrt(sdPixel[j][m] / _nImages); //now I have the std of the pixel j channel m
        }
    }

    //#pragma omp parallel for
    for(int i = 0; i < _nImages; i++) {
        double *tValues = new double[_nPixels];

        //Determine how different the image is in general
        for (int j = 0; j < _nPixels; j++) {
            tValues[j] = 0;

            //Set every pixel as foreground that is clearly different than pixel average across pictures
            double totalDiff = 0;
            double totalDist = 0;

            for (int m = 0; m < _nChannels; m++) {
                //getMean(double originalMean, int images, double imageMean){
                double imgColor = _pixelLut[_images[i]->_image[j * _nChannels + m]];//this is the color as value between 0 and 1 at pixel j channel m
                double realMean = getMean(_meanPixel[j][m], _nImages, imgColor); //meanPixel is the mean background color table(holding values between 0&1).
                // The color of the img we're
                //looking at is taken out of the mean calculation so that we can fairly judge its distance from the others

                double t = abs(std::min(1.0, std::max(0.0, realMean * _distImage[i][m])) - imgColor);
                //(note that I sometimes write indeces in the following note even if there are none, just for overview purposes)
                //remember that
                // dist[i][m]=meanImage[i][m]/meanAll[m]
                //          , where meanImage[i][m]=mean of color m across all pixels of img i
                //                  meanAll[m]= mean of color m across all pixels of all imgs
                //distImage[i][m]=dist[i][m], i.e. it is kind of a distortion eg due to brighness change (which I am not interested in while judging whether a pxl is fg or bg)
                //realMean[j][m]=mean of color m at pixel j across all background images, without the img we are looking at. Value between 0 &1
                //
                //Here we calculate:
                //realMean[j][m]*distImage[i][m] i.e. mean*distortion(brightness,..), cut the result at 0 and 1--> value between 0& 1 (entspricht col value 0 and 255)
                //then the imgColor[j][m] of our image is substracted, and the absolute of the result is taken to get a fair distance t

                //instead of all the following (except the integration of the neighbour distance, which might be necessary)
                //which is good but not the usual way (mehr der Tüftlerweg), we could use the Mahalanobis distance for
                //a heuristic classification

                //double getSD(double orginalSD, int images, double originalMean, double imageColor){
                double sd = getSD(sdPixel[j][m], _nImages, _meanPixel[j][m], imgColor); //remove the actual img from the sd calculation
                totalDiff += t;// store the total diff of pxl j in img i
                totalDist += sdPixel[j][m];// remember that sdPixel[j][m] is the std of pxl j color m (with the actual img)

                if (t > sd / 2 && sdPixel[j][m] > 0.001) { //If t (dist) is not too small & there is some variation among all imgs
                    //but why those values?
                    t /= sqrt((sd * sd) / _nImages); //this is equivalent to t=t*sqrt(nImages)/sd
                    //setting t in perspective to sd makes sense. Then, the nr of images on which t is based is also
                    //important (we are more sure that it's fg if the distance to the mean of many imgs is high instead
                    //of to the mean of only 2-3 imgs-- in the latter case, it would be also likely to be shadow effects)
                    //Taking the sqrt makes sense, because the effect to take 2 instead of 1 img to calc the mean is
                    //surely higher than to take 101 instead of 100 imgs. (instead of the sqrt, it would also make sense
                    //to take the log)

                    tValues[j] = std::max(tValues[j], std::abs(t)); //the color channel m with the largest t is stored
                    //tValues[j] += std::abs(t);
                }
            }
            //tValues[j] /= _nChannels;

            tValues[j] *= totalDiff; //Give more importance to pixels with a bigger change

            /*if(i == 0 && j == 113){
                std::cout << totalDiff << " " << totalDist << std::endl;
            }*/

            if (totalDiff < 0.005 || totalDist < min_pixel_sd) { //Max is _nChannels // if totalDiff (sum of t's of a pxl j in img i) is too small  ie the pxl is not different enough
                //or the sd of pxl j across all imgs is too small (ie probably just shadow effects and stuff I guess)
                tValues[j] = 0;
            }else if(use_neighbours){ //Additional heuristic, check difference with neighbour images
                double neighbourDiff = 0;

                if(i > 0){ //if its not the first image
                    double pixelDiff = 0;
                    for (int m = 0; m < _nChannels; m++) {

                        pixelDiff += abs(std::min(1.0, std::max(0.0, _pixelLut[_images[i - 1]->_image[j * _nChannels + m]] - _distImage[i - 1][m])) -
                                                  std::min(1.0, std::max(0.0, _pixelLut[_images[i]->_image[j * _nChannels + m]] - _distImage[i][m])) );
                        //get the absolute difference between the pxl of the preceeding img & the img of interest, taking into account the img distortion (brightness changes of whole img etc)

                    }

                    neighbourDiff = std::max(pixelDiff, neighbourDiff);
                }

                if(i + 1 < _nImages){ //if its  the last image: analagous to the above
                    double pixelDiff = 0;
                    for (int m = 0; m < _nChannels; m++) {
                        pixelDiff += abs(std::min(1.0, std::max(0.0, _pixelLut[_images[i + 1]->_image[j * _nChannels + m]] - _distImage[i + 1][m])) -
                                          std::min(1.0, std::max(0.0, _pixelLut[_images[i]->_image[j * _nChannels + m]] - _distImage[i][m])));
                    }

                    neighbourDiff = std::max(pixelDiff, neighbourDiff); //max neighbour diff across different col channels, ie max(comparison_with_preceding_img, comparison_with_following_img)
                    //neighbourDiff is between 0 & 3 (added differences of all 3 channels)
                }

                tValues[j] *= log(M_E + neighbourDiff); //M_E is the euler nr. t Values are weighted higher if the diff to one of the neighbours is high
                //tValues[j] is multiplied by a value between 1 and 1.74
                //weighting using the log makes sense because increasing neighbourDiff will not be meaningful above
                //a certain value (everything is likely fg above it). I.e. changes are more important if neighbourDiff
                //is small
            }
        }

        double *tValuesSorted = new double[_nPixels];
        std::memcpy(tValuesSorted, tValues, _nPixels * sizeof(double)); //copies values from tValues into tValuesSorted
        std::sort(tValuesSorted, tValuesSorted + _nPixels, sort_using_greater_than); //sort

        const double maxT = tValuesSorted[(int) (_nPixels * foregroundPercentage)] * max_t_flexibility; //foregroundPercentage=_heuristic defined from user
        //nPixels*foregroundPercentage= nr of foreground pxls
        //maxT is the min t that can still be classified as fg

        delete [] tValuesSorted;

        //std::cout << "!!! " << (tValuesSorted[(int) (_nPixels * foregroundPercentage)] * max_t_flexibility) << " " << tValuesSorted[(int) (_nPixels * foregroundPercentage)] << " " << tValuesSorted[_nPixels - 1] << std::endl;

        //Find the pixels that are most different in that image
        for (int j = 0; j < _nPixels; j++) {
            if (debug_t_test) { //this is just for testing
                _sumZ[i][j] = tValues[j] * 255;
            }

            if (tValues[j] >= maxT && maxT > 0 && tValues[j] > min_tvalue) { //maxT >0 because t's are highly unlikely to be 0 (pxl j must be exactly the mean over all imgs, and there must be no difference between it and the neighbour img pxl j
                //well this might not be so unlikely if bins are very small.
                _z[i][j] = 1;
            } else {
                _z[i][j] = 0;
            }
        }
        delete [] tValues;
    }

    //Remove pixels from foreground that are foreground in too many images
    int *fgCounter = new int[_nPixels];
    std::fill(fgCounter, fgCounter + _nPixels, 0);

    for(int i = 0; i < _nImages; i++) {
        for (int j = 0; j < _nPixels; j++) {
            if(_z[i][j] == 1){
                fgCounter[j]++; //counts how many times a pxl is classified as fg
            }
        }
    }

    for (int j = 0; j < _nPixels; j++) {
        //std::cout << fgCounter[j] << " " << (_nImages * 0.8) << std::endl;
        if(fgCounter[j] > _nImages * max_images_foreground){
            for(int i = 0; i < _nImages; i++) {
               _z[i][j] = 0; //set the pixel to bg everywhere, if it has been classified as fg in too many imgs
            }
        }
    }

    mem::FreeArray2d(&sdPixel, _nPixels, 3);

    delete [] fgCounter;

    int steps = 0;
    while(steps < max_smoothing_passes && HeuristicFilterZ() > 1){ //HeuristicFilterZ filters out fg or bg pixels with too many neighbours classified differently
        //as long as HeuristicFilterZ() does something, but at most 3 times, apply it.
        steps++;
    }

    if(!debug_t_test){
        //Set _sumZ to correct value according to our z initialization
        for(int i = 0; i < _nImages; i++){
            for(int j = 0; j < _nPixels; j++){
                if(_z[i][j] == 1){
                    _sumZ[i][j] = 1;
                }else{
                    _sumZ[i][j] = 0;
                }
            }
        }
    }

    if(init_fg_mean_from_heuristic){
        //Set foreground colors
        //Actually if there is >=1 pixel classified as fg in img i, then the mean fg color is stored in meanImage[i]
        //else, the mean (bg) color is stored in meanImage[i]
        for(int i = 0; i < _nImages; i++){
            for(int m = 0; m < _nChannels; m++){
                _meanImage[i][m] = 0;

                int fgPixels = 0;
                for(int j = 0; j < _nPixels; j++){
                    if(_z[i][j] == 1){
                        fgPixels++;
                        _meanImage[i][m] += _pixelLut[_images[i]->_image[j * _nChannels + m]];
                    }
                }

                if(fgPixels > 0){
                    _meanImage[i][m] /= fgPixels;
                }else{//if there is no fg we have to find some other good initialization for the fg that might be here
                    //ie we just take the mean of the img of course
                    for(int j = 0; j < _nPixels; j++){
                        _meanImage[i][m] += _pixelLut[_images[i]->_image[j * _nChannels + m]];
                    }
                    _meanImage[i][m] /= _nPixels;
                }
            }
        }
    }

    if(init_bg_mean_from_heuristic){
        TAlgorithm::InitMeanBG(_nPixels, _nImages, _nChannels, _z, _meanPixel, _images);
    }

    if(!init_z_from_heuristic){ //only for test purposes I guess
        for(int i = 0; i < _nImages; i++){
            for(int j = 0; j < _nPixels; j++){
                _z[i][j] = 0;
                _sumZ[i][j] = 0;
            }
        }
    }
}

int THeuristic::HeuristicFilterZ(){
    int total = 0;
    //Filter out single foreground/background pixels
    for(int i = 0; i < _nImages; i++){
        for(int j = 0; j < _nPixels; j++){
            int neighbours = 0;

            const bool canGoLeft = j % _imageWidth != 0;
            const bool canGoRight = j % _imageWidth != _imageWidth - 1;

            //Top
            int pixelIndex = j - _imageWidth;
            if(pixelIndex >= 0 && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //TopRight
            pixelIndex = j - _imageWidth + 1;
            if(pixelIndex >= 0 && canGoRight && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //Right
            pixelIndex =j + 1;
            if(pixelIndex < _nPixels && canGoRight && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //BottomRight
            pixelIndex = j + 1 + _imageWidth;
            if(pixelIndex < _nPixels && canGoRight && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //bottom
            pixelIndex = j + _imageWidth;
            if(pixelIndex < _nPixels && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //bottomLeft
            pixelIndex = j + _imageWidth - 1;
            if(pixelIndex < _nPixels && canGoLeft && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //left
            pixelIndex = j - 1;
            if(pixelIndex >= 0 && canGoLeft && _z[i][pixelIndex] == 1){
                neighbours++;
            }

            //topLeft
            pixelIndex = j - 1 - _imageWidth;
            if(pixelIndex >= 0 && canGoLeft && _z[i][pixelIndex] == 1) {
                neighbours++;
            }

            if(neighbours < 3){
                if(_z[i][j] == 1){
                    _z[i][j] = 0;
                    total++;
                }
            }else if(neighbours >= 6){
                if(_z[i][j] == 0){
                    _z[i][j] = 1;
                    total++;
                }
            }
        }
    }

    return total;
}

