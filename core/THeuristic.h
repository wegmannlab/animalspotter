//
// Created by beat on 26.07.18.
//

#ifndef ANIMALSPOTTER_THEURISTIC_H
#define ANIMALSPOTTER_THEURISTIC_H

#include "TImage.h"
#include <vector>

class THeuristic {

public:
    THeuristic(unsigned char** z, double** meanImage, double** distImage, double** meanPixel, unsigned long ** sumZ, std::vector<TImage *> images, double* pixelLut,
               int nImages, int nPixels, int imageWidth, int nChannels);
    void ExtractForeground(double _foregroundPercentage);

private:
    void Heuristic(double foregroundPercentage, int step);
    int HeuristicFilterZ();
    void RemoveSquares();

    unsigned char** _z;
    double** _meanImage;
    double** _distImage;
    double** _meanPixel;
    unsigned long ** _sumZ;
    std::vector<TImage *> _images;

    double* _pixelLut;

    int _nImages, _nPixels;
    int _imageWidth;
    int _nChannels;
};


#endif //ANIMALSPOTTER_THEURISTIC_H
