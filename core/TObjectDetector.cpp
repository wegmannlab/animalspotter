//
// Created by beat on 08.08.18.
//

#include "TObjectDetector.h"

#include <algorithm>
#include <iostream>

#include <cstring>

const bool debug_boxes = false;
const double min_search_size = 0.2;
const double min_box_size = 0.03;
const int search_start_box = 6;
const int max_boxes = debug_boxes ? 30 : 4;
const double max_weight_difference = 0.5;

TObjectDetector::TObjectDetector(TImage * image, double margin, double *zSum, unsigned char colors[3], double min_probabilty_threshold, bool do_blur) :
        _image(image), _margin(margin), _min_probabilty_threshold(min_probabilty_threshold){

    if (do_blur){
        _zSum = blur(zSum, image->GetWidth(), image->GetHeight());
        _memory_allocated=true;
    }
    else{
        _zSum = zSum;
        _memory_allocated=false;
    }

    _color[0] = colors[0];
    _color[1] = colors[1];
    _color[2] = colors[2];

}

TObjectDetector::~TObjectDetector(){
    if (_memory_allocated){
        delete [] _zSum;
    }

}

void TObjectDetector::visit(bool *connected, int x, int y, bool init){

    if(x >= _image->GetWidth() || y >= _image->GetHeight() || x < 0|| y < 0 || connected[y * _image->GetWidth() + x]){
        return;
    }

    const double probability = _zSum[x + y * _image->GetWidth()];

    if(init || probability >= _min_probabilty_threshold){
        connected[y * _image->GetWidth() + x] = probability >= _min_probabilty_threshold;

        for(int x2 = std::max(x - 1, 0); x2 <= x + 1; x2++) {
            for (int y2 = std::max(y - 1, 0); y2 <= y + 1; y2++) {
                visit(connected, x2, y2, false);
            }
        }
    }

}

std::vector<TBoundingBox *> TObjectDetector::SplitSearch(int startX, int stopX, int startY, int stopY, int recursionDepth){

    recursionDepth++;
    std::vector<TBoundingBox *> boundingBoxes;

    //if we are already very deep, don't go any deeper (else we'll get a stack overflow error)
    if (recursionDepth>10){
        return boundingBoxes;
    }

    if(stopX - startX < stopY - startY){ //split vertically
        int middle = (stopY + startY) / 2;
        for(TBoundingBox * box: GetBoundingBoxes(startX, stopX, startY, middle, recursionDepth)){
            boundingBoxes.push_back(box);
        }
        for(TBoundingBox * box: GetBoundingBoxes(startX, stopX, middle, stopY, recursionDepth)){
            boundingBoxes.push_back(box);
        }
    }else{//split horizontally
        int middle = (stopX + startX) / 2;

        for(TBoundingBox * box: GetBoundingBoxes(startX, middle, startY, stopY, recursionDepth)){
            boundingBoxes.push_back(box);
        }
        for(TBoundingBox * box: GetBoundingBoxes(middle, stopX, startY, stopY, recursionDepth)){
            boundingBoxes.push_back(box);
        }
    }

    return boundingBoxes;
}

std::vector<TBoundingBox *> TObjectDetector::GetBoundingBoxes(int searchStartX, int searchStopX, int searchStartY, int searchStopY, int recursionDepth){

    //first time this function is called the parameters are the left border indeces (start) and the width or height (stop), respectively, of the whole img, i.e. we start by assuming that the
    //whole img is a bb
    std::vector<TBoundingBox *> boundingBoxes;

    //if box is too small, return empty vector 'boundingBoxes' (no bb's)
    if(searchStopX - searchStartX < _image->GetWidth() * min_search_size || searchStopY - searchStartY < _image->GetHeight() * min_search_size){
        return boundingBoxes;
    }

    double totalX = 0;
    double totalY = 0;

    const int width = _image->GetWidth();
    const int height = _image->GetHeight();
    double totalProbab = 0;

    //for each pxl in bb, get prob(fg); if this prob is big enough, add prob^2 to totalProbab
    //& add x*prob^2 to totalX, y*prob^2 to totalY
    //note: the square function [0,1] derived twice is pos, i.e. we give less weight to small probs
    //& more weight to high probs than if we'd weigh linearly
    for(int y = searchStartY; y < searchStopY; y++){
        for(int x = searchStartX; x < searchStopX; x++){
            double probability = _zSum[x + y * width];
            if(probability >= _min_probabilty_threshold){
                probability = probability * probability;
                totalX += x * probability;
                totalY += y * probability;
                totalProbab += probability;
            }
        }
    }

    //TODO: Also stop when max probab is lower than min probab threshhold
    //if no pxl in the box is fg, return empty vector (no bb's)
    if(totalProbab == 0){
        return boundingBoxes;
    }
    //find center of the probabilities
    int middleX = (int)(totalX / totalProbab); //Mind 0
    int middleY = (int)(totalY / totalProbab);

    //Find the middle of the region
    int startX = middleX;
    int endX = std::min(searchStopX - 1, middleX + 1);//endX is 1 pxl to the right, if we are not already at the right border of the box
    int startY = middleY;
    int endY = std::min(searchStopY - 1, middleY + 1);

    bool *connected = new bool[_image->GetWidth() * _image->GetHeight()];
    std::fill(connected, connected + (_image->GetHeight() * _image->GetWidth()), false);

    for(int x = middleX - search_start_box; x <= middleX + search_start_box; x++) {
        for (int y = middleY - search_start_box; y <= middleY + search_start_box; y++) {
            visit(connected, x, y, true); //writes true if pxl is fg; then, checks all neighbouring pxls of any
            //detected fg pxl's & calls itself again through the newly found fg pxls.
        }
    }

    int connectedCount = 0;
    for(int x = 0; x < _image->GetWidth(); x++){ //new set x=0, y=0, and go until end of image. to be able to expand out of input bb borders
        for(int y = 0; y < _image->GetHeight(); y++){
            if(connected[y * _image->GetWidth() + x]){
                startX = std::min(startX, x); //get the smallest x that is fg & connected to middleX
                startY = std::min(startY, y);

                endX = std::max(endX, x);
                endY = std::max(endY, y);

                connectedCount++;
            }
        }
    }

    if(connectedCount < 4 * search_start_box * search_start_box * 0.1){ //smaller than 14.4

        for(TBoundingBox * box: SplitSearch(searchStartX, searchStopX, searchStartY, searchStopY, recursionDepth)){
            boundingBoxes.push_back(box);
        }

        delete [] connected;

        return boundingBoxes;
    }

    //up till now we just found the borders of our new bb; now we have to check for every pxl in it
    //whether it is fg:
    for(int x = startX; x < endX + 1; x++) {
        for (int y = startY; y < endY + 1; y++) {
            visit(connected, x, y, false);
        }
    }

    //expand the bb further to include fg pxl's connected to the box if they are
    //within the borders of the initial bb (given to this function GetBoundingBoxes(int,int,int,int))
    for(int x = searchStartX; x < searchStopX; x++){
        for(int y = searchStartY; y < searchStopY; y++){
            if(connected[y * _image->GetWidth() + x]){
                startX = std::min(startX, x);
                startY = std::min(startY, y);

                endX = std::max(endX, x);
                endY = std::max(endY, y);
            }
        }
    }

    middleX = (startX + endX) / 2;
    middleY = (startY + endY) / 2;
    if((endX - startX) / (double) _image->GetWidth() < min_box_size && (endY - startY) / (double) _image->GetHeight() < min_box_size){

        for(TBoundingBox * box: SplitSearch(searchStartX, searchStopX, searchStartY, searchStopY, recursionDepth)){
            boundingBoxes.push_back(box);
            //because then we have another center to start from--> we might end up with a bigger box
        }

        delete [] connected;

        return boundingBoxes;
    }

    int widthBox = std::max((int)(_image->GetWidth() * _margin), endX - startX);
    int heightBox = std::max((int)(_image->GetHeight() * _margin), endY - startY);

    if (_margin!=0){ //only then, the following has any effect
        widthBox *= static_cast<int>(1. + _margin);
        heightBox *= static_cast<int>(1. + _margin);

        startX = std::max(0, middleX - widthBox / 2);
        startY =std::max(0, middleY - heightBox / 2);
        endX = std::min(_image->GetWidth() - 1, middleX + widthBox / 2);
        endY = std::min(_image->GetHeight() - 1, middleY + heightBox / 2);
    }


    TBoundingBox *box = new TBoundingBox(startX, startY, endX - startX, endY - startY);

    //Count the total amount of foreground pixels in the box we just defined & sum the prob to be fg over all pxl in the bb
    double totalInBox = 0;
    int totalForeGround = 0;
    for(int x = startX; x < endX; x++) {
        for (int y = startY; y < endY; y++) {
            const double z = _zSum[x + y * width];
            totalInBox += z;
            if(z >= _min_probabilty_threshold){
                totalForeGround++;
            }
        }
    }

    //count the nr of pxl with connected [pxl]==true
    connectedCount = 0;
    for(int x = box->GetX(); x < box->GetX() + box->GetWidth(); x++){
        for(int y = box->GetY(); y < box->GetY() + box->GetHeight(); y++){
            if(connected[y * _image->GetWidth() + x]){
                connectedCount++;
            }
        }
    }

    delete [] connected;

    double boxWeight = totalInBox / ((endX - startX) * (endY - startY));
    box->SetForegroundCount(totalForeGround);
    box->SetWeight(boxWeight);
    box->SetConnectedCount(connectedCount);
    box->SetColor(0, _color[0]);
    box->SetColor(1, _color[1]);
    box->SetColor(2, _color[2]);

    int subbox_minX = std::max(0, (int)(box->GetX() - box->GetWidth() * 0.3));
    int subbox_minY = std::max(0, (int)( box->GetY() - box->GetHeight() * 0.3));
    int subbox_maxX = std::min(_image->GetWidth(), (int)(box->GetX() + box->GetWidth() * 1.3)); //new: the subbox can go further to the right than the bb found in this function (may even go further right than the input bb of this function i.e. larger than searchStopX)
    int subbox_maxY = std::min(_image->GetHeight(), (int)( box->GetY() + box->GetHeight() * 1.3)); //same thing is new


    if (recursionDepth>10){
        boundingBoxes.push_back(box);
        return boundingBoxes;
    }

    TBoundingBox subBoxSearch(subbox_minX, subbox_minY, subbox_maxX - subbox_minX, subbox_maxY - subbox_minY);
    if(subBoxSearch.GetX() != searchStartX && subBoxSearch.GetY() != searchStartY && subBoxSearch.GetMaxX() != searchStopX && subBoxSearch.GetMaxY() != searchStopY){

        recursionDepth++;

        for(TBoundingBox * subBox : GetBoundingBoxes(subBoxSearch.GetX(), subBoxSearch.GetY(), subBoxSearch.GetMaxX(), subBoxSearch.GetMaxY(), recursionDepth)){
            if(subBox->GetWeight() > box->GetWeight() && !box->IsInsideMe(*subBox)){
                boundingBoxes.push_back(subBox);
            }else{
                delete subBox;
            }
        }
    }
    for(TBoundingBox * split_box: SplitSearch(searchStartX, searchStopX, searchStartY, searchStopY, recursionDepth)){
        boundingBoxes.push_back(split_box);
    }

    boundingBoxes.push_back(box);

    return boundingBoxes;
}

void TObjectDetector::mergeBoxes(std::vector<TBoundingBox *> &boundingBoxes){
    bool merged;

    do{
        merged = false;
        for(size_t i = 0; i < boundingBoxes.size() && !merged; i++){
            TBoundingBox * first = boundingBoxes[i];
            for(size_t j = i + 1; j < boundingBoxes.size() && !merged; j++){
                TBoundingBox * second = boundingBoxes[j];

                if(i != j && first->isOverlapping(*second)){

                    if(first->IsInsideMe(*second)){ //if second box is inside first box, delete the second box
                        boundingBoxes.erase(boundingBoxes.begin() + j);

                        //std::cout << "Remove box " << second->toString() << " because of " << first->toString() << std::endl;
                        delete second;
                    }else if(second->IsInsideMe(*first)){ //other way around
                        boundingBoxes.erase(boundingBoxes.begin() + i);

                        //std::cout << "Remove box " << first->toString() << " because of " << second->toString() << std::endl;
                        delete first;
                    }else{
                        TBoundingBox * mergedBox = first->merge(second, _image, _zSum, _min_probabilty_threshold);
                        //std::cout << "MERGE BOXES " << first->toString() << " " << second->toString() << std::endl;

                        boundingBoxes.erase(boundingBoxes.begin() + j);
                        boundingBoxes.erase(boundingBoxes.begin() + i);

                        delete first;
                        delete second;

                        boundingBoxes.push_back(mergedBox);
                    }
                    merged = true;
                }
            }
        }

    }while(merged);
}

std::vector<TBoundingBox *> TObjectDetector::GetBoundingBoxes(){
    std::vector<TBoundingBox *> boundingBoxes;

    for(TBoundingBox * box: GetBoundingBoxes(0, _image->GetWidth(), 0, _image->GetHeight(), 0)){
        boundingBoxes.push_back(box);
    }

    if(boundingBoxes.size() == 0){ //if there are no bb, directly return empty vector
        return boundingBoxes;
    }

    //sort boxes by weight
    std::sort( boundingBoxes.begin( ), boundingBoxes.end( ), [ ]( const TBoundingBox* lhs, const TBoundingBox* rhs )
    //this is a lambda function (barbone syntax: [](){} )
    //where [] is the capture list, () the argument list, and
    {
        return lhs->GetWeight() > rhs->GetWeight(); //this is the function body of the lambda function
    });

    if(!debug_boxes) {
        mergeBoxes(boundingBoxes);
    }

    //as long as we have too many boxes, the last box is deleted (the one with the least fg)
    while(boundingBoxes.size() > max_boxes){
        delete boundingBoxes.back();
        boundingBoxes.pop_back();
    }

    if(!debug_boxes){
        while(boundingBoxes.front()->GetWeight() * max_weight_difference > boundingBoxes.back()->GetWeight()){
            delete boundingBoxes.back();
            boundingBoxes.pop_back();
        }
    }

    return boundingBoxes;
}

double averageBlur(double * zSum, int j, int width, int nPixels){
    double total = 0;

    int neighbours = 0;

    const bool canGoLeft = j % width != 0;
    const bool canGoRight = j % width != width - 1;

    //Top
    int pixelIndex = j - width;
    if(pixelIndex >= 0){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //TopRight
    pixelIndex = j - width + 1;
    if(pixelIndex >= 0 && canGoRight){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //Right
    pixelIndex =j + 1;
    if(pixelIndex < nPixels && canGoRight){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //BottomRight
    pixelIndex = j + 1 + width;
    if(pixelIndex < nPixels && canGoRight){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //bottom
    pixelIndex = j + width;
    if(pixelIndex < nPixels){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //bottomLeft
    pixelIndex = j + width - 1;
    if(pixelIndex < nPixels && canGoLeft){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //left
    pixelIndex = j - 1;
    if(pixelIndex >= 0 && canGoLeft){
        total += zSum[pixelIndex];
        neighbours++;
    }

    //topLeft
    pixelIndex = j - 1 - width;
    if(pixelIndex >= 0 && canGoLeft) {
        total += zSum[pixelIndex];
        neighbours++;
    }

    return total / neighbours;
}

double * blur(double * zSum, int width, int height){

    const int nPixels = width * height;
    double * blurred = new double[nPixels];

    for(int j = 0; j < nPixels; j++){
        blurred[j] = averageBlur(zSum, j, width, nPixels);
    }

    return blurred;
}