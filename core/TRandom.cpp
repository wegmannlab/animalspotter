//
// Created by beat on 13.06.18.
//

#include "TRandom.h"

#include <cmath>

TRandom::TRandom(TFastMath *fastMath, int x, int y) {
    _fastMath = fastMath;

    _x = x;
    _y = y;
    _z = 521288629;
    _w = 88675123;
}


double TRandom::RandomUniform() {
    uint32_t t; //uint32_t is a numeric type that guarantees 32 bits, the value is unsigned, ie the range of values is 0 to 232 - 1.
    t = _x ^ (_x << 11);
    _x = _y;
    _y = _z;
    _z = _w;
    return (_w = _w ^ (_w >> 19) ^ (t ^ (t >> 8))) / 4294967296.0;
}

double TRandom::RandomUniformInRange(double min, double max) {
    return (RandomUniform() * (max - min) + min);
}

double TRandom::RandomNormal(double mean, double sd) {
    // Box-Mueller is no good -> clamped between -6 and 6 with double
    // Should do Ratio-of-uniforms, which is better with only simple operations and a log 1/50th of the time

    double w, x1, x2;
    do {
        x1 = 2. * RandomUniform() - 1.;
        x2 = 2. * RandomUniform() - 1.;
        w = x1 * x1 + x2 * x2;
    } while (w >= 1.0 || w < 1E-30);

    w = sqrt((-2. * log(w)) / w);
    x1 *= w;
    double val = (x1 * sd + mean);
    return val;
}

double TRandom::MoveRandomNormal(double mean, double sd, double min,
                                    double max) {
    //mean is the mean value of col m of pxl j across all imgs, sd the std of that, min=0, max=1
    if (sd == 0)
        return mean;

    double val = RandomNormal(mean, sd);

    // Make this if else faster ...
    if (val > max)
        val = 2 * max - val;
    if (val < min)
        val = 2 * min - val;

    return val;
}

double TRandom::MoveRandomNormalNoBounds(double mean, double sd) {
    //propose a new value based on normally distributed transition kernel
    if (sd == 0)
        return mean;

    double val = cos(2.0 * 3.14159265359 * RandomUniform()) * sqrt(-2.0 * _fastMath->Log(RandomUniform()));

    val *= sd;
    val += mean;

    return val;
}

double TRandom::MoveRandomNormal(double mean, double sd, double min,
                                    double max, double randomValue1, double randomValue2) {
    //propose a new value based on a bounded normally distributed transition kernel
    if (sd == 0) {
        return mean;
    }

    double val = cos(2.0 * 3.14159265359 * randomValue1) * sqrt(-2.0 * _fastMath->Log(randomValue2));

    //sd = std::min(sd, max - min); //Otherwise we can go outside min/max range

    val *= sd;
    val += mean;

    // Make this if else faster ...
    if (val > max) {
        val = 2 * max - val;
    }

    //TODO: Check is this really minus?
    if (val < min) {
        val = 2 * min - val;
    }

    return val;
}

double TRandom::MoveRandomUniform(double from, double factor, double min,
                                     double max) {
    double move = factor * RandomUniformInRange(-1, 1);
    double val = from + move;

    if (val > max)
        val = 2 * max - val;
    if (val < min)
        val = 2 * min - val;

    return val;
}

double TRandom::MoveRandomUniform(double from, double factor, double min,
                                     double max, double randomValue) {
    double move = factor * randomValue;
    double val = from + move;

    if (val > max)
        val = 2 * max - val;
    if (val < min)
        val = 2 * min - val;

    return val;
}
