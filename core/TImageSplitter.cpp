//
// Created by beat on 24.09.18.
//

#include "TImageSplitter.h"
#include "TAlgorithm.h"
#include "TImageGroup.h"
#include <cmath>
#include <iostream>
#include <numeric>

TImageSplitter::TImageSplitter(int maxTimeDifferenceSeconds, size_t minBinSize, double minGrayscaleForNight, bool splitDayNight, float pxlFractionDist, int howManiestPxl) : _maxTimeDifferenceSeconds(maxTimeDifferenceSeconds), _minBinSize(minBinSize), _minGrayscaleForNight(minGrayscaleForNight), _splitDayNight(splitDayNight), _pxlFractionDist(pxlFractionDist) , _howManiestPxl(howManiestPxl){

}
void TImageSplitter::SetOutputPath(char *outputPath){
    _outputPath = outputPath;
}
std::vector<std::vector<TImage *>> TImageSplitter::SplitImages(std::vector<TImage *>& images){

    std::vector<std::vector<TImage *>> dayNight;

    //Split day and night
    if(_minGrayscaleForNight > 0){ //default value of minGrayscaleForNight in this cpp code is 0, but it can be set to
        // something different by the user (and if AS is called through Pipeline.py, the default is 0.75)
        std::vector<TImage *> day;//make an array 'day'
        std::vector<TImage *> night;
        std::vector<std::string> day_names;
        std::vector<std::string> night_names;
        int count_night=0;
        int count_day=0;

		for (TImage * image : images) {
            if(IsNighttime(image)){
                night.push_back(image);
                night_names.push_back(image->GetName());
                count_night+=1;
            }else{
                day.push_back(image);
                day_names.push_back(image->GetName());
                count_day+=1;
            }
        }
        if (_splitDayNight) {

            std::string myString = std::string(_outputPath);
            std::stringstream ss;
            ss << myString << "daynight.txt";
            std::string path = ss.str();


            std::ofstream file(path); //open in constructor

            std::string data("Night images are: ");
            file << data << "\n";
            int count = 0;
            for (std::string name: night_names) {
                file << name;
                count += 1;
                if (count != count_night) {
                    file << ";";
                }
            }
            file << "\n";
            count = 0;
            file << "Day images are: " << "\n";
            for (std::string name: day_names) {
                file << name;
                count += 1;
                if (count != count_day) {
                    file << ";";
                }
            }
            file << std::endl;
            file.close();

            std::cout << "Images were split into day and night; this classification can be found in: " << path
                      << std::endl;
            std::cout << "To actually run the Animalspotter, remove the parameter --dn" << std::endl;


            dayNight.push_back(images);
            return dayNight;
        }
        if (night.size()>1){
            dayNight.push_back(night);
        }
        else if (night.size()==1){
            night.clear(); //empties the night vector
            std::cout<<"There is only 1 night img; it has been discarded"<<std::endl;
        }
        if (day.size()>1){
            dayNight.push_back(day);
        }
        else if (day.size()==1){
            day.clear(); //empties the day vector
            std::cout<<"There is only 1 day img; it has been discarded"<<std::endl;
        }

    }else{
        std::vector<TImage *> all;
        for(TImage * image : images) {
            all.push_back(image);
        }

        dayNight.push_back(all);
    }
    //note: dayNight now contains either 2 or 1 vectors of images

    if (_splitDayNight && _minGrayscaleForNight == 0) {
        std::cout << "minGrayscaleForNight is 0; therefore, day and night images could not be separated"
                  << std::endl;
        return dayNight;
    }

    std::vector<std::vector<TImage *>> bins2;

    //Split day night groups into sensible bins
    int nrCalls=0;
    for(std::vector<TImage *> group : dayNight){ //for each vector in dayNight (either there are 1 or 2 vectors)


        if(!group.empty()){
            std::vector<std::vector<TImage *>> bins;//this array of arrays will contain all bins of 1 group in the end

            std::vector<TImage *> currentBin;
            currentBin.push_back(group[0]);//add first element

            //Split groups by time difference first
			for (size_t i = 1; i < group.size(); i++) { //for each img index
                if(_maxTimeDifferenceSeconds >= 0){
                    const int diff = DistanceSeconds(group[i - 1], group[i]);
                    if(diff > _maxTimeDifferenceSeconds){
                        bins.push_back(currentBin);
                        currentBin = std::vector<TImage *>();
                    }
                }

                currentBin.push_back(group[i]);//add the img to the currentBin
            }

            bins.push_back(currentBin);//add last bin to bins
			size_t minBinSize = _minBinSize;
            if(minBinSize > group.size()){
                minBinSize = group.size();
            }

            //Make groups bigger if possible and needed
            nrCalls++; //stores how many times MergeBins() is called
            bins = MergeBins(bins, minBinSize, nrCalls);
            //minBinSize is either exactly the # of imgs in the group (all imgs, day imgs or night imgs;
            //this is because I cannot make the bins smaller than this ever!) or some smaller value

            for(std::vector<TImage *> bin : bins){
                bins2.push_back(bin);//add all bins to bins2
            }
        }

    }

    return bins2;
}

//TODO: Move to TImage?
bool TImageSplitter::IsNighttime(const TImage *image) const {
    int grayPixels = 0;

    int nPixels = image->GetWidth() * image->GetHeight();

    for (int j = 0; j < nPixels; j++) {
        unsigned char pixel = image->_image[j * nChannels];

        bool grayScale = true;
        for (int m = 1; m < nChannels; m++) {
            if (pixel != image->_image[j * nChannels + m]) {
                grayScale = false;
            }
        }

        if (grayScale) {
            grayPixels++;
        }
    }

    const double grayRatio = grayPixels / (double) nPixels;

    return grayRatio >= _minGrayscaleForNight;
}


int TImageSplitter::DistanceSeconds(const TImage *imageA, const TImage *imageB) const{
    std::chrono::seconds seconds = std::chrono::duration_cast<std::chrono::seconds>(imageA->GetOriginalTime() - imageB->GetOriginalTime());

    return abs(seconds.count());
}

double TImageSplitter::Distance(const TImage *imageA, const TImage *imageB) const {

    int totalDiff = 0;
    int nPixels = imageA->GetWidth() * imageA->GetHeight();

    for (int j = 0; j < nPixels; j++) {
        double pixelDiff = 0;
        for (int m = 0; m < nChannels; m++) {
            pixelDiff += std::abs(imageA->_image[j * nChannels + m] - imageB->_image[j * nChannels + m]);
        }

        pixelDiff /= nChannels * 255.;

        if (pixelDiff > 0.1) {
            totalDiff++;
        } else {
            //std::cout << pixelDiff << "( " << (int)(imageA->_image[j][0]) <<" " << (int)(imageA->_image[j][1]) <<" "<< (int)(imageA->_image[j][2]) << ") - ( " << (int)(imageB->_image[j][0]) <<" " << (int)(imageB->_image[j][1]) <<" "<< (int)(imageB->_image[j][2]) << ")"<< std::endl;
        }
    }

    return (double) totalDiff / nPixels;
}
double TImageSplitter::ImageDifference(const TImage &imageA, const TImage &imageB) const {

    int nPixels = imageA.GetWidth() * imageA.GetHeight();

    if (imageA.GetWidth() != imageB.GetWidth() ||
        imageA.GetHeight() != imageB.GetHeight()) {
        throw std::runtime_error("Can not analyse images with variable resolutions");
    }

    if (_pxlFractionDist==1){ //this is much faster
        double totalDiff = 0;
        for (int j = 0; j < nPixels; j++) {
            for (int m = 0; m < nChannels; m++) {
                double diff = std::abs(imageA._image[j * nChannels + m] - imageB._image[j * nChannels + m]) /
                              255.;//((double) std::max(imageA->_image[j * nChannels + m], imageB->_image[j * nChannels + m]));

                if (!std::isnan(diff)) {
                    totalDiff += diff;
                }
            }
        }
        return(totalDiff/(nPixels*nChannels));
    }
    else{ //much slower
        std::vector<double> differences;

        for (int j = 0; j < nPixels; j++) {
            double pxlDiff=0;
            for (int m = 0; m < nChannels; m++) {
                double diff = std::abs(imageA._image[j * nChannels + m] - imageB._image[j * nChannels + m]) /
                              255.;//((double) std::max(imageA->_image[j * nChannels + m], imageB->_image[j * nChannels + m]));

                if (!std::isnan(diff)) {
                    pxlDiff+=diff;
                }
            }
            differences.push_back(pxlDiff/nChannels);

        }

        double kSmallestAverage;
        //now I only want the pxl_percent_dist% lowest distances to be considered
        int k=floor(_pxlFractionDist * nPixels);
        std::sort(differences.begin(), differences.end());
        kSmallestAverage=std::accumulate( differences.begin(), differences.begin()+k, 0.0) / k;
        return kSmallestAverage;
    }

    //for testing
    //totalDiff=totalDiff/(nPixels*nChannels);
    //std::cout<<kSmallestAverage<<"true would be"<<totalDiff<<std::endl;

}
double TImageSplitter::ImageRoughDifference(const TImage &imageA, const TImage &imageB) const {

    int nPixelsConsidered = imageA.GetWidth()/_howManiestPxl * imageA.GetHeight()/_howManiestPxl; //only look at each 5th pxl to get a rough dist (because sorting is costly)

    if (imageA.GetWidth() != imageB.GetWidth() ||
        imageA.GetHeight() != imageB.GetHeight()) {
        throw std::runtime_error("Can not analyse images with variable resolutions");
    }

    if (_pxlFractionDist==1){ //this is much faster
        double totalDiff = 0;
        for (int y=0; y < imageA.GetHeight(); y+=_howManiestPxl) {
            for (int x = 0; x < imageA.GetWidth(); x += _howManiestPxl) {
                for (int m = 0; m < nChannels; m++) {
                    double diff = std::abs(imageA._image[y * imageA.GetWidth() * nChannels + x * nChannels + m] -
                                           imageB._image[y * imageB.GetWidth() * nChannels + x * nChannels + m]) /
                                  255.;//((double) std::max(imageA->_image[j * nChannels + m], imageB->_image[j * nChannels + m]));

                    if (!std::isnan(diff)) {
                        totalDiff += diff;
                    }
                }
            }
        }
        return(totalDiff/(nPixelsConsidered*nChannels));
    }
    else{ //much slower
        std::vector<double> differences;

        for (int y=0; y < imageA.GetHeight(); y+=_howManiestPxl) {
            for (int x = 0; x < imageA.GetWidth(); x += _howManiestPxl) {
                double pxlDiff=0;
                for (int m = 0; m < nChannels; m++) {
                    double diff = std::abs(imageA._image[y * imageA.GetWidth() * nChannels + x * nChannels + m] -
                                           imageB._image[y * imageB.GetWidth() * nChannels + x * nChannels + m]) /
                                  255.;//((double) std::max(imageA->_image[j * nChannels + m], imageB->_image[j * nChannels + m]));

                    if (!std::isnan(diff)) {
                        pxlDiff += diff;
                    }
                }
                differences.push_back(pxlDiff / nChannels);
            }

        }

        double kSmallestAverage;
        //now I only want the pxl_percent_dist% lowest distances to be considered
        int k=floor(_pxlFractionDist * nPixelsConsidered);
        std::sort(differences.begin(), differences.end());
        kSmallestAverage=std::accumulate( differences.begin(), differences.begin()+k, 0.0) / k;
        return kSmallestAverage;
    }

    //for testing
    //totalDiff=totalDiff/(nPixels*nChannels);
    //std::cout<<kSmallestAverage<<"true would be"<<totalDiff<<std::endl;

}


std::vector<std::vector<TImage *>> TImageSplitter::MergeBins(std::vector<std::vector<TImage *>> &images, int minBinSize, int nrCalls) const{

    std::vector<TImageGroup> bins;

    for(std::vector<TImage *> imagesInGroup : images){//for each bin
        TImageGroup group(imagesInGroup);//create a TImageGroup obj called group; var _images is created (vector with all imgs of 1 bin) and average and median img of _images is created

        bins.push_back(group); //the above constructed objects are stored in bins
    }

    bool foundSmallBin;


    std::vector<unsigned int> smallBins=FindSmallBins(bins, minBinSize);
    std::vector<int> deletedBins;
    std::vector<int> allBestIndecesB;
    std::vector<double> allBestDistances;

    //initialize with max (i.e. worst possible) distance
    std::vector<std::vector<double>> roughDistances(smallBins.size(), std::vector<double>(bins.size(), 1.0));
    std::vector<std::vector<double>> exactDistances(smallBins.size(), std::vector<double>(bins.size(), 1.0));


    do{
        double bestDifference;
        int bestIndexA;
        int bestIndexB;
        foundSmallBin=false;
        std::tie(bestIndexA, bestIndexB, bestDifference)=FindRoughlyMostSimilar(bins, minBinSize, smallBins,deletedBins,roughDistances, exactDistances);


        if(bestIndexA !=-1){
            //if 2 bins should be merged, print their indeces (only the 2 most similar bins are merged in one loop!)
            std::cout << "Using the rough dist, merging bins "<<bestIndexA<<" and "<<bestIndexB<<"(distance "<<bestDifference<<")"<<std::endl;
            //note: bestIndexA is the too small bin ie we want to delete this one as deleting the smaller bin and moving therefore less imgs is better
            //(it is not sure that A is smaller as A and B could both be too small)
            for(TImage * image : bins[bestIndexA].GetImages()){
                bins[bestIndexB].AddImage(image);
            }

            bins[bestIndexB].CalculateMedian();//get the median img across all imgs in the bin

            //set distances with bin bestIndexA to 1 so they'll be recalculated
            for (int i=0; i<smallBins.size(); i++){
                roughDistances[i][bestIndexB]=1;
                exactDistances[i][bestIndexB]=1;
                if (smallBins[i]==bestIndexB){
                    for (int x=0; x<bins.size();x++){
                        roughDistances[i][x]=1;
                        exactDistances[i][x]=1;
                    }
                }
            }

            deletedBins.push_back(bestIndexA); //the deleted bins (all with index A)

            allBestIndecesB.push_back(bestIndexB);
            allBestDistances.push_back(bestDifference);

            foundSmallBin = true;
        }

    }while(foundSmallBin);

    std::sort(deletedBins.begin(), deletedBins.end());
    for (int bin=deletedBins.size()-1; bin>=0; bin--){ //Delete from behind because else, I would have to change the indeces to delete
        //note that using an unsigned data type (such as size_t) would lead to an infinite loop (if you decrease the value 0 by 1, you will end up at the max value of this data type again)
        bins.erase(bins.begin() + deletedBins[bin]);
    }

    bool testing=true;

    if (testing){
        std::string myString = std::string(_outputPath);
        std::stringstream ss;
        ss << myString << "roughDistanceMerging"<<nrCalls<<".txt"; //make new file if mergebins() is called twice for same cam (once for day and once for night)
        std::string path = ss.str();


        std::ofstream file(path); //open in constructor

        std::string data("The following groups were merged with the noted distance using rough distance merging ");
        file << data << "\n";
        for (int nr=0; nr<allBestDistances.size(); nr++) {
            file << "bins "<<deletedBins[nr]<<" and "<<allBestIndecesB[nr]<<"("<<allBestDistances[nr]<<")"<<std::endl;
        }
        file << std::endl;
        file.close();
        std::cout << "Images were split into bins; which bins were merged and their distances can be found in: " << path<< std::endl;
    }


    std::vector<std::vector<TImage *>> result;

    for(TImageGroup &bin : bins){
        result.push_back(bin.GetImages());
    }

    return result;//the actualized bins
}

std::vector<unsigned int> TImageSplitter::FindSmallBins(const std::vector <TImageGroup> &bins, int minBinSize) const{
    std::vector<unsigned int> smallBins;
    for(size_t bin = 0; bin < bins.size(); bin++) {//for each bin index
        if (bins[bin].size() < minBinSize) {//if bin is too small
            smallBins.push_back(bin);
        }
    }
    return smallBins;

}

std::tuple<int, int, double> TImageSplitter::FindMostSimilar(std::vector <TImageGroup> bins, int minBinSize) const {
    int bestIndexA = -1;
    int bestIndexB = -1;
    double bestDifference = 1;

    for(size_t bin = 0; bin < bins.size(); bin++){//for each bin index
        if(bins[bin].size() < minBinSize){//if bin is too small

            if(_discardSmallBins){
                bins.erase(bins.begin() + bin);//delete the bin

            }else{ //find the most similar bin

                for(size_t otherBin = 0; otherBin < bins.size(); otherBin++){
                    if(bin != otherBin){
                        //ImageDifference only looks at a certain percentage of the smaller distances between pixels (pxlFractionDist)
                        const double difference = ImageDifference(bins[bin].GetMedian(),
                                                                  bins[otherBin].GetMedian()); //will be between 0 &1
                        //std::cout << bin << " " << otherBin << " " << similarity << " bins " << bins.size() << std::endl;

                        if(difference < bestDifference){
                            bestDifference = difference;
                            bestIndexA = bin;
                            bestIndexB = otherBin;
                        }
                    }
                }
            }
        }
    }
    return std::make_tuple(bestIndexA, bestIndexB, bestDifference);
}


std::tuple<int, int, double> TImageSplitter::FindRoughlyMostSimilar( std::vector <TImageGroup> &bins, int minBinSize, const std::vector <unsigned int> &smallBins, const std::vector <int> &deletedBins,std::vector<std::vector<double>> &roughDistances, std::vector<std::vector<double>> &exactDistances) const {

    //first only look at the bins roughly (because of speed issue) to determine the 5 nearest bins of a too small bin
    int nr_best_indeces=5;

    std::vector <int> bestIndecesA(nr_best_indeces, -1);
    std::vector <int> bestIndecesB(nr_best_indeces, -1);
    std::vector <double> bestDifferences(nr_best_indeces, 1.0);

    double maxDifference = bestDifferences[0]; //in the beginning, all are same, I just take the first one as max
    //std::cout<<"maxdiff"<< maxDifference<<std::endl;
    int changeIndex=0;//the index at which the entry should be changed


    for(size_t bin = 0; bin < bins.size(); bin++){//for each bin index
        if(bins[bin].size() < minBinSize && std::find(deletedBins.begin(), deletedBins.end(), bin) == deletedBins.end()){//if bin is too small and it has not been marked to be deleted
            int indexSmallBin=-1;
            for (int smallBin=0; smallBin<smallBins.size(); smallBin++){
                if (bin==smallBins[smallBin]){
                    indexSmallBin=smallBin;
                }
            }

            if(_discardSmallBins){
                bins.erase(bins.begin() + bin);//delete the bin

            }else{ //find the most similar bin (for merging later)
                for(size_t otherBin = 0; otherBin < bins.size(); otherBin++){
                    if(bin != otherBin && std::find(deletedBins.begin(), deletedBins.end(), otherBin) == deletedBins.end() ){
                        //ImageRoughDifference()  only looks at a certain percentage of the smaller distances between pixels (pxlFractionDist) (just like ImageDifference() )
                        if (roughDistances[indexSmallBin][otherBin]==1){
                            roughDistances[indexSmallBin][otherBin] = ImageRoughDifference(bins[bin].GetMedian(),
                                                                                bins[otherBin].GetMedian()); //will be between 0 &1
                        }


                        if(roughDistances[indexSmallBin][otherBin] < maxDifference){
                            bestDifferences[changeIndex] = roughDistances[indexSmallBin][otherBin];
                            bestIndecesA[changeIndex] = bin;
                            bestIndecesB[changeIndex] = otherBin;

                            //recalculate maxDifference
                            maxDifference=0;
                            for(int currentIndex=0; currentIndex<nr_best_indeces; currentIndex++){
                                if (bestDifferences[currentIndex]>maxDifference){
                                    changeIndex=currentIndex;
                                    maxDifference=bestDifferences[changeIndex];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //then get the exact differences for the 5 best candidates for merging, to determine which should be merged in the end
    int bestIndexA = -1;
    int bestIndexB = -1;
    double bestDifference = 1;
    int goodIndexA;
    int goodIndexB;
    int smallBinIndexA;
    for(int i=0; i<nr_best_indeces; i++) {
        goodIndexA = bestIndecesA[i]; //this is the original index of the bin
        goodIndexB = bestIndecesB[i];
        if (goodIndexA==-1){
            //std::cout<<"continuing"<<std::endl;
            continue;
        }
        for (int x=0; x<smallBins.size();x++){ //get the index of the bin within smallBins!
            if (goodIndexA==smallBins[x]){
                smallBinIndexA=x;
                break;
            }
        }
        if (exactDistances[smallBinIndexA][goodIndexB]==1){
            exactDistances[smallBinIndexA][goodIndexB] = ImageDifference(bins[goodIndexA].GetMedian(),bins[goodIndexB].GetMedian());
        }

        if(exactDistances[smallBinIndexA][goodIndexB] < bestDifference){
            bestDifference = exactDistances[smallBinIndexA][goodIndexB];
            bestIndexA = goodIndexA;
            bestIndexB = goodIndexB;
        }
    }

    return std::make_tuple(bestIndexA, bestIndexB, bestDifference);
}


void TImageSplitter::SetVerbose(bool verbose){
    _verbose = verbose;
}

void TImageSplitter::SetDiscardSmallBins(bool discard){
    _discardSmallBins = discard;
}