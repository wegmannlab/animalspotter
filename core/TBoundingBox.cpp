//
// Created by beat on 08.08.18.
//

#include "TBoundingBox.h"
#include <algorithm>

TBoundingBox::TBoundingBox(int x, int y, int width, int height) : _x(x), _y(y), _width(width), _height(height){
    _color[0] = 255;
    _color[1] = 0;
    _color[2] = 0;
}

int TBoundingBox::GetX() const{
    return _x;
}

int TBoundingBox::GetMaxX() const{
    return _x + _width;
}

int TBoundingBox::GetY() const{
    return _y;
}

int TBoundingBox::GetMaxY() const{
    return _y + _height;
}

int TBoundingBox::GetWidth() const{
    return _width;
}

int TBoundingBox::GetHeight() const{
    return _height;
}

int TBoundingBox::GetMiddleX() const {
    return GetX() + GetWidth() / 2;
}

int TBoundingBox::GetMiddleY() const {
    return GetY() + GetHeight() / 2;
}

unsigned char * TBoundingBox::GetColor(){
    return _color;
}

void TBoundingBox::SetColor(int colorIndex, unsigned char value){
    _color[colorIndex] = value;
}


double TBoundingBox::GetWeight() const{
    return _weight;
}

void TBoundingBox::SetWeight(double weight){
    _weight = weight;
}

void TBoundingBox::SetConnectedCount(int connected){
    _connectedCount = connected;
}

int TBoundingBox::GetConnectedCount() const{
    return _connectedCount;
}

void TBoundingBox::SetForegroundCount(int foreground){
    _foregroundCount = foreground;
}

int TBoundingBox::GetForegroundCount() const{
    return _foregroundCount;
}

//https://stackoverflow.com/questions/13390333/two-rectangles-intersection/44120056#44120056
bool TBoundingBox::isOverlapping(const TBoundingBox &other) const{

    if (_x + _width < other.GetX() || other.GetX() + other.GetWidth() < _x ||
            _y + _height < other.GetY() || other.GetY() + other.GetHeight() < _y) {
        return false;
    }

    return true;
}

bool TBoundingBox::IsInsideMe(const TBoundingBox &other) const{
    if((other.GetX() >= _x && other.GetX() <= _x + _width) &&
            (other.GetX() + other.GetWidth() >= _x && other.GetX() + other.GetWidth() <= _x + _width) &&
            (other.GetY() >= _y && other.GetY() <= _y + _height) &&
            (other.GetY() + other.GetHeight() >= _y && other.GetY() + other.GetHeight() <= _y + _height)){
        return true;
    }

    return false;
}

TBoundingBox TBoundingBox::GetOriginalBox(TImage *image) const{
    //int x, int y, int width, int height
    const double xRatio = image->GetOriginalWidth() / (double) image->GetWidth();
    const double yRatio = image->GetOriginalHeight() / (double) image->GetHeight();

    TBoundingBox box( _x * xRatio, _y * yRatio, _width * xRatio, _height * yRatio);
    box.SetWeight(_weight);
    box.SetConnectedCount(_connectedCount);

    return box;
}

TBoundingBox * TBoundingBox::merge(TBoundingBox const * const other, TImage const * const image, double const * const zSum, double min_probabilty_threshold) {
    int tlX = std::min(_x, other->GetX());
    int tlY = std::min(_y, other->GetY());

    int brX = std::max(_x + _width, other->GetX() + other->GetWidth());
    int brY = std::max(_y + _height, other->GetY() + other->GetHeight());

    TBoundingBox * box = new TBoundingBox(tlX, tlY, brX - tlX, brY - tlY);

    double totalInBox = 0;
    int totalForeGround = 0;
    const int imageWidth = image->GetWidth();
    for(int x = box->GetX(); x < box->GetX() + box->GetWidth(); x++) {
        for (int y = box->GetY(); y < box->GetY() + box->GetHeight(); y++) {
            const double z = zSum[x + y * imageWidth];//zSum is a flat matrix containing the probabilities of each pxl to be foreground
            totalInBox += z;
            if(z >= min_probabilty_threshold){ //ie z is the prob to be foreground
                totalForeGround++;
            }
        }
    }
    double boxWeight = totalInBox / ((box->GetWidth()) * (box->GetHeight()));
    //the box weight is kind of the amount of foreground in the box

    box->SetWeight(boxWeight);
    box->SetForegroundCount(totalForeGround);
    box->SetConnectedCount(_connectedCount + other->GetConnectedCount());

    box->SetColor(0, GetColor()[0]);
    box->SetColor(1, GetColor()[1]);
    box->SetColor(2, GetColor()[2]);

    return box;
}

std::string TBoundingBox::toString(){
    return  std::to_string(_x) + "/" + std::to_string(_y) + " " + std::to_string(_x + _width) + "/" + std::to_string(_y + _height);
}

//returns the border coordinates
