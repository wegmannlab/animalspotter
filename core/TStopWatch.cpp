#include "TStopWatch.h"

//#define SHOW_BENCHMARKS

TStopWatch::TStopWatch()
{
}

void TStopWatch::Start()
{
	_start = GetTime();
}

double TStopWatch::Stop()
{
	return GetTime() - _start;
}

void TStopWatch::StopPrint()
{
#ifdef SHOW_BENCHMARKS
	std::cout << (Stop() * 1000.0) << " ms" << std::endl;
#endif
}

void TStopWatch::StopPrint(const char * message)
{
#ifdef SHOW_BENCHMARKS
	std::cout << message << ": " << Stop() << std::endl;
#endif
}

#ifdef WIN32

#include <windows.h>
double TStopWatch::GetTime()
{
    LARGE_INTEGER t, f;
    QueryPerformanceCounter(&t);
    QueryPerformanceFrequency(&f);
    return (double)t.QuadPart/(double)f.QuadPart;
}

#else

#include <sys/time.h>
#include <sys/resource.h>

double TStopWatch::GetTime()
{
    struct timeval t;
    struct timezone tzp;
    gettimeofday(&t, &tzp);
    return t.tv_sec + t.tv_usec*1e-6;
}

#endif

TStopWatch::~TStopWatch()
{
}
