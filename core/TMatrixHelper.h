#ifndef TMATRIXHELPER_H
#define TMATRIXHELPER_H

namespace mat {
	double Determinant(double** m, unsigned int n);
	double Determinant(double* m, unsigned int n);
	void Inverse(double** in, double** out, double det, unsigned int n);
	void Inverse(double* in, double* out, double det, unsigned int n);
}

#endif
