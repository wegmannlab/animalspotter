//
// Created by beat on 08.08.18.
//

#ifndef ANIMALSPOTTER_TOBJECTDETECTOR_H
#define ANIMALSPOTTER_TOBJECTDETECTOR_H

#include "TImage.h"
#include "TBoundingBox.h"

class TObjectDetector {

public:
    TObjectDetector(TImage * image, double margin, double *zSum, unsigned char colors[3], double min_probabilty_threshold, bool do_blur);
    ~TObjectDetector();

    std::vector<TBoundingBox *> GetBoundingBoxes();

private:
    std::vector<TBoundingBox *> GetBoundingBoxes(int startX, int stopX, int startY, int stopY, int recursionDepth);
    void visit(bool *connected, int x, int y, bool init);
    std::vector<TBoundingBox *> SplitSearch(int startX, int stopX, int startY, int stopY, int recursionDepth);

    void mergeBoxes(std::vector<TBoundingBox *> &boundingBoxes);

    TImage * _image;
    double * _zSum;
    const double _margin;
    double _min_probabilty_threshold;
    bool _memory_allocated;

    unsigned char _color[3];
};

double * blur(double * image, int width, int height);

#endif //ANIMALSPOTTER_TOBJECTDETECTOR_H
