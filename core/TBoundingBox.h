//
// Created by beat on 08.08.18.
//

#ifndef ANIMALSPOTTER_TBOUNDINGBOX_H
#define ANIMALSPOTTER_TBOUNDINGBOX_H

#include <string>

#include "TImage.h"

class TBoundingBox {

public:
    TBoundingBox(int x, int y, int width, int height);

    int GetX() const;
    int GetMaxX() const;

    int GetY() const;
    int GetMaxY() const;

    int GetMiddleX() const;
    int GetMiddleY() const;

    int GetWidth() const;
    int GetHeight() const;
    unsigned char *GetColor();
    void SetColor(int colorIndex, unsigned char value);
    double GetWeight() const;
    void SetWeight(double weight);
    void SetConnectedCount(int connected);
    int GetConnectedCount() const;
    void SetForegroundCount(int foreground);
    int GetForegroundCount() const;

    bool isOverlapping(const TBoundingBox &other) const;
    bool IsInsideMe(const TBoundingBox &other) const;

    TBoundingBox * merge(TBoundingBox const * const other, TImage const * const image, double const * const zSum, double min_probabilty_threshold);

    TBoundingBox GetOriginalBox(TImage *image) const;

    std::string toString();

private:
    const int _x, _y;
    const int _width, _height;
    unsigned char _color[3];
    double _weight = 0;
    int _foregroundCount = 0;
    int _connectedCount = 0;
};


#endif //ANIMALSPOTTER_TBOUNDINGBOX_H
