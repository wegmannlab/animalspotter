//
// Created by beat on 26.07.18.
//

#include "TImage.h"
#include <cstring>

TImage::TImage(const std::string &name, unsigned char* image, int width, int height, int originalWidth, int originalHeight) :
        _name(name), _image(image), _width(width), _height(height), _originalWidth(originalWidth), _originalHeight(originalHeight){
}


TImage::TImage(const TImage & image) : _width(image._width), _height(image._height), _originalWidth(image._originalWidth), _originalHeight(image._originalHeight), _originalImageTime(image._originalImageTime) {
  const size_t array_size = _width * _height * 3;
  unsigned char * tmp = new unsigned char[array_size];

  memcpy(tmp, image._image, array_size);
  _image = tmp;
}

TImage::~TImage(){
    delete [] _image;
}

const std::string & TImage::GetName() const{
    return _name;
}

int TImage::GetWidth() const{
    return _width;
}

int TImage::GetHeight() const{
    return _height;
}

int TImage::GetOriginalWidth() const{
    return _originalWidth;
}

int TImage::GetOriginalHeight() const{
    return _originalHeight;
}

void TImage::SetOriginalTime(std::chrono::system_clock::time_point &time){
    _originalImageTime = time;
}

std::chrono::system_clock::time_point TImage::GetOriginalTime() const{
    return _originalImageTime;
}