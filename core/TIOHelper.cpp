#include "TIOHelper.h"

#include "../libs/LodePNG/lodepng.h"

bool io::SavePNGGrayscale(unsigned char* _img, const int _w, const int _h, char* path) {
    std::vector<unsigned char> image;
    image.resize(_w * _h * 4);
    for(unsigned y = 0; y < _h; y++) {
        for (unsigned x = 0; x < _w; x++) {
            int pixelID = (x + _w * y) ;

            image[4 * _w * y + 4 * x + 0] = _img[pixelID];
            image[4 * _w * y + 4 * x + 1] = _img[pixelID];
            image[4 * _w * y + 4 * x + 2] = _img[pixelID];
            image[4 * _w * y + 4 * x + 3] = 255;
        }
    }

    lodepng::encode(path, image, _w, _h);
    return true;
}

bool io::SavePNG_RGB(unsigned char* _img, const int _w, const int _h, const char* path) {
    std::vector<unsigned char> image;
    image.resize(_w * _h * 4);
    for(unsigned y = 0; y < _h; y++) {
        for (unsigned x = 0; x < _w; x++) {
            int pixelID = (x + _w * y) * 3;

            image[4 * _w * y + 4 * x + 0] = _img[pixelID];
            image[4 * _w * y + 4 * x + 1] = _img[pixelID + 1] ;
            image[4 * _w * y + 4 * x + 2] = _img[pixelID + 2];
            image[4 * _w * y + 4 * x + 3] = 255;
        }
    }

    lodepng::encode(path, image, _w, _h);
    return true;
}


