//
// Created by beat on 26.07.18.
//

#ifndef ANIMALSPOTTER_TIMAGEBINNER_H
#define ANIMALSPOTTER_TIMAGEBINNER_H

#include "TImage.h"

#include <vector>

class TImageLoader {

public:
    static TImage* CreateImage(unsigned char * decoded,
                                unsigned int originalImageWidth,
                                unsigned originalImageHeight,
                                unsigned int imageReduction,
                                std::string &name);

    static std::vector<TImage *> LoadImages(std::string &outputPath, std::string &inputPath, std::vector<std::string> &fileNames, unsigned int imageReduction);

};


#endif //ANIMALSPOTTER_TIMAGEBINNER_H
