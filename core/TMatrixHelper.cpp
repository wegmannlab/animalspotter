#include "TMatrixHelper.h"

double mat::Determinant(double **m, unsigned int n) {
	return (m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2])
			- m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0])
			+ m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]));
}

double mat::Determinant(double *m, unsigned int n) {
	return (m[0] * (m[4] * m[8] - m[7] * m[5])
			- m[1] * (m[3] * m[8] - m[5] * m[6])
			+ m[2] * (m[3] * m[7] - m[4] * m[6]));
}

void mat::Inverse(double **in, double **out, double det, unsigned int n) {
	double inverseDet = 1 / det;

	if (n == 3) {
		out[0][0] = (in[1][1] * in[2][2] - in[2][1] * in[1][2]) * inverseDet;
		out[0][1] = (in[0][2] * in[2][1] - in[0][1] * in[2][2]) * inverseDet;
		out[0][2] = (in[0][1] * in[1][2] - in[0][2] * in[1][1]) * inverseDet;
		out[1][0] = (in[1][2] * in[2][0] - in[1][0] * in[2][2]) * inverseDet;
		out[1][1] = (in[0][0] * in[2][2] - in[0][2] * in[2][0]) * inverseDet;
		out[1][2] = (in[1][0] * in[0][2] - in[0][0] * in[1][2]) * inverseDet;
		out[2][0] = (in[1][0] * in[2][1] - in[2][0] * in[1][1]) * inverseDet;
		out[2][1] = (in[2][0] * in[0][1] - in[0][0] * in[2][1]) * inverseDet;
		out[2][2] = (in[0][0] * in[1][1] - in[1][0] * in[0][1]) * inverseDet;
	}
}

void mat::Inverse(double *in, double *out, double det, unsigned int n) {
	double inverseDet = 1 / det;

	if (n == 3) {
		out[0] = (in[4] * in[8] - in[7] * in[5]) * inverseDet;
		out[1] = (in[2] * in[7] - in[1] * in[8]) * inverseDet;
		out[2] = (in[1] * in[5] - in[2] * in[4]) * inverseDet;
		out[3] = (in[5] * in[6] - in[3] * in[8]) * inverseDet;
		out[4] = (in[0] * in[8] - in[2] * in[6]) * inverseDet;
		out[5] = (in[3] * in[2] - in[0] * in[5]) * inverseDet;
		out[6] = (in[3] * in[7] - in[6] * in[4]) * inverseDet;
		out[7] = (in[6] * in[1] - in[0] * in[7]) * inverseDet;
		out[8] = (in[0] * in[4] - in[3] * in[1]) * inverseDet;
	}
}
